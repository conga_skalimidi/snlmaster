import { api, LightningElement, track, wire } from 'lwc';
//import getRelatedRecs2 from '@salesforce/apex/APTPS_AccountConsolidationDynamic.getCurrentRecordId'
import getRelatedRecs2 from '@salesforce/apex/APTPS_AccountConsolidationOnQuote.getCurrentRecordId'
//import { refreshApex } from '@salesforce/apex';
//import downloadPDF from '@salesforce/apex/APTPS_AccountConsolidationDynamic.retrievePdf'
import downloadPDF from '@salesforce/apex/APTPS_AccountConsolidationOnQuote.retrievePdf'
import getCustomSettings from '@salesforce/apex/APTPS_AccountConsolidationOnQuote.getCustomSettings'
import labelName from '@salesforce/label/c.APTPS_Account_Interest_Information';

export default class aptps_AccountInterestInformation extends LightningElement {
    @api recordId;
    @track showdata;
    @track fileName;
    @track showDownloadBtn;
    @track hideCreateAII;
    @track refreshComp = 0;
    boolShowSpinner = false;

    label = {labelName};

    @wire(getCustomSettings)
    myCustomSettings;

    refreshComponent(event){
        /* eval("$A.get('e.force:refreshView').fire();"); */
        this.template.querySelector('form').reset();
    }
    refreshComponent2(event){
        this.refreshComp = this.refreshComp+1;
    }
    @wire(getRelatedRecs2, {qId:'$recordId',inputRefresh:'$refreshComp'})//
    wireRecords({ error, data }) 
    {
        console.log('record Id--> '+this.recordId);
        if (data){
            console.log('Data Received-->');
            let d = [];
            data.forEach(element => {
                this.fileName = element.APTPS_Proposal__r.Apttus_Proposal__Proposal_Name__c;
            let elt = {};
            if(element.APTPS_Asset_Line_Item__c == null){
                elt.Apttus_Proposal__Proposal_Name__c = element.APTPS_Proposal__r != null ? element.APTPS_Proposal__r.Apttus_Proposal__Proposal_Name__c : '';
            }else{// if(element.APTPS_Asset_Line_Item__r.Apttus_CMConfig__AgreementId__r.Name != null){
                elt.Name = element.APTPS_Asset_Line_Item__c != null ? element.APTPS_Asset_Line_Item__r.Apttus_CMConfig__AgreementId__r.Name : '';
            }
            //elt.APTPS_Proposal__c = element.APTPS_Proposal__r != null ? element.APTPS_Proposal__r.Name : '';
            
            elt.APTPS_Asset_Line_Item__c = element.APTPS_Asset_Line_Item__r != null ? element.APTPS_Asset_Line_Item__r.Name : '';
            elt.APTPS_Configuration_Summary__c = element.APTPS_Configuration_Summary__c;
            elt.APTPS_Configuration_Summary_1__c = element.APTPS_Configuration_Summary_1__c;
            elt.APTPS_Sequence__c = element.APTPS_Sequence__c;
            d.push(elt);
            })
            this.showdata=d;
            if(d!=''){
                this.hideCreateAII=false;
                this.showDownloadBtn=true;
            }else{
                this.hideCreateAII=true;
                this.showDownloadBtn=false;
            }
        }
    }
    
     generatePdf(){
        this.boolShowSpinner = true;
        //console.log('recordID 84-->'+this.recordId);
        downloadPDF({qId: this.recordId}).then(response => {//currentRecordId: this.recordId
            console.log('recordID 86-->'+this.recordId);
            this.boolShowSpinner = false;
            //generatePdf2();
            let pdfName = this.myCustomSettings.data.PDF_File_Name__c;
            //For Downloading the file
            var file_path = "data:application/pdf;base64,"+response;
            var a = document.createElement('A');
            a.href = file_path;
            a.download = pdfName;//'Interest Information';
            //a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        }).catch(error => {
            console.log('Error: ' +error);
        });
    }
}