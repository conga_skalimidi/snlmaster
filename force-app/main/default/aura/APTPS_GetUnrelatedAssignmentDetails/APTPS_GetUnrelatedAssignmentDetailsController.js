({
    init: function (cmp, helper) {
        //helper.getAgreements(cmp);
        //helper.getRelatedLegalEntities(cmp);
        
        //Get related agreements
        var assetAccId = cmp.get("v.assetAccId");
        var currencyCode = cmp.get("v.currencyCode");
        var action = cmp.get("c.getRelatedAgreements");
        action.setParams({accId : assetAccId, 
                          currencyCode : currencyCode});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Received related agreements");
                var agData = response.getReturnValue();
                var agList = [];
                for (var i=0; i<agData.length; i++) {
                    var item = {
                        "label": agData[i].Name + ' / ' + agData[i].Apttus__FF_Agreement_Number__c,
                        "value": agData[i].Id
                    };
                    agList.push(item);
                }
                cmp.set("v.relatedAgreements", agList);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error while fetching related agreements - Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Error while fetching related agreements - Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
        //Get Legal Entities
        var action1 = cmp.get("c.getLegalEntities");
        var currentAcId = cmp.get("v.currentAccId");
        action1.setParams({accId : currentAcId});
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Received Leagal Entities");
                var legalEntities = response.getReturnValue();
                var entityList = [];
                for (var i=0; i<legalEntities.length; i++) {
                    var item = {
                        "label": legalEntities[i].Name,
                        "value": legalEntities[i].Id
                    };
                    entityList.push(item);
                }
                cmp.set("v.relatedLegalEntities", entityList);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error while fetching legal entities - Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Error while fetching legal entities - Unknown error");
                }
            }
        });
        $A.enqueueAction(action1);
    },
    handleAgChange: function (cmp, event) {
        var selectedAgId = event.getParam("value");
        cmp.set("v.selectedAgId", selectedAgId);
    },
    handleLEChange: function (cmp, event) {
        var selectedLEId = event.getParam("value");
        cmp.set("v.selectedLEId", selectedLEId);
    },
    handleSubmit: function (cmp, event) {
        cmp.set("v.isProcessing", true);
        //var urlEvent = $A.get("e.force:navigateToURL");
        var agId = cmp.get("v.selectedAgId");
        var currentAcId = cmp.get("v.currentAccId");
        var legalEntity = cmp.get("v.selectedLEId");
        var newQuoteId = cmp.get("v.newQuoteId");
        
        var action = cmp.get("c.generateConfig");
        action.setParams({agId : agId, 
                          currentAccId : currentAcId, 
                          legalEntityId : legalEntity, 
                          newQuoteId: newQuoteId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Configuratoin created successfully!");
                cmp.set("v.isProcessing", false);
                $A.get("e.force:navigateToSObject").setParams({
                    "recordId": newQuoteId
                }).fire();
                //Set boolean for spinner and redirect
            } else if (state === "ERROR") {
                var errors = response.getError();
                cmp.set("v.isProcessing", false);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error while generating Unrelated Assignment config - Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Error while generating Unrelated Assignment config - Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
});