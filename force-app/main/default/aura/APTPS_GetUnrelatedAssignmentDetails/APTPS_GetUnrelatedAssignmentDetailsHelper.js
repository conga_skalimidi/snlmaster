({
	getAgreements : function(cmp) {
		//Get related agreements
        var assetAccId = cmp.get("v.assetAccId");
        var currencyCode = cmp.get("v.currencyCode");
        var action = cmp.get("c.getRelatedAgreements");
        action.setParams({accId : assetAccId, 
                          currencyCode : currencyCode});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Received related agreements");
                var agData = response.getReturnValue();
                console.log('Related agreements --> '+agData);
                var agList = [];
                for (var i=0; i<agData.length; i++) {
                    console.log('Data '+agData[i].Name);
                    var item = {
                        "label": agData[i].Name + ' / ' + agData[i].Apttus__FF_Agreement_Number__c,
                        "value": agData[i].Id
                    };
                    agList.push(item);
                }
                cmp.set("v.relatedAgreements", agList);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error while fetching related agreements - Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Error while fetching related agreements - Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    getRelatedLegalEntities : function(cmp) {
        //Get Legal Entities
        var action = cmp.get("c.getLegalEntities");
        var currentAcId = cmp.get("v.currentAccId");
        action.setParams({accId : currentAcId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Received Leagal Entities");
                var legalEntities = response.getReturnValue();
                console.log('Legal entities --> '+legalEntities);
                var entityList = [];
                for (var i=0; i<legalEntities.length; i++) {
                    console.log('Data '+legalEntities[i].Name);
                    var item = {
                        "label": legalEntities[i].Name,
                        "value": legalEntities[i].Id
                    };
                    entityList.push(item);
                }
                cmp.set("v.relatedLegalEntities", entityList);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error while fetching legal entities - Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Error while fetching legal entities - Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
});