({
    //START: Aircraft Information
    fetchData: function (cmp) {
        var action = cmp.get("c.getAssetDetails");
        action.setParams({recId : cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Aircraft Information - Success!!");
                cmp.set("v.data", JSON.parse(response.getReturnValue()));
                cmp.set("v.acIntLoaded", true);
            } else if (state === "ERROR") {
                var errors = response.getError();
                cmp.set("v.acIntLoaded", true);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Aircraft Information - Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Aircraft Information - Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    //END: Interest Information
    
    //Aircraft Guarantee
    fetchAcDetails: function (cmp) {
        var action = cmp.get("c.getDetails");
        action.setParams({recId : cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Aicraft Guarantee Success!!");
                var resDetails = JSON.parse(response.getReturnValue());
                console.log('resDetails --> '+response.getReturnValue());
                //Show/Hide New button
                if(response.getReturnValue())
                    cmp.set("v.acData", JSON.parse(response.getReturnValue()));
                else
                    cmp.set("v.isButtonActive", true);
                cmp.set("v.acGuaranteeLoaded", true);
            } else if (state === "ERROR") {
                var errors = response.getError();
                cmp.set("v.acGuaranteeLoaded", true);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Aicraft Guarantee Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Aicraft Guarantee Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getAccId: function (cmp) {
        var action = cmp.get("c.getAccountId");
        action.setParams({agId : cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Get Account Id Success!!");
                cmp.set("v.accId", response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Get Account Id Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Get Account Id Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    deleteRec: function (cmp, recId) {
        var action = cmp.get("c.deleteRecord");
        action.setParams({recId : recId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Delete - Success!!");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Delete - Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Delete - Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
    //End: Aircraft Guarantee
});