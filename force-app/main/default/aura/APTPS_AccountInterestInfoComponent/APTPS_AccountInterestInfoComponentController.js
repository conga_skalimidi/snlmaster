({
    init: function (cmp, event, helper) {
		//Interest Information
        cmp.set('v.columns', [
            {label: 'Cabin Rank', fieldName: 'cabinRank', type: 'double'},
            {label: 'Cabin Class', fieldName: 'cabinClass', type: 'text'},
            {label: 'Aircraft Type', fieldName: 'acType', type: 'text'},
            {label: 'Hours', fieldName: 'hours', type: 'text'},
            {label: 'Percentage', fieldName: 'percentage', type: 'decimal'},
            {label: 'Is Influenced?', fieldName: 'isInfluenced', type: 'boolean'}
        ]);
        helper.fetchData(cmp);
        //End: Interest Information
        
        //Aircraft Guarantee
        var actions = [
            { label: 'Create New', name: 'create_new' },
            { label: 'Edit', name: 'show_details' },
            { label: 'Delete', name: 'delete' }
        ];
        cmp.set('v.acColumns', [
            { label: 'Type', fieldName: 'typeOfGuarantee', type: 'text' },
            { label: 'Aircraft Type', fieldName: 'acType', type: 'text' },
            { label: 'Number Of Aircraft', fieldName: 'noAc', type: 'number' },
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);


        helper.fetchAcDetails(cmp);
        //END: Aircraft Guarantee
        
        helper.getAccId(cmp);
    },
    
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'create_new':
                cmp.set("v.getInputs", true);
                cmp.set("v.acRecId", '');
                break;
                
            case 'show_details':
                cmp.set("v.acRecId", row.acRecId);
                cmp.set("v.getInputs", true);
                break;
                
            case 'delete':
                helper.deleteRec(cmp, row.acRecId);
                $A.get('e.force:refreshView').fire();
        		helper.fetchAcDetails(cmp);
                //$A.get('e.force:refreshView').fire();
                break;
        }
    },
    
    handleSuccess: function (cmp, event, helper) {
        cmp.find("acForm").submit();
        cmp.set("v.getInputs", false);
       	$A.get('e.force:refreshView').fire();
        helper.fetchAcDetails(cmp);
        cmp.set("v.acRecId", '');
        cmp.set("v.isButtonActive", false);
        //$A.get('e.force:refreshView').fire();
    },
    
    handleCancel: function (cmp, event, helper) {
        cmp.set("v.getInputs", false);
        cmp.set("v.acRecId", '');
        $A.get('e.force:refreshView').fire();
    },
    
    createNew: function (cmp, event, helper) {
        cmp.set("v.getInputs", true);
    }
});