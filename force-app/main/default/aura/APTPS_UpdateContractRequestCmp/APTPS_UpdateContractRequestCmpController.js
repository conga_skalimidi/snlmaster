({
	init : function(component, event, helper) {  
      var recordId = component.get('v.pageReference').state.c__recordId;
        component.set("v.recordId", recordId);
       var action = component.get("c.updateContractRequest");
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response) {
           // alert(response.getState());
            if (response.getState() === 'SUCCESS'){
               var ctrl = response.getReturnValue();
                $A.get('e.force:refreshView').fire();
               var navEvent = $A.get("e.force:navigateToSObject");
                navEvent.setParams({
                    "recordId": component.get("v.recordId"),
                    "slideDevName": "related"
                });
                navEvent.fire();
                
            }

         });
         $A.enqueueAction(action);   
    },
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
})