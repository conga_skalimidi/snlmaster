({
	init : function(component, event, helper) {  
      var recordId = component.get('v.pageReference').state.c__recordId;
      var actionType = component.get('v.pageReference').state.c__action;
        console.log('actionType-->'+actionType);
        component.set("v.recordId", recordId);
        component.set("v.action", actionType);
       var action = component.get("c.activateAgreement");
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response) {
            //alert(response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                 var job_msg = '';
                if(component.get("v.action") == 'activate'){
                    job_msg = component.get("v.APTPS_Agreement_Job_Start_Msg");
                }
                if(component.get("v.action") == 'terminate'){
                    job_msg = component.get("v.APTPS_Agreement_Terminate_Job_Start_Msg");
                }
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "success",
                    "title": "Success!",
                    "message": job_msg
                });
                toastEvent.fire();

                if (state === "SUCCESS"){
                    var interval = setInterval($A.getCallback(function () {
                        var jobStatus = component.get("c.getBatchJobStatus");
                        //alert('jobStatus-->'+jobStatus);
                        //alert('returnVal-->'+response.getReturnValue());
                        if(jobStatus != null){
                            jobStatus.setParams({ jobID : response.getReturnValue()});
                            jobStatus.setCallback(this, function(jobStatusResponse){
                                
                                var jobState = jobStatus.getState();
                                if (jobState === "SUCCESS"){
                                    var job = jobStatus.getReturnValue();
                                    component.set('v.apexJob',job);
                                    var processedPercent = 0;
                                    if(job.JobItemsProcessed != 0){
                                        processedPercent = (job.JobItemsProcessed / job.TotalJobItems) * 100;
                                    }
                                    var progress = component.get('v.progress');
                                    
                                    console.log(job.Status+' '+component.get("v.action"));
                                    if(job.Status == 'Completed' &&  !component.get("v.actionCompleted")){
                                        
                                        if(component.get("v.action") == 'terminate'){
                                           console.log(component.get("v.actionCompleted"));
                                           helper.termianteAgreement(component, event, helper); 
                                        }
                                        if(component.get("v.action") == 'activate'){
                                           console.log(component.get("v.actionCompleted"));
                                           helper.backToAgreement(component, event, helper); 
                                        }
                                        
                                        
                                    } else {
                                        component.set('v.progress', processedPercent);
                                    }
                                }
                            });
                            $A.enqueueAction(jobStatus);
                        }
                    }), 2000);
                }
            }
            else if (state === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error!",
                    "message": "An Error has occured. Please try again or contact System Administrator."
                });
                toastEvent.fire();
            }

         });
         $A.enqueueAction(action);   
    },
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
   
    
})