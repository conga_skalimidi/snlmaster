({
	termianteAgreement : function(component, event, helper) {
        console.log('Terminate');
        var action = component.get("c.terminateAgreement");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            console.log(response.getState());
            if (response.getState() === 'SUCCESS'){
               
                helper.backToAgreement(component, event, helper); 
                
                
            }

         });
         $A.enqueueAction(action);
        
    },
    
   
    
    backToAgreement : function(component, event, helper) {
        var success_msg = '';
        component.set("v.actionCompleted",'true')
        if(component.get("v.action") == 'activate'){
            success_msg = component.get("v.APTPS_Agreement_Activate_Complete_Msg");
        }
        if(component.get("v.action") == 'terminate'){
            success_msg = component.get("v.APTPS_Agreement_Terminate_Complete_Msg");
        }
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "success",
            "title": "Success!",
            "message": success_msg
        });
        toastEvent.fire();
        var navEvent = $A.get("e.force:navigateToSObject");
        navEvent.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "related"
        });
        navEvent.fire();
        
    },
})