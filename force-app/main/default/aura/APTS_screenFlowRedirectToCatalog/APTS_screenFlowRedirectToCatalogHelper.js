({
	getCatalogURL : function(component, event) {
        var urlEvent = $A.get("e.force:navigateToURL");
		var programType = component.get("v.programType");
        var currencyCode = component.get("v.currencyCode");
        var actionType = component.get("v.actionType");
        var modificationType = component.get("v.modificationType");
        var qId = component.get("v.proposalId");
        var additionalModType = component.get("v.additionalModType");
        console.log('Parameters from screen flow. programType --> '+programType+' currencyCode --> '+currencyCode+' actionType --> '+actionType+' modificationType --> '+modificationType+' qId --> '+qId+' additionalModType --> '+additionalModType);
        //Get Catalog URL
        var action = component.get("c.getCatalogURL");
        action.setParams({programType : programType, 
                          currencyCode : currencyCode,
                          actionType : actionType,
                          modificationType : modificationType,
                          qId : qId,
                          additionalModType : additionalModType});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Catalog URL - Success!!");
                var retURL = response.getReturnValue();
                console.log('URL --> '+retURL);
                urlEvent.setParams({
                    "url": retURL
                });
                urlEvent.fire();
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Catalog URL - Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Catalog URL - Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	}
})