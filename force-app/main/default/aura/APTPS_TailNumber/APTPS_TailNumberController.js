({
    init : function(component, event, helper) {
        helper.getDataHelper(component,event,helper); 
    },
    updateInterim: function(component, event, helper){
        var tailNumberIntrim = event.getParam("value");
        var idrec = component.get("v.childAgreementId");
        console.log('Test tailNumberinterim in updateInterim:'+ tailNumberIntrim);
        console.log('childAgreementId-->updateInterim' +idrec);
        console.log('agreementExtId-->updateInterim' + component.get("v.interimAgreementExtId"));
        var action = component.get("c.populateInterimTailInformation");
        action.setParams({
            tailNumber:tailNumberIntrim,
            agreementid:idrec,
            agreementExtId: component.get("v.interimAgreementExtId")
        });
        action.setCallback(this,function (response) {
            var state = response.getState();
            var tail_details = [];
            if(state === "SUCCESS"){
                var recext = response.getReturnValue();
                console.log('oChildAgreementExtension-->'+recext);
                console.log('abc'+recext.APTPS_Cabin_Class_Size__c);
                component.set("v.oChildAgreementExtension",recext);
                component.set("v.interimtailInfoReady",'TRUE');
                helper.hadlesuccess(component, event, helper,"The Interim Tail has been updated successfully.");
                if(component.get("v.interimtailInfoReady") == 'TRUE'){
                    console.log('In condition ------->>>>>>>>');
                    helper.getInterimLease(component,event,helper);
                }
                

            }else if(state === "ERROR"){
                let errors = [];
                errors = response.getError();
                // Retrieve the error message sent by the server
                if (errors) {
                    var error = errors[0].message;
                    console.log( 'error msg '+errors[0].message);
                	helper.handleErrors(component, event, helper,error);
                }
                
                console.log('Error-->Response State:'+state);
            }			
        });
        $A.enqueueAction(action);
        
        console.log('populateTailInformation completed successfully');

    },
    updateTail : function(component, event, helper) {
        var selectedTail;        
        selectedTail = event.getParam("value");        
        var idrec = component.get("v.recordId");
        console.log('AgreementId-->'+idrec);
        component.set("v.selectedTail",selectedTail);
        var action = component.get("c.populateTailInformation");
        action.setParams({
            tailNumber:selectedTail,
            agreementid:idrec,
            agreementExtId: component.get("v.agreementExtId")
        });
        action.setCallback(this,function (response) {
            var state = response.getState();
            var tail_details = [];
            if(state === "SUCCESS"){
                var recext = response.getReturnValue();
                console.log('AgreementExtension-->'+recext);
                component.set("v.oAgreementExtension",recext);               
                component.set("v.tailInfoReady",'TRUE'); 
                helper.hadlesuccess(component, event, helper,"The Tail has been updated successfully.");
                
            }else{
                let errors = [];
                errors = response.getError();
                if (errors) {
                    var error = errors[0].message;
                }
                console.error( 'error msg '+errors[0].message);
                helper.handleErrors(component, event, helper,error);
            }			
        });
        $A.enqueueAction(action);
    },
    
    onChangeParkedAircraft:function(component, event, helper) {
    	var lookupId = event.getParam("value");
        component.set("v.tailList",'');
        var idrec = component.get("v.recordId");
        console.log('recordId in onChangeParkedAircraft :'+ idrec);
        //var idrec = component.get("v.childAgreementId");
        if(lookupId!=''){
        var action = component.get("c.updateParkedAircraftCheck");
        action.setParams({    
            agreementid:idrec,
            parkedAircraftId: lookupId[0],
            agreementExtId : component.get('v.agreementExtId')
        });
        action.setCallback(this,function (response) {
            var state = response.getState();
            console.log('state '+state);
            var tail_details = [];
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set('v.aircraftType',result);
                component.set('v.oAgreementExtension',component.get('v.EmptyAgreementExtension'));
                helper.getTailsHelper(component,event,helper);                
                
            }else if (state === "ERROR"){
                let errors = [];
                errors = response.getError();
                // Retrieve the error message sent by the server
                if (errors) {
                    var error = errors[0].message;
                }
                // Display the message      
                console.error( 'error msg '+errors[0].message);
                helper.handleErrors(component, event, helper,error);
                console.log('Error-->Response State:'+state);
            }
        });
            
            
            console.log('Out of onChangeParkedAircraft Function');
            
        $A.enqueueAction(action);

        }
	},
    
     onChangeAircraftTypeInterim:function(component, event, helper) {
    	var lookupId = event.getParam("value");
        component.set("v.tailListIntrim",'');
        //component.set('v.aircraftTypeInterim','');
        //var idrec = component.get("v.recordId");
        var idrec = component.get("v.childAgreementId");
         console.log('childAgreementId in onChangeAircraftTypeInterim :'+ idrec);
         console.log('agreementExtId in onChangeAircraftTypeInterim :'+ component.get('v.agreementExtId'));
         
        if(lookupId!=''){
        var action = component.get("c.updateAircraftTypeInterimCheck");
        action.setParams({    
            agreementid:idrec,
            aircraftTypeInterim: lookupId,
            agreementExtId : component.get('v.agreementExtId')
        });
        action.setCallback(this,function (response) {
            var state = response.getState();
            console.log('state '+state);
            var tail_details = [];
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.aircraftTypeInterim",result);
                console.log('Test aircraftTypeInterim :'+result);
                //component.set('v.oAgreementExtension',component.get('v.EmptyAgreementExtension'));
                try{
                helper.getInterimTailsHelper(component,event,helper);                
                }catch(err){
                   console.log('getInterimTailsHelper error :'+err); 
                }
            }else if (state === "ERROR"){
                let errors = [];
                errors = response.getError();
                // Retrieve the error message sent by the server
                if (errors) {
                    var error = errors[0].message;
                }
                // Display the message      
                console.error( 'error msg '+errors[0].message);
                helper.handleErrors(component, event, helper,error);
                console.log('Error-->Response State:'+state);
            }
        });
            
            console.log('Out of onChangeAircraftTypeInterim Function');
            $A.enqueueAction(action);
            
            var actionMonthlyLease = component.get("c.calculateInterimMonthlyLeaseFee");
            actionMonthlyLease.setParams({    
                aircraftTypeInterim: lookupId,
                agreementid:idrec
            });
            
            actionMonthlyLease.setCallback(this,function (response) {
                var state = response.getState();
                console.log('state in interimMonthly '+state);
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();
                    console.log('Outside methode --->>> Test interimMonthlyLeaseFee:'+result);
                    component.set("v.interimMonthlyLeaseFee",result);
                    
                }else if (state === "ERROR"){
                    let errors = [];
                    errors = response.getError();
                    // Retrieve the error message sent by the server
                    if (errors) {
                        var error = errors[0].message;
                    }
                    // Display the message      
                    console.error( 'error msg '+errors[0].message);
                    helper.handleErrors(component, event, helper,error);
                    console.log('Error-->Response State:'+state);
                }
            });
            
            
            console.log('Out of calculateInterimMonthlyLeaseFee Function');
            
            $A.enqueueAction(actionMonthlyLease); 

        }
	},
    onReserveClick: function(component, event, helper) {
    	helper.callReserveInventoryHelper(component, event, helper);
	}
 
})