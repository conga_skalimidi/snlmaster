({
    getDataHelper : function(component,event,helper){
        var idrec = component.get("v.recordId");
        var action = component.get("c.getData");
        action.setParams({
            agreementid:idrec
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var returning = [];
                var dataMap = response.getReturnValue();
                console.log('dataMap -> ' + JSON.stringify(dataMap));
                for(var key in dataMap){
                    if(key=='childAgreement'){
                        component.set('v.haschildAgreement',true);
                        component.set('v.childAgreementId',dataMap[key]);
                    }
                    component.set('v.'+key,dataMap[key]);
                    
                }
                var recType = component.get('v.RecordType_Name');
                if(recType=='NJA Interim Lease Agreement' || recType=='NJE Interim Lease Agreement' 
                   || recType=='NJUS Interim Lease Agreement' || recType=='On Hold Interim Lease'){
                    helper.getAgreementExtensionhelper(component,event,helper);
                }
                else{
                	helper.getTailsHelper(component,event,helper);             
                	helper.getAgreementExtensionhelper(component,event,helper);
                    console.log("getInterimAgreementExtensionhelper  function called !!!!!!!!!!!");
                	helper.getInterimAgreementExtensionhelper(component,event,helper);
                    helper.getInterimAgreementInfo(component,event,helper);
                    helper.getInterimTailsHelper(component,event,helper);
                    
                    
                }
                
            }
            else{
                let errors = [];
                errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                }
                // Display the message      
                console.error( errors[0].message);
                helper.handleErrors(component, event, helper,error);
                console.log("Temporary Error.");// Temporary, Will be changed to ToastError.
            }
        });
        $A.enqueueAction(action); 
    },   
    
    handleErrors : function(component, event, helper,error) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (error) {
            toastParams.message = error;
        }
        //Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },
    
    hadlesuccess : function(component, event, helper,msg) {    	
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "success",
            "message": msg
        });
        toastEvent.fire();
    },
    
    getTailsHelper : function(component, event, helper) { 
        var items = [];
        var idrec = component.get("v.recordId");
        var action3 = component.get("c.getTails");
        action3.setParams({
            agreementid:idrec,
            aircraftType: component.get("v.aircraftType"),
            RecordType_Name: component.get("v.RecordType_Name") 
        });
        action3.setCallback(this,function (response) {
            var state = response.getState();
            var tails = [], result = [];
            if(state === "SUCCESS"){
                result = response.getReturnValue();
                console.log('result');
                if(result){
                    tails = JSON.parse(result)["TailList"];
                    console.log('Tails-->');
                    console.log(JSON.stringify(tails));
                    var index = "";
                    for (index in tails){
                        var tail = tails[index];
                        var item = { "label" : tail.TailNumber,"value" : tail.TailNumber};
                        items.push(item);
                    }
                    component.set("v.tailList",items);
                    console.log(component.get("v.tailList"));
                }
            } else{
                let errors = [];
                errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                }
                // Display the message      
                console.error( errors[0].message);
                if(error==='List has no rows for assignment to SObject'){
                    error = 'No Tails found for Tail List';
                }
                handleErrors(component, event, helper,error);
                console.log('Error-->Response State:'+state);
            }
            
            console.log('Out of getTailsHelper Function');
        });
        $A.enqueueAction(action3);
        
    },
    
    getInterimTailsHelper : function(component, event, helper) { 
        var items = [];
        //var idrec = component.get("v.recordId");
        var idrec = component.get("v.childAgreementId");
        console.log('Test in helper idrec :'+ idrec);
        console.log('Test in helper aircraftTypeInterim :'+ component.get("v.aircraftTypeInterim"));
        console.log('Test in helper RecordType_Name :'+ component.get("v.RecordType_Name"));
        try{
        var action3 = component.get("c.getIntrimTailList");
        action3.setParams({
            agreementId:idrec,
            aircraftTypeInterim: component.get("v.aircraftTypeInterim"),
            RecordType_Name: component.get("v.RecordType_Name") 
        });
        action3.setCallback(this,function (response) {
            var state = response.getState();
            var tails = [], result = [];
            if(state === "SUCCESS"){
                result = response.getReturnValue();
                console.log('Test result'+ result);
                if(result){
                    tails = JSON.parse(result)["TailList"];
                    console.log('Tails-->' + tails);
                    console.log('JSON.stringify'+ JSON.stringify(tails));
                    var index = "";
                    for (index in tails){
                        var tail = tails[index];
                        var item = { "label" : tail.TailNumber,"value" : tail.TailNumber};
                        items.push(item);
                    }
                    component.set("v.tailListIntrim",items);
                    console.log(component.get("v.tailListIntrim"));
                }
            } else{
                let errors = [];
                errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                }
                // Display the message      
                console.error( errors[0].message);
                if(error==='List has no rows for assignment to SObject'){
                    error = 'No Tails found for Tail List';
                }
                handleErrors(component, event, helper,error);
                console.log('Error-->Response State:'+state);
            }
            
            console.log('Out of getInterimTailsHelper Function');
        });
        }catch(err){
            console.log('getInterimTailsHelper after calling method error :'+err);
        }
        $A.enqueueAction(action3);
        
    },
    getAgreementExtensionhelper : function(component, event, helper) { 
        var extId = component.get("v.agreementExtId");
        var action2 = component.get("c.getAgreementExtension");
        action2.setParams({
            agreementExtId:extId
        });
        action2.setCallback(this,function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log('agreement helper');
                var recext = response.getReturnValue();
                component.set("v.oAgreementExtension",recext);
                component.set("v.tailInfoReady",'TRUE');
            } else{
                let errors = [];
                errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                    console.log( errors[0].message);
                    helper.handleErrors(component, event, helper,error);
                }
                
                console.log('Error-->Response State:'+state);
            }		
            console.log('Out of Action2 INIT Function');
        });
        
        $A.enqueueAction(action2);	
    },
    
    getInterimAgreementExtensionhelper : function(component, event, helper) { 
        var extId = component.get("v.interimAgreementExtId");
        console.log('interimAgreementExtId'+ extId);
        var action4 = component.get("c.getAgreementExtension");
        action4.setParams({
            agreementExtId:extId
        });
        action4.setCallback(this,function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log('agreement helper :-  InterimAgreementExtensionhelper');
                var recext = response.getReturnValue();
                console.log('Intrim Extensionhelper result:- '+ recext);
                component.set("v.oChildAgreementExtension",recext);
                component.set("v.interimtailInfoReady",'TRUE');
            } else{
                let errors = [];
                errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                    console.log( errors[0].message);
                    helper.handleErrors(component, event, helper,error);
                }
                
                console.log('Error-->Response State:'+state);
            }		
            console.log('Out of getInterimAgreementExtensionhelper INIT Function');
        });
        
        $A.enqueueAction(action4);	
 
    },
    
    getInterimAgreementInfo : function(component, event, helper) {

        var extId = component.get("v.childAgreementId");
        console.log('interimAgreementExtId'+ extId);
        var action9 = component.get("c.getInterimAgreementInformation");
        action9.setParams({
            agreementExtId:extId
        });
        action9.setCallback(this,function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log('agreement helper :-  getInterimAgreementInfo');
                var recext = response.getReturnValue();
                console.log('Intrim getInterimAgreementInfo result:- '+ recext);
                component.set("v.oChildAgreement",recext);
                component.set("v.interimMonthlyLeaseFee",recext.APTPS_Interim_Monthly_Lease_Fee__c);
                component.set("v.interimLeaseDeposit",recext.APTPS_Interim_Lease_Deposit__c);
                component.set("v.aircraftTypeInterim",recext.APTPS_Aircraft_type_interim__c);
                component.set("v.interimtailInfoReady",'TRUE');
            } else{
                let errors = [];
                errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                    console.log( errors[0].message);
                    helper.handleErrors(component, event, helper,error);
                }
                
                console.log('Error-->Response State:'+state);
            }		
            console.log('Out of getInterimAgreementInfo INIT Function');
        });
        
        $A.enqueueAction(action9);	
        
    },
    
    callReserveInventoryHelper: function(component,event,helper){
        var idrec = component.get("v.recordId");
        var action = component.get("c.callReserveInventory");
        action.setParams({
            agreementId: idrec,
            tailNumber: component.get('v.selectedTail'),
            BusinessNumber: component.get('v.BusinessNumber'),
            interestSizePercent: component.get('v.interestSizePercent'),
            aircraftAllotmentId: component.get('v.aircraftAllotmentId')
        });
        action.setCallback(this,function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var allotmentId = response.getReturnValue();
                if(allotmentId!=''){
                   component.set('v.aircraftAllotmentId',allotmentId); 
                }else if(allotmentId==''){
                    helper.hadlesuccess(component, event, helper,"Aircraft allotment has been updated successfully.");
                }
                
            } else{
                let errors = [],error;
                errors = response.getError();
                if (errors) {
                    error = errors[0].message;
                    helper.handleErrors(component, event, helper,error);
                }               
                console.log('Error-->Response State:'+state);
            }		
            console.log('Out of callReserveInventoryHelper Function');
        });
        $A.enqueueAction(action);     
        
    },
    getInterimLease: function(component,event,helper){
        
        var idrecdepo = component.get("v.childAgreementId");
        console.log('In deposit childAgreementId-->'+idrecdepo);
        console.log('In deposit agreementExtId-->'+ component.get("v.interimAgreementExtId"));
        
        var actionDeposit = component.get("c.getInterimLeaseDeposit");
        actionDeposit.setParams({
            agreementid:idrecdepo,
            agreementExtId: component.get("v.interimAgreementExtId")
        });
        actionDeposit.setCallback(this,function (response) {
            var stateDeposite = response.getState();
            console.log('stateDeposite :'+ stateDeposite);
            if(stateDeposite === "SUCCESS"){
                var result = response.getReturnValue();
                console.log('v.interimLeaseDeposit-->'+result);
                component.set("v.interimLeaseDeposit", result);
                //helper.hadlesuccess(component, event, helper,"The Interim Tail has been updated successfully.");
            }else if(stateDeposite === "ERROR"){
                let errors = [];
                errors = response.getError();
                // Retrieve the error message sent by the server
                if (errors) {
                    var error = errors[0].message;
                    console.log( 'error msg '+errors[0].message);
                    helper.handleErrors(component, event, helper,error);
                }
                
                console.log('Error-->Response State:'+stateDeposite);
            }			
        });
        $A.enqueueAction(actionDeposit);
        
    }
    
    
})