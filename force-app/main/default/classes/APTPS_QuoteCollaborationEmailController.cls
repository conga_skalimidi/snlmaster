/************************************************************************************************************************
@Name: APTPS_QuoteCollaborationEmailController
@Author: Conga PS Dev Team
@CreateDate: 22 July 2021
@Description: APTPS_QuoteCollaborationEmailComponent component controller
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_QuoteCollaborationEmailController {
    public string qId {get; set;}
    public string collabId {get; set;}
    public string mode {get; set;}
    public string parentConfigId {get; set;}
    public string childConfigId {get; set;}
    public string transactionType {get; set;}
    public string modificationType {get; set;}
    public string acType {get; set;}
    public string vintage {get; set;}
    public string hours {get; set;}
    public string collabURL {get; set;}
    public string quoteURL {get; set;}
    public double shareSize {get; set;}
    public boolean isCancelled {get; set;}
    private final String NL = '\n';
    public Boolean isTesting{get;private set;}
    
    /**
    @description: Get all required details to form an email body/template.
    @param:
    @return: void
    */
    public void getDetails() {
        system.debug('Input parameters, Proposal Id = '+qId+' Collaboration Id = '+collabId+' Mode = '+mode
                     +' Parent Config Id = '+parentConfigId+' Child Config Id = '+childConfigId);
        String configId = null;
        Apttus_Config2__LineItem__c li = null;
        isCancelled = APTS_ConstantUtil.STATUS_CANCELLED.equalsIgnoreCase(mode) ? true : false;
        //When Collaboration request is New and getting assigned to the Queue/User at the time Child config is null.
        //This might be because of Async process to copy child config. So, read data from Parent config.
        if(childConfigId == null)
            configId = parentConfigId;
        else
            configId = APTS_ConstantUtil.STATUS_COMPLETED.equalsIgnoreCase(mode) ? childConfigId : parentConfigId;
        system.debug('Read data from config --> '+configId);
        if(configId != null) {
            //Query logic may differ in case of Lease. Revisit it once we start the implementation for Lease product setup.
            //For now, this query will work for Share product.
            li = [SELECT Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Id, 
                  Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Apttus_Proposal__Proposal_Name__c, 
                  Apttus_Config2__ConfigurationId__r.APTPS_Program_Type__c, 
                  Apttus_Config2__ConfigurationId__r.APTS_Modification_Type__c, 
                  Apttus_Config2__OptionId__r.Name, 
                  Apttus_Config2__AttributeValueId__r.APTPS_Vintage__c, 
                  Apttus_Config2__AttributeValueId__r.APTPS_Hours__c, 
                  Apttus_Config2__AttributeValueId__r.APTPS_Percentage__c 
                  FROM Apttus_Config2__LineItem__c 
                  WHERE Apttus_Config2__ConfigurationId__c =: configId
                  AND Apttus_Config2__LineType__c =: APTS_ConstantUtil.OPTION 
                  AND Apttus_Config2__IsPrimaryLine__c = true 
                  AND (Apttus_Config2__ChargeType__c =: APTS_ConstantUtil.PURCHASE_PRICE 
                       OR Apttus_Config2__ChargeType__c =: APTS_ConstantUtil.REPURCHASE_CT) LIMIT 1];
            
            if(li != null) {
                transactionType = li.Apttus_Config2__ConfigurationId__r.APTPS_Program_Type__c;
                modificationType = li.Apttus_Config2__ConfigurationId__r.APTS_Modification_Type__c;
                acType = li.Apttus_Config2__OptionId__r.Name;
                vintage = li.Apttus_Config2__AttributeValueId__r.APTPS_Vintage__c;
                hours = li.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c;
                shareSize = li.Apttus_Config2__AttributeValueId__r.APTPS_Percentage__c;
                collabURL = URL.getSalesforceBaseUrl().toExternalForm() +'/'+collabId;
                if(isCancelled && li.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Id != null)
                    quoteURL = URL.getSalesforceBaseUrl().toExternalForm() +'/'+li.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Id;
            }
        }
        
        system.debug('Configuration Details, Transaction Type = '+transactionType+' Modification Type = '+modificationType
                     +' Aircraft Type = '+acType+' Vintage = '+vintage+' Hours = '+hours+' Share Size = '+shareSize
                     +' Collaboration URL = '+collabURL);
    }
}