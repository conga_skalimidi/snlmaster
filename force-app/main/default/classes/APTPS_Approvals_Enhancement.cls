/************************************************************************************************************************
@Name: APTPS_Approvals_Enhancement
@Author: Conga PS Dev Team
@CreateDate: 15 December 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_Approvals_Enhancement implements APTPS_ApprovalUsersInterface{    
     // START: Read Enhancement Approvals details from Custom settings 
    APTPS_Enhancement_Approvals__c approvalLimits = APTPS_Enhancement_Approvals__c.getOrgDefaults();
    public Decimal DDC_WIF_Limit = approvalLimits.APTPS_DDC_WIF_Limit__c != null ? approvalLimits.APTPS_DDC_WIF_Limit__c : 0.0;
    public Decimal Sales_Director_WIF_Limit = approvalLimits.APTPS_Sales_Director_WIF_Limit__c   != null ? approvalLimits.APTPS_Sales_Director_WIF_Limit__c  : 0.0;
    public Decimal SalesOps_WIF_Limit = approvalLimits.APTPS_SalesOps_WIF_Limit__c != null ? approvalLimits.APTPS_SalesOps_WIF_Limit__c : 0.0;
    public Decimal Hours_Factor = approvalLimits.APTPS_Hours_Factor__c != null ? approvalLimits.APTPS_Hours_Factor__c : 0.0;   
    
    // END: Read Enhancement Approvals details from Custom settings  
   public void checkLegalApprovedRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
   public void checkCAManagerApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
   public void checkHeadofCommercialSalesApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
   public void checkDDCApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
       
        system.debug('DDC_WIF_Limit  --> ' + DDC_WIF_Limit);
        system.debug('Sales_Director_WIF_Limit --> '+ Sales_Director_WIF_Limit);
        system.debug('SalesOps_WIF_Limit --> '+ SalesOps_WIF_Limit);
        system.debug('Hours_Factor --> '+ Hours_Factor);
       
       for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Waiver of International Fees Product START*/
            if (proposalLineItem.Apttus_QPConfig__OptionId__c != null && proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode != null && 
            APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode)
            && proposalLineItem.Apttus_QPConfig__AttributeValueId__c != null && 
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c != null &&
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c != null &&
            Decimal.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 0 && 
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c > 0) {
                Decimal calculatedHour = Decimal.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                Decimal internationalFees = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c;
                Decimal caluculateFees = APTPS_SNL_Util.calculateWIF(calculatedHour,Hours_Factor,internationalFees);
                if (caluculateFees > DDC_WIF_Limit){
                    proposalObj.APTPS_Requires_DDC_Approval__c = true;
                    break;
                }
            }
            /*For Waiver of International Fees Product END*/
           

        }
        
        if(proposalObj.APTPS_Requires_DDC_Approval__c){
           proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = false;
           proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
        }
   }
    
    public void checkSalesOpsApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Waiver of International Fees Product START*/
            if (proposalLineItem.Apttus_QPConfig__OptionId__c != null && proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode != null && 
            APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode)
            && proposalLineItem.Apttus_QPConfig__AttributeValueId__c != null && 
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c != null &&
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c != null &&
            Decimal.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 0 && 
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c > 0) {
                Decimal calculatedHour = Decimal.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                Decimal internationalFees = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c;
                Decimal caluculateFees = APTPS_SNL_Util.calculateWIF(calculatedHour,Hours_Factor,internationalFees);
                if (caluculateFees > Sales_Director_WIF_Limit && caluculateFees <= SalesOps_WIF_Limit){
                    proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = true;
                    break;
                }
            }
            /*For Waiver of International Fees Product END*/
           

        }
        
        if(proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c ){
            proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
        }
        system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkSalesDirectorApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Waiver of International Fees Product START*/
            if (proposalLineItem.Apttus_QPConfig__OptionId__c != null && proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode != null && 
            APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode)
            && proposalLineItem.Apttus_QPConfig__AttributeValueId__c != null && 
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c != null &&
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c != null &&
            Decimal.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 0 && 
            proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c > 0) {
                Decimal calculatedHour = Decimal.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                Decimal internationalFees = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_International_Amount__c;
                Decimal caluculateFees = APTPS_SNL_Util.calculateWIF(calculatedHour,Hours_Factor,internationalFees);
                if (caluculateFees <= Sales_Director_WIF_Limit){
                    proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                    break;
                }
            }
            /*For Waiver of International Fees Product END*/
           

        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkDemoTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    public void checkDirectorARApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    public void checkRVPApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    public void checkSalesOpsTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    
    
    
    /** 
    @description: Enhancement NJE implementation
    @param:
    @return: 
    */
    public class NJE_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Enhancement NJE dApprovals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Enhancement obj = new APTPS_Approvals_Enhancement();
           
            
        }
    }
    
    /** 
    @description: Enhancement NJA implementation
    @param:
    @return: 
    */
    public class NJA_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Enhancement NJA Approvals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Enhancement obj = new APTPS_Approvals_Enhancement();
            obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesOpsApprovalRequired(proposalObj,quoteLines);
            obj.checkDDCApprovalRequired(proposalObj,quoteLines);
            
        }
    }
}