/************************************************************************************************************************
@Name: APTPS_EmailNotificationCallable
@Author: Conga PS Dev Team
@CreateDate: 21 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_EmailNotificationCallable {
    Object call(String productCode, Map<String, Object> args);
}