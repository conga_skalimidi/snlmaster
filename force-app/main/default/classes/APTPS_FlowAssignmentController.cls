/************************************************************************************************************************
@Name: APTPS_FlowAssignmentController
@Author: Conga PS Dev Team
@CreateDate: 21 JAN 2022
@Description: CPQ Config Flow assignment controller. It will read custom metadata and will do the flow assignment based on 
Program Type, Currency, User Action(New Sale/Modification) and Modification type.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
public class APTPS_FlowAssignmentController {
    @AuraEnabled
    public static String getCatalogURL(String programType, String currencyCode, String actionType, String modificationType, Id qId, String additionalModType) {
        system.debug('Parameters from screen flow to APEX --> programType = '+programType+' currencyCode = '+currencyCode+' actionType = '+actionType+' modificationType = '+modificationType+' qId = '+qId);
        String catalogURL = APTS_ConstantUtil.CATALOG_URL+qId;
        
        String key = APTPS_SNL_Util.getRTKey(programType, currencyCode, actionType, modificationType, additionalModType);
        system.debug('APTPS_FlowAssignmentController:getCatalogURL Key --> '+key);
        
        Map<String, String> catalogFlowMap = APTPS_SNL_Util.getProductMap(null, APTS_ConstantUtil.FLOW_ASSIGNMENT);
        system.debug('APTPS_FlowAssignmentController:getCatalogURL catalogFlowMap --> '+catalogFlowMap);
        
        if(catalogFlowMap != null) {
            String flowDetails = catalogFlowMap.containsKey(key) ? catalogFlowMap.get(key) : null;
            if(flowDetails != null)
                catalogURL += flowDetails;               
        } else
            system.debug('Product Map Metadata is missing.');
        
        system.debug('Final Catalog URL --> '+catalogURL);
        return catalogURL;
    }
}