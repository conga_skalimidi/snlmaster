public with sharing class APTPS_NumberToOrdinalFormat {
static String[] to_19 = new string[]{ 'Zero', 'One',  'Two', 'Three', 'Four',  'Five',  'Six',  
    'Seven', 'Eight', 'Nine', 'Ten',  'Eleven', 'Twelve', 'Thirteen',  
    'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'};  
    static String[] ordinalTo_19 = new string[]{'Zeroth', 'First',  'Second', 'Third', 'Fourth',  'Fifth',  'Sixth',  
    'Seventh', 'Eighth', 'Ninth', 'Tenth',  'Eleventh', 'Twelfth', 'Thirteenth',  
    'Fourteenth', 'Fifteenth', 'Sixteenth', 'Seventeenth', 'Eighteenth', 'Nineteenth'};
static String[] ordinaltens = new string[]{ 'Twentieth', 'Thirtieth', 'Fortieth', 'Fiftieth', 'Sixtieth', 'Seventieth', 'Eightieth', 'Ninetieth'};
static String[] tens = new string[]{ 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'};  
static String[] denom = new string[]{ 'Hundred',  
    'Thousand'};
static String[] ordinalDenom = new string[]{ 'Hundredth','Thousandth'};
       
    // convert a value < 100 to English Ordinal Format
    public static String convert_nn(integer val, Boolean ordinal) {
        String[] numberName = ordinal ? ordinalTo_19 : to_19;
        if (val < 20) {
            return numberName[val];
        }
        for (integer v = 0; v < tens.size(); v++) {  
            String dcap = tens[v];  
            integer dval = 20 + 10 * v;  
            if (dval + 10 > val) {  
                if (Math.Mod(val,10) != 0){  
                    return dcap + (ordinal?'-':' ') + numberName[Math.Mod(val,10)];  
                }
                return ordinal ? ordinaltens[v] : dcap;  
            }      
        }  
        return 'Should never get here, less than 100 failure';  
    }  
   
    // convert a value > 100 to English Ordinal Format
    public static String convert_nnn(integer val, Boolean ordinal) {  
        List<String> word = new List<String>();  
        integer rem = val / 100;  
        integer mod = Math.mod(val,100);
        if (rem >= 10) {  
            word.add(convert_nnn(rem/10,false));
            rem = Math.mod(rem,10);
            word.add(ordinal && (rem+mod)==0?ordinalDenom[1]:denom[1]);
        }
        if (rem > 0) {  
            word.add(to_19[rem]);
            word.add(ordinal && mod==0?ordinalDenom[0]:denom[0]);  
            if (mod > 0) {  
                word.add('and');  
            }  
        }  
        if (mod > 0) {  
            word.add(convert_nn(mod,ordinal));  
        }  
        return String.Join(word,' ');
    }  
    public static String toOrdinalFormat(Integer val) {
        if (val < 1000000) {  
            return convert_nnn(val,true);  
        }    
        else if (val < 100) {  
            return convert_nn(val,true);  
        }
        return 'Should never get here, Bottomed out in english_number';  
    }  
}