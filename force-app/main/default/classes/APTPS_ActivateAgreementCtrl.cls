/*************************************************************
@Name: APTPS_ActivateAgreementCtrl
@Author: Siva Kumar
@CreateDate: 02 July 2021
@Description : This  class is called to Activate SNL Agreement
******************************************************************/
public class APTPS_ActivateAgreementCtrl {
@AuraEnabled
    public static string activateAgreement(string recordId)
    {
        String jobId = autoActivateAgreements(new set<Id>{Id.valueOf(recordId)});
        return jobId;
    }
@AuraEnabled
    public static AsyncApexJob getBatchJobStatus(Id jobID){
        AsyncApexJob jobInfo = [SELECT Status, NumberOfErrors,JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id = :jobID];
        return jobInfo;
    }
@AuraEnabled
    public static boolean terminateAgreement(string recordId){
        Boolean response = Apttus.AgreementWebService.terminateAgreement(recordId);
        if (response){
          List<Apttus_Config2__AssetLineItem__c> updateALIList = new List<Apttus_Config2__AssetLineItem__c>();
          List<Apttus__APTS_Related_Agreement__c> relAgreementObj = new List<Apttus__APTS_Related_Agreement__c>();
          Apttus__APTS_Agreement__c updateAgreement = new Apttus__APTS_Agreement__c(id = recordId, Apttus__Termination_Date__c = system.today(), Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_TERMINATED, Chevron_Status__c = APTS_ConstantUtil.AGREEMENT_TERMINATED, APTS_Path_Chevron_Status__c = APTS_ConstantUtil.AGREEMENT_TERMINATED);
         system.debug('updateAgreement-->' + updateAgreement);
          update updateAgreement;
          for (Apttus_Config2__AssetLineItem__c updateALI : [SELECT Id, Apttus_CMConfig__AgreementId__c, Apttus_Config2__AssetStatus__c
                                                             FROM Apttus_Config2__AssetLineItem__c
                                                             WHERE Apttus_CMConfig__AgreementId__c = :recordId]){
            updateALI.Apttus_Config2__AssetStatus__c = APTS_ConstantUtil.CANCEL ;
            updateALI.APTS_Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_INACTIVE ;
            updateALI.APTS_NJ_Asset_Status__c = APTS_ConstantUtil.AGREEMENT_TERMINATED;
            updateALIList.add(updateALI);
          }
          relAgreementObj = [SELECT Id,Apttus__APTS_Contract_From__c,Apttus__APTS_Contract_To__c,APTS_Business_Object_Type__c FROM Apttus__APTS_Related_Agreement__c WHERE Apttus__APTS_Contract_From__c =:recordId Limit 1];

          system.debug('updateALIList-->' + updateALIList);
          system.debug('relAgreementObj-->' + relAgreementObj);
          if (!updateALIList.isEmpty()){
            update updateALIList;
          }
          if (!relAgreementObj.isEmpty()){
            Apttus__APTS_Agreement__c updateRelatedAgreement = new Apttus__APTS_Agreement__c(id = relAgreementObj[0].Apttus__APTS_Contract_To__c, Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_AMENDED , Chevron_Status__c = APTS_ConstantUtil.AGREEMENT_AMENDED );
            update updateRelatedAgreement;
          }
          
        }
        return response;
    }
    /** 
    @description: Auto Activate Agreement for Letter Agreement Extension Modification
    @param: activateAgrIds
    
    */
    
    public static String autoActivateAgreements(set<Id> activateAgrIds)
    {
        List<Id> agrIds = new List<Id>();
        agrIds.addAll(activateAgrIds);
        String jobId = Database.executeBatch(new APTPS_ActivateAgreementBatch(agrIds),1);
        
        return jobId;
        
        
    }
}