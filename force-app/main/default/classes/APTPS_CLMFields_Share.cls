/************************************************************************************************************************
@Name: APTPS_CLMFields_Share
@Author: Conga PS Dev Team
@CreateDate: 08 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMFields_Share {
    
    /** 
    @description: ppoulate Agreeement Fields
    @param: AgreementList and Currency Code
    @return: 
    */
    private static void populateShareFields(List<Apttus__APTS_Agreement__c> agrList, String currencyCode) {
          system.debug('Populate Agreement Fileds');
            set<Id> proposalIds = new set<Id>();
            set<Id> agrIds = new set<Id>();
            Map<Id,Apttus_Proposal__Proposal__c> proposalMap = new Map<Id,Apttus_Proposal__Proposal__c>();
            Map<String, String> NJEAircraftMap = new Map<String, String>();
            Map<String,APTPS_Interchange_Interim__mdt> interchangeInterimMap = APTPS_SNL_Util.getInterchangeInterimMap();
            if(currencyCode == APTS_ConstantUtil.CUR_EUR ) {
                for(APTPS_NJE_Aircraft_Mapping__mdt mapping : [SELECT Agreement_Type__c, Associated_Aircraft__c FROM APTPS_NJE_Aircraft_Mapping__mdt]) {
                    NJEAircraftMap.put(mapping.Agreement_Type__c, mapping.Associated_Aircraft__c);
                }
            }
            system.debug('NJEAircraftMap-->'+NJEAircraftMap);
            for(Apttus__APTS_Agreement__c agr : agrList) {
                proposalIds.add(agr.Apttus_QPComply__RelatedProposalId__c);
                agrIds.add(agr.Id);
            }
            for(Apttus_Proposal__Proposal__c pObj : [SELECT id, Apttus_Proposal__Amount__c,Name,Aircraft_Types__c,Approval_Primary_AE_User__c, Approval_Primary_SE_User__c, Approval_Sales_Director_User__c, Approval_RVP_User__c, APTPS_Approval_Secondary_SE_User__c,Legal_Entity_Name__c,Apttus_QPConfig__LocationId__c,Product_Line__c,APTPS_Start_of_Deferment__c,APTPS_End_of_Deferment__c,APTPS_Has_Interim_Lease_Option__c,APTPS_Share_Percentage__c FROM Apttus_Proposal__Proposal__c WHERE Id IN :proposalIds]) {
                proposalMap.put(pObj.id,pObj);
            }
            Id shareRecTypeId;
            if(currencyCode == APTS_ConstantUtil.CUR_EUR ) {
                shareRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.SHARE_NJE_RECORD_TYPE ).getRecordTypeId(); 
                
            } else if(currencyCode == APTS_ConstantUtil.CUR_USD ) {
                shareRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.SHARE_NJA_RECORD_TYPE).getRecordTypeId(); 
            }
            for(Apttus__APTS_Agreement__c agObj : agrList) {
                   
                agObj.Funding_Status__c = APTS_ConstantUtil.FUND_PENDING ;
                agObj.Document_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                agObj.Agreement_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                agObj.Chevron_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                agObj.APTS_Path_Chevron_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                agObj.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.APPROVAL_REQUIRED; 
                //agObj.RecordTypeId = shareRecTypeId;
                agObj.Apttus_CMConfig__AutoActivateOrder__c = true;
                agObj.Generate_Card_Number__c = true;
                if(agObj.APTPS_Share_Percentage__c != null || agObj.APTPS_Share_Percentage__c != null) {
                   //agObj.APTPS_Share_Percentage_Text__c = APTPS_SNLSharePercentageText.populateAgreementExtFields(agObj.APTPS_Share_Percentage__c);
                    agObj.APTPS_Share_Percentage__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_Share_Percentage__c;
                }
                
                if(!proposalMap.isEmpty()){
                    agObj.Account_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Primary_AE_User__c;
                    agObj.Primary_Sales_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Primary_SE_User__c;
                    agObj.Sales_Consultant__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Sales_Director_User__c;
                    agObj.RVP__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_RVP_User__c;
                    agObj.Secondary_Sales_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_Approval_Secondary_SE_User__c;
                     String agName = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Legal_Entity_Name__c+', '+proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Product_Line__c+' - '+proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Aircraft_Types__c;
                    agObj.Name = agName;
                    agObj.Document_Name__c = agName;
                    agObj.Apttus_CMConfig__LocationId__c=  proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__LocationId__c;
                    agObj.Account_Location__c =  proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__LocationId__c;
                    agObj.APTS_Total_Purchase_Price__c=  proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_Proposal__Amount__c;
                    
                    //Start of GCM-12869
                    if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(agObj.APTS_Modification_Type__c)){
                        APTPS_SNL_Util.defermentModificationOnAgreement(agObj,proposalMap);
                    }
                    //End of GCM-12869
                    
                    //Start of GCM-13175
                    if(proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_Has_Interim_Lease_Option__c && !interchangeInterimMap.isEmpty()) {
                        if(interchangeInterimMap.containsKey(proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Aircraft_Types__c)) {
                            APTPS_Interchange_Interim__mdt interchageInterim = interchangeInterimMap.get(proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Aircraft_Types__c);
                            agObj.APTPS_Interchange_Ex__c = interchageInterim.Interchange_Ex__c;
                            agObj.APTPS_Interchange_Interim_AC__c = interchageInterim.Interchange_Interim_AC__c;
                            agObj.APTPS_Interchange_Interim_Ex_AC__c = interchageInterim.Interchange_Interim_Ex_AC__c;
                            agObj.APTPS_Interchange_Interim_Ex_Number__c = interchageInterim.Interchange_Interim_Ex_Number__c;
                        }
                    }
                    //End of GCM-13175
                }
                if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(currencyCode) && !NJEAircraftMap.isEmpty()) {
                     String aircraftTypes = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Aircraft_Types__c;
                     system.debug('aircraftTypes-->'+aircraftTypes);
                    for(String agreementTypeKey : NJEAircraftMap.keyset()) {
                        if(aircraftTypes!= null && NJEAircraftMap.get(agreementTypeKey).contains(aircraftTypes)) {
                            agObj.APTPS_NJE_Aircraft_Type__c = agreementTypeKey;
                            break;
                        }
                    }
                }
            }
            system.debug('agrList-->'+agrList);
    }
    
    /** 
    @description: NJE Share implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
             APTPS_CLMFields_Share.populateShareFields(agrList,APTS_ConstantUtil.CUR_EUR);
            
                
            
        }
    }
    
    /** 
    @description: NJA Share implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
             APTPS_CLMFields_Share.populateShareFields(agrList,APTS_ConstantUtil.CUR_USD);
            
                
            
        }
    }
}