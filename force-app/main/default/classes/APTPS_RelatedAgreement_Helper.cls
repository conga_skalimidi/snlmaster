/************************************************************************************************************************
@Name: APTPS_RelatedAgreement_Helper
@Author: Conga PS Dev Team
@CreateDate:
@Description: Related Agreement helper. Business logic for Related Agreement Trigger.
************************************************************************************************************************
@ModifiedBy: Conga PS Dev Team
@ModifiedDate: 13/04/2022
@ChangeDescription: Added logic for Shares and Leases.
************************************************************************************************************************/
public class APTPS_RelatedAgreement_Helper {
    public static void updateSubstitutionDealSummaryBeforeUpdate(Map<Id, Apttus__APTS_Related_Agreement__c> agMap) {
        List<Apttus__APTS_Related_Agreement__c> relAgList =[SELECT Apttus__APTS_Contract_From__r.Id,Apttus__APTS_Contract_To__r.Id,
                                                            Apttus__APTS_Contract_From__r.APTPS_Agreement_Extension__r.Id,
                                                            Apttus__APTS_Contract_From__r.APTPS_Program_Type__c
                                                            FROM Apttus__APTS_Related_Agreement__c 
                                                            WHERE Apttus__APTS_Related_Agreement__c.Id in: agMap.values()];
        system.debug('relAgList-->'+relAgList);
        
        List<Apttus__APTS_Agreement__c> updateAgr = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c ag=new Apttus__APTS_Agreement__c();
        
        for(Apttus__APTS_Related_Agreement__c agObj : relAgList) {
            system.debug('agObj-->'+agObj);
            Map<Id,Apttus__APTS_Agreement__c> substitutionAgMap2 = new Map<Id,Apttus__APTS_Agreement__c>();
            
            ag.Id = agObj.Apttus__APTS_Contract_To__r.Id;
            ag.APTPS_Parent_Agreement_Extension__c = agObj.Apttus__APTS_Contract_From__r.APTPS_Agreement_Extension__r.Id;
            ag.APTPS_Program_Type__c = agObj.Apttus__APTS_Contract_From__r.APTPS_Program_Type__c;
            substitutionAgMap2.put(ag.Id, ag);
            string dsText = APTPS_DealSummary_Share.getSubstitutionSummary(substitutionAgMap2);
            system.debug('dsText-->'+dsText);
            ag.Deal_Description__c = dsText;
        }
        updateAgr.add(ag);
        update updateAgr;
    }
}