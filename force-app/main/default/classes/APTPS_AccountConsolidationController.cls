/************************************************************************************************************************
@Name: APTPS_AccountConsolidationController
@Author: Conga PS Dev Team
@CreateDate: 29 Dec 2021
@Description: VF Page controller for creating records in Account Interest Information and schedule a job for creating on PDF's on Proposal.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
public class APTPS_AccountConsolidationController {
    public static id qId{get;set;}
    public static List<APTPS_Account_Interest_Information__c> accIntInfo{get;set;}
    public static Attachment at{get;set;}
    
    public APTPS_AccountConsolidationController(ApexPages.StandardController Controller)
    {
        at = new Attachment();
        qId=ApexPages.currentPage().getParameters().get('id');
        system.debug('line 23-->'+ qId);
    }
    /**
     @description: Button action on Proposal to create records in Account Interest Information.
     @param: 
     @return: returns back to same proposal record
     */
    public static Pagereference insertAccountInfoRecords(){
        //Create Aircraft information records
        //APTPS_AccountConsolidationDynamic.getAccountConsolidation(qId);
        APTPS_AccountConsolidationOnQuote.getAccountSummary(qId);

        //Schedule a call to generate a pdf
        Integer Scheduletime;
        APTPS_Account_Consolidation_Settings__c consolidationParam = APTPS_Account_Consolidation_Settings__c.getOrgDefaults();
        Scheduletime = Integer.valueOf(consolidationParam.Schedule_Time__c) != null ? Integer.valueOf(consolidationParam.Schedule_Time__c) : Scheduletime;
        
        /*List<APTPS_Account_Interest_Information__c> getAIIdetails =[SELECT Id, Name,APTPS_Proposal__c,APTPS_Configuration_Summary__c
                                                                    FROM APTPS_Account_Interest_Information__c
                                                                    WHERE APTPS_Proposal__c =: qId];*/
        if(Scheduletime != null){
            String nextExecutionTime = Datetime.now().addSeconds(Scheduletime).format('s m H d M ? yyyy');  
            System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextExecutionTime, new APTPS_ConsolidatePdfScheduler(qId, 1));
        }
        
        //Redirect back to current proposal record
        PageReference pg = null;
        String url = '/'+qId;
        pg = new PageReference(url);
        pg.setRedirect(true);
        return pg;
    }
}