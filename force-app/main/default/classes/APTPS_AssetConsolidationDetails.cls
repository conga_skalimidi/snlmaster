/************************************************************************************************************************
@Name: APTPS_AssetConsolidationDetails
@Author: Conga PS Dev Team
@CreateDate: 22 Dec 2021
@Description: Account Interest Information and Arcraft Gurantee Component - Controller
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_AssetConsolidationDetails {
    public static Map<Double, String> rankCabinMap = new Map<Double, String>();
    public static Map<Double, String> rankACMap = new Map<Double, String>();
    public static Set<double> infRank = new Set<Double>();
    
    /**
    @description: Get Account Interest information(Read Asset line items and current agreement line items)  
    @param:  recId - Current Agreement Id
    @return: String - JSON string output
    */
    @AuraEnabled
    public static String getAssetDetails(Id recId) {
        Id accId = null;
        String resultJSON = '';
        List<AssetConsolidationWrapper> finalResultList = new List<AssetConsolidationWrapper>();
        List<AssetConsolidationWrapper> assetWrapperList = new List<AssetConsolidationWrapper>();
        List<AssetConsolidationWrapper> wrapperLIList = new List<AssetConsolidationWrapper>();
        Map<Double, Integer> rankHoursMap = new Map<Double, Integer>();
        rankCabinMap = new Map<Double, String>();
        rankACMap = new Map<Double, String>();
        infRank = new Set<Double>();
        //Get corresponding Account ID
        Apttus__APTS_Agreement__c ag = [SELECT Id, Apttus__Account__c, Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =:recId LIMIT 1];
        accId = (ag != null && ag.Apttus__Account__c != null) ? ag.Apttus__Account__c : null;
        
        //Read Asset Line Items and get the information
        rankHoursMap = getAggDetailsFromAssets(accId);
        
        //Read current Agreement ALIs if Agreement is not 'Active'/'Inactive'
        if(!APTS_ConstantUtil.AGREEMENT_ACTIVE.equalsIgnoreCase(ag.Agreement_Status__c) && !APTS_ConstantUtil.AGREEMENT_INACTIVE.equalsIgnoreCase(ag.Agreement_Status__c)) {
            for(Apttus__AgreementLineItem__c ali : [SELECT Id, 
                                                    Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c, 
                                                    Apttus_CMConfig__OptionId__r.Cabin_Class__c, 
                                                    Apttus_CMConfig__OptionId__r.Name, 
                                                    Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c, 
                                                    Apttus_CMConfig__AttributeValueId__r.APTPS_Percentage__c  
                                                    FROM Apttus__AgreementLineItem__c 
                                                    WHERE Apttus__AgreementId__c =: ag.Id 
                                                    AND Apttus_CMConfig__IsPrimaryLine__c = true 
                                                    AND Apttus_CMConfig__OptionId__r.Family =: APTS_ConstantUtil.AIRCRAFT]) {
                                                        if(!rankHoursMap.isEmpty() && rankHoursMap.containsKey(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c)) {
                                                            Integer prevHr = rankHoursMap.get(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c);
                                                            prevHr += Integer.valueOf(ali.Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c);
                                                            rankHoursMap.put(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c, prevHr);
                                                            infRank.add(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c);
                                                        } else
                                                            rankHoursMap.put(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c, Integer.valueOf(ali.Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c));
                                                        
                                                        if(!rankCabinMap.containsKey(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c))
                                                            rankCabinMap.put(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c, ali.Apttus_CMConfig__OptionId__r.Cabin_Class__c);
                                                        
                                                        if(!rankACMap.containsKey(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c))
                                                            rankACMap.put(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c, ali.Apttus_CMConfig__OptionId__r.Name);
                                                    }
        }
        
        //Prepare final list
        wrapperLIList = generateResult(rankHoursMap);
        finalResultList.addAll(wrapperLIList);
        resultJSON = JSON.serialize(finalResultList);
        return resultJSON;
    }

	/**
    @description: Get Aircraft Gurantee records related to the corresponding Account
    @param:  recId - Current Agreement Id
    @return: String - JSON string output
    */   
    @AuraEnabled
    public static String getDetails(Id recId) {
        Id accId;
        String resultJSON = '';
        List<AircraftGuaranteeWrapper> wrapperList = new List<AircraftGuaranteeWrapper>();
        Apttus__APTS_Agreement__c ag = [SELECT Id, Apttus__Account__c, Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =:recId LIMIT 1];
        accId = ag != null ? ag.Apttus__Account__c : null;
        for(APTPS_Aircraft_Guarantee__c ac : [SELECT Id, APTPS_Type__c,APTPS_Aircraft_Type__c, 
                                              APTPS_Number_Of_Aircraft__c, APTPS_Status__c 
                                              FROM APTPS_Aircraft_Guarantee__c 
                                              WHERE APTPS_Account__c =: accId]){
                                                  wrapperList.add(new AircraftGuaranteeWrapper(ac));
                                              }
        resultJSON = !wrapperList.isEmpty() ? JSON.serialize(wrapperList) : null;
        return resultJSON;
    }
    
    @AuraEnabled
    public static Id getAccountId(Id agId) {
        Apttus__APTS_Agreement__c ag = [SELECT Id, Apttus__Account__c FROM Apttus__APTS_Agreement__c WHERE Id =:agId LIMIT 1];
        return ag.Apttus__Account__c;
    }
    
    /**
    @description: Delete Aircraft Guarantee record
    @param:  recId - Record Id to be deleted
    @return: void
    */
    @AuraEnabled 
    public static void deleteRecord(Id recId) {
        List<APTPS_Aircraft_Guarantee__c> acList = [SELECT Id FROM APTPS_Aircraft_Guarantee__c WHERE Id=: recId];
        if(!acList.isEmpty())
            delete acList;
    }
    
    /**
    @description: Prepare Aircraft Interest Information data.
    @param:  rankHoursMap - Map of Aircraft Rank and consolidated hours
    @return: List<AssetConsolidationWrapper>
    */
    public static List<AssetConsolidationWrapper> generateResult(Map<Double, Integer> rankHoursMap) {
        List<AssetConsolidationWrapper> wrapperLIList = new List<AssetConsolidationWrapper>();
        system.debug('rankHoursMap --> '+rankHoursMap);
        system.debug('rankCabinMap --> '+rankCabinMap);
        system.debug('rankACMap --> '+rankACMap);
        for(Double key : rankHoursMap.keySet()) {
            decimal per = Decimal.valueOf(rankHoursMap.get(key))/8.0;
            boolean isInfluenced = infRank.contains(key) ? true : false;
            wrapperLIList.add(new AssetConsolidationWrapper(key, rankCabinMap.get(key), rankACMap.get(key), String.valueOf(rankHoursMap.get(key)), per, isInfluenced));
        }
        return wrapperLIList;
    }
    
    /**
    @description: Read Asset Line Items
    @param:  accId - Account Id
    @return: Map<Double, Integer>
    */
    public static Map<Double, Integer> getAggDetailsFromAssets(Id accId) {
        system.debug('Received record Id --> '+accId);
        List<AssetConsolidationWrapper> wrapperList = new List<AssetConsolidationWrapper>();
        Map<Double, Integer> rankHoursMap = new Map<Double, Integer>();
        if(accId != null) {
            for(Apttus_Config2__AssetLineItem__c assetLi : [SELECT Apttus_CMConfig__AgreementId__c, 
                                                            Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, 
                                                            Apttus_Config2__OptionId__r.Cabin_Class__c, 
                                                            Apttus_Config2__OptionId__r.Name, 
                                                            Apttus_Config2__AttributeValueId__r.APTPS_Hours__c, 
                                                            Apttus_Config2__AttributeValueId__r.APTPS_Percentage__c  
                                                            FROM Apttus_Config2__AssetLineItem__c 
                                                            WHERE Apttus_CMConfig__AgreementId__r.Agreement_Status__c =: APTS_ConstantUtil.AGREEMENT_ACTIVE 
                                                            AND (Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c =: APTS_ConstantUtil.SHARE OR Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c =: APTS_ConstantUtil.LEASE) 
                                                            AND Apttus_Config2__AccountId__c =: accId 
                                                            AND Apttus_Config2__IsPrimaryLine__c = true 
                                                            AND Apttus_Config2__OptionId__r.Family =: APTS_ConstantUtil.AIRCRAFT]) {
                                                                if(!rankHoursMap.isEmpty() && rankHoursMap.containsKey(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c)) {
                                                                    Integer prevHr = rankHoursMap.get(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c);
                                                                    prevHr += Integer.valueOf(assetLi.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c);
                                                                    rankHoursMap.put(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, prevHr);
                                                                } else {
                                                                    rankHoursMap.put(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, Integer.valueOf(assetLi.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c));
                                                                }
                                                                
                                                                if(!rankCabinMap.containsKey(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c))
                                                                    rankCabinMap.put(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, assetli.Apttus_Config2__OptionId__r.Cabin_Class__c);
                                                                
                                                                if(!rankACMap.containsKey(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c))
                                                                    rankACMap.put(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, assetli.Apttus_Config2__OptionId__r.Name);
                                                                wrapperList.add(new AssetConsolidationWrapper(assetLi));
                                                            }
        } else
            system.debug('Account Id is NULL.');
        system.debug('wrapperList size --> '+wrapperList.size());
        return rankHoursMap;
    }
    
    //Inner Wrapper class
    public class AssetConsolidationWrapper {
        public double cabinRank;
        public string cabinClass;
        public string acType;
        public string hours;
        public decimal percentage;
        public boolean isInfluenced = false;
        
        public AssetConsolidationWrapper(double rank, string acClass, string type, string hr, decimal per, boolean isInf) {
            this.cabinRank = rank;
            this.cabinClass = acClass;
            this.acType = type;
            this.hours = hr;
            this.percentage = per;
            this.isInfluenced = isInf;
        }
        
        public AssetConsolidationWrapper(Apttus_Config2__AssetLineItem__c ali) {
            this.cabinRank = ali.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c;
            this.cabinClass = ali.Apttus_Config2__OptionId__r.Cabin_Class__c;
            this.acType = ali.Apttus_Config2__OptionId__r.Name;
            this.hours = ali.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c;
            this.percentage = ali.Apttus_Config2__AttributeValueId__r.APTPS_Percentage__c;
        }
        
        public AssetConsolidationWrapper(Apttus_Config2__LineItem__c li) {
            this.cabinRank = li.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c;
            this.cabinClass = li.Apttus_Config2__OptionId__r.Cabin_Class__c;
            this.acType = li.Apttus_Config2__OptionId__r.Name;
            this.hours = li.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c;
            this.percentage = li.Apttus_Config2__AttributeValueId__r.APTPS_Percentage__c;
        }
        
        public AssetConsolidationWrapper(Apttus__AgreementLineItem__c agLi) {
            this.cabinRank = agLi.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c;
            this.cabinClass = agLi.Apttus_CMConfig__OptionId__r.Cabin_Class__c;
            this.acType = agLi.Apttus_CMConfig__OptionId__r.Name;
            this.hours = agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c;
            this.percentage = agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Percentage__c;
        }
    }
    
    //Inner Wrapper class
    public class AircraftGuaranteeWrapper {
        public String typeOfGuarantee;
        public String acType;
        public Integer noAc;
        public String status;
        public Id acRecId;        
        public AircraftGuaranteeWrapper(APTPS_Aircraft_Guarantee__c ac) {
            this.typeOfGuarantee = ac.APTPS_Type__c;
            this.acType = ac.APTPS_Aircraft_Type__c;
            this.noAc = Integer.valueOf(ac.APTPS_Number_Of_Aircraft__c);
            this.status = ac.APTPS_Status__c;
            this.acRecId = ac.Id;
        }
    }
}