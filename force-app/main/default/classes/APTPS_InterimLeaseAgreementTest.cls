/************************************************************************************************************************
 @Name: APTPS_InterimLeaseAgreementTest
 @Author: Conga PS Dev Team
 @CreateDate: 17 Aug 2021
 @Description: Test coverage for Interim Lease Agreement
 ************************************************************************************************************************
 @ModifiedBy: 
 @ModifiedDate: 
 @ChangeDescription: 
 ************************************************************************************************************************/
@isTest
public class APTPS_InterimLeaseAgreementTest {
	@testsetup
    public static void makeData() {
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Share';
        insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Service_Account__c serviceAcc = new Service_Account__c();
        serviceAcc.Account__c = accToInsert.Id;
        serviceAcc.NetJets_Company__c = 'NJA';
        serviceAcc.Status__c = 'Active';
        insert serviceAcc;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
        List<Product2> prodList = new List<Product2>();
        Product2 NJA_SHARE = APTS_CPQTestUtility.createProduct('Share', 'NJUS_SHARE', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJA_SHARE);
        
        Product2 NJA_INTERIM_LEASE = APTS_CPQTestUtility.createProduct('Interim Lease', APTS_ConstantUtil.INTERIM_LEASE_CODE, 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJA_INTERIM_LEASE);
        
        Product2 PHENOM_NJUS = APTS_CPQTestUtility.createProduct('Phenom 300', 'PHENOM_300_NJUS', 'Apttus', 'Option', true, true, true, true);
        PHENOM_NJUS.Family = 'Aircraft';
        prodList.add(PHENOM_NJUS);
        insert prodList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c shareNJUSQuote = APTS_CPQTestUtility.createProposal('Share NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        shareNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        shareNJUSQuote.APTPS_Program_Type__c = 'Share';
        shareNJUSQuote.CurrencyIsoCode = 'USD';
        proposalList.add(shareNJUSQuote);
        insert proposalList;
        
        //Create Quote/Proposal Line Items
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSInterim = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSInterim.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSInterim.Apttus_QPConfig__OptionId__c = NJA_INTERIM_LEASE.Id;
        shareNJUSInterim.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        pliList.add(shareNJUSInterim);
        
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSAc = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSAc.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.PURCHASE_PRICE;
        shareNJUSAc.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSAc.Apttus_QPConfig__OptionId__c = PHENOM_NJUS.Id;
        shareNJUSAc.Apttus_QPConfig__NetPrice__c = 1000;
        pliList.add(shareNJUSAc);
        insert pliList;
        
        //Create Proposal Attribute Value
        List<Apttus_QPConfig__ProposalProductAttributeValue__c> pavList = new List<Apttus_QPConfig__ProposalProductAttributeValue__c>();
        Apttus_QPConfig__ProposalProductAttributeValue__c shareNJUSAcPAV = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        shareNJUSAcPAV.Apttus_QPConfig__LineItemId__c = shareNJUSAc.Id;
        shareNJUSAcPAV.APTPS_Vintage__c = '2015';
        shareNJUSAcPAV.APTPS_Hours__c = '50';
        shareNJUSAcPAV.APTPS_Percentage__c = 50;
        shareNJUSAcPAV.APTPS_Contract_Length__c = 60.0;
        shareNJUSAcPAV.APTPS_Delayed_Start_Date__c = Date.today();
        shareNJUSAcPAV.APTPS_Minimum_Commitment_Period__c = '36';
        shareNJUSAcPAV.APTPS_End_Date__c = Date.today();
        shareNJUSAcPAV.APTPS_Delayed_Start_Months__c = '2';
        pavList.add(shareNJUSAcPAV);
        insert pavList;
        
        //Link PAVs to respective PLIs
        List<Apttus_Proposal__Proposal_Line_Item__c> pliToUpdate = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        shareNJUSAc.Apttus_QPConfig__AttributeValueId__c = shareNJUSAcPAV.Id;
        pliToUpdate.add(shareNJUSAc);
        update pliToUpdate;
        Test.stopTest();
    }
    
    @isTest
    public static void testInterimLeaseAgreement() {
        Apttus_Proposal__Proposal__c interimLeaseQuote = [SELECT Id, Apttus_Proposal__Account__c FROM Apttus_Proposal__Proposal__c LIMIT 1];
        
        Apttus__APTS_Agreement__c interimLeaseAg = new Apttus__APTS_Agreement__c();
        interimLeaseAg.Name = 'Interim Lease Agreement';
        interimLeaseAg.Agreement_Status__c = 'Draft';
        interimLeaseAg.Apttus__Account__c = interimLeaseQuote.Apttus_Proposal__Account__c;
        interimLeaseAg.Apttus_QPComply__RelatedProposalId__c = interimLeaseQuote.Id;
        interimLeaseAg.APTPS_Program_Type__c = APTS_ConstantUtil.SHARE;
        interimLeaseAg.Apttus__Status__c = 'Request';
        interimLeaseAg.Apttus__Status_Category__c = 'Request';
        interimLeaseAg.CurrencyIsoCode = 'USD';
        interimLeaseAg.APTPS_Closing_Date__c = Date.today();
        interimLeaseAg.First_Flight_Date__c = Date.today();
        insert interimLeaseAg;
        
		Apttus__APTS_Agreement__c resultAg = [SELECT Id, RecordTypeId FROM Apttus__APTS_Agreement__c WHERE Apttus__Parent_Agreement__c =: interimLeaseAg.Id];
		system.assert(resultAg != null);
    }
}