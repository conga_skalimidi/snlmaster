/************************************************************************************************************************
@Name: APTPS_AccountConsolidationOnQuote
@Author: Conga PS Dev Team
@CreateDate: 29 Dec 2021
@Description: Generate Account Interest Information (Consolidate data from Assets and Cureent Configuration)
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
public class APTPS_AccountConsolidationOnQuote {
    /** 
    @description: Get Account Summary - Assets and Current configuration details
    @param: qId - Quote/Proposal Id
    @return: 
    */
    public static void getAccountSummary(Id qId) {
        Id accId = null;
        Set<Id> agIds = new Set<Id>();
        List<Apttus_Config2__AssetLineItem__c> assetList = new List<Apttus_Config2__AssetLineItem__c>();
        List<String> enhCodes = new List<String>();
        Map<Id, Id> agConfigMap = new Map<Id, Id>();
        List<APTPS_Account_Interest_Information__c> intInfoList = new List<APTPS_Account_Interest_Information__c>();
        List<APTPS_Account_Interest_Information__c> accInfoDelList = new List<APTPS_Account_Interest_Information__c>();
        /*Enhancement Approvals related variables START*/
        Map<Id, String> agHoursMap = new Map<Id, String>();
        Map<Id, Decimal> agCabinClassMap = new Map<Id, Decimal>();
        Map<String, Integer> enhAggHrsMap = new Map<String, Integer>();
        Map<String, list<decimal>> enhAggCRMap = new Map<String, list<decimal>>();
        Map<String, decimal> enhCRMap = new Map<String, decimal>();
        Integer enhAccAggHrs = 0;
        /*Enhancement Approvals related variables END*/
        //Delete any existing Account Interest Information records for the current Quote/Proposal
        accInfoDelList = [SELECT Id FROM APTPS_Account_Interest_Information__c WHERE APTPS_Proposal__c =: qId];
        system.debug('Number of existing Account Interest Information records to be deleted --> '+accInfoDelList.size()+' Record Details  --> '+accInfoDelList);
        if(!accInfoDelList.isEmpty())
            delete accInfoDelList;
        //Get Account Id
        Apttus_Proposal__Proposal__c quote = [SELECT Id, Apttus_Proposal__Account__c, Apttus_QPConfig__ConfigurationFinalizedDate__c,
                                              Deal_Description__c, APTPS_Program_Type__c,APTPS_Requires_DDC_Approval__c, APTS_Requires_VP_Sales_Ops_Approval__c, APTS_Requires_Sales_Director_Approval__c,
                                              APTS_Require_AE_SalesVP_Approval__c, Apttus_Proposal__Approval_Stage__c, Apttus_QPApprov__Approval_Status__c,
                                              APTPS_Requires_Director_AR_Approval__c 
                                              FROM Apttus_Proposal__Proposal__c
                                              WHERE Id =:qId LIMIT 1];
        accId = (quote != null && quote.Apttus_Proposal__Account__c != null) ? quote.Apttus_Proposal__Account__c : null;
        //Generate Account Interest Record for current proposal
        APTPS_Account_Interest_Information__c tempInt = generateInterestInfo(qId, null, 2, quote.Deal_Description__c, true);
        intInfoList.add(tempInt);
        
        if(accId != null) {
            //Get agreements related to the Account Id
            for(Apttus_Config2__AssetLineItem__c assetLI : [SELECT Id, Apttus_CMConfig__AgreementId__c, Apttus_Config2__LineType__c, 
                                                            Apttus_CMConfig__AgreementId__r.Deal_Description__c, 
                                                            Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c, 
                                                            Apttus_Config2__ProductId__r.ProductCode, 
                                                            Apttus_Config2__OptionId__r.ProductCode,
                                                            Apttus_Config2__AttributeValueId__r.APTPS_Hours__c,
                                                            Apttus_Config2__OptionId__r.Cabin_Class_Rank__c 
                                                            FROM Apttus_Config2__AssetLineItem__c 
                                                            WHERE Apttus_CMConfig__AgreementId__r.Apttus__Account__c =: accId 
                                                            AND Apttus_Config2__IsPrimaryLine__c = true 
                                                            AND Apttus_CMConfig__AgreementId__r.Agreement_Status__c =: APTS_ConstantUtil.STATUS_ACTIVE]) {
                                                                if(!agIds.contains(assetLI.Apttus_CMConfig__AgreementId__c) 
                                                                   && APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(assetLI.Apttus_Config2__LineType__c)) {
                                                                       agIds.add(assetLI.Apttus_CMConfig__AgreementId__c);
                                                                       assetList.add(assetLI);
                                                                   }
                                                                
                                                                if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(assetLI.Apttus_Config2__LineType__c) 
                                                                   && (APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equalsIgnoreCase(assetLI.Apttus_Config2__ProductId__r.ProductCode) 
                                                                       || APTS_ConstantUtil.SNL_NJE_ENHANCEMENT.equalsIgnoreCase(assetLI.Apttus_Config2__ProductId__r.ProductCode)) 
                                                                   && !enhCodes.contains(assetLI.Apttus_Config2__OptionId__r.ProductCode))
                                                                    enhCodes.add(assetLI.Apttus_Config2__OptionId__r.ProductCode);
                                                                //For Enhancement Approvals map with Agreement and hours    
                                                                if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(assetLI.Apttus_Config2__LineType__c) 
                                                                   && (APTS_ConstantUtil.NJA_SHARE_CODE.equalsIgnoreCase(assetLI.Apttus_Config2__ProductId__r.ProductCode) 
                                                                       || APTS_ConstantUtil.NJE_SHARE_CODE.equalsIgnoreCase(assetLI.Apttus_Config2__ProductId__r.ProductCode) || APTS_ConstantUtil.NJA_LEASE_CODE.equalsIgnoreCase(assetLI.Apttus_Config2__ProductId__r.ProductCode) 
                                                                       || APTS_ConstantUtil.NJE_LEASE_CODE.equalsIgnoreCase(assetLI.Apttus_Config2__ProductId__r.ProductCode)) 
                                                                   ) {
                                                                        agHoursMap.put(assetLI.Apttus_CMConfig__AgreementId__c,assetLI.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c);  
                                                                        enhAccAggHrs += Integer.ValueOf(assetLI.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c);
                                                                        if(assetLI.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c !=null) {
                                                                            agCabinClassMap.put(assetLI.Apttus_CMConfig__AgreementId__c,assetLI.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c);
                                                                        }
                                                                        
                                                                   }
                                                                    
            
                                                }
            //For Enhancement Approvals map with Proposals and hours  
            for(Apttus_Config2__LineItem__c configLi: [SELECT id, Apttus_Config2__ConfigurationId__c,Apttus_Config2__LineType__c,Apttus_Config2__ProductId__r.ProductCode,Apttus_Config2__OptionId__r.ProductCode,Apttus_Config2__AttributeValueId__r.APTPS_Hours__c,Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__r.Apttus_Config2__Status__c =: APTS_ConstantUtil.FINALIZED AND Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Apttus_Proposal__Account__c =: accId AND Apttus_Config2__IsPrimaryLine__c = true]) {
            //For Enhancement Approvals map with Proposals and hours    
                if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(configLi.Apttus_Config2__LineType__c) 
                && (APTS_ConstantUtil.NJA_SHARE_CODE.equalsIgnoreCase(configLi.Apttus_Config2__ProductId__r.ProductCode) 
                || APTS_ConstantUtil.NJE_SHARE_CODE.equalsIgnoreCase(configLi.Apttus_Config2__ProductId__r.ProductCode) || APTS_ConstantUtil.NJA_LEASE_CODE.equalsIgnoreCase(configLi.Apttus_Config2__ProductId__r.ProductCode) 
                || APTS_ConstantUtil.NJE_LEASE_CODE.equalsIgnoreCase(configLi.Apttus_Config2__ProductId__r.ProductCode))) {
                    agHoursMap.put(configLi.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c,configLi.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c);  
                    if(configLi.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c !=null) {
                        agCabinClassMap.put(configLi.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c,configLi.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c);
                    }
                        
                }   
            }
            system.debug('agIds --> '+agIds);
            system.debug('agHoursMap --> '+agHoursMap);
            system.debug('enhAccAggHrs --> '+enhAccAggHrs);
            system.debug('agCabinClassMap --> '+agCabinClassMap);
            
            
            //Get Configurations for all Agreements
            for(Apttus_Config2__ProductConfiguration__c config: [SELECT Id, Apttus_CMConfig__AgreementId__c 
                                                                 FROM Apttus_Config2__ProductConfiguration__c 
                                                                 WHERE Apttus_CMConfig__AgreementId__c IN :agIds]) {
                                                                     agConfigMap.put(config.Apttus_CMConfig__AgreementId__c, config.Id);
                                                                 }
            
            //Get Finalized Configuration for the current Quote/Proposal
            for(Apttus_Config2__ProductConfiguration__c config: [SELECT Id FROM Apttus_Config2__ProductConfiguration__c 
                                                                 WHERE Apttus_QPConfig__Proposald__c =: qId 
                                                                 AND Apttus_Config2__Status__c =: APTS_ConstantUtil.FINALIZED]) {
                                                                     agConfigMap.put(qId, config.Id);
                                                                 }
            system.debug('agConfigMap --> '+agConfigMap);
            Id quoteConfigId = null;
            if(!agConfigMap.isEmpty() && agConfigMap.containsKey(qId))
                quoteConfigId = agConfigMap.get(qId);
            
            //Get Configured Enhancements from current Quote/Proposal
            if(quoteConfigId != null) {
                for(Apttus_Config2__LineItem__c li : [SELECT Id, Apttus_Config2__OptionId__r.ProductCode,Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c,Apttus_Config2__AttributeValueId__r.APTPS_Quote_Proposal__c, Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c   
                                                      FROM Apttus_Config2__LineItem__c 
                                                      WHERE Apttus_Config2__ConfigurationId__c =: quoteConfigId 
                                                      AND Apttus_Config2__LineType__c =: APTS_ConstantUtil.OPTION 
                                                      AND Apttus_Config2__IsPrimaryLine__c = true 
                                                      AND (Apttus_Config2__ProductId__r.ProductCode =: APTS_ConstantUtil.SNL_NJA_ENHANCEMENT 
                                                           OR Apttus_Config2__ProductId__r.ProductCode =: APTS_ConstantUtil.SNL_NJE_ENHANCEMENT)]) {
                                                               if(!enhCodes.contains(li.Apttus_Config2__OptionId__r.ProductCode)) 
                                                                   enhCodes.add(li.Apttus_Config2__OptionId__r.ProductCode);
                                                               //For Enhancement Approvals create Map for Product code and Aggregate hours
                                                               if(APTS_ConstantUtil.SNL_ENTITLEMENT_ACCOUNT.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) {        enhAggHrsMap.put(li.Apttus_Config2__OptionId__r.ProductCode,enhAccAggHrs);
                                                               }
                                                               if(APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) { 
                                                                 if(agHoursMap.containsKey(li.Apttus_Config2__AttributeValueId__r.APTPS_Quote_Proposal__c)) {
                                                                    enhAccAggHrs+=  Integer.ValueOf(agHoursMap.get(li.Apttus_Config2__AttributeValueId__r.APTPS_Quote_Proposal__c));
                                                                 }
                                                                enhAggHrsMap.put(li.Apttus_Config2__OptionId__r.ProductCode,enhAccAggHrs);
                                                               }
                                                               if(APTS_ConstantUtil.SNL_ENTITLEMENT_AGR.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c) || APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) {
                                                                    /*if(agHoursMap.containsKey(li.Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c)) {
                                                                        if(enhAggHrsMap.containsKey(li.Apttus_Config2__OptionId__r.ProductCode)) {
                                                                           Integer hrs = enhAggHrsMap.get(li.Apttus_Config2__OptionId__r.ProductCode) + Integer.ValueOf(agHoursMap.get(li.Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c));
                                                                           enhAggHrsMap.put(li.Apttus_Config2__OptionId__r.ProductCode,hrs);
                                                                           
                                                                       } else {
                                                                           enhAggHrsMap.put(li.Apttus_Config2__OptionId__r.ProductCode,Integer.ValueOf(agHoursMap.get(li.Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c)));
                                                                       }
                                                                    }*/
                                                                   
                                                                   if(agCabinClassMap.containsKey(li.Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c) && (APTS_ConstantUtil.NJUS_UPGRADES.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode) || APTS_ConstantUtil.NJE_UPGRADES.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode))) {
                                                                        if(enhAggCRMap.containsKey(li.Apttus_Config2__OptionId__r.ProductCode)) {
                                                                           list<decimal> crList = enhAggCRMap.get(li.Apttus_Config2__OptionId__r.ProductCode) ;
                                                                           if(APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) {
                                                                              crList.add(agCabinClassMap.get(li.Apttus_Config2__AttributeValueId__r.APTPS_Quote_Proposal__c));
                                                                                enhAggCRMap.put(li.Apttus_Config2__OptionId__r.ProductCode,crList); 
                                                                           }
                                                                           if(APTS_ConstantUtil.SNL_ENTITLEMENT_AGR.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) {
                                                                              crList.add(agCabinClassMap.get(li.Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c));
                                                                                enhAggCRMap.put(li.Apttus_Config2__OptionId__r.ProductCode,crList); 
                                                                           }
                                                                           
                                                                           
                                                                       } else {
                                                                           if(APTS_ConstantUtil.SNL_ENTITLEMENT_AGR.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) {
                                                                            enhAggCRMap.put(li.Apttus_Config2__OptionId__r.ProductCode,new List<decimal>{agCabinClassMap.get(li.Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c)});
                                                                           }
                                                                            if(APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) {
                                                                            enhAggCRMap.put(li.Apttus_Config2__OptionId__r.ProductCode,new List<decimal>{agCabinClassMap.get(li.Apttus_Config2__AttributeValueId__r.APTPS_Quote_Proposal__c)});
                                                                           }
                                                                       }
                                                                    }
                                                                   
                                                               }
                                                           }
            }
            system.debug('Referring to assets, enhCodes --> '+enhCodes);
            system.debug('enhAggHrsMap --> '+enhAggHrsMap);
            system.debug('enhAggCRMap --> '+enhAggCRMap);
            if(!enhAggCRMap.isEmpty()) {
                for(String crKey : enhAggCRMap.keySet()) {
                    decimal maxRank = getMaxRank(enhAggCRMap.get(crKey));
                    enhCRMap.put(crKey,maxRank);
                }
            }
            system.debug('enhCRMap --> '+enhCRMap);
            //START: Custom Metadata Logic. Note - If required, replace it with Cache Logic in future.
            //Read Custom Metadata and form Product Specific Maps
            Map<String, String> prodFldSetMap = new Map<String, String>();
            Map<String, List<String>> fieldMap = new Map<String, List<String>>();
            Map<String, Map<String, String>> prodApiMAP = new Map<String, Map<String, String>>();
            for(APTPS_Account_Consolidation_Field_Map__mdt fldMap : [SELECT Id, Product_Code__c, Field_Set__c 
                                                                     FROM APTPS_Account_Consolidation_Field_Map__mdt 
                                                                     WHERE Product_Code__c IN :enhCodes]) {
                                                                         prodFldSetMap.put(fldMap.Product_Code__c, fldMap.Field_Set__c);
                                                                     }
            system.debug('prodFldSetMap --> '+prodFldSetMap);
            for(String key : prodFldSetMap.keySet()) {
                List<String> dsFields = new List<String>();
                Map<String, String> apiMap = new Map<String, String>();
                for(Schema.FieldSetMember fld : Schema.SObjectType.Apttus_Config2__ProductAttributeValue__c.fieldSets.getMap().get(prodFldSetMap.get(key)).getFields()) {
                    String fldName = 'Apttus_Config2__AttributeValueId__r.'+fld.getFieldPath();
                    apiMap.put(fld.getFieldPath(), fld.getLabel());
                    if(!dsFields.contains(fldName))
                        dsFields.add(fldName);
                }
                fieldMap.put(key, dsFields);
                prodApiMAP.put(key, apiMap);
            }
            system.debug('fieldMap --> '+fieldMap);
            system.debug('prodApiMAP --> '+prodApiMAP);
            //END: Custom Metadata logic
            
            //Read extra parameters from Custom Settings
            String extraParam = '';
            APTPS_Account_Consolidation_Settings__c consolidationParam = APTPS_Account_Consolidation_Settings__c.getOrgDefaults();
            extraParam = consolidationParam.Additional_Parameters__c != null ? consolidationParam.Additional_Parameters__c : extraParam;
            if(consolidationParam.Additional_Parameters_1__c != null) {
                if(!String.isEmpty(extraParam)) 
                    extraParam = extraParam+','+consolidationParam.Additional_Parameters_1__c;
                else 
                    extraParam = consolidationParam.Additional_Parameters_1__c;
            }
            if(consolidationParam.Additional_Parameters_2__c != null) {
                if(!String.isEmpty(extraParam)) 
                    extraParam = extraParam+','+consolidationParam.Additional_Parameters_2__c;
                else 
                    extraParam = consolidationParam.Additional_Parameters_2__c;
            }
            if(consolidationParam.Additional_Parameters_3__c != null) {
                if(!String.isEmpty(extraParam)) 
                    extraParam = extraParam+','+consolidationParam.Additional_Parameters_3__c;
                else 
                    extraParam = consolidationParam.Additional_Parameters_3__c;
            }
            if(consolidationParam.Additional_Parameters_4__c != null) {
                if(!String.isEmpty(extraParam)) 
                    extraParam = extraParam+','+consolidationParam.Additional_Parameters_4__c;
                else 
                    extraParam = consolidationParam.Additional_Parameters_4__c;
            }

            //Get details from Configuration/Assets and generate Account Interest Information data.
            intInfoList = getConfigDetails(agConfigMap, fieldMap, prodApiMAP, extraParam, assetList, qId, intInfoList);
            //For Enhancement Approvals  update proposal object
            quote =  APTPS_Enhancement_Approvals.updateEnhancementApprovals(quoteConfigId, fieldMap, extraParam, enhAggHrsMap, enhCRMap,quote);
            if (!APTS_ConstantUtil.APPROVAL_REQUIRED.equalsIgnoreCase(quote.Apttus_QPApprov__Approval_Status__c) && (quote.APTS_Require_AE_SalesVP_Approval__c || quote.APTPS_Requires_DDC_Approval__c || quote.APTS_Requires_VP_Sales_Ops_Approval__c || quote.APTS_Requires_Sales_Director_Approval__c || quote.APTPS_Requires_CA_Manager_Approval__c || quote.APTPS_Requires_Mgr_Receivables_Approval__c || quote.APTPS_Requires_BillingReceivablesManager__c || quote.APTPS_Requires_Dir_Of_Finance_Approval__c || quote.APTS_Require_AE_SalesVP_Approval__c || quote.APTS_Requires_RVP_Approval__c || quote.APTPS_Requires_Dir_of_Sales_Ops_Approval__c || quote.APTS_Ex_Dir_Sales_Mktng_Approval__c || quote.Requires_Legal_Approval__c || quote.APTPS_Requires_CFO_Approval__c || quote.APTPS_Requires_DRC_Approval__c || quote.APTPS_Requires_EDC_Approvals__c)) {
                    quote.Apttus_Proposal__Approval_Stage__c = APTS_ConstantUtil.APPROVAL_REQUIRED ;
                    quote.Apttus_QPApprov__Approval_Status__c = APTS_ConstantUtil.APPROVAL_REQUIRED ;
            }
            system.debug('quote --> '+quote);
            system.debug('intInfoList --> '+intInfoList);
            
            if(!intInfoList.isEmpty())
                insert intInfoList;
            if(quote != null) {
                update quote;
            }
            
        } else
            system.debug('Account Id is not available.');
    }
    
    /** 
    @description: Get Account Summary - Assets and Current configuration details
    @param: configMap - Agreement/Quote Proposal and respective Config Id Map
    @param: fieldMap - Product Code and related Attribute API Map
    @param: prodApiMAP - Product Code and related Attribute Label and API Map
    @param: extraParam - Extra Parameters to be queried
    @param: assetList - List of Assets for respective Account
    @param: qId - Quote/Proposal Id
    @param: intInfoList - List of Account Interest Information records
    @return: List of Account Interest Information records
    */
    public static List<APTPS_Account_Interest_Information__c> getConfigDetails(Map<Id, Id> configMap, Map<String, List<String>> fieldMap, Map<String, Map<String, String>> prodApiMAP, String extraParam, List<Apttus_Config2__AssetLineItem__c> assetList, Id qId, List<APTPS_Account_Interest_Information__c> intInfoList) {
        
        //Prepare Attribute List
        
        List<Id> configIds = configMap != null ? configMap.values() : null; 
        String fieldSet = '';
        Map<String, List<String>> prodAttrMap = new Map<String, List<String>>();
        Map<String, String> apiMap = new Map<String, String>();
        system.debug('apiMap --> '+apiMap);
        List<Apttus_Config2__LineItem__c> lineItems = new List<Apttus_Config2__LineItem__c>();
        lineItems = getConfigLI(configIds,fieldMap,extraParam);
        
        
        //Get Share and Lease data from Assets
        if(!assetList.isEmpty()) {
            for(Apttus_Config2__AssetLineItem__c assetLI : assetList) {
                if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(assetLI.Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c) 
                   || APTS_ConstantUtil.LEASE.equalsIgnoreCase(assetLI.Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c)) {
                       Integer seq = 3;
                       if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(assetLI.Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c))
                           seq = 3;
                       else if(APTS_ConstantUtil.LEASE.equalsIgnoreCase(assetLI.Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c))
                           seq = 4;
                       APTPS_Account_Interest_Information__c tempInt = generateInterestInfo(qId, assetLI.Id, seq, assetLI.Apttus_CMConfig__AgreementId__r.Deal_Description__c, false);
                       intInfoList.add(tempInt);
                   }
            }
        }
        
        //Get Enhancements details
        if(!lineItems.isEmpty()) {
            APTPS_Account_Interest_Information__c tempInt = new APTPS_Account_Interest_Information__c();
            tempInt.APTPS_Proposal__c = qId;
            tempInt.APTPS_Sequence__c = 1;
            tempInt.APTPS_Is_Enhancement_Bundle_Summary__c = true;
            String ds = '';
            String quoteEnhancements = '';
            for(Apttus_Config2__LineItem__c li : lineItems) {
                if((APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equalsIgnoreCase(li.Apttus_Config2__ProductId__r.ProductCode) || APTS_ConstantUtil.SNL_NJE_ENHANCEMENT.equalsIgnoreCase(li.Apttus_Config2__ProductId__r.ProductCode)) 
                   && APTS_ConstantUtil.OPTION.equalsIgnoreCase(li.Apttus_Config2__LineType__c)) {
                       if(APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(li.Apttus_Config2__ConfigurationId__r.Apttus_Config2__BusinessObjectType__c))
                           quoteEnhancements = generateSummary(quoteEnhancements, li, apiMap, prodApiMAP);
                       else
                           ds = generateSummary(ds, li, apiMap, prodApiMAP);
                   }
            }
            //Highlight Quote/Proposal Enhancements
            quoteEnhancements = !String.isEmpty(quoteEnhancements) ? '<span style="color:blue;">' + quoteEnhancements + '</span>' : quoteEnhancements;
            ds = quoteEnhancements + ds;
            tempInt.APTPS_Configuration_Summary__c = ds;
            intInfoList.add(tempInt);
        }
        
        system.debug('intInfoList, size --> '+intInfoList.size()+' Values --> '+intInfoList);
        return intInfoList;
    }
    
    /** 
    @description: Reusable code to read Field Set
    @param: fieldSet - Field Set Name
    @param: dsFields - List of APIs
    @return: List of APIs
    */
    public static List<String> getAttributes(String fieldSet, List<String> dsFields) {
        for(Schema.FieldSetMember fld : Schema.SObjectType.Apttus_QPConfig__ProposalProductAttributeValue__c.fieldSets.getMap().get(fieldSet).getFields()) {
            String fldName = 'Apttus_Config2__AttributeValueId__r.'+fld.getFieldPath();
            if(!dsFields.contains(fldName))
                dsFields.add(fldName);
        }
        return dsFields;
    }
    
    /** 
    @description: Reusable code to generate Account Interest Information record
    @param: qId - Quote/Proposal Id
    @param: aliId - Asset Line Item Id
    @param: seq - Sequence
    @param: ds - Summary
    @param: isCurrentTransaction - flag to represent if it's current configuration or Asset
    @return: Account Interest Information record/Object
    */
    public static APTPS_Account_Interest_Information__c generateInterestInfo(Id qId, Id aliId, Integer seq, String ds, Boolean isCurrentTransaction) {
        APTPS_Account_Interest_Information__c tempInt = new APTPS_Account_Interest_Information__c();
        tempInt.APTPS_Proposal__c = qId;
        tempInt.APTPS_Asset_Line_Item__c = aliId;
        tempInt.APTPS_Sequence__c = seq;
        ds = ds != null ? ds.replaceAll('<br><br>', '<br>') : ds;
        String formattedText = ds;
        formattedText = isCurrentTransaction ? '<span style="color:blue;">'+ds+'</span>' : ds;
        tempInt.APTPS_Configuration_Summary__c = formattedText;
        return tempInt;
    }
    
    /** 
    @description: Reusable code to generate Enhancement Summary
    @param: ds - Summary
    @param: li - Lie Item
    @return: Enhancement Summary
    */
    public static String generateSummary(String ds, Apttus_Config2__LineItem__c li, Map<String, String> apiMap, Map<String, Map<String, String>> prodApiMAP) {
        //First stamp Entitlement related details, then read details dynamically.
        ds += '<b>Enhancement Name: </b>'+li.Apttus_Config2__OptionId__r.Name+'<br/>';
        if('Account' == li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c) 
            ds += '<b>Account : </b>'+li.Apttus_Config2__AttributeValueId__r.Account__r.Name+'<br/>';
        else if(APTS_ConstantUtil.SNL_ENTITLEMENT_AGR.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) 
            ds += '<b>Selected Agreement : </b>'+li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreements__c+'<br/>';
        else 
            ds += '<b>Selected Quote : </b>'+li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Quotes__c+'<br/>';
        ds += '<b>Entitlement Level : </b>'+li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c+'<br/>';
        
        //Read all other attributes : Dynamic Way
        apiMap = (prodApiMAP != null && prodApiMAP.containsKey(li.Apttus_Config2__OptionId__r.ProductCode)) ? prodApiMAP.get(li.Apttus_Config2__OptionId__r.ProductCode) : null;
        if(apiMap != null) {
            for(String api : apiMap.keySet()) {
                Object obj = li.getSObject('Apttus_Config2__AttributeValueId__r').get(api);
                if(obj != null)
                    ds += '<b>'+apiMap.get(api)+' : </b>'+obj+'<br/>';
            }
        }
        ds +='<br/>';
        return ds;
    }
    
     /** 
    @description: Reusable code to get Config Line Items
    @param: configIds - Config Id Set
    @param: fieldMap - Product Code and related Attribute API Map
    @param: extraParam - Extra Parameters to be queried
    @return: List of Config LineItem records
    */
    public static List<Apttus_Config2__LineItem__c> getConfigLI(List<Id> configIds, Map<String, List<String>> fieldMap,String extraParam) {
        List<Apttus_Config2__LineItem__c> lineItems = new List<Apttus_Config2__LineItem__c>();
        String query = 'SELECT Id,Apttus_Config2__AttributeValueId__c';
        List<String> dsFields = new List<String>();
        String family = APTS_ConstantUtil.OPTION;
        List<String> enhCodes = new List<String>{APTS_ConstantUtil.SNL_NJA_ENHANCEMENT, APTS_ConstantUtil.SNL_NJE_ENHANCEMENT};
            //Form List of all Unique attributes
            for(String key : fieldMap.keySet()) {
                List<String> fields = fieldMap.get(key);
                for(String fld : fields) {
                    if(!dsFields.contains(fld))
                        dsFields.add(fld);
                }
            }
        system.debug('dsFields --> '+dsFields);
        String enhFields = dsFields != null ? String.join(dsFields,',') : null;
        query = !String.isEmpty(extraParam) ? query+','+extraParam : query;
        query = enhFields != null ? query+','+enhFields : query;
        query = query+' FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c IN :configIds AND Apttus_Config2__IsPrimaryLine__c = true AND Apttus_Config2__LineType__c =: family AND Apttus_Config2__ProductId__r.ProductCode IN :enhCodes';
        system.debug('Dynamic SOQL Query --> '+query);
        lineItems = Database.query(query);
        system.debug('Line Items result, list size --> '+lineItems.size()+' Actual records --> '+lineItems);
        return lineItems;
        
    }
    
    /** 
    @description: Code to get Largest Cabin Class Rank
    @param: crList - Cabin Class Ranks List
    @return: maxRank - Max Rank in the List
    */
    public static decimal getMaxRank(List<decimal> crList) {
        Decimal maxvalue = crList[0];
        for (integer i =0;i<crList.size();i++)
        {            
            if( crList[i] > maxvalue)
                maxvalue = crList[i];             
        }    
        return maxvalue;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<APTPS_Account_Interest_Information__c> getCurrentRecordId(Id qId, integer inputRefresh){
        List<APTPS_Account_Interest_Information__c> getAIIdetails =[SELECT Name, APTPS_Proposal__r.Name,APTPS_Proposal__r.Apttus_Proposal__Proposal_Name__c,
                                                                    APTPS_Asset_Line_Item__r.Name,APTPS_Account__c,
                                                                    APTPS_Asset_Line_Item__r.Apttus_CMConfig__AgreementId__r.Name,
                                                                    APTPS_Sequence__c,APTPS_Configuration_Summary__c,
                                                                    APTPS_Configuration_Summary_1__c 
                                                                    FROM APTPS_Account_Interest_Information__c
                                                                    WHERE APTPS_Proposal__c =: qId
                                                                    ORDER BY APTPS_Sequence__c asc];
        System.debug(getAIIdetails);
        return getAIIdetails;
    }
    
    @AuraEnabled(cacheable=true)
    public static string retrievePdf(Id qId){
        
        Attachment AttachedFile = [SELECT Id, Name, Body, ContentType 
                                   FROM Attachment 
                                   WHERE ParentId =: qId and Name LIKE '%AccountInformation%' limit 1];
        system.debug('Attached File -->'+AttachedFile);
        Blob pdfBlob = AttachedFile.Body;
        String base64Pdf = EncodingUtil.base64Encode(pdfBlob);
        return base64Pdf;
    }
    
    @AuraEnabled(cacheable=true) 
    public static APTPS_Account_Consolidation_Settings__c getCustomSettings(){
        return APTPS_Account_Consolidation_Settings__c.getOrgDefaults();
    }
}