/************************************************************************************************************************
@Name: APTPS_CreateConsolidationPdfController
@Author: Conga PS Dev Team
@CreateDate: 29 Dec 2021
@Description: VF Page controller for creating data on PDF.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
public class APTPS_CreateConsolidationPdfController {
    public id qId{get;set;}
    public List<APTPS_Account_Interest_Information__c> accIntInfo{get;set;}
    
    public APTPS_CreateConsolidationPdfController() {
        //Fetching the current Quote/Proposal Id
        this.qId = System.currentPageReference().getParameters().get('qId');
        system.debug('controller qId --> '+qId);
    }
    
    public PageReference init()
    {
        //Quering on the Account Interest Information Object to fetch details
        //Also used in VF page for creation on Account Interest details PDF
        accIntInfo =[SELECT Name, APTPS_Proposal__r.Name,
                     APTPS_Proposal__r.Apttus_Proposal__Proposal_Name__c,
                     APTPS_Asset_Line_Item__r.Name,APTPS_Account__c,
                     APTPS_Asset_Line_Item__r.Apttus_CMConfig__AgreementId__r.Name,
                     APTPS_Sequence__c,APTPS_Configuration_Summary__c,
                     APTPS_Configuration_Summary_1__c 
                     FROM APTPS_Account_Interest_Information__c
                     WHERE APTPS_Proposal__c =: this.qId
                     ORDER BY APTPS_Sequence__c asc];
        system.debug('accIntInfo --> '+accIntInfo.size()+' Details --> '+accIntInfo);
        return null;
    }
}