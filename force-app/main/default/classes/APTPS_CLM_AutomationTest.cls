/************************************************************************************************************************
@Name: APTPS_CLM_AutomationTest
@Author: Conga PS Dev Team
@CreateDate: 25 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

@isTest
public class APTPS_CLM_AutomationTest {
	@testSetup
    public static void makeData(){
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial';
		insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
        List<Product2> prodList = new List<Product2>();
        Product2 NJUS_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJUS_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJUS_DEMO);
        
        Product2 NJE_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJE_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJE_DEMO);
        
        Product2 NJE_CORP = APTS_CPQTestUtility.createProduct('Corporate Trial', 'NJE_Corporate_Trial', 'Apttus', 'Standalone', true, true, true, true);
        prodList.add(NJE_CORP);
        insert prodList;
        
        List<Apttus_Config2__PriceListItem__c> pList = new List<Apttus_Config2__PriceListItem__c>();
        Apttus_Config2__PriceListItem__c demoNJUSPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, NJUS_DEMO.Id, 0.0, 'Demo Price', 'One Time', 'Per Unit', 'Each', true);
        pList.add(demoNJUSPLI);
        Apttus_Config2__PriceListItem__c demoNJEPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, NJE_DEMO.Id, 0.0, 'Demo Price', 'One Time', 'Per Unit', 'Each', true);
        pList.add(demoNJEPLI);
        insert pList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = APTS_CPQTestUtility.createProposal('CLM Automation DEMO NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJUSQuote.APTPS_Program_Type__c = APTS_ConstantUtil.DEMO;
        demoNJUSQuote.Apttus_QPApprov__Approval_Status__c = 'Approval Required';
        proposalList.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = APTS_CPQTestUtility.createProposal('CLM Automation DEMO NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJEQuote.APTPS_Program_Type__c = APTS_ConstantUtil.DEMO;
        demoNJEQuote.Apttus_QPApprov__Approval_Status__c = 'Approval Required';
        proposalList.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corpNJEQuote = APTS_CPQTestUtility.createProposal('CLM Automation Corporate NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        corpNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        proposalList.add(corpNJEQuote);
        insert proposalList;
        
        List<Attachment> docList = new List<Attachment>();
        for(Apttus_Proposal__Proposal__c qObj : proposalList) {
            Attachment dummyAttachment = new Attachment();
            dummyAttachment.Name = 'Test Title';
            dummyAttachment.Body = Blob.valueOf('Test Body');
            dummyAttachment.ContentType = 'text/plain';
            dummyAttachment.ParentId = qObj.Id;
            docList.add(dummyAttachment);
        }
        insert docList;
        
        Id demoNJUSConfig = APTS_CPQTestUtility.createConfiguration(demoNJUSQuote.Id);
        Id demoNJEConfig = APTS_CPQTestUtility.createConfiguration(demoNJEQuote.Id);
        
        List<Apttus_Config2__LineItem__c> liList = new List<Apttus_Config2__LineItem__c>();
        Apttus_Config2__LineItem__c liDemoNJUS = APTS_CPQTestUtility.getLineItem(demoNJUSConfig, demoNJUSQuote, demoNJUSPLI, NJUS_DEMO, NJUS_DEMO.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        liList.add(liDemoNJUS);
        
        Apttus_Config2__LineItem__c liDemoNJE = APTS_CPQTestUtility.getLineItem(demoNJEConfig, demoNJEQuote, demoNJEPLI, NJE_DEMO, NJE_DEMO.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        liList.add(liDemoNJE);
        insert liList;
        
        //Create Quote/Proposal Line Items
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c demoUS = APTS_CPQTestUtility.getPropLI(NJUS_DEMO.Id, 'New', demoNJUSQuote.Id, '', null);
        demoUS.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoUS.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoUS.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoUS);
        
        Apttus_Proposal__Proposal_Line_Item__c demoNJE = APTS_CPQTestUtility.getPropLI(NJE_DEMO.Id, 'New', demoNJEQuote.Id, '', null);
        demoNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoNJE.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoNJE);
        insert pliList;
        
        //Create Proposal Attribute Value
        List<Apttus_QPConfig__ProposalProductAttributeValue__c> pavList = new List<Apttus_QPConfig__ProposalProductAttributeValue__c>();
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJUSPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJUSPav.Apttus_QPConfig__LineItemId__c = demoUS.Id;
        demoNJUSPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJUSPav.APTPS_Demo_Aircraft__c = 'Citation XLS';
        demoNJUSPav.APTPS_Rate_Type__c = 'Non-Premium';
        pavList.add(demoNJUSPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJEPav.Apttus_QPConfig__LineItemId__c = demoNJE.Id;
        demoNJEPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJEPav.APTPS_Demo_Aircraft__c = 'Phenom 300';
        demoNJEPav.APTPS_Rate_Type__c = 'Non-Premium';
        pavList.add(demoNJEPav);
        insert pavList;
        
        //Corporate NJE Agreement lifecycle test data
        Apttus__APTS_Agreement__c ctAg = new Apttus__APTS_Agreement__c();
        ctAg.Name = 'CLM Automation Corporate NJE Agreement';
        ctAg.Agreement_Status__c = 'Draft';
        ctAg.Apttus__Account__c = accToInsert.Id;
        ctAg.Apttus_QPComply__RelatedProposalId__c = corpNJEQuote.Id;
        ctAg.APTPS_Program_Type__c = APTS_ConstantUtil.CORP_TRIAL;
        ctAg.Apttus__Status__c = 'Request';
        ctAg.Apttus__Status_Category__c = 'Request';
        //ctAg.CurrencyIsoCode = 'EUR';
        ctAg.APTPS_Term_Months__c = String.valueOf(6);
        ctAg.APTPS_Term_End_Date__c = Date.today();
        insert ctAg;

        Test.stopTest();
    }
    
    @isTest
    public static void testCLMAutomation(){
        Test.startTest();
        List<Apttus_Proposal__Proposal__c> quotesToUpdate = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = [SELECT Id, Apttus_QPApprov__Approval_Status__c FROM Apttus_Proposal__Proposal__c 
                                                      WHERE Apttus_Proposal__Proposal_Name__c = 'CLM Automation DEMO NJUS Quote' LIMIT 1];
        demoNJUSQuote.Apttus_QPApprov__Approval_Status__c = 'Approved';
        quotesToUpdate.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = [SELECT Id, Apttus_QPApprov__Approval_Status__c FROM Apttus_Proposal__Proposal__c 
                                                     WHERE Apttus_Proposal__Proposal_Name__c = 'CLM Automation DEMO NJE Quote' LIMIT 1];
        demoNJEQuote.Apttus_QPApprov__Approval_Status__c = 'Approved';
        quotesToUpdate.add(demoNJEQuote);
        //quotesToUpdate.add(corporateNJEQuote);
        update quotesToUpdate;
        
        List<Apttus__APTS_Agreement__c> agList = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c agNJUS = new Apttus__APTS_Agreement__c();
        agNJUS.APTPS_Program_Type__c = APTS_ConstantUtil.DEMO;
        //agNJUS.CurrencyIsoCode = 'USD';
        agNJUS.Apttus_QPComply__RelatedProposalId__c = demoNJUSQuote.Id;
        agList.add(agNJUS);
        
        Apttus__APTS_Agreement__c agNJE = new Apttus__APTS_Agreement__c();
        agNJE.APTPS_Program_Type__c = APTS_ConstantUtil.DEMO;
        //agNJE.CurrencyIsoCode = 'EUR';
        agNJE.Apttus_QPComply__RelatedProposalId__c = demoNJEQuote.Id;
        agList.add(agNJE);
        
        insert agList;
        
        //Corporate NJE Agreement lifecycle testing
        Apttus__APTS_Agreement__c ctAg = [SELECT Id, Apttus__Status__c, Apttus__Status_Category__c FROM Apttus__APTS_Agreement__c 
                                         WHERE APTPS_Program_Type__c =:APTS_ConstantUtil.CORP_TRIAL LIMIT 1];
        ctAg.Apttus__Status__c = 'Ready for Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Other Party Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        Payment_Entry__c payObj = new Payment_Entry__c();
        payObj.Agreement__c = ctAg.Id;
        //payObj.CurrencyIsoCode = 'EUR';
        payObj.Payment_Amount__c = 1000;
        payObj.Payment_Date__c = Date.today();
        payObj.Payment_Method__c = 'Wire';
        insert payObj;
        
        ctAg.Apttus__Status__c = 'Other Party Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Fully Signed';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Activated';
        ctAg.Apttus__Status_Category__c = 'In Effect';
        //End_Date_Check validation rule on Agreement is not Active but still, it's throwing validation error at the time of package creation.
        //Just to avoid this error, these 2 fields are added to the test data.
        ctAg.Apttus__Contract_End_Date__c = Date.today();
        ctAg.Apttus__Term_Months__c = 1;
        update ctAg;
        
        //START: GCM-11585 Code coverage
        ctAg.Apttus__Status__c = APTS_ConstantUtil.READY_FOR_SIGN;
        ctAg.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
        ctAg.Document_Status__c = APTS_ConstantUtil.DOC_GENERATED;
        delete payObj;
        update ctAg;
        
        Payment_Entry__c payObj1 = new Payment_Entry__c();
        payObj1.Agreement__c = ctAg.Id;
        //payObj1.CurrencyIsoCode = 'EUR';
        payObj1.Payment_Amount__c = 1000;
        payObj1.Payment_Date__c = Date.today();
        payObj1.Payment_Method__c = 'Wire';
        insert payObj1;
        //END: GCM-11585
        
        //Added coverage for APTPS_CLMLifeCycle_Demo class
        Apttus__APTS_Agreement__c demoAgActivated = [SELECT Id, Apttus__Status__c, Apttus__Status_Category__c, Agreement_Status__c, Chevron_Status__c, Funding_Status__c, Document_Status__c 
                                                    FROM Apttus__APTS_Agreement__c WHERE ID =: agNJUS.Id];
        demoAgActivated.Apttus__Status__c = APTS_ConstantUtil.FULLY_SIGNED;
        demoAgActivated.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
        update demoAgActivated;
        Apttus__APTS_Agreement__c resultAg = APTPS_CLMLifeCycle_Demo.setAgreementStatus(demoAgActivated, APTS_ConstantUtil.STATUS_ACTIVE, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_GENERATED);
        system.assert(resultAg!=null);
        demoAgActivated.Apttus__Status__c = APTS_ConstantUtil.ACTIVATED;
        demoAgActivated.Apttus__Status_Category__c = APTS_ConstantUtil.IN_EFFECT;
          //End_Date_Check validation rule on Agreement is not Active but still, it's throwing validation error at the time of package creation.
        //Just to avoid this error, these 2 fields are added to the test data.
        demoAgActivated.Apttus__Contract_End_Date__c = Date.today();
        demoAgActivated.Apttus__Term_Months__c = 1;
        update demoAgActivated;
        Test.stopTest();
    }
}