/****************************************************************************************************************
********************** Description Section **********************************************************************
@Name			: APTS_MassUpdateLegalEntityController
@Author			: Avinash Bamane
@CreateDate		: 16 January 2020
@Description	: Controller class to update Legal Entity/Location
*****************************************************************************************************************
*******************  Version Control Section  *******************************************************************
@ModifiedBy			: 
@ModifiedDate		: 
@ChangeDescription	: 
****************************************************************************************************************/

public class APTS_MassUpdateLegalEntityController {
    private String configID;
    private String proposalID;
    private String retId;
    private String configRequestId;
    public List<SelectOption> accLocationOptions {get;set;}
    public Id selectedAccLocation {get;set;}
    List<Apttus_Config2__AccountLocation__c> accLegalEntity = new List<Apttus_Config2__AccountLocation__c>();
    private String programType;
    public APTS_MassUpdateLegalEntityController(ApexPages.StandardController controller) {
        configID = apexpages.currentpage().getparameters().get('id');
        proposalID = apexpages.currentpage().getparameters().get('businessObjectId');
        retId = apexpages.currentpage().getparameters().get('retId');
        configRequestId = apexpages.currentpage().getparameters().get('configRequestId');
        if(!String.isEmpty(proposalID)) {
            accLegalEntity = [SELECT Id, Name 
                              FROM Apttus_Config2__AccountLocation__c 
                              WHERE Apttus_Config2__AccountId__c in 
                              (SELECT Apttus_Proposal__Account__c 
                               FROM Apttus_Proposal__Proposal__c 
                               WHERE Id = :proposalID)];
            accLocationOptions = new List<SelectOption>();
            for(Apttus_Config2__AccountLocation__c accLocation : accLegalEntity){
                accLocationOptions.add(new SelectOption(accLocation.Id, accLocation.Name));
            }
            
            //Get Program Type from the proposal
            //TODO: Try out action parameter,but in that case may need to update Card flow, verify
            Apttus_Proposal__Proposal__c qObj = [SELECT APTPS_Program_Type__c FROM Apttus_Proposal__Proposal__c WHERE Id =: proposalID LIMIT 1];
            programType = qObj.APTPS_Program_Type__c;
        }
    }
    
    public PageReference updateLegalEntity() {
        if(!String.isEmpty(configID)) {
            List<Apttus_Config2__LineItem__c> lineItems = [SELECT Id, Apttus_Config2__LocationId__c 
                                                           FROM Apttus_Config2__LineItem__c 
                                                           WHERE Apttus_Config2__ConfigurationId__c = :configID];
            if(!lineItems.isEmpty()) {
                for(Apttus_Config2__LineItem__c li : lineItems) {
                    li.Apttus_Config2__LocationId__c = selectedAccLocation;
                }
                
                try {
                    update lineItems;
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.APTS_MassUpdateSuccessMessage));
                } catch(DmlException e) {
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.APTS_MassUpdateErrorMessage));
                    system.debug('DML Exception ---> '+e);
                }
            }
        }
        return null;
    }
    
    public PageReference returnToCartPage() {
        PageReference cartDetailPage;
        cartDetailPage = System.Page.Apttus_Config2__Cart;
        cartDetailPage.getParameters().put('Id', configID);
        cartDetailPage.getParameters().put('configRequestId', configRequestId);
        cartDetailPage.getParameters().put('cartStatus', 'New');
        if(APTPS_SNL_Util.isSNLProgramType(programType))
            cartDetailPage.getParameters().put('flow','SNL_RelatedAssignment');
        else
            cartDetailPage.getParameters().put('flow','ModificationFlow');
        cartDetailPage.getParameters().put('launchState','cartgrid');
        return cartDetailPage;
    }
}