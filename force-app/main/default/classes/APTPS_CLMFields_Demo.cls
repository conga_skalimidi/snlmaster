/************************************************************************************************************************
@Name: APTPS_CLMFields_Demo
@Author: Conga PS Dev Team
@CreateDate: 28 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMFields_Demo {
    
    /** 
    @description: ppoulate Agreeement Fields
    @param: AgreementList and Currency Code
    @return: 
    */
    private static void popualateDemoFields(List<Apttus__APTS_Agreement__c> agrList, String currencyCode) {
        set<Id> proposalIds = new set<Id>();
        set<Id> agrIds = new set<Id>();
        Map<Id,Apttus_Proposal__Proposal__c> proposalMap = new Map<Id,Apttus_Proposal__Proposal__c>();
        for(Apttus__APTS_Agreement__c agr : agrList) {
                proposalIds.add(agr.Apttus_QPComply__RelatedProposalId__c);
                agrIds.add(agr.Id);
            }
            for(Apttus_Proposal__Proposal__c pObj : [SELECT id, Name,Approval_Primary_AE_User__c, Approval_Primary_SE_User__c, Approval_Sales_Director_User__c, Approval_RVP_User__c, APTPS_Approval_Secondary_SE_User__c,Legal_Entity_Name__c,Product_Line__c,Aircraft_Types__c,Apttus_QPConfig__LocationId__c FROM Apttus_Proposal__Proposal__c WHERE Id IN :proposalIds]) {
                proposalMap.put(pObj.id,pObj);
            }
             Id demoRecTypeId;
             if(currencyCode == APTS_ConstantUtil.CUR_EUR ) {
                demoRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.DEMO_NJE_RECORD_TYPE).getRecordTypeId(); 
                
             } else if(currencyCode == APTS_ConstantUtil.CUR_USD ) {
                demoRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.DEMO_NJA_RECORD_TYPE).getRecordTypeId(); 
             }
             
            for(Apttus__APTS_Agreement__c agObj : agrList) {
                  agObj.Agreement_Status__c = APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(agObj.CurrencyIsoCode) ? APTS_ConstantUtil.STATUS_ACTIVE: APTS_ConstantUtil.PENDING_ACTIVATE ;
                    agObj.Chevron_Status__c = APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(agObj.CurrencyIsoCode) ? APTS_ConstantUtil.STATUS_ACTIVE : APTS_ConstantUtil.PENDING_ACTIVATE;
                    agObj.APTS_Path_Chevron_Status__c = APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(agObj.CurrencyIsoCode) ? APTS_ConstantUtil.STATUS_ACTIVE : APTS_ConstantUtil.PENDING_ACTIVATE;
                    //agObj.Apttus__Status_Category__c = APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(agObj.CurrencyIsoCode) ? APTS_ConstantUtil.IN_EFFECT  : APTS_ConstantUtil.IN_SIGN ;
                    //agObj.Apttus__Status__c = APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(agObj.CurrencyIsoCode) ? APTS_ConstantUtil.ACTIVATED  : APTS_ConstantUtil.FULLY_SIGNED;
                    agObj.Apttus__Status_Category__c =  APTS_ConstantUtil.IN_SIGN ;
                    agObj.Apttus__Status__c = APTS_ConstantUtil.FULLY_SIGNED;
                    agObj.Funding_Status__c = APTS_ConstantUtil.FUNDED_NOTREQUIRED ;
                    agObj.Document_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                    agObj.RecordTypeId = demoRecTypeId;
                    //GCM-11175: For Demo, Asset line items, Order should not be there.
                    agObj.Apttus_CMConfig__AutoActivateOrder__c = false;
                	//START: GCM-12118 Set Accounting Compnay as NJA for Demo NJA Agreements.
                	if(APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(agObj.CurrencyIsoCode))
                		agObj.Accounting_Company__c = 'NJA';
                	//END: GCM-12118
                    agObj.Generate_Card_Number__c = true;
                    if(!proposalMap.isEmpty()){
                        agObj.Account_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Primary_AE_User__c;
                        agObj.Primary_Sales_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Primary_SE_User__c;
                        agObj.Sales_Consultant__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Sales_Director_User__c;
                        agObj.RVP__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_RVP_User__c;
                        agObj.Secondary_Sales_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_Approval_Secondary_SE_User__c;
                         String agName = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Legal_Entity_Name__c+', '+proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Product_Line__c+' - '+proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Aircraft_Types__c;
                        agObj.Name = agName;
                        agObj.Document_Name__c = agName;
                        agObj.Apttus_CMConfig__LocationId__c =  proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__LocationId__c ;
                        
                    }
            }
            system.debug('agrList-->'+agrList);
    }
    
    /** 
    @description: NJE Demo implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
             APTPS_CLMFields_Demo.popualateDemoFields(agrList,APTS_ConstantUtil.CUR_EUR);
            
                
            
        }
    }
    
    /** 
    @description: NJA Demo implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
             APTPS_CLMFields_Demo.popualateDemoFields(agrList,APTS_ConstantUtil.CUR_USD);
            
                
            
        }
    }
}