/************************************************************************************************************************
@Name: APTPS_PAVTriggerHelper
@Author: Conga PS Dev Team
@CreateDate: 27 JAN 2022
@Description: Product Attribute Value Trigger Helper
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_PAVTriggerHelper {
    /**
    @description: Reset SNL Entitlement Filter attributes
    @param: liAttrMap - Map of Line Item Id and respective Product Attribute Value
    @return: void
    */
    public static void resetSNLEntitlementFilterAttributes(Map<Id, Apttus_Config2__ProductAttributeValue__c> liAttrMap) {
        for(Apttus_Config2__LineItem__c li : [SELECT Id, Apttus_Config2__ConfigurationId__r.Apttus_Config2__Status__c, 
                                              Apttus_Config2__ConfigurationId__r.Apttus_Config2__BusinessObjectType__c, 
                                              Apttus_Config2__ConfigurationId__r.APTPS_Program_Type__c, Apttus_Config2__LineStatus__c, 
                                              Apttus_Config2__ConfigurationId__r.APTS_Modification_Type__c, Apttus_Config2__ConfigurationId__r.APTS_Additional_Modification_Type__c 
                                              FROM Apttus_Config2__LineItem__c 
                                              WHERE ID IN :liAttrMap.keySet()]) {
                                                  //START: When we clone option, Bundle Name, Modification Type and Additional Modification Type is not getting copied
                                                  Apttus_Config2__ProductAttributeValue__c newAttr = liAttrMap.get(li.Id);
                                                  newAttr.APTPS_Bundle_Name__c = li.Apttus_Config2__ConfigurationId__r.APTPS_Program_Type__c;
                                                  newAttr.APTS_Modification_Type__c = li.Apttus_Config2__ConfigurationId__r.APTS_Modification_Type__c;
                                                  newAttr.APTS_Additional_Modification_Type__c = li.Apttus_Config2__ConfigurationId__r.APTS_Additional_Modification_Type__c;
                                                  
                                                  //SNL Quote/Proposal + Saved or New Config Status + Business Object Type is Proposal + New Line Status
                                                  if(APTPS_SNL_Util.isSNLProgramType(li.Apttus_Config2__ConfigurationId__r.APTPS_Program_Type__c) 
                                                     && (APTS_ConstantUtil.CONFIG_SAVE_STATUS.equalsIgnoreCase(li.Apttus_Config2__ConfigurationId__r.Apttus_Config2__Status__c) 
                                                         || APTS_ConstantUtil.STATUS_NEW.equalsIgnoreCase(li.Apttus_Config2__ConfigurationId__r.Apttus_Config2__Status__c)) 
                                                     && APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(li.Apttus_Config2__ConfigurationId__r.Apttus_Config2__BusinessObjectType__c) 
                                                     && APTS_ConstantUtil.STATUS_NEW.equalsIgnoreCase(li.Apttus_Config2__LineStatus__c)) {
                                                         system.debug('Reset Entitlement Filter Attributes.');
                                                         Apttus_Config2__ProductAttributeValue__c attr = liAttrMap.get(li.Id);
                                                         //Reset Quote/Proposal Filter attributes
                                                         attr.APTPS_Quote_Proposal__c = null;
                                                         attr.APTPS_Selected_Quotes__c = '';
                                                         attr.APTPS_Selected_Quote_Ids__c = '';
                                                         attr.APTPS_Override_Quote_Proposal_Selection__c = true;
                                                         
                                                         //Reset Agreement Filter attributes
                                                         attr.APTPS_Agreement__c = null;
                                                         attr.APTPS_Selected_Agreements__c = '';
                                                         attr.APTPS_Selected_Agreement_Ids__c = '';
                                                         attr.APTPS_Override_Agreement_Selection__c = true;
                                                         attr.APTPS_Is_Generated_By_Clone_API__c = false;
                                                     }
                                              }
    }
}