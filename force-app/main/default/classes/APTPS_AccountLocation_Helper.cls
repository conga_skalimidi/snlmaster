/*************************************************************
@Name: APTPS_AccountLocation_Helper
@Author: Siva Kumar
@CreateDate: 21 October 2020
@Description : This class is used to prevent Duplicate values for Account Location
******************************************************************
@ModifiedBy: Siva Kumar
@ModifiedDate: 29 November 2021
@ChangeDescription: Added method to update Corporate Trust Fields
*******************************************************************/
public class APTPS_AccountLocation_Helper {
    /** 
    @description: prevent Duplicate values for Account Location
    @param: accountLocationList
    @return: 
    */
    public static void AvoidDuplicate(List<Apttus_Config2__AccountLocation__c> accountLocationList,Map<id,Apttus_Config2__AccountLocation__c> oldMap){
        set<string> newNameSet = new set<string>();
        set<string> newFullNameSet = new set<string>();
        set<string> newAccountIdSet = new set<string>();
        set<APTPS_AccountLocation_Wrapper> dbWrapperSet = new set<APTPS_AccountLocation_Wrapper>();
        set<APTPS_AccountLocation_Wrapper> newWrapperSet = new set<APTPS_AccountLocation_Wrapper>();
        for(Apttus_Config2__AccountLocation__c newLE : accountLocationList){
            if((oldMap ==null && ((newLE.Name!=null || newLE.Full_Legal_Name__c!=null) && newLE.Apttus_Config2__AccountId__c!=null)) ||(oldMap !=null && (newLE.Name!=oldMap.get(newLE.Id).Name || newLE.Full_Legal_Name__c!=oldMap.get(newLE.Id).Full_Legal_Name__c || newLE.Apttus_Config2__AccountId__c!=oldMap.get(newLE.Id).Apttus_Config2__AccountId__c))) {
                APTPS_AccountLocation_Wrapper iKey = new APTPS_AccountLocation_Wrapper(newLE.Name, newLE.Full_Legal_Name__c, newLE.Apttus_Config2__AccountId__c);
            
                
                if(newWrapperSet.contains(iKey)){
                    newLE.addError('Duplicate in new list');
                }else{
                    newNameSet.add(newLE.Name);
                    newFullNameSet.add(newLE.Full_Legal_Name__c);
                    newAccountIdSet.add(newLE.Apttus_Config2__AccountId__c);
                    newWrapperSet.add(iKey);
                }
            }
        }

        for(Apttus_Config2__AccountLocation__c dbLE : [select id, Name, Full_Legal_Name__c, Apttus_Config2__AccountId__c from Apttus_Config2__AccountLocation__c where (Name IN:newNameSet OR Full_Legal_Name__c IN:newFullNameSet) AND Apttus_Config2__AccountId__c IN:newAccountIdSet]){
            dbWrapperSet.add(new APTPS_AccountLocation_Wrapper(dbLE.Name, dbLE.Full_Legal_Name__c, dbLE.Apttus_Config2__AccountId__c));
        }
        system.debug('dbWrapperSet-->'+dbWrapperSet);
        for(Apttus_Config2__AccountLocation__c newALE : accountLocationList){
            APTPS_AccountLocation_Wrapper iKey = new APTPS_AccountLocation_Wrapper(newALE.Name, newALE.Full_Legal_Name__c, newALE.Apttus_Config2__AccountId__c);
            if(dbWrapperSet.contains(iKey)){
                newALE.addError('Duplicate Account Location found');
            }

            
        }

    }  
    /** 
    @description: prevent Duplicate values for Account Location
    @param: accountLocationList
    @return: 
    */
    public static void updateCTFields(List<Apttus_Config2__AccountLocation__c> accountLocationList,Map<id,Apttus_Config2__AccountLocation__c> oldMap){
        Map<id,Apttus_Config2__AccountLocation__c> ctMap = new Map<id,Apttus_Config2__AccountLocation__c>();
        for(Apttus_Config2__AccountLocation__c newLE : accountLocationList){
            if(newLE.APTPS_Corporate_Trust__c != null) {
                ctMap.put(newLE.APTPS_Corporate_Trust__c,newLE);
            }
        }
        if(!ctMap.isEmpty()){
            for(APTPS_Corporate_Trust__c ct : [SELECT id,APTPS_Amended__c,APTPS_Contact_Address_1__c,APTPS_Contact_Address_2__c,APTPS_Contact_City_State_Zip__c,APTPS_Contact_Email__c,APTPS_Contact_Fax__c,APTPS_Contact_First_Name__c,APTPS_Contact_Gender__c,APTPS_Contact_Last_Name__c,APTPS_Contact_Phone__c,APTPS_Owner_Entity_Name__c,APTPS_Owner_Trust_Entity_Name__c,APTPS_Owner_Trust_Short_Name__c,APTPS_State__c,APTPS_Sub_Entity_Name__c FROM APTPS_Corporate_Trust__c WHERE id IN:ctMap.keySet()]) {
                if(ctMap.containsKey(ct.id)) {
                    Apttus_Config2__AccountLocation__c updateAccLoc = ctMap.get(ct.id);
                    updateAccLoc.APTPS_CT_Amended__c = ct.APTPS_Amended__c;
                    updateAccLoc.APTPS_CT_Contact_Address_1__c = ct.APTPS_Contact_Address_1__c;
                    updateAccLoc.APTPS_CT_Contact_Address_2__c = ct.APTPS_Contact_Address_2__c;
                    updateAccLoc.APTPS_CT_Contact_City_State_Zip__c = ct.APTPS_Contact_City_State_Zip__c;
                    updateAccLoc.APTPS_CT_Contact_Email__c = ct.APTPS_Contact_Email__c;
                    updateAccLoc.APTPS_CT_Contact_Fax__c = ct.APTPS_Contact_Fax__c;
                    updateAccLoc.APTPS_CT_Contact_First_Name__c = ct.APTPS_Contact_First_Name__c;
                    updateAccLoc.APTPS_CT_Contact_Gender__c = ct.APTPS_Contact_Gender__c;
                    updateAccLoc.APTPS_CT_Contact_Last_Name__c = ct.APTPS_Contact_Last_Name__c;
                    updateAccLoc.APTPS_CT_Contact_Phone__c = ct.APTPS_Contact_Phone__c;
                    updateAccLoc.APTPS_CT_Owner_Entity_Name__c = ct.APTPS_Owner_Entity_Name__c;
                    updateAccLoc.APTPS_CT_Owner_Trust_Entity_Name__c = ct.APTPS_Owner_Trust_Entity_Name__c;
                    updateAccLoc.APTPS_CT_Owner_Trust_Short_Name__c = ct.APTPS_Owner_Trust_Short_Name__c;
                    updateAccLoc.APTPS_CT_State__c = ct.APTPS_State__c;
                    updateAccLoc.APTPS_CT_Sub_Entity_Name__c = ct.APTPS_Sub_Entity_Name__c;
                }
            }
        }
    }
   
    
}