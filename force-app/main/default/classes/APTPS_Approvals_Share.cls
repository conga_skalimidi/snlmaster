/************************************************************************************************************************
@Name: APTPS_Approvals_Share
@Author: Conga PS Dev Team
@CreateDate: 10 October 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_Approvals_Share implements APTPS_ApprovalUsersInterface_V2{    
    
   //set EDC Legal flag for letter agreement modification approval      
   public void checkLegalApprovedRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}

   //set HCS flag for letter agreement modification approval        
   public void checkHeadofCommercialSalesApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
   public void checkDDCApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
       for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_ConstantUtil.NJA_SHARE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) && Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) == 25) {                        
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //proposalObj.APTPS_Hours__c = Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                
                break;                                        
            }
            if (APTS_ConstantUtil.NJA_SHARE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null && (Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 6)) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;
            }
            if (APTS_ConstantUtil.NJA_SHARE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && (proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c != 60)) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //proposalObj.APTPS_Contract_Length__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c);
                break;
            }
        }
        if(proposalObj.APTPS_Requires_DDC_Approval__c){
               proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = false;
               proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
           }
   }
    //set flag for Letter Agreement Extension 
   public void checkCAManagerApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
    
    public void checkSalesOpsApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Share NJA START*/
            if (APTS_Constants.NJUS_SHARE .equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null &&(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 3) && (Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <=6) ) {                        
                proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c  = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;                                        
            }
            /*For Share NJA END*/
           if(proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c ){
               proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
           }

        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkSalesDirectorApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Share NJA START*/
            if (APTS_Constants.NJUS_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null &&(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <= 3) ) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;                                        
            }
            if (APTS_Constants.NJUS_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c != null ) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Date__c = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c;
                break;                                        
            }
            if (APTS_ConstantUtil.NJA_SHARE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) && Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 25) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Hours__c = Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                break;                                        
            }
            /*For Share NJA END*/
           

        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkDemoTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    public void checkDirectorARApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJUS_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_Constants.DISCOUNT_AMOUNT.equals(proposalLineItem.Apttus_QPConfig__AdjustmentType__c)&& proposalLineItem.Apttus_QPConfig__AdjustmentAmount__c > 0) {                        
                proposalObj.APTPS_Requires_Director_AR_Approval__c = true;
                //proposalObj.APTPS_Operating_Fund_Discount_Applied__c = true;
                break;                                        
            }
        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    //set RVP flag for letter agreement modification approval
    public void checkRVPApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) { }
    
    public void checkSalesOpsTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    public void checkAssigmentApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        if(APTS_ConstantUtil.ASSIGNMENT.equals(proposalObj.APTS_Modification_Type__c)){
            APTPS_SNL_Util.updateAssignmentApprovals(proposalObj,quoteLines);           
        }
    }
    
    public void checkExtensionApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        if(APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equals(proposalObj.APTS_Modification_Type__c)){
            APTPS_SNL_Util.updateExtensionApprovals(proposalObj,quoteLines);            
        }
    }
    
    public void checkDefermentApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        system.debug('checkDefermentApprovalRequired method==>'+proposalObj.APTS_Modification_Type__c);
        if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equals(proposalObj.APTS_Modification_Type__c)){
            APTPS_SNL_Util.updateDefermentApprovals(proposalObj,quoteLines);            
        }
    }
    
    public void checkRepurchaseApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        if(APTS_ConstantUtil.REPURCHASE.equals(proposalObj.APTS_Modification_Type__c)){
            for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {
                if(APTS_ConstantUtil.LINE_TYPE.equals(proposalLineItem.Apttus_QPConfig__LineType__c) && APTS_ConstantUtil.REPURCHASE_CT.equals(proposalLineItem.Apttus_QPConfig__ChargeType__c)) {
                    decimal percentageDifference = APTPS_SNL_Util.caluculatePercentageDiff(proposalLineItem.Apttus_QPConfig__OptionPrice__c,proposalLineItem.Apttus_QPConfig__AdjustedPrice__c);
                    system.debug('percentageDifference-->'+percentageDifference);
                    if(percentageDifference > 0 && percentageDifference <= 3) {
                       if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                            proposalObj.Requires_Legal_Approval__c = true;  
                            break;
                        } else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                            proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;  
                            break;
                        }
                        
                    }
                    if(percentageDifference > 0 && percentageDifference > 3) {
                        if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                            proposalObj.APTPS_Requires_DDC_Approval__c = true;  
                            break;
                        } else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                            proposalObj.APTPS_Requires_EDC_Approvals__c = true;  
                            break;
                        }
                        
                    }
                }
                if(proposalLineItem.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__LineType__c) && (APTS_ConstantUtil.NJUS_WAIVE_REMARKETING_FEE.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) || APTS_ConstantUtil.NJE_WAIVE_REMARKETING_FEE.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode))) {
                    proposalObj.APTPS_Requires_DDC_Approval__c = true;  
                        break;
                }
            }
                
        }
    }
    //GCM-7320 Reduction Approvals
    public void checkReductionApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        if(APTS_ConstantUtil.REDUCTION.equals(proposalObj.APTS_Modification_Type__c)){
            for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {
                if(APTS_ConstantUtil.LINE_TYPE.equals(proposalLineItem.Apttus_QPConfig__LineType__c) && APTS_ConstantUtil.REPURCHASE_CT.equals(proposalLineItem.Apttus_QPConfig__ChargeType__c)) {
                    decimal percentageDifference = APTPS_SNL_Util.caluculatePercentageDiff(proposalLineItem.Apttus_QPConfig__OptionPrice__c,proposalLineItem.Apttus_QPConfig__AdjustedPrice__c);
                    system.debug('percentageDifference-->'+percentageDifference);
                    if(percentageDifference > 0 && percentageDifference <= 3) {
                        proposalObj.Requires_Legal_Approval__c = true;  
                        break;
                    }
                    if(percentageDifference > 0 && percentageDifference > 3) {
                        proposalObj.APTPS_Requires_DDC_Approval__c = true;  
                        break;
                    }
                }
                if(proposalLineItem.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__LineType__c) && (APTS_ConstantUtil.NJUS_WAIVE_REMARKETING_FEE.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) || APTS_ConstantUtil.NJE_WAIVE_REMARKETING_FEE.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode))) {
                    proposalObj.APTPS_Requires_DDC_Approval__c = true;  
                        break;
                }
            }
                
        }
    }
    
    //GCM-7328 Size Increase Approvals
    public void checkSizeIncreaseApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        if(APTS_ConstantUtil.SHARE_SIZE_INCREASE.equals(proposalObj.APTS_Modification_Type__c) && APTS_ConstantUtil.NONE.equals(proposalObj.APTS_Additional_Modification_Type__c)){
            for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {
                 if(proposalLineItem.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode))) {
                        proposalObj.APTPS_Requires_DDC_Approval__c = true;  
                        break;
                }
            }
                
        }
    }
    //GCM-12253 Share NJE Approvals
    public void checkNJEApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        String  vintage = null;
        String  acName = null;
        Map<String,APTPS_Standard_Purchase_Price_Incentives__mdt> ppiData = APTPS_Standard_Purchase_Price_Incentives__mdt.getAll();
        for (Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
               && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                    vintage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Vintage__c;
                    acName = pli.Apttus_QPConfig__OptionId__r.ProductCode;                                     
               }
        } 
        system.debug('vintage==>'+vintage);
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {
            /*Purchase Price Incentive Approvals START*/
            if (APTS_ConstantUtil.NJE_SHARE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode)) {                        
                if(vintage != null) {
                    Integer ac_vintage = Integer.valueOf(vintage);
                    Integer currentYear = System.Today().year();
                    system.debug('ac_vintage-->'+ac_vintage+' currentYear-->'+currentYear);
                    if(currentYear - ac_vintage >= 2 ) {
                        proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                        break;
                    }
                }
                if(acName != null) {
                    decimal discount = 0;
                    for(String ppi: ppiData.keySet()){
                        if(ppiData.get(ppi).Aircraft_Product_code__c == acName){
                            system.debug('discount -->'+ppiData.get(ppi).Discount_Amount__c);
                            discount = ppiData.get(ppi).Discount_Amount__c;  
                            break;
                        }
                    }
                    system.debug('discount==>'+discount);
                    if(discount == 0) {
                        proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;
                        break;
                    } else {
                        Integer hrs = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                        decimal applicableDiscount = (discount * hrs)/800 ;
                        decimal incentiveAmount = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Incentive_Amount__c;
                        system.debug('applicableDiscount==>'+applicableDiscount+' incentiveAmount-->'+incentiveAmount);
                        if(incentiveAmount > applicableDiscount) {
                            proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;
                            break;
                        }                   
                        
                    }                   
                    
                }                                                    
            }
            /*Purchase Price Incentive Approvals END*/
            /*Operating Fund Exceptions  Approvals START*/
            if(APTS_ConstantUtil.LINE_TYPE.equals(proposalLineItem.Apttus_QPConfig__LineType__c) && APTS_ConstantUtil.OF_CHARGE_TYPE.equals(proposalLineItem.Apttus_QPConfig__ChargeType__c)) {
                if(proposalLineItem.Apttus_QPConfig__AdjustedPrice__c < proposalLineItem.Apttus_QPConfig__OptionPrice__c) {
                    proposalObj.APTPS_Requires_BillingReceivablesManager__c = true;
                    proposalObj.APTPS_Requires_Dir_Of_Finance_Approval__c = true;
                    proposalObj.APTPS_Requires_CFO_Approval__c = true;
                    break;
                 
                }
            }           
            /*Operating Fund Exceptions  Approvals END*/
            /*25 Hour Share  Approvals for New Sale,Reduction, Renewal START*/
            if(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c == '25' && Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c) <= 60 &&(APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(proposalObj.APTS_Modification_Type__c) ||APTS_ConstantUtil.RENEWAL.equalsIgnoreCase(proposalObj.APTS_Modification_Type__c) || APTS_ConstantUtil.NEW_SALE.equalsIgnoreCase(proposalObj.APTS_New_Sale_or_Modification__c))){
                proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                break;
            }
            /*25 Hour Share  Approvals for New Sale,Reduction, Renewal END*/
            /*Delayed Start Amount Period  Approvals START*/
            if (APTS_Constants.NJE_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c != null ) {    
                if(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <= 3) { 
                    proposalObj.APTS_Require_AE_SalesVP_Approval__c = true;
                    break;
                } else if(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 3 && Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <= 6) { 
                    proposalObj.APTS_Requires_RVP_Approval__c = true;
                    break;
                } else if(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 6) { 
                    proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;
                    break;
                }
            }
            if (APTS_Constants.NJE_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c != null ) {                        
                proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                 break;                                        
            }
            /*Delayed Start Amount Period  Approvals END*/
            /*Non-Standard Term Length  Approvals START*/
            if (APTS_Constants.NJE_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c != null ) { 
                if(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c) < 60) {
                    proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                    break;
                }
                                                        
            }
            /*Non-Standard Term Length  Approvals End*/
            /* 36 month Minimum Commitment Date  Approvals START*/
            if(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c != '25' && Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c) < 36 && !APTS_ConstantUtil.NEW_SALE.equalsIgnoreCase(proposalObj.APTS_New_Sale_or_Modification__c)){
                proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                break;
            }
            /* 36 month Minimum Commitment Date  Approvals END*/
            
        }
    }

    
    
    
    
    /** 
    @description: Share NJE implementation
    @param:
    @return: 
    */
    public class NJE_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Share NJE Approvals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Share obj = new APTPS_Approvals_Share();
            //obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            //obj.checkSalesOpsTeamApprovalRequired(proposalObj,quoteLines);
            //obj.checkExtensionApprovalRequired(proposalObj,quoteLines);
            //obj.checkRepurchaseApprovalRequired(proposalObj,quoteLines);
            //obj.checkAssigmentApprovalRequired(proposalObj,quoteLines);
            //obj.checkDefermentApprovalRequired(proposalObj,quoteLines);
            //obj.checkReductionApprovalRequired(proposalObj,quoteLines);
            //obj.checkSizeIncreaseApprovalRequired(proposalObj,quoteLines);
            obj.checkNJEApprovalRequired(proposalObj,quoteLines);
            obj.checkAssigmentApprovalRequired(proposalObj,quoteLines);
            obj.checkExtensionApprovalRequired(proposalObj,quoteLines);
            obj.checkRepurchaseApprovalRequired(proposalObj,quoteLines);
            obj.checkDefermentApprovalRequired(proposalObj,quoteLines);
        }
    }
    
    /** 
    @description: Share NJA implementation
    @param:
    @return: 
    */
    public class NJA_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Share NJA dApprovals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Share obj = new APTPS_Approvals_Share();
            obj.checkDirectorARApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesOpsApprovalRequired(proposalObj,quoteLines);
            obj.checkDDCApprovalRequired(proposalObj,quoteLines);
            obj.checkExtensionApprovalRequired(proposalObj,quoteLines);
            obj.checkAssigmentApprovalRequired(proposalObj,quoteLines);
            obj.checkRepurchaseApprovalRequired(proposalObj,quoteLines);
            //obj.checkDefermentApprovalRequired(proposalObj,quoteLines);
            obj.checkReductionApprovalRequired(proposalObj,quoteLines);
            obj.checkSizeIncreaseApprovalRequired(proposalObj,quoteLines);
            
        }
    }
}