/*************************************************************
@Name: APTPS_UpdateAssetAgrQueueble
@Author: Siva Kumar
@CreateDate: 04 January 2022
@Description : This Queueable class is used to update AssetAgreement in Modification Scenarios
*****************************************************************/
public class APTPS_UpdateAssetAgrQueueble implements Queueable {
    public set<Id>  assetIds;
    public integer  jobChainLimit;
    public APTPS_UpdateAssetAgrQueueble(set<Id> assetIdList){
        this.assetIds =  assetIdList;  
        this.jobChainLimit = 0;
    }
    public void execute(QueueableContext context) {
        Integer jobs = [SELECT count() FROM AsyncApexJob where (ApexClass.name=:APTS_ConstantUtil.ORDERWORKFLOWQJOB OR ApexClass.name=:APTS_ConstantUtil.CREATEORDERQJOB) AND  (Status =:APTS_ConstantUtil.STATUS_QUEUED or Status =:APTS_ConstantUtil.STATUS_PROCESSING or Status =:APTS_ConstantUtil.STATUS_PREPARING)];
        jobChainLimit++;
        if(jobs == 0 ) {
            set<Id> assetAgreementIds = new set<Id>();
            List<Apttus__APTS_Agreement__c> updateAgrList = new List<Apttus__APTS_Agreement__c>();
            List<String> modificationsList = new List<String>{APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION,APTS_ConstantUtil.LEASE_TERMINATION,APTS_ConstantUtil.DEFERMENT_MORATORIUM,APTS_ConstantUtil.SHARE_SIZE_INCREASE,APTS_ConstantUtil.REDUCTION};
            for(Apttus_Config2__AssetLineItem__c ALIObj : [SELECT Id,Apttus_CMConfig__AgreementId__c,APTS_Original_Agreement__c,Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c  FROM Apttus_Config2__AssetLineItem__c WHERE Id IN:assetIds AND Apttus_Config2__IsPrimaryLine__c = true AND Apttus_Config2__LineType__c =:APTS_ConstantUtil.LINE_TYPE AND Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c IN :modificationsList]) {
                assetAgreementIds.add(ALIObj.APTS_Original_Agreement__c);
            }
            system.debug('assetAgreementIds-->'+assetAgreementIds);
            for(Id assetAgrId : assetAgreementIds) {
                updateAgrList.add(new Apttus__APTS_Agreement__c(id=assetAgrId,Agreement_Status__c=APTS_ConstantUtil.AGREEMENT_AMENDED,Chevron_Status__c=APTS_ConstantUtil.AGREEMENT_AMENDED));
            }
            if(!updateAgrList.isEmpty()) {
                update updateAgrList;
            }

            
        } else{
            if(jobChainLimit < 5) {
                System.enqueueJob(new APTPS_UpdateAssetAgrQueueble(assetIds));
            }
                
        }
    }
}