/************************************************************************************************************************
@Name: APTPS_DealSummary_Share
@Author: Conga PS Dev Team
@CreateDate: 16 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public with sharing class APTPS_DealSummary_Lease {
    /** 
    @description: Read PAVs and prepare deal summary
    @param: Quote Id and Currency Sign
    @return: Final value of Deal Summary
    */
    private static String getSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        String dsText = '';
        Map<Id, String> saNameMap = new Map<Id, String>();
        Map<Id, Double> saAllocatedHrMap = new Map<Id, Double>();
        Map<Id, Double> saPercent = new Map<Id, Double>();
        Double fet, incentiveFee, accFees, leaseDeposit, totalPurchasePrice, cardGraduateCredit;
        totalPurchasePrice = 0;
        fet=incentiveFee=accFees=leaseDeposit=cardGraduateCredit = null;
        Boolean isInterimAvailable, isPrePaymentAvailable;
        isInterimAvailable=isPrePaymentAvailable = false;
        String acName, commitmentPeriod, vintage, hours;
        acName=commitmentPeriod=vintage=hours = null;
        Double percentage = null;
        String delayedStartMonths='';
        String contractLength='';
        Date delayedStartDate, endDate;
        delayedStartDate=endDate = null;
        try{
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && APTS_ConstantUtil.FEDERAL_EXCISE_TAX.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       fet = pli.Apttus_QPConfig__NetPrice__c;
                   }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           incentiveFee = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Incentive_Amount__c;
                       }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.ACC_FEE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.ACC_FEE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           accFees = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Fee_Amount__c;
                       }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.INTERIM_LEASE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.INTERIM_LEASE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           isInterimAvailable = true;
                       }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.SPLIT_BILL_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.SPLIT_BILL_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           saNameMap.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__r.Name);
                           saAllocatedHrMap.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Allocated_Hours__c);
                           saPercent.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percent__c);
                       }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PREPAY_FEE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PREPAY_FEE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           isPrePaymentAvailable = true; 
                       }
                
                if(APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                   && APTS_ConstantUtil.LEASE_DEPOSIT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       acName = pli.Apttus_QPConfig__OptionId__r.Name;
                       leaseDeposit = pli.Apttus_QPConfig__NetPrice__c;
                       //vintage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Vintage__c;
                       hours = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                       percentage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c;
                       contractLength = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c;
                       delayedStartDate = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_End_Date__c;
                       //commitmentPeriod = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c;
                       endDate = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_End_Date__c;
                       delayedStartMonths = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c;
                       
                   }
                
                /*if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.CARD_GRADUATE_CREDIT_NJA.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.CARD_GRADUATE_CREDIT_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           cardGraduateCredit = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_10_Card_Graduate_Credit_Amount__c;
                       }*/
                
            }
            system.debug('Pricing data, fet --> '+fet+' incentiveFee --> '+incentiveFee+' accFees --> '+accFees
                         +'leaseDeposit --> '+leaseDeposit+' cardGraduateCredit --> '+cardGraduateCredit);
            totalPurchasePrice = leaseDeposit;
            dsText += '<b>Transaction Type:</b> Lease<br/><br/>';
            dsText += '<b>Aircraft Type:</b> '+acName+'<br/><br/>';
            //dsText += '<b>Vintage:</b> '+vintage+'<br/><br/>';
            dsText += '<b>Hours:</b> '+hours+'<br/><br/>';
            dsText += '<b>Share Size:</b> '+hours+' hrs'+' ('+percentage+'%)<br/><br/>';
            if(contractLength !='')
                dsText += '<b>Contract Length:</b> '+contractLength+'<br/><br/>';
            if(endDate != null)
                dsText += '<b>End Date:</b> '+endDate+'<br/><br/>';
            if(delayedStartDate != null)
                dsText += '<b>Delayed Start End Date:</b> '+delayedStartDate+'<br/><br/>';
            if(delayedStartMonths != null)
                dsText += '<b>Delayed Start Months:</b> '+delayedStartMonths+'<br/><br/>';
            //dsText += '<b>Minimum Commitment Period:</b> '+commitmentPeriod+'<br/><br/>';
            //TODO: For now as it's NJUS Share only so adding currency as it is. Add logic(if required) once we implement NJE Share.
            dsText += '<b>Lease Deposit:</b> '+currencySign+leaseDeposit+'<br/><br/>';
            /*if(fet != null && fet > 0) {
                dsText += '<b>FET:</b> '+currencySign+fet+'<br/><br/>';
                totalPurchasePrice += fet;
            }
            
            if(incentiveFee != null && incentiveFee > 0) {
                dsText += '<b>Purchase Price Incentive:</b> '+currencySign+incentiveFee+'<br/><br/>';
                totalPurchasePrice -= incentiveFee;
            }
            
            if(accFees != null && accFees > 0) {
                dsText += '<b>ACC Fees:</b> '+currencySign+accFees+'<br/><br/>';
                totalPurchasePrice += accFees;
            }
            
            if(cardGraduateCredit != null && cardGraduateCredit > 0) {
                dsText += '<b>10% Card Graduate Credit:</b> '+currencySign+cardGraduateCredit+'<br/><br/>';
                totalPurchasePrice -= cardGraduateCredit;
            }
            if(totalPurchasePrice != null && totalPurchasePrice != leaseDeposit) 
                dsText += '<b>Total Purchase Price:</b> '+currencySign+totalPurchasePrice+'<br/><br/>'; */
            dsText += '<b>Options:</b><br/>';
            String optionText = '';
            system.debug('isInterimAvailable --> '+isInterimAvailable+' isPrePaymentAvailable --> '+isPrePaymentAvailable+' Map --> '+saNameMap.isEmpty());
            if(isInterimAvailable || isPrePaymentAvailable || !saNameMap.isEmpty()) {
                optionText = '<ul>';
                if(isInterimAvailable)
                    optionText += '<li>Interim Lease</li>';
                if(isPrePaymentAvailable)
                    optionText += '<li>Prepayment of Fees</li>';
                if(!saNameMap.isEmpty()) {
                    optionText += '<li>Split Billing</li>';
                    for(Id key : saNameMap.keySet()) {
                        String saName = saNameMap.get(key);
                        Double hrs = saAllocatedHrMap.get(key);
                        Double percent = saPercent.get(key);
                        optionText += '<span>Service Account '+saName+' - '+hrs+' hrs ('+percent+'%)</span><br/>';
                    }
                }
                optionText +='</ul>';
            }
            if(!String.isEmpty(optionText))
                dsText += optionText;
            system.debug('Lease Deal Summary --> '+dsText);
        }catch(Exception e){
            system.debug('Exception while computing Deal Summary for Lease NJA. Error details --> '+e.getMessage());
        }
        return dsText;
    }
    //START: Used for Deferment Modification
    private static String getDefermentSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines, String modType) {
        String dsText = '';
        String acName, hours, modificationType;
        acName=hours=modificationType = null;
        Date startOfDeferment, endOfDeferment = null;
        Boolean isWaiveRemarketingFeeAvailable = false;
        try{
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                   && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       //modificationType = pli.APTS_Modification_Type__c;
                       acName = pli.Apttus_QPConfig__OptionId__r.Name;
                       hours = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                       startOfDeferment = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Start_of_Deferment__c;
                       endOfDeferment = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_End_of_Deferment__c;
                   }
            }
            
            dsText += '<b>Modification Type:</b> '+modType+'<br/><br/>';
            dsText += '<b>Aircraft Type:</b> '+acName+'<br/><br/>';
            dsText += '<b>Hours:</b> '+hours+'<br/><br/>';
            dsText += '<b>Start Date of Deferment:</b> '+startOfDeferment.format()+'<br/><br/>';
            dsText += '<b>End Date of Deferment:</b>'+endOfDeferment.format()+'<br/>';
            
        }catch(Exception e){
            system.debug('Exception while computing Deal Summary for Deferment Lease. Error details --> '+e.getMessage());
        }
        
        return dsText;
    }
    //END: Used for Deferment Modification
    
    public static String getUnrelatedAssignmentSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines, String modType, String addModType) {
        String dsText = '';
        Id agId = null;
        String acName, hours, assignorAccName, assignorLegalEntityName, assigneeAccName, assigneeLegalEntityName;
        acName=hours=assignorAccName=assignorLegalEntityName=assigneeAccName=assigneeLegalEntityName = '';
        try{
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                   && APTS_ConstantUtil.PURCHASEPRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       acName = pli.Apttus_QPConfig__OptionId__r.Name;
                       hours = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                       assigneeAccName = pli.Apttus_Proposal__Proposal__r.Apttus_Proposal__Account__r.Name;
                       assigneeLegalEntityName = pli.Apttus_QPConfig__LocationId__r.Name;
                       agId = pli.Apttus_Proposal__Proposal__r.APTPS_Assignor_Agreement__c != null ? pli.Apttus_Proposal__Proposal__r.APTPS_Assignor_Agreement__c : null;
                       break;
                   }
            }
            
            //There is no other way, have to query the agreement to get Acocunt and Legal Entity details.
            //Else we have to create new field on Quote/Proposal and stamp the values from screen flow.
            if(agId != null) {
                Apttus__APTS_Agreement__c agDetails = [SELECT Apttus__Account__r.Name, Legal_Entity_Name__c 
                                                       FROM Apttus__APTS_Agreement__c 
                                                       WHERE ID =: agId LIMIT 1];
                assignorAccName = agDetails.Apttus__Account__r.Name;
                assignorLegalEntityName = agDetails.Legal_Entity_Name__c;
            }
            
            dsText += '<b>Transaction Type:</b> '+modType+'<br/><br/>';
            dsText += '<b>Sub Modification Type:</b> '+addModType+'<br/><br/>';
            dsText += '<b>Aircraft Type:</b> '+acName+'<br/><br/>';
            dsText += '<b>Hours Assigned:</b> '+hours+'<br/><br/>';
            dsText += '<b>Assignor/Assignee Account Name:</b> '+assignorAccName+'/'+assigneeAccName+'<br/><br/>';
            dsText += '<b>Assignor/Assignee Legal Entity Name:</b> '+assignorLegalEntityName+'/'+assigneeLegalEntityName+'<br/><br/>';
        }catch(Exception e){
            system.debug('Exception while computing Deal Summary for Unrelated Assignment Share. Error details --> '+e.getMessage());
        }
        return dsText;
    }
    
    //START: Used for Lease Termination Modification
    private static String getLeaseTerminationSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines, String modType) {
        String dsText = '';
        String acName, hours, modificationType;
        acName=hours=modificationType = null;
        Date dateOfNotice = null;
        Boolean isReinstateUnderflownHoursAvailable = false;
        try{
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                   && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       //modificationType = pli.APTS_Modification_Type__c;
                       acName = pli.Apttus_QPConfig__OptionId__r.Name;
                       hours = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                       dateOfNotice = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Date_of_Notice__c;
                   }
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.NJUS_REINSTATE_UNDERFLOWN_HOURS.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) 
                       //|| APTS_ConstantUtil.NJE_REINSTATE_UNDERFLOWN_HOURS.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode)
                      )) {
                          isReinstateUnderflownHoursAvailable = true;
                      }
            }
            
            dsText += '<b>Modification Type:</b> '+modType+'<br/><br/>';
            dsText += '<b>Aircraft Type:</b> '+acName+'<br/><br/>';
            dsText += '<b>Hours:</b> '+hours+'<br/><br/>';
            dsText += '<b>Date of Notice:</b> '+dateOfNotice.format()+'<br/><br/>';
            dsText += '<b>Enhancements:</b> ';
            String optionText = '';
            system.debug('isReinstateUnderflownHoursAvailable --> '+isReinstateUnderflownHoursAvailable);
            if(isReinstateUnderflownHoursAvailable) {
                //optionText = '<ul>';
                if(isReinstateUnderflownHoursAvailable)
                    optionText += 'Reinstate Underflown Hours<br/>';
                //optionText +='</ul>';
            }
            if(!String.isEmpty(optionText))
                dsText += optionText;
        }catch(Exception e){
            system.debug('Exception while computing Deal Summary for Lease Termination. Error details --> '+e.getMessage());
        }
        
        return dsText;
    }
    //END: Used for Lease Termination Modification
    
    //START: Used for RENEWAL Modification
    private static String getRenewalSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines, String modType) {
        String dsText = '';
        String acName, hours, modificationType, commitmentPeriod, contractLength;
        acName=hours=modificationType=commitmentPeriod=contractLength = null;
        Boolean isRenewalIncentiveAvailable = false;
        try{
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                   && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       //modificationType = pli.APTS_Modification_Type__c;
                       acName = pli.Apttus_QPConfig__OptionId__r.Name;
                       hours = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                       contractLength = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c;
                       commitmentPeriod = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c;
                   }
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.NJUS_RENEWAL_INCENTIVE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode)
                       || APTS_ConstantUtil.NJE_RENEWAL_INCENTIVE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode)
                      )) {
                          isRenewalIncentiveAvailable = true;
                      }
            }
            
            dsText += '<b>Modification Type:</b> '+modType+'<br/><br/>';
            dsText += '<b>Aircraft Type:</b> '+acName+'<br/><br/>';
            dsText += '<b>Hours:</b> '+hours+'<br/><br/>';
            dsText += '<b>Term/Contract Length:</b> '+contractLength+'<br/><br/>';
            dsText += '<b>Minimum Commitment Period:</b> '+commitmentPeriod+'<br/><br/>';
            dsText += '<b>Enhancements:</b> ';
            String optionText = '';
            system.debug('isRenewalIncentiveAvailable --> '+isRenewalIncentiveAvailable);
            if(isRenewalIncentiveAvailable) {
                //optionText = '<ul>';
                if(isRenewalIncentiveAvailable)
                    optionText += '(5% discount for non-premium MMF)<br/>';
                //optionText +='</ul>';
            }
            if(!String.isEmpty(optionText))
                dsText += optionText;
        }catch(Exception e){
            system.debug('Exception while computing Deal Summary for Share Renewal. Error details --> '+e.getMessage());
        }
        
        return dsText;
    }
    //END: Used for RENEWAL Modification
    
    //START: Used for Letter Agreement Extension Modification
    public static String getExtensionSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines, String modType) {
        
        String dsText = APTPS_SNL_Util.generateExtensionSummary(quoteLines,modType);    
        
        return dsText;
    }
    //END: Used for  Letter Agreement Extension Modification
    
    private static String getRenewalReductionSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines, String modType) {
        String dsText = '';
        String acName, hours, assetHours, commitmentPeriod;
        acName=hours=assetHours=commitmentPeriod = null;
        Double contractLength,incentiveFee;
        contractLength=incentiveFee = null;
        Map<Id, String> saNameMap = new Map<Id, String>();
        Map<Id, Double> saAllocatedHrMap = new Map<Id, Double>();
        Map<Id, Double> saPercent = new Map<Id, Double>();
        Boolean isPrePaymentAvailable = false;
        Date dateOfNotice = null;
        try{
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                   && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       acName = pli.Apttus_QPConfig__OptionId__r.Name;
                       hours = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                       assetHours = String.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_SNL_Original_Hours__c);
                       contractLength = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c;
                       commitmentPeriod = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c;
                       dateOfNotice = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Date_of_Notice__c;
                   }
                
                //Purchase Price Incentive
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode)) 
                   && pli.Apttus_QPConfig__LineStatus__c == 'New') {
                       incentiveFee = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Incentive_Amount__c;
                   }
                
                //Split Billing
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.SPLIT_BILL_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.SPLIT_BILL_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           saNameMap.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__r.Name);
                           saAllocatedHrMap.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Allocated_Hours__c);
                           saPercent.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percent__c);
                       }
            }
            
            dsText += '<b>Modification Type:</b> '+modType+'<br/><br/>';
            dsText += '<b>Aircraft Type:</b> '+acName+'<br/><br/>';
            dsText += '<b>Current Hours:</b> '+assetHours+'<br/><br/>';
            dsText += '<b>New Hours:</b> '+hours+'<br/><br/>';
            dsText += '<b>Date of Notice:</b> '+dateOfNotice.format()+'<br/><br/>';
            dsText += '<b>Options:</b><br/>';
            String optionText = '';
            if(incentiveFee != null || !saNameMap.isEmpty()) {
                optionText = '<ul>';
                if(!saNameMap.isEmpty()) {
                    optionText += '<li>Split Billing</li>';
                    for(Id key : saNameMap.keySet()) {
                        String saName = saNameMap.get(key);
                        Double hrs = saAllocatedHrMap.get(key);
                        Decimal percent = Decimal.valueOf(saPercent.get(key));
                        percent = percent.setScale(2);
                        optionText += '<span>Service Account '+saName+' - '+hrs+' hrs ('+percent+'%)</span><br/>';
                    }
                }
                optionText +='</ul>';
            }
            if(!String.isEmpty(optionText))
                dsText += optionText;
            
        }catch(Exception e){
            system.debug('Exception while computing Deal Summary for Renewal with reduction. Error details --> '+e.getMessage());
        }
        
        return dsText;
    }
    
    /** 
    @description: NJUS Lease implementation
    @param:
    @return: 
    */
    public class NJUS_Summary implements APTPS_DealSummaryInterface {
        public Apttus_Proposal__Proposal__c updateSummary(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Lease NJUS deal summary.');
            Id qId = (Id)args.get('qId');
            String modType = (String)args.get('modType');
            String addModType = (String)args.get('addModType');
            String dsText = '';
            if(String.isBlank(modType)) //Lease New Sale
                dsText = APTPS_DealSummary_Lease.getSummary(qId, '$', quoteLines);
            else if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(modType))
                dsText = APTPS_DealSummary_Lease.getDefermentSummary(qId, '$', quoteLines, modType);
            else if(APTS_ConstantUtil.ASSIGNMENT.equalsIgnoreCase(modType) && APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(addModType))
                dsText = APTPS_DealSummary_Lease.getUnrelatedAssignmentSummary(qId, '$', quoteLines, modType, addModType);
            else if(APTS_ConstantUtil.LEASE_TERMINATION.equalsIgnoreCase(modType))
                dsText = APTPS_DealSummary_Lease.getLeaseTerminationSummary(qId, '$', quoteLines, modType);
            else if(APTS_ConstantUtil.RENEWAL.equalsIgnoreCase(modType) && 
                    APTS_ConstantUtil.NONE.equalsIgnoreCase(addModType))
                dsText = APTPS_DealSummary_Lease.getRenewalSummary(qId, '$', quoteLines, modType);
            else if(APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equalsIgnoreCase(modType))
                dsText = APTPS_DealSummary_Lease.getExtensionSummary(qId, '$', quoteLines, modType);
            else if(APTS_ConstantUtil.RENEWAL.equalsIgnoreCase(modType) && 
                    APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(addModType))
                dsText = APTPS_DealSummary_Lease.getRenewalReductionSummary(qId, '$', quoteLines, modType);
            
            Apttus_Proposal__Proposal__c quoteToUpdate = new Apttus_Proposal__Proposal__c(Id = qId);
            quoteToUpdate.Deal_Description__c = dsText;
            return quoteToUpdate;
        }
    }
    
    /** 
    @description: NJE Share implementation
    @param:
    @return: 
    */
    public class NJE_Summary implements APTPS_DealSummaryInterface {
        public Apttus_Proposal__Proposal__c updateSummary(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Share NJE deal summary.');
            Id qId = (Id)args.get('qId');
            String modType = (String)args.get('modType');
            String addModType = (String)args.get('addModType');
            String dsText = '';
            if(String.isBlank(modType)) //Lease New Sale
                dsText = APTPS_DealSummary_Lease.getSummary(qId, '€', quoteLines);
            else if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(modType))
                dsText = APTPS_DealSummary_Lease.getDefermentSummary(qId, '€', quoteLines, modType);
            else if(APTS_ConstantUtil.ASSIGNMENT.equalsIgnoreCase(modType) && APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(addModType))
                dsText = APTPS_DealSummary_Lease.getUnrelatedAssignmentSummary(qId, '€', quoteLines, modType, addModType);
            else if(APTS_ConstantUtil.LEASE_TERMINATION.equalsIgnoreCase(modType))
                dsText = APTPS_DealSummary_Lease.getLeaseTerminationSummary(qId, '€', quoteLines, modType);
            else if(APTS_ConstantUtil.RENEWAL.equalsIgnoreCase(modType) && 
                    APTS_ConstantUtil.NONE.equalsIgnoreCase(addModType))
                dsText = APTPS_DealSummary_Lease.getRenewalSummary(qId, '€', quoteLines, modType);
            else if(APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equalsIgnoreCase(modType))
                dsText = APTPS_DealSummary_Lease.getExtensionSummary(qId, '€', quoteLines, modType);
            else if(APTS_ConstantUtil.RENEWAL.equalsIgnoreCase(modType) && 
                    APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(addModType))
                dsText = APTPS_DealSummary_Lease.getRenewalReductionSummary(qId, '€', quoteLines, modType);
            
            Apttus_Proposal__Proposal__c quoteToUpdate = new Apttus_Proposal__Proposal__c(Id = qId);
            quoteToUpdate.Deal_Description__c = dsText;
            return quoteToUpdate;
        }
    }
}