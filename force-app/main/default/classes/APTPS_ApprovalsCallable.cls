/************************************************************************************************************************
@Name: APTPS_ApprovalsCallable
@Author: Conga PS Dev Team
@CreateDate: 25 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_ApprovalsCallable {
    Object call(Map<Id, String> quoteProductMap, Map<Id, Map<String, Object>> quoteArgMap);
}