/************************************************************************************************************************
@Name: APTPS_Approvals_Corporate
@Author: Conga PS Dev Team
@CreateDate: 25 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_Approvals_Corporate implements APTPS_ApprovalUsersInterface{
    
    public void checkCAManagerApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    public void checkHeadofCommercialSalesApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
    public void checkLegalApprovedRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.CORPORATE_TRAIL.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode)) {                        
                proposalObj.Requires_Legal_Approval__c = true;
                break;                                        
            }
        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkSalesOpsApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    public void checkSalesDirectorApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    public void checkDemoTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    public void checkDirectorARApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    public void checkRVPApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    public void checkSalesOpsTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    public void checkDDCApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
   
    
   
    /** 
    @description: Coprporate Trail implementation
    @param:
    @return: 
    */
    public class NJE_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Corporate NJE dApprovals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Corporate obj = new APTPS_Approvals_Corporate();
            obj.checkLegalApprovedRequired(proposalObj,quoteLines);            
            
        }
    }
}