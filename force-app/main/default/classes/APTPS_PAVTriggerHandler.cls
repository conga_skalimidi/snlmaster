/************************************************************************************************************************
@Name: APTPS_PAVTriggerHandler
@Author: Conga PS Dev Team
@CreateDate: 02 Nov 2021
@Description: Product Attribute Value Trigger Handler
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_PAVTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        List<Apttus_Config2__ProductAttributeValue__c> newList = Trigger.new;
        Map<Id, Apttus_Config2__ProductAttributeValue__c> liAttrMap = new Map<Id, Apttus_Config2__ProductAttributeValue__c>();

        for(Apttus_Config2__ProductAttributeValue__c attr : newList) {
            liAttrMap.put(attr.Apttus_Config2__LineItemId__c, attr);
        }
        system.debug('liAttrMap --> '+liAttrMap);
        
        if(!liAttrMap.isEmpty())
            APTPS_PAVTriggerHelper.resetSNLEntitlementFilterAttributes(liAttrMap);
    }
}