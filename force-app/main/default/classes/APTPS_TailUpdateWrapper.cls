public class APTPS_TailUpdateWrapper {
    public Date actualDeliveryDate;
    public Date warrantyDate;
    public String serialNumber; 
    public String tailNumber;
    public String aircraftTypeName;
    public String manufacturer;
    public integer noOfEngines;
    public String cabinClassSize;
    public APTPS_TailUpdateWrapper(String aircraftTailjson){
        Map<String, Object> airTail = (Map<String, Object>)JSON.deserializeUntyped(aircraftTailjson);
        
        Map<String, Object> airTail2 = (Map<String, Object>) airTail.get('TailInformation');
        if(!String.isBlank((String)airTail2.get('ActualDeliveryDate'))){
            String strdate = (String)airTail2.get('ActualDeliveryDate');
            Date newDate = Date.parse(strdate);
            Datetime dt = Datetime.newInstanceGMT(newDate.year(),NewDate.month(),newDate.day());
            Date myDate = date.newinstance(dt.year(), dt.month(), dt.day());
            this.actualDeliveryDate = myDate;
        }
        if(!String.isBlank((String)airTail2.get('WarrantyDate'))){
            String strdate = (String)airTail2.get('WarrantyDate');
            Date newDate = Date.parse(strdate);
            Datetime dt = Datetime.newInstanceGMT(newDate.year(),NewDate.month(),newDate.day());
            Date myDate = date.newinstance(dt.year(), dt.month(), dt.day());
            this.warrantyDate = myDate;
        }
        if(!String.isBlank((String)airTail2.get('SerialNumber'))){
            this.serialNumber = string.valueOf(airTail2.get('SerialNumber')); 
        }
        if(!String.isBlank((String)airTail2.get('TailNumber'))){
            this.tailNumber = string.valueOf(airTail2.get('TailNumber')); 
        } 
        if(airTail2.get('AircraftTypeName')!=''){                        
            this.aircraftTypeName = string.valueOf(airTail2.get('AircraftTypeName'));
        }
        if(airTail2.get('Manufacturer')!=''){
            this.manufacturer = string.valueOf(airTail2.get('Manufacturer')); 
        }
        if(airTail2.get('NoOfEngines')!=''){
            this.noOfEngines = integer.valueOf(airTail2.get('NoOfEngines'));
        }
        if(airTail2.get('CabinClassSize')!=''){
            this.cabinClassSize =  string.valueOf(airTail2.get('CabinClassSize'));
        }
        
    }
}