/**
 * This apex class provides methods to create test data for Apttus CPQ flow.
 *
 **/
public class APTS_CPQTestUtility {
    
    /*
     * Create Test Account
     */
    public static Account createAccount(String strName, String strType) {
        Account testAccount = new Account();
        testAccount.Name = strName;
        testAccount.Type = strType;
        
        return testAccount;
        
    }
    
    /*
     * Create Test Contact
     */
    public static Contact createContact(String strFirstName, String strLastName, Id strAccountId) {
        Contact testContact = new Contact();
        testContact.FirstName = strFirstName;
        testContact.LastName = strLastName;
        testContact.AccountId = strAccountId;
        
        return testContact;
        
    }
    
    /*
     * Create Test Opportunity
     */
    public static Opportunity createOpportunity(String strName, String strType, Id accountId, String strStageName) {
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = strName;
        testOpportunity.Type = strType;
        testOpportunity.AccountId = accountId;
        testOpportunity.StageName = strStageName;
        testOpportunity.CloseDate = date.Today().addDays(365);
        
        return testOpportunity;
    }
    
    /*
     * Create Price List
     */
    public static Apttus_Config2__PriceList__c createPriceList(String strName, Boolean bIsActive) {
        Apttus_Config2__PriceList__c priceList = new Apttus_Config2__PriceList__c();
        priceList.Name = strName;
        priceList.Apttus_Config2__Active__c = bIsActive;
        
        return priceList;
    }
    
    /*
     * Create  Product
     */
    public static Product2 createProduct(String productName, String productCode, String productFamily, String strConifgurationType, Boolean bCustomizable, Boolean bHasOptions, Boolean bHasAttributes, Boolean bIsActive) {
        Product2 product = new Product2();
        product.Name = productName;
        product.ProductCode = productCode;
        product.Family = productFamily;
        product.Apttus_Config2__ConfigurationType__c = strConifgurationType;
        product.Apttus_Config2__Customizable__c = bCustomizable;
        product.Apttus_Config2__HasOptions__c = bHasOptions;
        product.Apttus_Config2__HasAttributes__c = bHasAttributes;
        product.IsActive = bIsActive;
        
        return product; 
    }
    
    /**
        Create PricebookEntry 
    **/
    public static PricebookEntry createPricebookEntry(String pricebookId, String productID, Integer iUnitPrice, Boolean bIsActive){                                        
        PricebookEntry standardPrice = new PricebookEntry();
        standardPrice.Pricebook2Id = pricebookId;
        standardPrice.Product2Id = productID;
        standardPrice.UnitPrice = iUnitPrice;
        standardPrice.IsActive = bIsActive;
        
        return standardPrice;                                        
     } 
    
    /*
     * Create Price List Item
     */
    public static Apttus_Config2__PriceListItem__c createPriceListItem(Id priceListId, Id productId, decimal dListPrice, String strChargeType, String strPriceType, String strPriceMethod, String strPriceUoM, Boolean bIsActive) {
        Apttus_Config2__PriceListItem__c pli = new Apttus_Config2__PriceListItem__c();
        pli.Apttus_Config2__PriceListId__c = priceListId;
        pli.Apttus_Config2__ProductId__c = productId;
        pli.Apttus_Config2__ListPrice__c = dListPrice;
        pli.Apttus_Config2__ChargeType__c = strChargeType;
        pli.Apttus_Config2__PriceType__c = strPriceType;
        pli.Apttus_Config2__PriceMethod__c = strPriceMethod;
        pli.Apttus_Config2__PriceUom__c = strPriceUoM;
        pli.Apttus_Config2__Active__c = bIsActive;
                
        return pli;
    }
    
    /*
     * Create Proposal
     */
    public static Apttus_Proposal__Proposal__c createProposal( String quoteName, ID accId, ID optyId, String recordTypeName, ID priceList){
        
        Apttus_Proposal__Proposal__c proposal = new Apttus_Proposal__Proposal__c();
        proposal.Apttus_Proposal__Proposal_Name__c = quoteName;
        proposal.Apttus_Proposal__Opportunity__c = optyId;
        proposal.Apttus_Proposal__Account__c = accId;
        proposal.RecordTypeId = Schema.SObjectType.Apttus_Proposal__Proposal__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        proposal.Apttus_QPConfig__PriceListId__c = priceList;
        
        return proposal;
    }
    
    /*
     * Create Configuration
     */
    public static String createConfiguration(String strProposalId) {
        Apttus_CPQApi.CPQ.CreateCartRequestDO request = new Apttus_CPQApi.CPQ.CreateCartRequestDO();
        request.QuoteId = strProposalId;
        Apttus_CPQApi.CPQ.CreateCartResponseDO response = Apttus_CPQApi.CPQWebService.createCart(request);
        return response.CartId; 
    }
    
    /*
     * Create Classification Hierarchy
     */
    public static Apttus_Config2__ClassificationHierarchy__c createClassificationHierarchy(String strCategoryId, String strLabel) {
        
        Apttus_Config2__ClassificationHierarchy__c classificationHierarchy = new Apttus_Config2__ClassificationHierarchy__c();
        classificationHierarchy.Apttus_Config2__HierarchyId__c = strCategoryId;
        classificationHierarchy.Apttus_Config2__Label__c = strLabel;
        
        return classificationHierarchy;
    }
    
    /*
     * Create Category
     */
    public static Apttus_Config2__ClassificationName__c createCategory(String strName, String strHierarchyLabel, String strType, String strGuidePage, Boolean bIsActive) {
        Apttus_Config2__ClassificationName__c category = new Apttus_Config2__ClassificationName__c();
        category.Name = strName;
        category.Apttus_Config2__HierarchyLabel__c = strHierarchyLabel;
        category.Apttus_Config2__Type__c = strType;
        category.Apttus_Config2__GuidePage__c = strGuidePage;
        category.Apttus_Config2__Active__c = bIsActive;
        return category;
    }

    /*
     * Create Product Option Component
     */
    public static Apttus_Config2__ProductOptionComponent__c createProductOptionComponent(Integer iSequence) {
        Apttus_Config2__ProductOptionComponent__c productOptionComponent = new Apttus_Config2__ProductOptionComponent__c();
        productOptionComponent.Apttus_Config2__Sequence__c = iSequence;
        return productOptionComponent;
    }
    
    /*
     * Create Summary Group
     */
    public static Apttus_Config2__SummaryGroup__c createSummaryGroup(String strConfigId, Integer itemSequence, Integer lineNumber) {
    
        Apttus_Config2__SummaryGroup__c summaryGroup = new Apttus_Config2__SummaryGroup__c ();
        summaryGroup.Apttus_Config2__ConfigurationId__c = strConfigId;
        summaryGroup.Apttus_Config2__ItemSequence__c = itemSequence;
        summaryGroup.Apttus_Config2__LineNumber__c = lineNumber;
        return summaryGroup ;   
    }
    
    /*
     *  Create Line Item (Max 32 param limit exceeded)
     */
    public static void createLineItem_OLD(Id cartId, Id productId, Integer iQty){
        
        Apttus_CPQApi.CPQ.SelectedProductDO selProdDO = new Apttus_CPQApi.CPQ.SelectedProductDO();
        selProdDO.ProductID = productId;
        selProdDO.Quantity = iQty;
        
        Apttus_CPQApi.CPQ.AddMultiProductRequestDO requestBundle = new Apttus_CPQApi.CPQ.AddMultiProductRequestDO ();
        requestBundle.CartId = cartId;
        requestBundle.SelectedProducts = new List<Apttus_CPQApi.CPQ.SelectedProductDO>{selProdDO};
        Apttus_CPQApi.CPQ.AddMultiProductResponseDO response = Apttus_CPQApi.CPQWebService.addMultiProducts(requestBundle);
    }
    
    public static void createLineItem(Id cartId, Id productId, Integer iQty){
        Product2 prod = [SELECT Id from Product2 where Id=:productId];
        
        Account testAccount = APTS_CPQTestUtility.createAccount('Test PCB', 'Other');
		insert testAccount;
        
        Contact testContact = APTS_CPQTestUtility.createContact('ContactFN', 'PCB Contact', testAccount.Id);
        insert testContact;

		Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', testAccount.Id, 'Closed Won');
		insert testOpportunity;

		Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
		insert testAccLocation;

		Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
		insert testPriceList;
        
        //Card PLI
		Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, prod.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
		insert cardPLI;
        
        //Create proposal
		Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
		nsProposal.APTS_New_Sale_or_Modification__c = 'Modification';
		nsProposal.APTS_Modification_Type__c = 'Direct Conversion';
		nsProposal.APTS_Additional_Modification_Type__c = 'Term Extension';
		insert nsProposal;
        
        
        Apttus_Config2__LineItem__c cardLineItem = getLineItem(cartId, nsProposal, cardPLI, prod, prod.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
		cardLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
		insert cardLineItem;
    }
    
    public static void createLineItemwithAcct(Id cartId, Id productId, Integer iQty, Account testAccount){
        Product2 prod = [SELECT Id from Product2 where Id=:productId];
        
       /*  Account testAccount = APTS_CPQTestUtility.createAccount('Test PCB', 'Other');
		insert testAccount;
        
        Contact testContact = APTS_CPQTestUtility.createContact('ContactFN', 'PCB Contact', testAccount.Id);
        insert testContact;
 */
		Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', testAccount.Id, 'Closed Won');
		insert testOpportunity;

		Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
		insert testAccLocation;

		Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
		insert testPriceList;
        
        //Card PLI
		Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, prod.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
		insert cardPLI;
        
        //Create proposal
		Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
		nsProposal.APTS_New_Sale_or_Modification__c = 'Modification';
		nsProposal.APTS_Modification_Type__c = 'Direct Conversion';
		nsProposal.APTS_Additional_Modification_Type__c = 'Term Extension';
		insert nsProposal;
        
        
        Apttus_Config2__LineItem__c cardLineItem = getLineItem(cartId, nsProposal, cardPLI, prod, prod.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
		cardLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
		insert cardLineItem;
    }
    //This method is created for Product Attribute Callback only. It is cretaed to avoid 101 SOQL error. 
    public static Apttus_Config2__LineItem__c createLiForPAV(Id cartId, Apttus_Proposal__Proposal__c nsProposal, Apttus_Config2__PriceListItem__c pli, 
                                                            Product2 prod) {
        Apttus_Config2__LineItem__c li = getLineItem(cartId, nsProposal, pli, prod, prod.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        return li;
    }
    
    /*
     * Create Proposal line Item                                        
     */
    public static void createProposalLineItem(Id cartId){        
        Apttus_CPQApi.CPQ.FinalizeCartRequestDO finalRequest = new Apttus_CPQApi.CPQ.FinalizeCartRequestDO();
        finalRequest.CartId = cartId;
        Apttus_CPQApi.CPQ.FinalizeCartResponseDO finalResponse = Apttus_CPQApi.CPQWebService.finalizeCart(finalRequest);
    }
    
    /*
     * Create Product Attribute Value
     */
    public static Apttus_Config2__ProductAttributeValue__c createAttributeValue(Id lineItemId) {
        
        Apttus_Config2__ProductAttributeValue__c pav= new Apttus_Config2__ProductAttributeValue__c();
        pav.Apttus_Config2__LineItemId__c = lineItemId;
        pav.Accounting_Company__c = 'NetJets UK Limited';
        pav.Preferred_Bank__c = 'Barclays Bank Plc';
        return pav;
    }
    
    /*
     * Create Product Attribute Group
     */
    public static Apttus_Config2__ProductAttributeGroup__c createProductAttributeGroup(String strName, String businessObject, String strDescription, Boolean bTwoColumnDisplay) {
        Apttus_Config2__ProductAttributeGroup__c productAttributeGroup = new Apttus_Config2__ProductAttributeGroup__c();
        productAttributeGroup.Name = strName;
        productAttributeGroup.Apttus_Config2__BusinessObject__c = businessObject;
        productAttributeGroup.Apttus_Config2__Description__c = strDescription;
        productAttributeGroup.Apttus_Config2__TwoColumnAttributeDisplay__c = bTwoColumnDisplay;
        
        return productAttributeGroup;
    }
    
    /*
     * Create Product Attribute
     */
    public static Apttus_Config2__ProductAttribute__c createProductAttribute(String strAttributeGroupId, String strField, Boolean bIsHidden, Boolean bIsReadOnly, String strProductAttributeValueField, Integer iSequence) {
        Apttus_Config2__ProductAttribute__c pa = new Apttus_Config2__ProductAttribute__c();
        pa.Apttus_Config2__AttributeGroupId__c = strAttributeGroupId;
        pa.Apttus_Config2__Field__c = strField;
        pa.Apttus_Config2__IsHidden__c = bIsHidden;
        pa.Apttus_Config2__IsReadOnly__c = bIsReadOnly;
        pa.Apttus_Config2__ProductAttributeValueField__c = strProductAttributeValueField;
        pa.Apttus_Config2__Sequence__c = iSequence;
        
        return pa;
    }
    
    /* 
     * Create Proposal Product Attribute value                                        
     */
    public static Apttus_QPConfig__ProposalProductAttributeValue__c createProposalProductAttributeValue(String strProposalLineItemId){
        
        Apttus_QPConfig__ProposalProductAttributeValue__c ProposalProductAttributeValue = new Apttus_QPConfig__ProposalProductAttributeValue__c ();
        ProposalProductAttributeValue.Apttus_QPConfig__LineItemId__c = strProposalLineItemId;
               
        return ProposalProductAttributeValue;
    }   
    
    /*
     * Create Price Dimension
     */
    public static Apttus_Config2__PriceDimension__c createPriceDimension(String dimensionName, String contextType, String businessObject,
                                                                        String field, Id productAttributeId) {
        Apttus_Config2__PriceDimension__c priceDimension = new Apttus_Config2__PriceDimension__c();
        priceDimension.Name = dimensionName;
        priceDimension.Apttus_Config2__ContextType__c = contextType;
        priceDimension.Apttus_Config2__BusinessObject__c = businessObject;
        priceDimension.Apttus_Config2__Datasource__c = field;
        priceDimension.Apttus_Config2__AttributeId__c = productAttributeId;
        
        return priceDimension;
    }
    
    /*
     * Create Price Matrix
     */
    public static Apttus_Config2__PriceMatrix__c createPriceMatrix(Id priceListItemId, Id dimension1Id, Id dimension2Id, Id dimension3Id, String strDimension1ValueType, String strDimension2ValueType, String strDimension3ValueType) {
        Apttus_Config2__PriceMatrix__c pm = new Apttus_Config2__PriceMatrix__c();
        pm.Apttus_Config2__Sequence__c = 1;
        pm.Apttus_Config2__PriceListItemId__c = priceListItemId;
        pm.Apttus_Config2__Dimension1Id__c = dimension1Id;
        pm.Apttus_Config2__Dimension1ValueType__c = strDimension1ValueType;
        pm.Apttus_Config2__Dimension2Id__c = dimension2Id;
        pm.Apttus_Config2__Dimension2ValueType__c = strDimension2ValueType;
        pm.Apttus_Config2__Dimension3Id__c = dimension3Id;
        pm.Apttus_Config2__Dimension3ValueType__c = strDimension3ValueType;
        
        return pm;
    }
    
    /*
     * Create Price Matrix Entry
     */
    public static Apttus_Config2__PriceMatrixEntry__c createPriceMatrixEntry(String strPriceMatrixId, Integer iSequence, String strDimension1Value, String strDimension2Value, String strDimension3Value, decimal dPriceOverride, decimal dUnitPrice, decimal dAdjustmentAmount, String strAdjustmentType) {
        Apttus_Config2__PriceMatrixEntry__c pme = new Apttus_Config2__PriceMatrixEntry__c();
        pme.Apttus_Config2__PriceMatrixId__c = strPriceMatrixId;
        pme.Apttus_Config2__Sequence__c = iSequence;
        pme.Apttus_Config2__Dimension1Value__c = strDimension1Value;
        pme.Apttus_Config2__Dimension2Value__c = strDimension2Value;
        pme.Apttus_Config2__Dimension3Value__c = strDimension3Value;
        pme.Apttus_Config2__PriceOverride__c = dPriceOverride;
        pme.Apttus_Config2__UsageRate__c = dUnitPrice;
        pme.Apttus_Config2__AdjustmentAmount__c = dAdjustmentAmount;
        pme.Apttus_Config2__AdjustmentType__c = strAdjustmentType;
        
        return pme;
    }
    
    /*
     * Create Usage Price Tier
     */
    public static Apttus_Config2__UsagePriceTier__c createUsagePriceTier(String strLineItemId, String strPriceMatrixId, integer iSequence, String strDimension1Value, String strDimension2Value, String strDimension3Value, decimal dStartValue, decimal dEndValue, decimal dUnitPrice, decimal dPriceOverride, decimal dAdjustmentAmount, String strAdjustmentType) {
        Apttus_Config2__UsagePriceTier__c upt = new Apttus_Config2__UsagePriceTier__c();
        upt.Apttus_Config2__LineItemId__c = strLineItemId;
        upt.Apttus_Config2__PriceMatrixId__c = strPriceMatrixId;
        upt.Apttus_Config2__Dimension1Value__c = strDimension1Value;
        upt.Apttus_Config2__Dimension2Value__c = strDimension2Value;
        upt.Apttus_Config2__Dimension3Value__c = strDimension3Value;
        upt.Apttus_Config2__TierStartValue__c = dStartValue;
        upt.Apttus_Config2__TierEndValue__c = dEndValue;
        upt.Apttus_Config2__UsageRate__c = dUnitPrice;
        upt.Apttus_Config2__PriceOverride__c = dPriceOverride;
        upt.Apttus_Config2__AdjustmentAmount__c = dAdjustmentAmount;
        upt.Apttus_Config2__AdjustmentType__c = strAdjustmentType;
        upt.Apttus_Config2__Sequence__c = iSequence;
        
        return upt;
    }
    
    /*
     * Create Proposal Usage Price Tier
     */
    public static Apttus_QPConfig__ProposalUsagePriceTier__c createProposalUsagePriceTier(String strProposalLineItemId, String strPriceMatrixId, integer iSequence, String strDimension1Value, String strDimension2Value, String strDimension3Value, decimal dStartValue, decimal dEndValue, decimal dUnitPrice, decimal dPriceOverride, decimal dAdjustmentAmount, String strAdjustmentType) {
        Apttus_QPConfig__ProposalUsagePriceTier__c pupt = new Apttus_QPConfig__ProposalUsagePriceTier__c();
        pupt.Apttus_QPConfig__LineItemId__c = strProposalLineItemId;
        pupt.Apttus_QPConfig__PriceMatrixId__c = strPriceMatrixId;
        pupt.Apttus_QPConfig__Dimension1Value__c = strDimension1Value;
        pupt.Apttus_QPConfig__Dimension2Value__c = strDimension2Value;
        pupt.Apttus_QPConfig__Dimension3Value__c = strDimension3Value;
        pupt.Apttus_QPConfig__TierStartValue__c = dStartValue;
        pupt.Apttus_QPConfig__TierEndValue__c = dEndValue;
        pupt.Apttus_QPConfig__UsageRate__c = dUnitPrice;
        pupt.Apttus_QPConfig__PriceOverride__c = dPriceOverride;
        pupt.Apttus_QPConfig__AdjustmentAmount__c = dAdjustmentAmount;
        pupt.Apttus_QPConfig__AdjustmentType__c = strAdjustmentType;
        pupt.Apttus_QPConfig__Sequence__c = iSequence;
        
        return pupt;
    }
    
    // Create Account Location
    public static Apttus_Config2__AccountLocation__c createAccountLocation(String name, Id accountId){
        Apttus_Config2__AccountLocation__c accLoc = new Apttus_Config2__AccountLocation__c();
        accLoc.Name = name;
        accLoc.Apttus_Config2__AccountId__c = accountId;
        return accLoc;
    }
    
    public static List<Apttus_Config2__ConfigLineItemCustomFields__c> getConfigLineItemFields(){
        List<Apttus_Config2__ConfigLineItemCustomFields__c> configLineItemCustomFieldList = new List<Apttus_Config2__ConfigLineItemCustomFields__c>();

        Apttus_Config2__ConfigLineItemCustomFields__c objLineItemFields1 = new Apttus_Config2__ConfigLineItemCustomFields__c();
        objLineItemFields1.Name = 'System Properties';
        objLineItemFields1.Apttus_Config2__CustomFieldNames__c = 'Apttus_Config2__OptionId__r.Program__c,Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c,Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Program__c, Apttus_Config2__AttributeValueId__r.APTS_Referring_Owner_Account__c';
        objLineItemFields1.Apttus_Config2__CustomFieldNames2__c = 'Apttus_Config2__AttributeValueId__r.Aircraft_Hours__c,Apttus_Config2__ChargeType__c,Apttus_Config2__ListPrice__c,APTS_Modification_Type__c,APTS_Cabin_Class__c,Apttus_Config2__OptionId__r.Cabin_Class__c,Apttus_Config2__LocationId__c';
        objLineItemFields1.Apttus_Config2__CustomFieldNames3__c = 'Apttus_Config2__ParentBundleNumber__c,Apttus_Config2__OptionId__r.ProductCode,Apttus_Config2__AttributeValueId__r.APTS_Current_Value__c,APTS_Total_Current_Value__c,Apttus_Config2__ProductId__r.ProductCode,Apttus_Config2__ProductId__r.Program__c';
        objLineItemFields1.Apttus_Config2__CustomFieldNames4__c =  'Apttus_Config2__RelatedAdjustmentType__c,Apttus_Config2__RelatedAdjustmentAmount__c,Apttus_Config2__RelatedAdjustmentAppliesTo__c, Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c';
        objLineItemFields1.Apttus_Config2__CustomFieldNames5__c =  'APTS_Additional_Modification_Type__c,Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.APTS_Sales_Margin__c,APTS_New_Sale_or_Modification__c,Grace_Period_End_Date__c,Apttus_Config2__ConfigurationId__r.APTS_Modification_Type__c';
        configLineItemCustomFieldList.add(objLineItemFields1);
        
        Apttus_Config2__ConfigLineItemCustomFields__c objLineItemFields2 = new Apttus_Config2__ConfigLineItemCustomFields__c();
        objLineItemFields2.Name = 'Asset Pricing Ext';
        objLineItemFields2.Apttus_Config2__CustomFieldNames__c = 'Apttus_Config2__OptionId__r.Cabin_Class_Rank__c,Apttus_Config2__NetUnitPrice__c';
        objLineItemFields2.Apttus_Config2__CustomFieldNames2__c = 'Apttus_Config2__AdjustmentType__c,Apttus_Config2__NetAdjustmentPercent__c';
        objLineItemFields2.Apttus_Config2__CustomFieldNames3__c = 'APTPS_Referral_Referee_Type__c,Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__r.APTPS_Aggregated_Hourly_Rate__c';
        objLineItemFields2.Apttus_Config2__CustomFieldNames4__c =  'APTPS_FET_Percentage_Charged__c';
        objLineItemFields2.Apttus_Config2__CustomFieldNames5__c =  '';
        configLineItemCustomFieldList.add(objLineItemFields2);
        
        Apttus_Config2__ConfigLineItemCustomFields__c objLineItemFields3 = new Apttus_Config2__ConfigLineItemCustomFields__c();
        objLineItemFields3.Name = 'Modification';
        objLineItemFields3.Apttus_Config2__CustomFieldNames__c = 'Apttus_Config2__AttributeValueId__r.APTS_Override_Hours_To_Unexpire__c,Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c,Apttus_Config2__AttributeValueId__r.APTS_Hours_To_Unexpire__c';
        objLineItemFields3.Apttus_Config2__CustomFieldNames2__c = 'Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c,Apttus_Config2__LocationId__c';
        objLineItemFields3.Apttus_Config2__CustomFieldNames3__c = 'Apttus_Config2__AttributeValueId__r.Non_Standard__c,Apttus_Config2__AttributeValueId__r.Death_or_Disability_Recipient__c,Apttus_Config2__AttributeValueId__r.APTS_Death_or_Disability_Recipient_2__c';
        objLineItemFields3.Apttus_Config2__CustomFieldNames4__c =  'Apttus_Config2__AttributeValueId__r.Death_or_Disability_Clause__c';
        objLineItemFields3.Apttus_Config2__CustomFieldNames5__c =  'APTPS_List_Price__c,Apttus_Config2__AttributeValueId__r.APTPS_List_Price__c';
        configLineItemCustomFieldList.add(objLineItemFields3);
        
        Apttus_Config2__ConfigLineItemCustomFields__c objLineItemFields4 = new Apttus_Config2__ConfigLineItemCustomFields__c();
        objLineItemFields4.Name = 'Asset Pricing';
        objLineItemFields4.Apttus_Config2__CustomFieldNames__c = 'Apttus_Config2__AttributeValueId__r.APTS_Bonus_Hours__c,Apttus_Config2__AttributeValueId__r.Amount__c';
        objLineItemFields4.Apttus_Config2__CustomFieldNames2__c = 'Apttus_Config2__IsAssetPricing__c,Apttus_Config2__AttributeValueId__r.Requested_Hours__c,Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c';
        objLineItemFields4.Apttus_Config2__CustomFieldNames3__c = 'Apttus_Config2__AttributeValueId__r.Override_Requested_Hours__c,APTPS_Price_Factor__c,APTPS_Purchased_Hourly_Rate__c';
        objLineItemFields4.Apttus_Config2__CustomFieldNames4__c =  'Apttus_Config2__AttributeValueId__r.APTS_Waive_FET__c,Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c';
        objLineItemFields4.Apttus_Config2__CustomFieldNames5__c =  'Apttus_Config2__AttributeValueId__r.APTS_All_Inclusive_Blended_Rate__c,Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c';
        configLineItemCustomFieldList.add(objLineItemFields4);
        
        Apttus_Config2__ConfigLineItemCustomFields__c objLineItemFields5 = new Apttus_Config2__ConfigLineItemCustomFields__c();
        objLineItemFields5.Name = 'Transaction Type';
        objLineItemFields5.Apttus_Config2__CustomFieldNames__c = '';
        objLineItemFields5.Apttus_Config2__CustomFieldNames2__c = 'APTS_Transaction_Type__c';
        objLineItemFields5.Apttus_Config2__CustomFieldNames3__c = '';
        objLineItemFields5.Apttus_Config2__CustomFieldNames4__c =  '';
        objLineItemFields5.Apttus_Config2__CustomFieldNames5__c =  '';
        configLineItemCustomFieldList.add(objLineItemFields5);
        
        Apttus_Config2__ConfigLineItemCustomFields__c objLineItemFields6 = new Apttus_Config2__ConfigLineItemCustomFields__c();
        objLineItemFields6.Name = 'SNL Pricing Fields';
        objLineItemFields6.Apttus_Config2__CustomFieldNames__c = 'Apttus_Config2__AttributeValueId__r.APTPS_Is_Non_Premium_MMF_Incentive__c,Apttus_Config2__AttributeValueId__r.APTPS_Hours__c';
        objLineItemFields6.Apttus_Config2__CustomFieldNames2__c = '';
        objLineItemFields6.Apttus_Config2__CustomFieldNames3__c = '';
        objLineItemFields6.Apttus_Config2__CustomFieldNames4__c =  '';
        objLineItemFields6.Apttus_Config2__CustomFieldNames5__c =  '';
        configLineItemCustomFieldList.add(objLineItemFields6);

        return configLineItemCustomFieldList;
    }
    
    public static Apttus_Config2__LineItem__c getLineItem(ID cartID2
                                                            , Apttus_Proposal__Proposal__c objProposal
                                                            , Apttus_Config2__PriceListItem__c objPriceListItem
                                                            , Product2 objProduct
                                                            , ID productID
                                                            , ID optionID
                                                            , String lineType
                                                            , Integer lineNumber
                                                            , Integer itemSequence
                                                          	, Integer primaryLineNumber
                                                            , Integer optionSequence
                                                          	, Integer parentBundleNumber
                                                            , Integer quantity){

        Apttus_Config2__LineItem__c objLineItem = new Apttus_Config2__LineItem__c();
        objLineItem.Apttus_Config2__ConfigurationId__c          = cartID2;
        objLineItem.Apttus_Config2__ProductId__c                = productID;
        objLineItem.Apttus_Config2__BaseProductId__c            = productID;
        objLineItem.Apttus_Config2__OptionId__c                 = optionID;
        objLineItem.Apttus_Config2__LineType__c                 = lineType;
        objLineItem.Apttus_Config2__ItemSequence__c             = itemSequence;
        objLineItem.Apttus_Config2__LineNumber__c               = lineNumber;
        objLineItem.Apttus_Config2__LineSequence__c             = lineNumber;
        objLineItem.Apttus_Config2__PrimaryLineNumber__c        = primaryLineNumber;
        objLineItem.Apttus_Config2__OptionSequence__c           = optionSequence;
        objLineItem.Apttus_Config2__ParentBundleNumber__c       = parentBundleNumber;
        objLineItem.Apttus_Config2__Quantity__c                 = quantity;
        objLineItem.Apttus_Config2__LineStatus__c               = 'New';   
        objLineItem.Apttus_Config2__IsPrimaryLine__c            = TRUE;
        
        //PLI
        objLineItem.Apttus_Config2__PriceListItemId__c          = objPriceListItem.ID;
        objLineItem.Apttus_Config2__PriceListId__c              = objPriceListItem.Apttus_Config2__PriceListId__c;
        objLineItem.Apttus_Config2__ChargeType__c               = 'Purchase Price';                                                       
        /*objLineItem.Apttus_Config2__ChargeType__c               = objPriceListItem.Apttus_Config2__ChargeType__c;
        objLineItem.Apttus_Config2__PriceType__c                = objPriceListItem.Apttus_Config2__PriceType__c;
        objLineItem.Apttus_Config2__Frequency__c                = objPriceListItem.Apttus_Config2__Frequency__c;
        objLineItem.Apttus_Config2__SellingFrequency__c         = objPriceListItem.Apttus_Config2__Frequency__c;
        objLineItem.Apttus_Config2__PriceMethod__c              = objPriceListItem.Apttus_Config2__PriceMethod__c;
        objLineItem.Apttus_Config2__BasePriceMethod__c          = objPriceListItem.Apttus_Config2__PriceMethod__c;
        objLineItem.Apttus_Config2__RollupPriceMethod__c        = objPriceListItem.Apttus_Config2__PriceMethod__c;
        objLineItem.Apttus_Config2__PriceUom__c                 = objPriceListItem.Apttus_Config2__PriceUom__c;
        objLineItem.Apttus_Config2__Uom__c                      = objPriceListItem.Apttus_Config2__PriceUom__c;
		*/
        return objLineItem;
    }
    
    //Create Agreement
    public static Apttus__APTS_Agreement__c createAgreement(String agName, String status, Id acId, Id qId) {
        Apttus__APTS_Agreement__c ag = new Apttus__APTS_Agreement__c();
        ag.Name = agName;
        ag.Agreement_Status__c = status;
        ag.Apttus__Account__c = acId;
        ag.Apttus_QPComply__RelatedProposalId__c = qId;
        ag.Active_Until_Date__c = Date.today();
        ag.Grace_Period_End_Date__c = Date.today();
        ag.Funding_Date__c = Date.today();
        return ag;
    }
    
    //Create Agreement Line Item
    public static Apttus__AgreementLineItem__c createAgreementLineItem(Id agId, Product2 prod, String liStatus, Boolean isPrimary, Id locId) {
        Apttus__AgreementLineItem__c agALI = new Apttus__AgreementLineItem__c();
        agALI.Apttus__AgreementId__c = agId;
        agALI.Apttus_CMConfig__OptionId__c = prod.Id;
        agALI.Option_Product_Name__c = prod.Name;
        agALI.Apttus_CMConfig__LineNumber__c = 1;
        agALI.Apttus__Quantity__c = 1;
        agALI.Apttus_CMConfig__LineStatus__c = liStatus;
        agALI.Option_Product_Family_System__c = prod.Family;
        agALI.Apttus_CMConfig__IsPrimaryLine__c = isPrimary;
        agALI.Apttus_CMConfig__LocationId__c = locId;
        return agALI;
    }
    
    //Create Asset Line Item
    public static Apttus_Config2__AssetLineItem__c createAssetLI(Id prodId, String liType, String status, Id agId) {
        Apttus_Config2__AssetLineItem__c assetLi = new Apttus_Config2__AssetLineItem__c();
        assetLi.Apttus_Config2__OptionId__c = prodId;
        assetLi.Apttus_Config2__LineType__c = liType;
        assetLi.Apttus_Config2__AssetStatus__c = status;
        assetLi.Apttus_CMConfig__AgreementId__c = agId;
        return assetLi;
    }
    
    //Create Proposal Line Item - This method is very specific to APTS_QuoteProposalBaseTriggerTest
    public static Apttus_Proposal__Proposal_Line_Item__c getPropLI(Id prodId, String liStatus, Id qId, String optName, Id derId) {
        Apttus_Proposal__Proposal_Line_Item__c qli = new Apttus_Proposal__Proposal_Line_Item__c();
        qli.Apttus_Proposal__Product__c = prodId;
        qli.Apttus_QPConfig__LineStatus__c = liStatus;
        qli.Apttus_Proposal__Proposal__c = qId;
        qli.Option_Product_Name__c = optName;
        qli.Apttus_QPConfig__DerivedFromId__c = derId;
        return qli;
    }
}