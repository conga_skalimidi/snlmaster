/************************************************************************************************************************
@Name: APTPS_CLMFields
@Author: Conga PS Dev Team
@CreateDate: 28 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMFields implements APTPS_CLMFieldsCallable {
    private static String dsMetadataName = 'APTPS_CLMFields_Registry__mdt';
    public static Map<String, String> prodCodeClassNameMap = new Map<String, String>();
    
    public APTPS_CLMFields() {
        setProdCodeClassNameMap();        
    }
    
    /** 
    @description: Find out the APEX class implementation for the given productCode
    @param: Product Code
    @return: APEX Class name
    */
    private String findCLMFields_Implementation(String productCode) {
        String className = null;
        if(productCode != null && !prodCodeClassNameMap.isEmpty() 
           && prodCodeClassNameMap.containsKey(productCode)) {
               className = prodCodeClassNameMap.get(productCode);
           }
        system.debug('APEX Class Details --> '+className);
        return className;
    }
    
    /** 
    @description: Execute product specific Deal Summary logic.
    @param: Product code and arguments
    @return: 
    */
    public Object call(Map<String,Object> productArgMap) {
   
        system.debug('Arguments to call method --> '+productArgMap);
        String retMsg = null;
        for(String productCode : productArgMap.keySet()){
         system.debug('APTPS_CLMFields productCode -->'+productCode);
            if(!String.isEmpty(productCode)) {
                Object args = productArgMap.get(productCode);
                if(args == null) 
                    throw new ExtensionMalformedCallException('Agreement data is missing.');
                String className = findCLMFields_Implementation(productCode);
                if(className != null) {
                    system.debug('APTPS_CLMFields Class Name -->'+className);
                    APTPS_CLMFieldsInterface dsObj = (APTPS_CLMFieldsInterface)Type.forName(className).newInstance();
                    dsObj.populateCLMFields(args);
                    updateModificationsFields(args);
                    retMsg = 'SUCCESS';
                }
            }
        }
        
        return retMsg;
    }
    
    /** 
    @description: Custom Exception implementation
    @param:
    @return: 
    */
    public class ExtensionMalformedCallException extends Exception {
        public String errMsg = '';
        public void ExtensionMalformedCallException(String errMsg) {
            this.errMsg = errMsg;
        }
    }
    
    /** 
    @description: Read CLM Fields Registry metadata and prepare the Map
    @param:
    @return: 
    */
    private void setProdCodeClassNameMap() {
        List<APTPS_CLMFields_Registry__mdt> fieldsSummaryHandlers = [SELECT Product_Code__c, Class_Name__c FROM APTPS_CLMFields_Registry__mdt];
        for(APTPS_CLMFields_Registry__mdt handler : fieldsSummaryHandlers) {
            if(handler.Product_Code__c != null && handler.Class_Name__c != null) 
                prodCodeClassNameMap.put(handler.Product_Code__c, handler.Class_Name__c);
        }
    }
    
    /** 
    @description: Update Fields for Letter Agreement Extension, Add Enhancements and other Modifications
    @param: AgreementList    
    */
    private void updateModificationsFields(Object args) {
        List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
        set<Id> proposalIds = new set<Id>();
        Map<id,Apttus_Proposal__Proposal_Line_Item__c> pliMap = new Map<id,Apttus_Proposal__Proposal_Line_Item__c>();
        List<String> enhancementsList = new List<String>{APTS_ConstantUtil.SNL_NJA_ENHANCEMENT,APTS_ConstantUtil.SNL_NJE_ENHANCEMENT };
        List<String> modificationsList = new List<String>{APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION,APTS_ConstantUtil.LEASE_TERMINATION,APTS_ConstantUtil.DEFERMENT_MORATORIUM,APTS_ConstantUtil.REPURCHASE};
        List<String> additioanlModList = new List<String>{APTS_ConstantUtil.UNRELATED_FULL};
        for(Apttus__APTS_Agreement__c agr : agrList) {
            if(modificationsList.contains(agr.APTS_Modification_Type__c) || additioanlModList.contains(agr.APTS_Additional_Modification_Type__c)){
                proposalIds.add(agr.Apttus_QPComply__RelatedProposalId__c);
            }         
            
        }
        for(Apttus_Proposal__Proposal_Line_Item__c pli : [SELECT Id, Apttus_Proposal__Proposal__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c, 
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Closing_Date__c, 
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Minimum_Commitment_Date__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.Delayed_Start_Date__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.APTS_Original_Agreement__r.APTPS_Agreement_Extension__r.APTPS_Tail_Number__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.APTS_Original_Agreement__r.APTPS_Agreement_Extension__r.APTPS_Number_of_Engines__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.APTS_Original_Agreement__r.APTPS_Agreement_Extension__r.APTPS_Legal_Name__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.APTS_Original_Agreement__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.APTS_Original_Agreement__r.APTPS_Agreement_Extension__r.APTPS_Manufacturer__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.APTS_Original_Agreement__r.APTPS_Agreement_Extension__r.APTPS_Cabin_Class_Size__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.APTS_Original_Agreement__r.APTPS_Agreement_Extension__r.APTPS_Serial_Number__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.APTS_Original_Agreement__r.APTPS_Agreement_Extension__r.APTPS_Actual_Delivery_Date__c,
                                                        Apttus_Proposal__Proposal__r.APTS_Modification_Type__c   
                                                        FROM Apttus_Proposal__Proposal_Line_Item__c 
                                                        where Apttus_Proposal__Proposal__c IN : proposalIds 
                                                        AND Apttus_QPConfig__IsPrimaryLine__c = TRUE 
                                                        AND Apttus_QPConfig__AssetLineItemId__c!=null 
                                                        AND Apttus_QPConfig__LineType__c =:APTS_ConstantUtil.LINE_TYPE 
                                                        AND Apttus_Proposal__Product__r.ProductCode NOT IN :enhancementsList]) {
            
            pliMap.put(pli.Apttus_Proposal__Proposal__c, pli);      
            
        }
        system.debug('proposalliMap-->'+pliMap);
        for(Apttus__APTS_Agreement__c agObj : agrList) {
            system.debug('modificationType-->'+agObj.APTS_Modification_Type__c);
            if(!pliMap.isEmpty() && APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equalsIgnoreCase(agObj.APTS_Modification_Type__c)){
                 agObj.APTPS_Minimum_Commitment_Date__c = pliMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Minimum_Commitment_Date__c; 
                 agObj.APTPS_Closing_Date__c = pliMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Closing_Date__c; 
                 agObj.Apttus__Contract_Start_Date__c = pliMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c; 
                 agObj.Delayed_Start_Date__c = pliMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.Delayed_Start_Date__c; 
                 agObj.Apttus__Contract_End_Date__c = agObj.APTS_Requested_End_Date__c;
                 agObj.Funding_Status__c = APTS_ConstantUtil.FUNDED_NOTREQUIRED ;
            }
            if(APTS_ConstantUtil.ADD_ENHANCEMENTS.equalsIgnoreCase(agObj.APTS_Modification_Type__c)) {
                agObj.Funding_Status__c = APTS_ConstantUtil.FUNDED_NOTREQUIRED ; 
            }
             
            if(APTS_ConstantUtil.REPURCHASE.equalsIgnoreCase(agObj.APTS_Modification_Type__c)) {
                agObj.Funding_Status__c = APTS_ConstantUtil.PENDING_REFUND; 
                system.debug('agObj-->'+agObj);
            }
            if(APTS_ConstantUtil.LEASE_TERMINATION.equalsIgnoreCase(agObj.APTS_Modification_Type__c)) {               
                agObj.Funding_Status__c = APTS_ConstantUtil.FUNDED_NOTREQUIRED;
                agObj.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.NOT_SUBMITTED;
            }
            //GCM-13045,GCM-13046 for unrelated Agreement Flow
            if(APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(agObj.APTS_Additional_Modification_Type__c)) {
                agObj.Funding_Status__c = APTS_ConstantUtil.FUNDED_NOTREQUIRED; 
            }
        }
    } 
}