/************************************************************************************************************************
@Name: APTPS_CLMFieldsTest
@Author: Conga PS Dev Team
@CreateDate: 20 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

@isTest
public class APTPS_CLMFieldsTest {
    @testSetup
    public static void makeData(){
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial,Share';
        insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
          
       
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
               
        Apttus_Proposal__Proposal__c corpNJEQuote = APTS_CPQTestUtility.createProposal('CLM Automation Corporate NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        corpNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        corpNJEQuote.CurrencyIsoCode = APTS_ConstantUtil.CUR_EUR;
        proposalList.add(corpNJEQuote);
        
        Apttus_Proposal__Proposal__c demoQuote = APTS_CPQTestUtility.createProposal('CLM Automation Demo  Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoQuote.CurrencyIsoCode = APTS_ConstantUtil.CUR_USD;
        proposalList.add(demoQuote);
        
        Apttus_Proposal__Proposal__c shareCLMLifeCycleQuote = APTS_CPQTestUtility.createProposal('Share CLM Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        shareCLMLifeCycleQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        shareCLMLifeCycleQuote.CurrencyIsoCode = APTS_ConstantUtil.CUR_USD;
        proposalList.add(shareCLMLifeCycleQuote);
         
        insert proposalList;
      
        
        
        List<Apttus__APTS_Agreement__c> agList = new List<Apttus__APTS_Agreement__c>();
        //Corporate NJE Agreement lifecycle test data
        
        Apttus__APTS_Agreement__c ctAg = new Apttus__APTS_Agreement__c();
        ctAg.Name = 'CLM Automation Corporate NJE Agreement';
        ctAg.Agreement_Status__c = 'Draft';
        ctAg.Apttus__Account__c = accToInsert.Id;
        ctAg.Apttus_QPComply__RelatedProposalId__c = corpNJEQuote.Id;
        ctAg.APTPS_Program_Type__c = APTS_ConstantUtil.CORP_TRIAL;
        ctAg.Apttus__Status__c = 'Request';
        ctAg.Apttus__Status_Category__c = 'Request';
        ctAg.CurrencyIsoCode = 'EUR';
        ctAg.APTPS_Term_Months__c = String.valueOf(6);
        ctAg.APTPS_Term_End_Date__c = Date.today();
        agList.add(ctAg);
        
        Apttus__APTS_Agreement__c demoAg = new Apttus__APTS_Agreement__c();
        demoAg.Name = 'CLM Automation Demo NJA Agreement';
        demoAg.Agreement_Status__c = 'Draft';
        demoAg.Apttus__Account__c = accToInsert.Id;
        demoAg.Apttus_QPComply__RelatedProposalId__c = demoQuote.Id;
        demoAg.APTPS_Program_Type__c = APTS_ConstantUtil.DEMO;
        demoAg.Apttus__Status__c = 'Request';
        demoAg.Apttus__Status_Category__c = 'Request';
        demoAg.CurrencyIsoCode = 'USD';
        demoAg.APTPS_Term_Months__c = String.valueOf(6);
        demoAg.APTPS_Term_End_Date__c = Date.today();
        agList.add(demoAg);
        
        Apttus__APTS_Agreement__c shareCLMLifeCycleAg = new Apttus__APTS_Agreement__c();
        shareCLMLifeCycleAg.Name = 'CLM LifeCycle Share Agreement';
        shareCLMLifeCycleAg.Agreement_Status__c = 'Draft';
        shareCLMLifeCycleAg.Apttus__Account__c = accToInsert.Id;
        shareCLMLifeCycleAg.Apttus_QPComply__RelatedProposalId__c = shareCLMLifeCycleQuote.Id;
        shareCLMLifeCycleAg.APTPS_Program_Type__c = APTS_ConstantUtil.SHARE;
        shareCLMLifeCycleAg.Apttus__Status__c = 'Request';
        shareCLMLifeCycleAg.Apttus__Status_Category__c = 'Request';
        shareCLMLifeCycleAg.CurrencyIsoCode = 'USD';
        agList.add(shareCLMLifeCycleAg);
        
        insert agList;

        Test.stopTest();
    }
    
    @isTest
    public static void testCT(){
        Test.startTest();
        
        //Corporate NJE Agreement lifecycle testing
        Apttus__APTS_Agreement__c ctAg = [SELECT Id, Apttus__Status__c, Apttus__Status_Category__c FROM Apttus__APTS_Agreement__c 
                                         WHERE APTPS_Program_Type__c =:APTS_ConstantUtil.CORP_TRIAL LIMIT 1];
        ctAg.Apttus__Status__c = 'Ready for Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Other Party Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        Payment_Entry__c payObj = new Payment_Entry__c();
        payObj.Agreement__c = ctAg.Id;
        payObj.CurrencyIsoCode = 'EUR';
        payObj.Payment_Amount__c = 1000;
        payObj.Payment_Date__c = Date.today();
        payObj.Payment_Method__c = 'Wire';
        insert payObj;
        
        ctAg.Apttus__Status__c = 'Other Party Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Fully Signed';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
       APTPS_ActivateAgreementCtrl.activateAgreement(ctAg.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void testDemo(){
        Test.startTest();
        
        //Corporate NJE Agreement lifecycle testing
        Apttus__APTS_Agreement__c ctAg = [SELECT Id, Apttus__Status__c, Apttus__Status_Category__c FROM Apttus__APTS_Agreement__c 
                                         WHERE APTPS_Program_Type__c =:APTS_ConstantUtil.DEMO LIMIT 1];
        ctAg.Apttus__Status__c = 'Ready for Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Other Party Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        Payment_Entry__c payObj = new Payment_Entry__c();
        payObj.Agreement__c = ctAg.Id;
        payObj.CurrencyIsoCode = 'EUR';
        payObj.Payment_Amount__c = 1000;
        payObj.Payment_Date__c = Date.today();
        payObj.Payment_Method__c = 'Wire';
        insert payObj;
        
        ctAg.Apttus__Status__c = 'Other Party Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Fully Signed';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
       APTPS_ActivateAgreementCtrl.activateAgreement(ctAg.Id);
        Test.stopTest();
    }
    
    @isTest
    static void testShareCLM() {
        try{
            Apttus__APTS_Agreement__c ag = [SELECT Id, Agreement_Status__c, Chevron_Status__c, APTS_Path_Chevron_Status__c, Funding_Status__c, 
                                            Document_Status__c, Apttus_Approval__Approval_Status__c, APTPS_Owner_Signed__c, APTPS_Request_Aircraft_Positioning__c, 
                                            Accrual_Date__c, APTPS_Aircraft_Positioned_State__c, APTPS_Aircraft_Positioned_Date__c, Apttus__Status__c, Apttus__Status_Category__c 
                                            FROM Apttus__APTS_Agreement__c 
                                            WHERE CurrencyIsoCode = 'USD' AND APTPS_Program_Type__c =: APTS_ConstantUtil.SHARE LIMIT 1];
            
            if(ag != null) {
                //Document generated
                ag.Apttus__Status__c = APTS_ConstantUtil.READY_FOR_SIGN;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
                //Send for Review
                ag.Apttus__Status__c = APTS_ConstantUtil.APTTUS_OTHER_PARTY_REVIEW;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Submit For Approvals
                ag.Apttus__Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Submit For Approvals
                ag.Apttus__Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Rejected
                ag.Apttus__Status__c = APTS_ConstantUtil.APPR_REJ;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Reset the status to execute Approved scenario
                ag.Apttus__Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Approved
                ag.Apttus__Status__c = APTS_ConstantUtil.APPR_REQ;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Send for signatures
                ag.Apttus__Status__c = APTS_ConstantUtil.OTHER_PARTY_SIGN;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
                //Payment entry
                Payment_Entry__c payObj = new Payment_Entry__c();
                payObj.Agreement__c = ag.Id;
                payObj.CurrencyIsoCode = 'USD';
                payObj.Payment_Amount__c = 1000;
                payObj.Payment_Date__c = Date.today();
                payObj.Payment_Method__c = 'Wire';
                insert payObj;
                
                //Pending Positioning
                ag.APTPS_Owner_Signed__c = true;
                update ag;
                
                //Positioning Requested
                ag.APTPS_Request_Aircraft_Positioning__c = true;
                update ag;
                
                //Aircraft Positioned
                ag.APTPS_Aircraft_Positioned_State__c = 'Alaska';
                ag.APTPS_Aircraft_Positioned_Date__c = Date.today();
                update ag;
                
                //Reset Agreement status
                ag.Apttus__Status__c = APTS_ConstantUtil.OTHER_PARTY_SIGN;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
                //Fully Signed
                ag.Apttus__Status__c = APTS_ConstantUtil.FULLY_SIGNED;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
                //Activation
                ag.Apttus__Status__c = APTS_ConstantUtil.ACTIVATED;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_EFFECT;
                update ag;
            }
        } catch(Exception e) {
            System.debug('APTPS_CLMFieldsTest.testShareCLM --> Error while executing test class. Error details --> '+e);
        }
    }
}