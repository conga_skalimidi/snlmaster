/************************************************************************************************************************
 @Name: APTS_ConstantUtil
 @Author: Conga PS Dev Team
 @CreateDate:
 @Description: APTS_ConstantUtil. Used to define constants which are used across multilpe classes/triggers
 ************************************************************************************************************************
 @ModifiedBy: Conga PS Dev Team
 @ModifiedDate: 26/11/2020
 @ChangeDescription: Added more constants for Shares and Leases.
 ************************************************************************************************************************/
public class APTS_ConstantUtil{
    public static final string NEW_CARD_SALE_CONVERSION = 'New Card Sale Conversion';
    public static final string DIRECT_CONVERSION = 'Direct Conversion';
    public static final string TERMINATION = 'Termination';
    public static final string TERM_EXTENSION = 'Term Extension';
    public static final string EXPIRED_HOURS_CARD = 'Expired Hours Card';
    public static final string NJ_EUROPE_PROGRAM = 'NetJets Europe';
    public static final string AIRCRAFT_HOURS = 'Aircraft Hours';
    public static final string AIRCRAFT_REMAINING_HOURS = 'Aircraft Remaining Hours';
    public static final string NEW_SALE = 'New Sale';
    public static final string ADD_ENHANCEMENTS = 'Add Enhancements';
    public static final string REDEMPTION_PRODUCT_TYPE = 'Redemption';
    public static final string COMBO_PRODUCT_TYPE = 'Combo';
    public static final string ELITE_COMBO_PRODUCT_TYPE = 'Elite Combo';
    public static final string CLASSIC_COMBO_PRODUCT_TYPE = 'Classic Combo';
    public static final string COMBO_PRODUCT_NJE = 'Inclusive Combo Card';
    public static final string REFEREE_LINE = 'Referee Card';
    public static final string REFERRAL_LINE = 'Referral Card';
    
    //Program Type
    public static final string NETJETS_USD = 'NetJets U.S.';
    public static final string NETJETS_EUR = 'NetJets Europe';
    //PAV Field Mapping
    public static final string AIRCRAFT_HOURS_PAV = 'Aircraft_Hours__c';
    public static final string PRICE_LIST = 'APTPS_List_Price__c';
    public static final string REPURCHASE_PRICE_LIST = 'APTPS_Override_Repurchase_Price__c';
    //Lineitem Field Mapping
    public static final string FEDERAL_EXCISE_TAX = 'Federal Excise Tax';
    public static final string HALF_CARD_PREMIUM_FET = 'Half Card Premium FET';
    public static final string PURCHASE_PRICE = 'Purchase Price';
    public static final string LEASE_DEPOSIT = 'Lease Deposit';
    //AgreementLineitem Field Mapping
    public static final string COMBO_CARD_PREMIUM = 'Combo Card Premium';
    public static final string HALF_CARD_PREMIUM = 'Half Card Premium';
    //Assignment
    public static final string ASSIGNMENT = 'Assignment';
    public static final string CROSS_ACCOUNT_ASSIGNMENT = 'Cross-account assignment';
    public static final string INTRA_ACCOUNT_ASSIGNMENT = 'Intra-Account Assignment';
    //Agreement Status
    public static final string AGREEMENT_CONTRACT_GENERATED = 'Contract Generated';
    public static final string AGREEMENT_SALES_REVIEW = 'Sales Review';
    public static final string AGREEMENT_PENDING_FUNDING = 'Pending Funding';
    public static final string AGREEMENT_ACTIVE = 'Active';
    public static final string AGREEMENT_CANCELLED = 'Cancelled';
    public static final string AGREEMENT_CONTRACT_PENDING = 'Contract Pending';
    public static final string AGREEMENT_PENDING_ACTIVATION = 'Pending Activation';
    public static final string AGREEMENT_INACTIVE = 'Inactive';
    public static final string AGREEMENT_TERMINATED = 'Terminated';
    public static final string AGREEMENT_IN_MODIFICATION = 'In Modification';
    public static final string AGREEMENT_AMENDED = 'Amended';
    public static final string AGREEMENT_ON_HOLD = 'On Hold';
    public static final string AGREEMENT_MASTER_AGREEMENT = 'Master Agreement';
    public static final string AGREEMENT_DRAFT = 'Draft';
    public static final string AGREEMENT_IN_REVIEW = 'In Review';
    public static final string AGREEMENT_IN_APPROVALS = 'In Approvals';
    public static final string AGREEMENT_CONTRACT_APPROVED = 'Contract Approved';
    public static final string AGREEMENT_POSITIONING_REQUESTED = 'Positioning Requested';
    public static final string ACCRUAL = 'Accrual';
    public static final string LACONSOLIDATIONINPROGRESS = 'LA Consolidation In Progress';
    public static final string LACONSOLIDATIONCOMPLETED = 'LA Consolidation Completed';
    public static final string ASSETSTATUSACTIVE= 'Active';
    public static final string PURCHASEPRICE = 'Purchase Price';
    public static final string ASSETSTATUS= 'Active';
    //Apttus Status
    public static final string APTTUS_OTHER_PARTY_REVIEW = 'Other Party Review';
    public static final string APTTUS_AUTHER_CONTRACT = 'Author Contract';
    public static final string APTTUS_READY_FOR_SIGNATURE = 'Ready for Signatures';
    
    //Proposal Status
    public static final string APPROVED = 'Approved';
    public static final string FINALIZED = 'Finalized';
    public static final string LINE_AMENDED = 'Amended';
    public static final string CONFIG_SAVE_STATUS = 'Saved';
    //Agreement Record Types
    public static final string AGREEMENT_DEFAULT = 'Default';
    public static final string AGREEMENT_MASTER = 'Master';
    public static final string AGREEMENT_MOD = 'Modification';
    public static final string AGREEMENT_NJE_MOD = 'APTS_NJE_Modification';
    public static final string AGREEMENT_NJE_NEW = 'NJE_New_Card_Sale';
    public static final string AGREEMENT_NJUS_LEASE = 'NJUS_Lease_Purchase_Agreement';
    public static final string AGREEMENT_NJUS_MOD = 'APTS_NJUS_Modification';
    public static final string AGREEMENT_NJUS_NEW = 'NJUS_New_Card_Sale';
    public static final string AGREEMENT_NJUS_SHARE = 'NJUS_Share_Purchase_Agreement';
    public static final string AGREEMENT_HOLD = 'On_Hold';
    public static final string INTERIM_AGREEMENT_HOLD= 'APTPS_On_Hold_Interim_Lease';
    public static final string AGREEMENT_PENDING = 'Pending_Agreement';
    public static final string AGREEMENT_REFEREE = 'Referee';
    public static final string AGREEMENT_REFERRER = 'Referrer';
    public static final string AGREEMENT_TERMINATION = 'Termination';
    public static final string AGREEMENT_NJA_INTERIM_LEASE = 'NJA Interim Lease Agreement';
    public static final string AGREEMENT_ON_HOLD_INTERIM_LEASE = 'On Hold Interim Lease';
    public static final string AGREEMENT_NJE_INTERIM_LEASE = 'NJE Interim Lease Agreement';
    public static final string AGREEMENT_NJA_LEASE_PURCHASE = 'NJA Lease Purchase Agreement';
    public static final string AGREEMENT_NJE_LEASE_PURCHASE = 'NJE Lease Purchase Agreement';
    public static final string RECORDTYPE = 'RecordType';
    public static final string PRODUCTCODE = 'ProductCode';
    public static final string NJA_INTERIM_CHILD = 'APTPS_NJA_Interim_Lease_Agreement';
    public static final string NJE_INTERIM_CHILD = 'APTPS_NJE_Interim_Lease_Agreement';
    public static final string APTPS_NJA_Unrelated_Assignor_Agreement= 'APTPS_NJA_Unrelated_Assignor_Agreement';
    public static final string APTPS_NJE_Unrelated_Assignor_Agreement= 'APTPS_NJE_Unrelated_Assignor_Agreement';
    public static final string APTPS_NJA_Substitution= 'APTPS_NJA_Substitution';
    public static final string APTPS_NJE_Substitution= 'APTPS_NJE_Substitution';
    
    //Asset Job Class and Status
    public static final string ORDERWORKFLOWQJOB = 'OrderWorkflowQJob';
    public static final string CREATEORDERQJOB= 'CreateOrderQJob';
    public static final string STATUS_QUEUED= 'Queued';
    public static final string STATUS_PROCESSING= 'Processing';
    public static final string STATUS_PREPARING= 'Preparing';
    
    //Deal Summary constants
    public static final string NJUS_DEMO = 'NJUS_DEMO';
    public static final string NJE_DEMO = 'NJE_DEMO';
    public static final string CT_PRODUCT_CODE = 'NJE_Corporate_Trial';
    public static final string LINE_TYPE = 'Product/Service';
    public static final string OPTION = 'Option';
    public static final string AIRCRAFT = 'Aircraft';
    public static final string NETJETS_PRODUCT = 'Netjets Product';
    
    
    //SNL Product names
    public static final string DEMO = 'Demo';
    public static final string CORP_TRIAL = 'Corporate Trial';
    public static final string SHARE = 'Share';
    public static final string LEASE = 'Lease';
    public static final string G450_AIRCRAFT = 'Gulfstream 450';
    public static final string GLOBAL7500_AIRCRAFT = 'Global 7500';
    public static final string SPLIT_BILL_CODE = 'SPLIT_BILLING_NJUS';
    public static final string PRICE_INCENTIVE_CODE = 'PURCHASE_PRICE_INCENTIVES_NJUS';
    public static final string ACC_FEE_CODE = 'ACC_FEES_NJUS';
    public static final string INTERIM_LEASE_CODE = 'INTERIM_LEASE_NJUS';
    public static final string PREPAY_FEE_CODE = 'PREPAYMENT_OF_FEES_NJUS';
    public static final string NJA_SHARE_CODE = 'NJUS_SHARE';
    public static final string NJE_SHARE_CODE = 'NJE_SHARE';
    public static final string NJA_LEASE_CODE = 'NJUS_LEASE';
    public static final string NJE_LEASE_CODE = 'NJE_LEASE';
    public static final string SPLIT_BILL_CODE_NJE = 'SPLIT_BILLING_NJE';
    public static final string PRICE_INCENTIVE_CODE_NJE = 'PURCHASE_PRICE_INCENTIVES_NJE';
    public static final string ACC_FEE_CODE_NJE = 'ACC_FEES_NJE';
    public static final string INTERIM_LEASE_CODE_NJE = 'INTERIM_LEASE_NJE';
    public static final string PREPAY_FEE_CODE_NJE = 'PREPAYMENT_OF_FEES_NJE';
    public static final string MMF_CHARGE_TYPE = 'Monthly Management Fee';
    public static final string MLF_CHARGE_TYPE = 'Monthly Lease Payment';
    public static final string OF_CHARGE_TYPE = 'Operating Fund';
    public static final string OHF_CHARGE_TYPE = 'Occupied Hourly Fee';
    public static final string LONG_RANGE_OHF_CHARGE_TYPE = 'Long Range OHF';
    public static final string OHF_CHARGE_TYPE_1 = 'Occupied Hourly Fee 1';
    public static final string OHF_CHARGE_TYPE_2 = 'Occupied Hourly Fee 2';
    public static final string OHF_CHARGE_TYPE_3 = 'Occupied Hourly Fee 3';
    public static final string OHF_CHARGE_TYPE_4 = 'Occupied Hourly Fee 4';
    public static final string FUEL_CHARGE_TYPE = 'Fuel Hourly Rate';
    public static final string SR1_CHARGE_TYPE = 'Supplemental Rate 1';
    public static final string SR2_CHARGE_TYPE = 'Supplemental Rate 2';
    public static final string FERRY_CHARGE_TYPE = 'Ferry Rate';
    public static final string CARD_GRADUATE_CREDIT_NJA = '10_Card_Graduate_Credit_Incentive_NJUS';
    public static final string CARD_GRADUATE_CREDIT_NJE = '10_Card_Graduate_Credit_Incentive_NJE';
    public static final string SNL_NJA_ENHANCEMENT = 'NJUS_ENHANCEMENT';
    public static final string SNL_NJE_ENHANCEMENT = 'NJE_ENHANCEMENT';
    public static final string SNL_ENTITLEMENT_AGR = 'Share/Lease Agreement';
    public static final string SNL_ENTITLEMENT_QUOTE = 'Proposal';
    public static final string SNL_ENTITLEMENT_ACCOUNT = 'Account';
    public static final string NJA_EARLY_OUT = 'NJA_EARLY_EXIT_OPTIONS';
    public static final string NJE_EARLY_OUT = 'NJE_EARLY_OUT_OPTION';
    public static final string SNL_ENHANCEMENT = 'Enhancement';
    public static final string NJUS_WAIVE_INTERNATIONAL_FEES= 'NJUS_WAIVE_INTERNATIONAL_FEES'; 
    public static final string NJUS_SIMULTANEOUS_USAGE  = 'NJUS_SIMULTANEOUS_USAGE'; 
    public static final string NJUS_LOCK_CURRENT_INTERCHANGE = 'NJUS_LOCK_CURRENT_INTERCHANGE';
    public static final string NJUS_UPGRADES = 'NJUS_UPGRADES';
    public static final string NJE_UPGRADES = 'NJE_UPGRADES';
    public static final string NJUS_GUARANTEED_RENEWAL_OPTION = 'NJUS_GUARANTEED_RENEWAL_OPTION';
    public static final string NJUS_NON_STANDARD = 'NJUS_NON_STANDARD';
    public static final string NJUS_INTERCHANGE_RATE_DEVIATION = 'NJUS_INTERCHANGE_RATE_DEVIATION';
    public static final string NJUS_ADDITIONAL_HOURS = 'NJUS_ADDITIONAL_HOURS';
    public static final string NJUS_WAIVE_FERRY_CHARGES = 'NJUS_WAIVE_FERRY_CHARGES';
    public static final string NJUS_WAIVE_REMARKETING_FEE = 'WAIVE_REMARKETING_FEE_NJUS';
    public static final string NJE_WAIVE_REMARKETING_FEE = 'WAIVE_REMARKETING_FEE_NJE';
    public static final string NJUS_REINSTATE_UNDERFLOWN_HOURS = 'REINSTATEMENT_OF_UNDER_FLOWN_HOURS_NJUS';
    public static final string NJE_REINSTATE_UNDERFLOWN_HOURS = 'REINSTATEMENT_OF_UNDER_FLOWN_HOURS_NJE';
    public static final string INTERIM_LEASE = 'Interim Lease';
    public static final string PREPAYMENT_OF_FEES = 'Prepayment of Fees';
    public static final string MMF_INCENTIVE_5X = '5% MMF Incentive';
    public static final string NJUS_RENEWAL_INCENTIVE = 'RENEWAL_INCENTIVE_NJUS';
    public static final string NJE_RENEWAL_INCENTIVE = 'RENEWAL_INCENTIVE_NJE';
    
    //Enhancement constants
    public static final string BLACKOUT_DAYS = 'Blackout (90 Days)';
    public static final string CARD_PEAK_PERIOD_DAYS = 'Card Peak Period Days (45 Days)';
    public static final string PEAK_PERIOD_DAYS = 'Peak Period (10 Days)';
    public static final string USAGE_TYPE_SEGMENTS = 'Segments';
    public static final string COLLECTIVE_SERVICE_AREA = 'Collective Service Area';
    public static final string UPGRADE_GULFSTREAM = 'Gulfstream';
    public static final string UPGRADE_GLOBAL = 'Global';
    public static final string NO_RESTRICTIONS = 'No restrictions';  
    public static final string UPGRADE_ONE_INTERCHANGE = '1:1 Interchange';   
    public static final string UPGRADE_APPLICABLE_INTERCHANGE = 'Applicable Interchange'; 
    
    
    
    public static final string NJ_SALES_ENTITY = 'NetJets Sales, Inc';
    public static final string NJI_SALES_ENTITY = 'NJI Sales, Inc';
    public static final string NJ_MANAGER_ENTITY= 'Netjets Aviation, Inc';
    public static final string NJI_MANAGER_ENTITY = 'NetJets International Inc';
    public static final string LIGHT_CABIN= 'Light';
    public static final string MID_CABIN= 'Mid-Size';
    public static final string SUPER_MID_CABIN= 'Super-Midsize';
    public static final string LARGE_CABIN = 'Large';

    //SNL - Agreement Lifecycle constants
    public static final string REQUEST = 'Request';
    public static final string IN_SIGN = 'In Signatures';
    public static final string READY_FOR_SIGN = 'Ready for Signatures';
    public static final string OTHER_PARTY_SIGN = 'Other Party Signatures';
    public static final string FULLY_SIGNED = 'Fully Signed';
    public static final string IN_EFFECT = 'In Effect';
    public static final string ACTIVATED = 'Activated';
    public static final string STATUS_ACTIVE = 'Active';
    public static final string PENDING_ACTIVATE = 'Pending Activation';
    public static final string DOC_GENERATED = 'Document Generated';
    public static final string DOC_PENDING = 'Document Pending';
    public static final string FUND_PENDING = 'Pending Funding';
    public static final string FUNDED_CONTRACT = 'Funded Contract';
    public static final string FUNDED_NOTREQUIRED = 'Funding Not Required';
    public static final string PENDING_REFUND = 'Pending Refund';
    public static final string REFUND_COMPLETED = 'Refund Completed';
    public static final string STATUS_DRAFT = 'Draft';
    public static final string DOC_SIGNED = 'Document Signed';
    public static final string CUR_USD = 'USD';
    public static final string CUR_EUR = 'EUR';
    public static final string CT_RECORD_TYPE = 'APTPS_NJE_Corporate_Trial';
    public static final string DEMO_NJE_RECORD_TYPE = 'APTPS_NJE_Demo_Agreement';
    public static final string DEMO_NJA_RECORD_TYPE = 'APTPS_NJUS_Demo_Agreement';
    public static final string SHARE_NJA_RECORD_TYPE = 'APTPS_NJA_Share_Purchase_Agreement';
    public static final string SHARE_NJE_RECORD_TYPE = 'APTPS_NJE_Share_Purchase_Agreement';
    public static final string LEASE_NJA_RECORD_TYPE = 'APTPS_NJA_Lease_Purchase_Agreement';
    public static final string LEASE_NJE_RECORD_TYPE = 'APTPS_NJE_Lease_Purchase_Agreement';
    public static final string AGREEMENT_OBJ = 'Apttus__APTS_Agreement__c';
    public static final string IN_AUTH = 'In Authoring';
    public static final string PENDING_POSITIONING= 'Pending Positioning';
    public static final string DOC_RECEIVED = 'Document Received';
    public static final string APPR_REQ = 'Approved Request';
    public static final string APPR_REJ = 'Rejected Approval';
    public static final string AC_POSITIONED = 'Aircraft Positioned';
    public static final string ACCRUAL_AC_POSITIONED = 'Accrual AC Positioned';
    public static final string ACCRUAL_PENDING_ACTIVATION = 'Accrual Pending Activation';
    public static final string ACCRUAL_ACTIVE = 'Accrual Active';
    public static final string CANCEL = 'Cancelled';
    public static final string CANCEL_REQ = 'Cancelled Request';
    public static final string FUNDED_SIGN_PENDING = 'Funded Contract Signature Pending';
    public static final string INTERIM_LEASE_FIELD_SET = 'APTPS_Interim_Lease_Agreement';
    public static final string INTERIM_POSITIONING = 'Interim Positioning';
    public static final string FUNDED_DEPOSIT = 'Funded Deposit';
    public static final string IN_AMENDMENT = 'In Amendment';
    public static final string NOT_SUBMITTED = 'Not Submitted';

    //SNL - Quote/Proposal Lifecycle constants
    public static final string APPROVAL_REQUIRED = 'Approval Required';
    public static final string GENERATED = 'Generated';
    public static final string PROPOSAL_SOLUTION = 'Proposal/Solution';
    public static final string PENDING_APPROVAL = 'Pending Approval';
    public static final string IN_REVIEW = 'In Review';
    public static final string IN_APPROVAL = 'In Approval';
    public static final string REJECTED = 'Rejected';
    public static final string QUOTE_DRAFT = 'Draft';
    public static final string QUOTE_OBJ = 'Apttus_Proposal__Proposal__c';
    public static final string NEGOTIATION = 'Contract Negotiation';
    
    //Quote Collaboration constants
    public static final string STATUS_SUBMITTED = 'Submitted';
    public static final string STATUS_COMPLETED = 'Completed';
    public static final string STATUS_ACCEPTED = 'Accepted';
    public static final string STATUS_CANCELLED = 'Cancelled';
    public static final string STATUS_NEW = 'New';
    public static final string COLLAB_REQ_OBJECT_API = 'Apttus_Config2__CollaborationRequest__c';
    public static final string COLLAB_REQ = 'Collaboration Request';
    public static final string PAGE_ACTION = 'Finalize';
    
    //OHF Increace Text
    public static final string OHF_NA = 'Not Applicable';
    public static final string OHF_TBD = 'To be determined and take effect on or after the twenty-fourth/thirty-first month following delivery of the Aircraft to Sales by the manufacturer.';
    public static final string OHF_WB_NODATE = 'to take effect on the twenty-fourth/thirty-first month following delivery of the Aircraft to Sales by the manufacturer.';
    public static final string OHF_WB_WITH_FUTURE_DATE = 'to take effect on the sixtieth month following delivery of the Aircraft to Sales by the manufacturer.';
    public static final string OHF_WB_WITH_PAST_DATE = 'and has already been incorporated into the Occupied Hourly Fee specified in Item C.5 above.';
    public static final string OHF_EFFECT_ON = 'effective on';
    
    //Share & Lease Modification constants
    public static final string LETTER_AGREEMENT_EXTENSION = 'Letter Agreement Extension';
    public static final string LEASE_TERMINATION = 'Lease Termination';
    public static final string REPURCHASE = 'Repurchase';
    public static final string REPURCHASE_CT = 'Repurchase Price';
    public static final string REMARKETING_CT = 'Remarketing Fee';
    public static final string FLOW_ASSIGNMENT = 'Flow Assignment';
    public static final string DEFERMENT_MORATORIUM = 'Deferment/Moratorium';
    public static final string RENEWAL = 'Renewal';
    public static final string CATALOG_URL = '/apex/Apttus_QPConfig__ProposalConfiguration?id=';
    public static final string RELATED_PARTIAL = 'Related (Partial)';
    public static final string RELATED_FULL = 'Related (Full)';
    public static final string ASSIGNMENT_CT = 'Assignment Fee';
    public static final string BILLING_TASK_SUBJECT = 'Billing to process the credits';
    public static final string ACCOUNTING_TASK_SUBJECT = 'Accounting to integrate credits into Great Plains.(outside of Apttus)';
    public static final string OAA_TASK_SUBJECT = 'OAA to create the hours reconciliation. (outside of Apttus)';
    public static final string ACCOUNTS_RECEIVABLE_TASK_SUBJECT = 'Accounts Receivable to enter all invoices outstanding into Apttus Payment Entry Screen';
    public static final string CONTRACT_ADMIN_TASK_SUBJECT = 'Contract Specialist to generate the Final Accounting Statement out of Apttus and send to Owner';
    public static final string NJA_REPURCHASE_ACCOUNTING = 'NJA_REPURCHASE_ACCOUNTING';
    public static final string NJA_REPURCHASE_AR = 'NJA_REPURCHASE_AR';
    public static final string NJA_REPURCHASE_BILLING = 'NJA_REPURCHASE_BILLING';
    public static final string NJA_REPURCHASE_CA = 'NJA_REPURCHASE_CA';
    public static final string NJA_REPURCHASE_OAA = 'NJA_REPURCHASE_OAA';
    public static final string UNRELATED_FULL = 'Unrelated (Full)';
    public static final string CONTRACT = 'Contract';
    public static final string SHARE_SIZE_INCREASE = 'Share Size Increase';
    public static final string OPERATING_FUND_CT = 'Operating Fund';
    public static final string REDUCTION = 'Reduction';
    public static final string SUBSTITUTION = 'Substitution';
    public static final string NONE = 'None';
    public static final string AMENDMENT = 'Amendment to Premium/Non-Premium';
    public static final string TRADE = 'Trade';
    public static final string INCENTIVE_CT = 'Incentive';
    public static final string IS_AMENDMENT_FOR = 'Is Amendment For';
    
    
    // Interim lease Tail Number functionality constant
    public static final string CONTRACT_LENGTH = 'Contract Length';
    public static final string MONTHLY_LEASE_PAYMENT = 'Monthly Lease Payment';
    public static final string DIMENTION_VALUE = '36';
    
    //Consolidate Letter Agreement Constant
    public static final string HE_PRICING = 'High-Efficiency Pricing';
}