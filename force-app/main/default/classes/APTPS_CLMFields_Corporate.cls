/************************************************************************************************************************
@Name: APTPS_CLMFields_Corporate
@Author: Conga PS Dev Team
@CreateDate: 17 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMFields_Corporate {
    /** 
    @description: Coprporate Trail implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            set<Id> proposalIds = new set<Id>();
            set<Id> agrIds = new set<Id>();
            Map<Id,Apttus_Proposal__Proposal__c> proposalMap = new Map<Id,Apttus_Proposal__Proposal__c>();
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
            
            for(Apttus__APTS_Agreement__c agr : agrList) {
                proposalIds.add(agr.Apttus_QPComply__RelatedProposalId__c);
                agrIds.add(agr.Id);
            }
            for(Apttus_Proposal__Proposal__c pObj : [SELECT id, Name,Aircraft_Types__c,Approval_Primary_AE_User__c, Approval_Primary_SE_User__c, Approval_Sales_Director_User__c, Approval_RVP_User__c, APTPS_Approval_Secondary_SE_User__c,Legal_Entity_Name__c,Apttus_QPConfig__LocationId__c,Product_Line__c FROM Apttus_Proposal__Proposal__c WHERE Id IN :proposalIds]) {
                proposalMap.put(pObj.id,pObj);
            }
             Id CTRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.CT_RECORD_TYPE).getRecordTypeId();
            for(Apttus__APTS_Agreement__c agObj : agrList) {
                   
                    agObj.Funding_Status__c = APTS_ConstantUtil.FUND_PENDING ;
                    agObj.Document_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                    agObj.Agreement_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                    agObj.Chevron_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                    agObj.APTS_Path_Chevron_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                    agObj.RecordTypeId = CTRecTypeId;
                    //GCM-11175: For Demo, Asset line items, Order should not be there.
                    agObj.Apttus_CMConfig__AutoActivateOrder__c = false;
                    agObj.Generate_Card_Number__c = true;
                    if(!proposalMap.isEmpty()){
                        agObj.Account_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Primary_AE_User__c;
                        agObj.Primary_Sales_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Primary_SE_User__c;
                        agObj.Sales_Consultant__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Sales_Director_User__c;
                        agObj.RVP__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_RVP_User__c;
                        agObj.Secondary_Sales_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_Approval_Secondary_SE_User__c;
                         String agName = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Legal_Entity_Name__c+', '+proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Product_Line__c+' - '+proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Aircraft_Types__c;
                        agObj.Name = agName;
                        agObj.Document_Name__c = agName;
                        agObj.Apttus_CMConfig__LocationId__c=  proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__LocationId__c;

                    }
            }
            system.debug('agrList-->'+agrList);
                
            
        }
    }
}