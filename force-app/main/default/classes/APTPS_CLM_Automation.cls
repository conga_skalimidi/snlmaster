/************************************************************************************************************************
@Name: APTPS_CLM_Automation
@Author: Conga PS Dev Team
@CreateDate: 20 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public abstract class APTPS_CLM_Automation implements APTPS_CLM_Automation_Interface{
    public void createAgreementWithLines(Id proposalId) {
        try{
            if(proposalId != null) {
                Boolean isSuccess = Apttus_QPConfig.QPConfigWebService.acceptQuote(proposalId);
                system.debug('Outcome of agreement creation with line items for Quote/Proposal Id --> '+proposalId+' is --> '+isSuccess);
            }
        }catch(Exception e){
            system.debug('Error while creating agreement with line items. Error Details --> '+e.getStackTraceString());
        }
    }
    
    public void copyAttachment(Id agreementId, Id attachmentId) {
        try{
            if(agreementId != null && attachmentId != null)
                Apttus_Proposal.ProposalWebService.copyAttachment(agreementId,attachmentId);
        }catch(Exception e){
            system.debug('Error while copying attachment from Quote/Proposal to Agreement. Agreement Id --> '
                         +agreementId+' Error details --> '+e.getStackTraceString());
        } 
    }
    
    abstract void activateAgreement(Id agreenentId, Id docId);
}