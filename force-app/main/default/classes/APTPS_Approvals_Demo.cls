/************************************************************************************************************************
@Name: APTPS_Approvals_Demo
@Author: Conga PS Dev Team
@CreateDate: 28 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_Approvals_Demo implements APTPS_ApprovalUsersInterface{    
    
    public void checkLegalApprovedRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    public void checkCAManagerApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    public void checkDDCApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    public void checkHeadofCommercialSalesApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
    public void checkSalesOpsApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJUS_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_Constants.SPECIALRATE.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Rate_Type__c)) {                        
                proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = true;
                proposalObj.Requires_VP_Sales_Approval__c = true;
                break;                                        
            }
        }
        system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkSalesDirectorApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Demo NJA START*/
            if (APTS_Constants.NJUS_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && (APTS_Constants.PREMIUM.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Rate_Type__c) || APTS_Constants.NONPREMIUM.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Rate_Type__c) || proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_Positioning_Fees__c)) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                break;                                        
            }
            /*For Demo NJA END*/
            /*For Demo NJE START*/
            if (APTS_Constants.NJE_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && (APTS_Constants.PROMOTIONALRATE.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Rate_Type__c) || APTS_Constants.SPECIALRATE.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Rate_Type__c) || proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_Positioning_Fees__c)) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                break;                                        
            }
            /*For Demo NJE END*/
            
        }
        system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkDemoTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJUS_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode)) {                        
                proposalObj.APTPS_Requires_Demo_Team_Approval__c = true;
                break;                                        
            }
        }
        system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkDirectorARApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJUS_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode)&& proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_Credit_Card_Authorization__c) {                        
                proposalObj.APTPS_Requires_Director_AR_Approval__c = true;
                break;                                        
            }
        }
        system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkRVPApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJE_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && (APTS_Constants.PREMIUM.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Rate_Type__c) || APTS_Constants.NONPREMIUM.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Rate_Type__c))) {                        
                proposalObj.APTS_Requires_RVP_Approval__c = true;
                break;                                        
            }
        }
        system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkSalesOpsTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJE_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode)) {                        
                proposalObj.APTPS_Requires_Sales_Ops_Team_Approval__c = true;
                break;                                        
            }
        }
        system.debug('proposalObj-->'+proposalObj);        
    }
    
    
    
    
    /** 
@description: DEMO NJE implementation
@param:
@return: 
*/
    public class NJE_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJE dApprovals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Demo obj = new APTPS_Approvals_Demo();
            obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            obj.checkRVPApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesOpsTeamApprovalRequired(proposalObj,quoteLines);
            
        }
    }
    
    /** 
@description: DEMO NJA implementation
@param:
@return: 
*/
    public class NJA_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJA dApprovals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Demo obj = new APTPS_Approvals_Demo();
            obj.checkSalesOpsApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            obj.checkDemoTeamApprovalRequired(proposalObj, quoteLines);
            obj.checkDirectorARApprovalRequired(proposalObj,quoteLines);
            
        }
    }
}