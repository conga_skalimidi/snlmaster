/************************************************************************************************************************
@Name: APTPS_CPQFields_Lease
@Author: Conga PS Dev Team
@CreateDate: 3 Nov 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CPQFields_Lease {
public void populateLeaseFields(Apttus_Proposal__Proposal__c oProposal,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        Double leaseDeposit, MMF, OHF, SupplementalRate1,SupplementalRate2,incentiveFee,contractDeposit,percentage,assignmentFee,MLF;
        String acName,contractLength = null;
        Id parkedAircraft = null;
        Date startOfDeferment,endOfDeferment = null;
        boolean hasInterimLeaseOption = false;
        boolean hasEnhancementBundle = false;
        incentiveFee=contractDeposit=percentage=assignmentFee=0;
        for (Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {   
            if(APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
               && APTS_ConstantUtil.LEASE_DEPOSIT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                    acName = pli.Apttus_QPConfig__OptionId__r.Name;
                    parkedAircraft = pli.Apttus_QPConfig__OptionId__c;
                    percentage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c;
                    leaseDeposit = pli.Apttus_QPConfig__NetPrice__c;
                    contractLength = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c;
                    //This is used for Modification Type = Deferment/Moratorium
                   //Start of GCM-12869
                   if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(oProposal.APTS_Modification_Type__c)) {
                       startOfDeferment = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Start_of_Deferment__c;
                       endOfDeferment = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_End_of_Deferment__c;
                   }
                   //End of GCM-12869    
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.MMF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   MMF = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.OHF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   OHF = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.SR1_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   SupplementalRate1 = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.SR2_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   SupplementalRate2 = pli.Apttus_QPConfig__NetPrice__c;
            }
            
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           incentiveFee = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Incentive_Amount__c;
            }
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.INTERIM_LEASE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.INTERIM_LEASE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           contractDeposit = pli.Apttus_QPConfig__NetPrice__c;
                           hasInterimLeaseOption = true;
            }
            /**GCM-13045,GCM-13046 Assign fields to proposal in case of Unrelated Assignment  Modification START**/
            if(APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(oProposal.APTS_Additional_Modification_Type__c) && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) && APTS_ConstantUtil.ASSIGNMENT_CT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                assignmentFee = pli.Apttus_QPConfig__NetPrice__c;        
                
            }
            /** GCM-13045,GCM-13046Assign fields to proposal in case of Unrelated Assignment Modification END**/
            //Check whether configuration has Enhancement Bundle
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && (APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equalsIgnoreCase(pli.Apttus_Proposal__Product__r.ProductCode) 
                   || APTS_ConstantUtil.SNL_NJE_ENHANCEMENT.equalsIgnoreCase(pli.Apttus_Proposal__Product__r.ProductCode))) {
                   hasEnhancementBundle = true;
               }
               if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.MLF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   MLF = pli.Apttus_QPConfig__NetPrice__c;
            }
        }      
        oProposal.Aircraft_Types__c = acName;
        oProposal.APTPS_Contract_aircraft_type__c = acName;
        oProposal.Product_Line__c = APTS_ConstantUtil.LEASE;
        oProposal.APTPS_MMF__c = MMF;
        oProposal.APTPS_OHF__c = OHF;
        oProposal.APTPS_Supplemental_Rate1__c = SupplementalRate1;
        oProposal.APTPS_Supplemental_Rate2__c = SupplementalRate2;
        oProposal.APTPS_Purchase_Price_Incentive__c = incentiveFee;    
        oProposal.APTPS_Contract_Purchase_Deposit__c = contractDeposit;
        oProposal.APTPS_Has_Interim_Lease_Option__c = hasInterimLeaseOption;
        oProposal.APTPS_Share_Percentage__c = percentage;
        oProposal.APTPS_Contract_Lease_Deposit__c = leaseDeposit;
        oProposal.APTPS_Parked_Aircraft__c= parkedAircraft;
        oProposal.APTPS_Is_Share_Lease_Enhancement__c = hasEnhancementBundle;
        oProposal.APTPS_Term_Months_Lease__c = Decimal.valueOf(contractLength);
        //This is used for Modification Type = Deferment/Moratorium
        //Start of GCM-12869
        oProposal.APTPS_Start_of_Deferment__c = startOfDeferment;
        oProposal.APTPS_End_of_Deferment__c = endOfDeferment;
        //End of GCM-12869
        //GCM-13045,GCM-13046 for unrelated Assignment Modification
        oProposal.APTPS_Assignment_Fee__c = assignmentFee;
        oProposal.APTPS_Contract_monthly_lease_fee__c = MLF;
         system.debug('oProposal-->'+oProposal);
    }   
   
    /** 
    @description: Demo NJE implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Lease NJE CPQFields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Lease obj = new APTPS_CPQFields_Lease();
            obj.populateLeaseFields(proposalObj,quoteLines);            
            
        }
    }
    
     /** 
    @description: Demo NJA implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Lease NJA CPQFields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Lease obj = new APTPS_CPQFields_Lease();
            obj.populateLeaseFields(proposalObj,quoteLines);            
            
        }
    }
}