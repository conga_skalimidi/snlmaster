/************************************************************************************************************************
@Name: APTPS_CLMLifeCycle
@Author: Conga PS Dev Team
@CreateDate: 6 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMLifeCycle implements APTPS_CLMLifeCycleCallable {
    private static String dsMetadataName = 'APTPS_CLMLifeCycle_Registry__mdt';
    public static Map<String, String> prodCodeClassNameMap = new Map<String, String>();
    
    /** 
    @description: Find out the APEX class implementation for the given productCode
    @param: Product Code
    @return: APEX Class name
    */
    private String findCLMAction_Implementation(String productCode) {
        String className = null;
        if(productCode != null && !prodCodeClassNameMap.isEmpty() 
           && prodCodeClassNameMap.containsKey(productCode)) {
               className = prodCodeClassNameMap.get(productCode);
           }
        system.debug('APEX Class Details --> '+className);
        return className;
    }
    
    /** 
    @description: Execute product specific Deal Summary logic.
    @param: Product code and arguments
    @return: 
    */
    public Object call(String productCode, Map<String, Object> args) {
        system.debug('Product Code to update deal summary --> '+productCode);
        system.debug('Arguments to call method --> '+args);
        String retMsg = null;
        if(args == null || args.get('newAgrList') == null) 
            throw new ExtensionMalformedCallException('Agreement data is missing.');
        
        if(!String.isEmpty(productCode)) {
            if(prodCodeClassNameMap.isEmpty()) {
                setProdCodeClassNameMap();
            }
            
            String className = findCLMAction_Implementation(productCode);
            if(className != null) {
                APTPS_CLMLifeCycleInterface dsObj = (APTPS_CLMLifeCycleInterface)Type.forName(className).newInstance();
                dsObj.callCLMAction(args);
                modificationsCLMFlow(args);
                retMsg = 'SUCCESS';
            }
        }
        return retMsg;
    }
    
    /** 
    @description: Custom Exception implementation
    @param:
    @return: 
    */
    public class ExtensionMalformedCallException extends Exception {
        public String errMsg = '';
        public void ExtensionMalformedCallException(String errMsg) {
            this.errMsg = errMsg;
        }
    }
    
    /** 
    @description: Read CLM Fields Registry metadata and prepare the Map
    @param:
    @return: 
    */
    private void setProdCodeClassNameMap() {
        List<APTPS_CLMLifeCycle_Registry__mdt> fieldsSummaryHandlers = [SELECT Product_Code__c, Class_Name__c FROM APTPS_CLMLifeCycle_Registry__mdt];
        for(APTPS_CLMLifeCycle_Registry__mdt handler : fieldsSummaryHandlers) {
            if(handler.Product_Code__c != null && handler.Class_Name__c != null) 
                prodCodeClassNameMap.put(handler.Product_Code__c, handler.Class_Name__c);
        }
    }
    /** 
    @description: modificationsCLMFlow for SNL Modifications
    @param: AgreementList
    
    */
    public void modificationsCLMFlow(Map<String, Object> args) {
        List<Apttus__APTS_Agreement__c> newAg = (List<Apttus__APTS_Agreement__c>)args.get('newAgrList');
        Map<Id,SObject> oldAgMap = (Map<Id,SObject>)args.get('oldAgrList');
        List<Task> tasksList = new List<Task>(); 
        for(Apttus__APTS_Agreement__c ag : newAg) {
            Apttus__APTS_Agreement__c oldAgObj = (Apttus__APTS_Agreement__c) oldAgMap.get(ag.Id);
            String oldStatus = oldAgObj.Apttus__Status__c;
            String oldStatusCat = oldAgObj.Apttus__Status_Category__c;
            String newStatus = ag.Apttus__Status__c;
            String newStatusCat = ag.Apttus__Status_Category__c;
            String oldAgStatus = oldAgObj.Agreement_Status__c;
            if(APTS_ConstantUtil.REPURCHASE.equalsIgnoreCase(ag.APTS_Modification_Type__c) || APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(ag.APTS_Modification_Type__c) || APTS_ConstantUtil.LEASE_TERMINATION.equalsIgnoreCase(ag.APTS_Modification_Type__c)) {
                if(APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                    if(!oldAgObj.APTPS_Owner_Signed__c && ag.APTPS_Owner_Signed__c) {
                        ag.Document_Status__c  = APTS_ConstantUtil.DOC_RECEIVED;                    
                    }
                    if(!oldAgObj.APTPS_Send_to_Billing__c && ag.APTPS_Send_to_Billing__c) {
                        //create Task for Billing 
                        id taskOwnerId = UserInfo.getUserId();
                        Task billingTask = APTPS_SNL_Util.createTaskForRepurchase(ag.Id,APTS_ConstantUtil.BILLING_TASK_SUBJECT,taskOwnerId);
                        tasksList.add(billingTask);
                        /*Send Email Notification to Billing START*/                    
                        APTPS_SNL_Util.sendEmailNotification(APTS_ConstantUtil.AGREEMENT_OBJ,ag.Id,ag.Agreement_Status__c,APTS_ConstantUtil.NJA_REPURCHASE_BILLING);
                        /*Send Email Notification to Billing END*/              
                    }
                    if(!oldAgObj.APTPS_Send_to_Accounting__c && ag.APTPS_Send_to_Accounting__c) {    
                        //create Task for Accounting 
                        id taskOwnerId = UserInfo.getUserId();
                        Task accountingTask = APTPS_SNL_Util.createTaskForRepurchase(ag.Id,APTS_ConstantUtil.ACCOUNTING_TASK_SUBJECT,taskOwnerId);
                        tasksList.add(accountingTask);
                        /*Send Email Notification to Accounting START*/                 
                        APTPS_SNL_Util.sendEmailNotification(APTS_ConstantUtil.AGREEMENT_OBJ,ag.Id,ag.Agreement_Status__c,APTS_ConstantUtil.NJA_REPURCHASE_ACCOUNTING);
                        /*Send Email Notification to Accounting END*/
                    }
                    if(!oldAgObj.APTPS_Send_to_OAA__c && ag.APTPS_Send_to_OAA__c) {
                        //create Task for OAA 
                        id taskOwnerId = UserInfo.getUserId();
                        Task OAATask = APTPS_SNL_Util.createTaskForRepurchase(ag.Id,APTS_ConstantUtil.OAA_TASK_SUBJECT,taskOwnerId);
                        tasksList.add(OAATask);
                        /*Send Email Notification to OAA START*/                    
                        APTPS_SNL_Util.sendEmailNotification(APTS_ConstantUtil.AGREEMENT_OBJ,ag.Id,ag.Agreement_Status__c,APTS_ConstantUtil.NJA_REPURCHASE_OAA);
                        /*Send Email Notification to OAA END*/
                    }
                    if(!oldAgObj.APTPS_Send_to_Accounts_Receivable__c && ag.APTPS_Send_to_Accounts_Receivable__c) {                        
                        //create Task for AR
                        id taskOwnerId = UserInfo.getUserId();
                        Task ARTask = APTPS_SNL_Util.createTaskForRepurchase(ag.Id,APTS_ConstantUtil.ACCOUNTS_RECEIVABLE_TASK_SUBJECT,taskOwnerId);
                        tasksList.add(ARTask);
                        /*Send Email Notification to Accounts Receivable START*/                    
                        APTPS_SNL_Util.sendEmailNotification(APTS_ConstantUtil.AGREEMENT_OBJ,ag.Id,ag.Agreement_Status__c,APTS_ConstantUtil.NJA_REPURCHASE_AR);
                        /*Send Email Notification to Accounts Receivable END*/
                    }
                    if(!oldAgObj.APTPS_Send_to_Contract_Admin__c && ag.APTPS_Send_to_Contract_Admin__c) {
                        //create Task for CA
                        id taskOwnerId = UserInfo.getUserId();
                        Task CATask = APTPS_SNL_Util.createTaskForRepurchase(ag.Id,APTS_ConstantUtil.CONTRACT_ADMIN_TASK_SUBJECT,taskOwnerId);
                        tasksList.add(CATask);
                        /*Send Email Notification to CA START*/                 
                        APTPS_SNL_Util.sendEmailNotification(APTS_ConstantUtil.AGREEMENT_OBJ,ag.Id,ag.Agreement_Status__c,APTS_ConstantUtil.NJA_REPURCHASE_CA);
                        /*Send Email Notification to CA END*/
                    }
                    if(!oldAgObj.APTPS_Payment_Complete__c && ag.APTPS_Payment_Complete__c) {               
                        ag.Funding_Status__c = APTS_ConstantUtil.REFUND_COMPLETED ;
                    }
                    if(!oldAgObj.APTPS_Final_Accounting_Statement_Signed__c && ag.APTPS_Final_Accounting_Statement_Signed__c) {
                         ag.Agreement_Status__c  = APTS_ConstantUtil.PENDING_POSITIONING;
                         ag.Document_Status__c  = APTS_ConstantUtil.DOC_RECEIVED;
                         ag.Chevron_Status__c  = APTS_ConstantUtil.PENDING_POSITIONING;
                    }                        
                }                
                if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                    if(APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) && APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                       ag.Funding_Status__c = APTS_ConstantUtil.REFUND_COMPLETED ;
                    }
                }
                
            } else  if(APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equalsIgnoreCase(ag.APTS_Modification_Type__c)) {
                if(!oldAgObj.APTPS_Owner_Signed__c && ag.APTPS_Owner_Signed__c ) {
                    ag.Document_Status__c  = APTS_ConstantUtil.DOC_RECEIVED;
                }
            } else  if(APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(ag.APTS_Additional_Modification_Type__c)) {
                if(!oldAgObj.APTPS_Owner_Signed__c && ag.APTPS_Owner_Signed__c ) {
                     ag.Agreement_Status__c  = APTS_ConstantUtil.PENDING_POSITIONING;
                     ag.Document_Status__c  = APTS_ConstantUtil.DOC_RECEIVED;
                     ag.Chevron_Status__c  = APTS_ConstantUtil.PENDING_POSITIONING;
                }
            }
        }
        //insert tasks 
        if(!tasksList.isEmpty()) {
            insert tasksList;
        }
    }
    
}