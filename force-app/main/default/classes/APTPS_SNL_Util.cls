/************************************************************************************************************************
 @Name: 
 @Author: Conga PS Dev Team
 @CreateDate: 04 June 2021
 @Description: Util class for SNL business logic
 ************************************************************************************************************************
 @ModifiedBy:
 @ModifiedDate:
 @ChangeDescription:
 ************************************************************************************************************************/

public class APTPS_SNL_Util {
    /**
     @description: Check if provided Program Type is of SNL type or not
     @param: progType - Program Type
     @return: true in case of SNL products
     */
    public static boolean isSNLProgramType(String progType) {
        boolean isSNL = false;
        APTPS_SNL_Products__c snlProducts = APTPS_SNL_Products__c.getOrgDefaults();
        String prodNames = snlProducts.Products__c;
        if(progType != null && prodNames.contains(progType))
            isSNL = true;
        system.debug('Is SNL program type --> '+isSNL);
        return isSNL;
    }

    /**
     @description: Check if it's NJA/NJE Share product or not.
     @param: productCode - Product Code.
     @return: true in case of Share product
     */
    public static boolean isShareProduct(String productCode) {
        boolean result = false;
        if(productCode != null && (APTS_ConstantUtil.NJA_SHARE_CODE.equalsIgnoreCase(productCode) || APTS_ConstantUtil.NJE_SHARE_CODE.equalsIgnoreCase(productCode) || 'NJUS_LEASE'.equalsIgnoreCase(productCode) || 'NJE_LEASE'.equalsIgnoreCase(productCode)))
            result = true;

        system.debug('isShareProduct --> '+result);
        return result;
    }

    /**
     @description: Check if it's NJA/NJE Lease product or not.
     @param: productCode - Product Code.
     @return: true in case of Lease product
     */
    public static boolean isLeaseProduct(String productCode) {
        boolean result = false;
        if(productCode != null && (APTS_ConstantUtil.NJA_LEASE_CODE.equalsIgnoreCase(productCode) || APTS_ConstantUtil.NJE_LEASE_CODE.equalsIgnoreCase(productCode)))
            result = true;

        system.debug('isLeaseProduct --> '+result);
        return result;
    }

    /**
     @description: Compute the MMF price for Share
     @param: item - Line Item
     @param: listPrice - List Price
     @return: Computed MMF price
     */
    public static Decimal computeMMFPrice(Apttus_Config2__LineItem__c item, Decimal listPrice, Decimal definedPriceFactor) {
        Decimal computedMMFPrice = 0;
        try{
            Decimal shareHours = item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c != null ? Decimal.valueOf(item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c) : 0;
            Decimal mmfPriceFactor = shareHours/definedPriceFactor; //We are not adding validation to check for zero, instead execution should fail.
            Decimal mmfListPrice = listPrice != null ? listPrice : 0;
            system.debug('definedPriceFactor --> '+definedPriceFactor+'shareHours --> '+shareHours+' mmfPriceFactor --> '+mmfPriceFactor+' mmfListPrice --> '+mmfListPrice);
            computedMMFPrice = mmfPriceFactor*mmfListPrice;
        } catch(Exception e) {
            system.debug('Error while computing MMF Price. Error details --> ' + e + ' Line Number-->' + e.getLineNumber());
        }

        system.debug('computeMMFPrice, MMF computed Price is --> '+computedMMFPrice);
        return computedMMFPrice;
    }

    /**
    @description: Reusable method to set Agreement Status
    @param: ag - Agreement record
    @param: agStatus - Agreement Status and Chevron Status
    @param: fundingStatus - Funding Status
    @param: docStatus - Document Status
    @return: void
    */
    public static Apttus__APTS_Agreement__c setAgreementStatus(Apttus__APTS_Agreement__c ag, String agStatus, String fundingStatus, String docStatus) {
        ag.Agreement_Status__c = agStatus;
        ag.Chevron_Status__c = agStatus;
        ag.APTS_Path_Chevron_Status__c = agStatus;
        List<String> modificationsList = new List<String>{APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION,APTS_ConstantUtil.REPURCHASE,APTS_ConstantUtil.REDUCTION,APTS_ConstantUtil.RENEWAL,APTS_ConstantUtil.DEFERMENT_MORATORIUM,APTS_ConstantUtil.LEASE_TERMINATION,APTS_ConstantUtil.SUBSTITUTION};
        List<String> additionalModList = new List<String>{APTS_ConstantUtil.UNRELATED_FULL,APTS_ConstantUtil.REDUCTION};
        if(!modificationsList.contains(ag.APTS_Modification_Type__c) && !additionalModList.contains(ag.APTS_Additional_Modification_Type__c)){
            ag.Funding_Status__c = fundingStatus;
        }
        
        ag.Document_Status__c = docStatus;
        return ag;
    }
    
    /**
    @description: Reusable method to populate Agreement Dates
    @param: ag - Agreement record
    @param: agLiList - agreementLineItemList
    @return: void
    */
    public static void populateDates(Apttus__APTS_Agreement__c ag,List<Apttus__AgreementLineItem__c> agLiList) {
        Date delayedStartDate, minimumCommitmentDate,effectiveDate,contractEndDate,closeDate;
        delayedStartDate=minimumCommitmentDate=effectiveDate=contractEndDate=closeDate= null;
        String delayedStartMonths = '';
        Integer termMonths = 0;
        String minimumCommitmentPeriod = '';
        if(!APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equalsIgnoreCase(ag.APTS_Modification_Type__c) &&(APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c))){
            for(Apttus__AgreementLineItem__c agLi : agLiList) {
                //system.debug('Terms-->'+agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c);
                if(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c != null) {
                    minimumCommitmentPeriod = agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c;
                }
                if(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c != null) {
                    delayedStartDate = agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c;
                }               
                if(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c != null) {
                    delayedStartMonths = agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c;
                }
                if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) && agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length__c != null) {
                    termMonths = Integer.ValueOf(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length__c);
                }
                if(APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c) && agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c != null) {
                    termMonths = Integer.ValueOf(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c);
                }
                                
            }
            effectiveDate = ag.Apttus__Contract_Start_Date__c;
            closeDate = ag.APTPS_Closing_Date__c;
            if(effectiveDate == null) {
                ag.Apttus__Contract_Start_Date__c = ag.APTPS_Closing_Date__c;
                effectiveDate = ag.APTPS_Closing_Date__c;
                
            }
            if(delayedStartDate != null) {
                ag.Delayed_Start_Date__c = delayedStartDate;
            }
            if(delayedStartMonths != '' && effectiveDate!=null) {
                ag.Delayed_Start_Date__c = effectiveDate.addMonths(Integer.valueOf(delayedStartMonths));
            }
            system.debug('ag.First_Flight_Date__c-->'+ag.First_Flight_Date__c+' delayedStartDate-->'+delayedStartDate);
            if(ag.First_Flight_Date__c!=null && ag.Delayed_Start_Date__c!=null && ag.First_Flight_Date__c < ag.Delayed_Start_Date__c) {
                delayedStartDate = ag.First_Flight_Date__c;
                ag.Delayed_Start_Date__c = delayedStartDate;
            }
            if(minimumCommitmentPeriod != '') {
                minimumCommitmentDate = effectiveDate.addMonths(Integer.valueOf(minimumCommitmentPeriod));
                if(delayedStartMonths!='') {
                    minimumCommitmentDate = minimumCommitmentDate.addMonths(Integer.valueOf(delayedStartMonths));       
                }
                if(delayedStartDate!=null) {
                    minimumCommitmentDate = delayedStartDate.addMonths(Integer.valueOf(minimumCommitmentPeriod));       
                }               
                ag.APTPS_Minimum_Commitment_Date__c = minimumCommitmentDate;
                
            }
            contractEndDate = effectiveDate.addMonths(termMonths);
            if(delayedStartMonths!='') {
                contractEndDate = contractEndDate.addMonths(Integer.valueOf(delayedStartMonths));
            }
            if(delayedStartDate!=null) {
                contractEndDate = delayedStartDate.addMonths(termMonths);       
            }
            ag.Apttus__Contract_End_Date__c = contractEndDate;
            
        }
    }
    
    /**
    @description: Calculate the number of months between the dates
    @param: startDate - Start date
    @param: dueDate - End date
    @return: Number of Months
    */
    public static Integer calculateMonths(Date startDate, Date dueDate) {
        Integer numberOfMonths;
        numberOfMonths = startDate.monthsBetween(dueDate);
        system.debug('numberOfMonths --> '+numberOfMonths);
        return numberOfMonths;
    }
    
    /**
    @description: Calculate the number of months between the dates
    @param: startDate - Start date
    @param: dueDate - End date
    @return: Number of Months
    */
    public static Map<Integer, String> getOHFPricingConfig() {
        Map<Integer, String> ohfPricingConfigMap = new Map<Integer, String>();
        for(APTPS_SNL_OHF_Pricing_Configuration__mdt ohfConfig : [SELECT Id, Age_of_Aircraft__c, Charge_Type__c 
                                                                  FROM APTPS_SNL_OHF_Pricing_Configuration__mdt]) {
                                                                      ohfPricingConfigMap.put(Integer.valueOf(ohfConfig.Age_of_Aircraft__c), ohfConfig.Charge_Type__c);
                                                                  }
        return ohfPricingConfigMap;
    }
    
    /**
     @description: Check if it's Early Out or not
     @param: item - Line Item.
     @return: true in case of Early Out
     */
    public static boolean isEarlyOut(Apttus_Config2__LineItem__c item) {
        boolean result = false;
        if(item.Apttus_Config2__OptionId__r.ProductCode != null && 
           (APTS_ConstantUtil.NJA_EARLY_OUT.equalsIgnoreCase(item.Apttus_Config2__OptionId__r.ProductCode) 
            || APTS_ConstantUtil.NJE_EARLY_OUT.equalsIgnoreCase(item.Apttus_Config2__OptionId__r.ProductCode)) 
           && item.Apttus_Config2__LineStatus__c != null 
           && (item.Apttus_Config2__LineStatus__c == 'New' || item.Apttus_Config2__LineStatus__c == 'Amended')) 
            result = true;
        system.debug('isEarlyOut --> '+result);
        return result;
    }
    
    /**
    @description: Caluculate Waiver International Fees for Enhancement Approvals
    @param: calculatedHours - Calculated Hours
    @param: hoursFactor - Hours Factor
    @param: internationalFees - International Fees
    @return: caluculatedFees - Caluculated Fees
     */
    public static Decimal calculateWIF(Decimal calculatedHours, Decimal hoursFactor, Decimal internationalFees) {
        Decimal caluculatedFees = 0.0;
        caluculatedFees = (hoursFactor/calculatedHours)*internationalFees;        
        return caluculatedFees;
    }
    
    /**
    @description: Caluculate 1/16 for Enhancement Approvals
    @param: calculatedHours - Calculated Hours
    @param: hoursFactor - Hours Factor
    @param: attributeValue - Attribute Value
    @return: caluculatedValue - Caluculated 1/16
     */
    public static Decimal calculatePer16(Decimal calculatedHours, Decimal hoursFactor, Decimal attributeValue) {
        Decimal caluculatedValue = 0.0;
        caluculatedValue = (hoursFactor/calculatedHours)*attributeValue;        
        return caluculatedValue;
    }
    
    /**
    @description: Caluculate hours for SimultaneousUsage for Enhancement Approvals
    @param: calculatedHours - Calculated Hours
    @param: hoursFactor - Hours Factor
    @param: numberOfdays - number Of days
    @return: caluculatedDays - 1/16 calucalated days
     */
    public static Decimal calculateSUHours(Decimal calculatedHours, Decimal hoursFactor, Decimal numberOfdays) {
        Decimal caluculatedDays = 0.0;
        caluculatedDays = (calculatedHours/hoursFactor)*numberOfdays;        
        return caluculatedDays;
    }
    
    /**
    @description: Generate RecordType Map from Custom Metadata for Proposal and Agreement   
    @param: sObj - sObject 
    @return: rtMap or productMap - RecordType Map or Product Map
    
    */
    public static Map<String,String> getProductMap(String sObj, String returnType) {
        List<APTPS_SNL_Product_Map__mdt> SNLProductMapList = [SELECT CurrencyCode__c, Product_Code__c,Program_Type__c,Record_Type__c,APTPS_Action_type__c,Modification_Type__c,Agreement_RecordType__c, Cart_Flow_Name__c, Cart_Launch_State__c, Additional_Modification_Type__c FROM APTPS_SNL_Product_Map__mdt];
        Map<String,String> rtMap = new Map<String,String>();
        Map<String,String> productMap = new Map<String,String>();
        Map<String, String> catalogFlowMap = new Map<String, String>();
        for(APTPS_SNL_Product_Map__mdt SNLProductMap : SNLProductMapList) { 
            String key = getRTKey(SNLProductMap.Program_Type__c, SNLProductMap.CurrencyCode__c, SNLProductMap.APTPS_Action_type__c, SNLProductMap.Modification_Type__c, SNLProductMap.Additional_Modification_Type__c);            
            if(!rtMap.containsKey(key)) {
                if(APTS_ConstantUtil.QUOTE_OBJ.equalsIgnoreCase(sObj)) {
                    rtMap.put(key,SNLProductMap.Record_Type__c);
                } else if(APTS_ConstantUtil.AGREEMENT_OBJ.equalsIgnoreCase(sObj)) {
                    rtMap.put(key,SNLProductMap.Agreement_RecordType__c);
                }
                
            }
            if(!productMap.containsKey(key)) {
                productMap.put(key,SNLProductMap.Product_Code__c);          
                
            }
            //Prepare Catalog flow Map
            if(!catalogFlowMap.containsKey(key)){
                String flowDetails = SNLProductMap.Cart_Launch_State__c != null ? '&flow='+SNLProductMap.Cart_Flow_Name__c+'&launchstate='+SNLProductMap.Cart_Launch_State__c : '&flow='+SNLProductMap.Cart_Flow_Name__c;
                catalogFlowMap.put(key, flowDetails);
            }
            
            
        }  
        if(APTS_ConstantUtil.RECORDTYPE.equalsIgnoreCase(returnType)) {
            return rtMap;
        }
        if(APTS_ConstantUtil.PRODUCTCODE.equalsIgnoreCase(returnType)) {
            return productMap;
        }
        if(APTS_ConstantUtil.FLOW_ASSIGNMENT.equalsIgnoreCase(returnType)) {
            return catalogFlowMap;
        }
        return null;
        
    }
    
    /**
    @description: generate dynamic Key for RecordType assignment
    @param: programType - program Type
    @param: currencyCode - currency ISO Code
    @param: actionType - Action Type
    @param: modificationType - Modification Type
    @return: rtKey - Key
    */
    public static String getRTKey(String programType, String currencyCode, String actionType, String modificationType, String additionalModType) {
        String rtKey = '';
        if(programType != null ) {
            rtKey += programType;
        } 
        if(currencyCode != null ) {
            rtKey += '_'+currencyCode;
        }
        if(actionType != null ) {
            rtKey += '_'+actionType;
        }
        if(modificationType != null ) {
            rtKey += '_'+modificationType;
        }
        if(additionalModType != null ) {
            rtKey += '_'+additionalModType;
        }
              
        return rtKey;
    }
    
    /**
    @description: get Assets WhereClause 
    @param: programType - program Type    
    @param: modificationType - Modification Type
    @param: currencyCode - currency ISO Code
    @return: whereClause - whereClause
    */
    public static String getAssetsWhereClause(String programType, String modificationType, String currencyType) {
        system.debug('programType-->'+programType+' modificationType-->'+modificationType+' currencyType-->'+currencyType);
        List<APTPS_SNL_Product_Map__mdt> SNLProductMapList = [SELECT CurrencyCode__c, Product_Code__c,Program_Type__c,Modification_Type__c,Assets_Filter__c FROM APTPS_SNL_Product_Map__mdt WHERE   Modification_Type__c=:modificationType AND CurrencyCode__c=:currencyType AND Program_Type__c=:programType LIMIT 1];
        System.debug('SNLProductMapList-->'+SNLProductMapList);
        String whereClause = '(Apttus_Config2__BundleAssetId__r.Agreement_Status__c = \'Active\' AND Apttus_Config2__BundleAssetId__r.APTS_Is_Expired__c = false)';
        if(!SNLProductMapList.isEmpty()) {          
            String assetFilter = SNLProductMapList[0].Assets_Filter__c;
            if(assetFilter != null) {
                whereClause += 'AND Apttus_Config2__BundleAssetId__r.Apttus_Config2__ProductId__r.ProductCode IN (';
                List<String> lstAssetFilter = assetFilter.split(',');
                String productCodes = '';
                for(String productCode : lstAssetFilter) { 
                    productCodes += '\''+productCode+'\',';
                }
                productCodes =  productCodes.removeEnd(',');
                whereClause += productCodes+')';
            }
        }
        
        System.debug('whereClause-->'+whereClause);
        
        return whereClause;
        
    }    
   
    //This is used for Modification Type = Deferment/Moratorium
    //Start of GCM-12869
    public static void defermentModificationOnAgreement(Apttus__APTS_Agreement__c agObj, Map<Id,Apttus_Proposal__Proposal__c> proposalMap){
        //if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(agObj.APTS_Modification_Type__c)){
            //Difference between Start and End of Deferment
            Integer amountOfExtension = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_Start_of_Deferment__c.daysBetween(proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_End_of_Deferment__c);
            //Updating the value for contract end date with difference of deferment dates
            agObj.Apttus__Contract_End_Date__c = agObj.Apttus__Contract_End_Date__c.addDays(amountOfExtension);
            if(agObj.APTPS_Minimum_Commitment_Period__c < '36'){
                agObj.APTPS_Minimum_Commitment_Date__c = agObj.APTPS_Minimum_Commitment_Date__c.addDays(amountOfExtension);
            }
            if(agObj.APTPS_Deferment_End_Date__c != NULL){
                Integer earlyTermination = agObj.Apttus__Contract_End_Date__c.daysBetween(agObj.APTPS_Deferment_End_Date__c);
                agObj.Apttus__Contract_End_Date__c = agObj.APTPS_Minimum_Commitment_Date__c.addDays(earlyTermination);
                agObj.APTPS_Minimum_Commitment_Date__c = agObj.APTPS_Minimum_Commitment_Date__c.addDays(earlyTermination);
            }
            agObj.APTPS_Start_of_Deferment__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_Start_of_Deferment__c;
            agObj.APTPS_End_of_Deferment__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_End_of_Deferment__c;
            //}
        //}
    }
    //End of GCM-12869
    
    //Set Approvals flags for Letter Agreement Extension Modification   
    public static void updateExtensionApprovals(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        System.debug('Letter Agreement Extension Modification Approvals');
        Set<String> enhancementsList = new Set<String>{APTS_ConstantUtil.SNL_NJA_ENHANCEMENT,APTS_ConstantUtil.SNL_NJE_ENHANCEMENT };
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTS_Requested_End_Date__c != null && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Current_End_Date__c!=null && !enhancementsList.contains(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_ConstantUtil.LINE_TYPE.equals(proposalLineItem.Apttus_QPConfig__LineType__c)) {
                Date RED = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTS_Requested_End_Date__c;
                Date CED = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Current_End_Date__c;
                Integer monthDiff = CED.monthsBetween(RED);  
                System.debug('monthDiff-->'+monthDiff+' proposalObj-->'+proposalObj);               
                if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode) && monthDiff > 12){
                    proposalObj.APTPS_Requires_CA_Manager_Approval__c = true;
                    break;     
                }
                if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode) && monthDiff <= 3){
                    proposalObj.APTS_Requires_RVP_Approval__c = true;
                    break;     
                }
                if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode) && (monthDiff > 3 && monthDiff <= 6)){
                    proposalObj.APTPS_Requires_HCS_Approval__c = true;
                    break;     
                }
                if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode) && monthDiff > 6){
                    proposalObj.Requires_Legal_Approval__c = true;
                    break;     
                }
                
            }
            
        }
        
            
    }
    
    //Set Approvals flags for Deferment Modification   
    public static void updateDefermentApprovals(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        system.debug('Deferment Modification Approvals');
        /*if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
            proposalObj.APTPS_Requires_Director_AR_Approval__c = true;
            proposalObj.APTPS_RequiresGlobalReceivables_Approval__c = true;
        }*/
        if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
            proposalObj.APTPS_Requires_BillingReceivablesManager__c = true;
            proposalObj.APTPS_Requires_EDC_Approvals__c = true;
        }     
    }
    
    //Set Approvals flags for Renewal Modification   
    public static void updateRenewalApprovals(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        system.debug('Renewal Approvals');
                    if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                        proposalObj.APTPS_Requires_DDC_Approval__c = true;
                    }
                    if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                        proposalObj.Requires_Legal_Approval__c = true;
                    }       
    }
    
   //Set Approvals flags for Assignment Modification   
    public static void updateAssignmentApprovals(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {
            if(APTS_ConstantUtil.LINE_TYPE.equals(proposalLineItem.Apttus_QPConfig__LineType__c) && APTS_ConstantUtil.ASSIGNMENT_CT.equals(proposalLineItem.Apttus_QPConfig__ChargeType__c)) {
                if(proposalLineItem.Apttus_QPConfig__AdjustedPrice__c < proposalLineItem.Apttus_QPConfig__OptionPrice__c) {
                    /*NJA Assignment Approvals START*/
                    if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode) && (APTS_ConstantUtil.RELATED_FULL.equals(proposalObj.APTS_Additional_Modification_Type__c) || APTS_ConstantUtil.UNRELATED_FULL.equals(proposalObj.APTS_Additional_Modification_Type__c))) {
                        proposalObj.APTPS_Assignment_Approval_Required__c = true;
                        break;
                    }
                    /*NJA Assignment Approvals END*/
                    /*NJE Assignment Approvals START*/
                    if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode) && APTS_ConstantUtil.UNRELATED_FULL.equals(proposalObj.APTS_Additional_Modification_Type__c)) {
                         if(proposalLineItem.Apttus_QPConfig__OptionPrice__c > proposalLineItem.Apttus_QPConfig__AdjustedPrice__c) {
                            proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                            break;
                        }
                        proposalObj.APTPS_Requires_BillingReceivablesManager__c = true;
                        proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;
                        proposalObj.Requires_Legal_Approval__c = true;
                        break;
                    }
                    if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode) && APTS_ConstantUtil.RELATED_FULL.equals(proposalObj.APTS_Additional_Modification_Type__c) ) {
                        if(proposalLineItem.Apttus_QPConfig__OptionPrice__c > proposalLineItem.Apttus_QPConfig__AdjustedPrice__c) {
                            proposalObj.APTS_Requires_RVP_Approval__c = true;
                            proposalObj.Requires_Legal_Approval__c = true;
                            break;
                        }
                        proposalObj.APTPS_Requires_BillingReceivablesManager__c = true;
                        proposalObj.APTS_Requires_RVP_Approval__c = true;
                        proposalObj.Requires_Legal_Approval__c = true;
                        
                        break;
                    }
                    /*NJE Assignment Approvals END*/
                }
            }
                
        }
        if(APTS_ConstantUtil.RELATED_PARTIAL.equals(proposalObj.APTS_Additional_Modification_Type__c)){
            proposalObj.APTPS_Requires_DDC_Approval__c = true;
        }
        
            
    }
    
    /**
    @description: Caluculate Percentage difference for Repurchase Approvals
    @param: optionPrice - Option Price
    @param: adjustedPrice - adjusted Price
    @return: percentDiff - Percentage Difference
     */
    public static decimal caluculatePercentageDiff(decimal optionPrice, decimal adjustedPrice){
        decimal percentDiff = 0;
        percentDiff = ((adjustedPrice - optionPrice)/optionPrice)*100;
        return percentDiff;
    }
    
    /**
    @description: create Task For Repurchase Modification Workflow
    @param: agrId - WhatId
    @param: subject - Subject
    @param: taskOwnerId - Task Owner
    @return: taskObj - Task Object
    */
    public static Task createTaskForRepurchase(id agrId, String subject,id taskOwnerId) {
        Task taskObj = new Task();
        taskObj.WhatId = agrId;
        taskObj.Subject = subject;
        taskObj.OwnerId = taskOwnerId;
        return taskObj;
    }
    /**
    @description: Send Email Notification For Repurchase Modification Workflow
    @param: Business_Object - Business Object
    @param: objId - obj Id
    @param: Status - Agreement Status
    @return: Product_Code - product Code
    */
    public static void sendEmailNotification(String Business_Object,string objId,String Status,String Product_Code) {
        APTPS_EmailNotification enObj = new APTPS_EmailNotification();
        enObj.call(Product_Code, new Map<String, Object>{'Id' => objId,'Business_Object'=>Business_Object,'Product_Code'=>Product_Code,'Status'=>Status});
    }
    
    /**
    @description: Generate Deal Summary For Letter Agreement extension Modification 
    @param: quoteLines - quoteLines
    @param: modType - modType
    @return: dsText - dsText
    */
    
    public static String generateExtensionSummary(List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines, String modType) {
        String dsText = '';
        String acName, hours, modificationType;
        acName=hours=modificationType = null;
        Date requestedEndDate = null;
        Boolean isWaiveRemarketingFeeAvailable = false;
        try{
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                   && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       acName = pli.Apttus_QPConfig__OptionId__r.Name;
                       hours = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                       requestedEndDate = pli.Apttus_QPConfig__AttributeValueId__r.APTS_Requested_End_Date__c;
                       
                   }
            }
            
            dsText += '<b>Transaction Type:</b> '+APTS_ConstantUtil.AGREEMENT_MOD+'<br/><br/>';
            dsText += '<b>Modification Type:</b> '+modType+'<br/><br/>';
            dsText += '<b>Aircraft Type:</b> '+acName+'<br/><br/>';
            dsText += '<b>Hours:</b> '+hours+'<br/><br/>';
            dsText += '<b>End Date:</b> '+requestedEndDate.format()+'<br/>';
            
        }catch(Exception e){
            system.debug('Exception while computing Deal Summary for Letter Agreement Extension. Error details --> '+e.getMessage());
        }
        
        return dsText;
    }
    
    /**
    @description: Generate Interchange Interim Map from Custom Metadata Agreement   
    @return: interchangeInterimMap - Interchange Interim Map
    
    */
    public static Map<String,APTPS_Interchange_Interim__mdt> getInterchangeInterimMap() {
        List<APTPS_Interchange_Interim__mdt> InterchangeInterimList = [SELECT Aircraft_Type__c, Interchange_Ex__c,Interchange_Interim_AC__c,Interchange_Interim_Ex_AC__c,Interchange_Interim_Ex_Number__c FROM APTPS_Interchange_Interim__mdt];
        Map<String,APTPS_Interchange_Interim__mdt> interchangeInterimMap = new Map<String,APTPS_Interchange_Interim__mdt>();
        for(APTPS_Interchange_Interim__mdt interim : InterchangeInterimList) {
            interchangeInterimMap.put(interim.Aircraft_Type__c,interim);
        } 
        
        return interchangeInterimMap;
            
        
    }
    /**
    @description: create dummy Unrelated Assignor Agreement for unrelated Flow    
    **/
     public static void handleUnrelatedAgreementFlow(set<id> agIdList) {
        String fieldSetName = APTS_ConstantUtil.INTERIM_LEASE_FIELD_SET;
        List<Apttus__APTS_Agreement__c> agList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> updateAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> newAgList = new List<Apttus__APTS_Agreement__c>();
        List<String> dsFields = new List<String>();
        List<Apttus_Config2__AssetLineItem__c> updateALIList  = new List<Apttus_Config2__AssetLineItem__c>();
        for(Schema.FieldSetMember fld : Schema.SObjectType.Apttus__APTS_Agreement__c.fieldSets.getMap().get(fieldSetName).getFields()) {
            String fldName = fld.getFieldPath();
            if(!dsFields.contains(fldName))
                dsFields.add(fldName);
        }
        String query = 'SELECT Id,CurrencyIsoCode,'+String.join(dsFields,',')+' FROM Apttus__APTS_Agreement__c WHERE ID IN :agIdList';
        system.debug('SNL Unrelated Agreement query --> '+query);
        agList = Database.query(query);
        
        if(!agList.isEmpty()) {
            Id assignorRecType = null;
            
            for(Apttus__APTS_Agreement__c ag : agList){
                Apttus__APTS_Agreement__c newAg = new Apttus__APTS_Agreement__c();
                for(String fieldName : dsFields) {
                    newAg.put(fieldName,ag.get(fieldName));
                }
                if(APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                    assignorRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.APTPS_NJA_Unrelated_Assignor_Agreement).getRecordTypeId();
                } else if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                    assignorRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.APTPS_NJE_Unrelated_Assignor_Agreement).getRecordTypeId();
                }
                system.debug('assignorRecType --> '+assignorRecType );
                newAg.RecordTypeId = assignorRecType;
                newAg.APTPS_Assignor_Agreement__c = ag.id;
                newAg.Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_DRAFT ;
                newAg.Chevron_Status__c = APTS_ConstantUtil.AGREEMENT_DRAFT ;
                newAg.Apttus__Status__c = APTS_ConstantUtil.REQUEST;
                newAg.Apttus__Status_Category__c = APTS_ConstantUtil.REQUEST;
                newAg.Funding_Status__c = APTS_ConstantUtil.FUNDED_NOTREQUIRED;
                newAg.APTS_New_Sale_or_Modification__c = APTS_ConstantUtil.NEW_SALE ;
                newAg.APTS_Modification_Type__c = APTS_ConstantUtil.AGREEMENT_MOD;
                newAg.APTS_Additional_Modification_Type__c = APTS_ConstantUtil.UNRELATED_FULL ;
                system.debug('newAg--> '+newAg);
                newAgList.add(newAg);
                Apttus__APTS_Agreement__c updateAgreement = new Apttus__APTS_Agreement__c(id = ag.id, Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_AMENDED , Chevron_Status__c = APTS_ConstantUtil.AGREEMENT_AMENDED );
                system.debug('updateAgreement-->' + updateAgreement);
                updateAgList.add(updateAgreement);
            }
                
            try{
                if(!newAgList.isEmpty()){
                    Insert newAgList;
                    List<Apttus__APTS_Related_Agreement__c> updateRelAgList = new List<Apttus__APTS_Related_Agreement__c>();
                    for(Apttus__APTS_Agreement__c agNew : newAgList) {
                        Id relatedToId = agNew.APTPS_Assignor_Agreement__c;
                        Id relatedFromId = agNew.id;
                        Apttus__APTS_Related_Agreement__c relAgreement = new Apttus__APTS_Related_Agreement__c(Apttus__APTS_Contract_To__c=relatedToId,Apttus__APTS_Contract_From__c=relatedFromId);
                        updateRelAgList.add(relAgreement);
                    }
                    if(!updateRelAgList.isEmpty()){
                        insert updateRelAgList;
                    }
                    if(!updateAgList.isEmpty()){
                        update updateAgList;
                    }               
                    
                    for (Apttus_Config2__AssetLineItem__c updateALI : [SELECT Id, Apttus_CMConfig__AgreementId__c, Apttus_Config2__AssetStatus__c
                                                                         FROM Apttus_Config2__AssetLineItem__c
                                                                         WHERE Apttus_CMConfig__AgreementId__c IN :agIdList]){
                        updateALI.Apttus_Config2__AssetStatus__c = APTS_ConstantUtil.CANCEL;
                        updateALI.APTS_Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_INACTIVE ;
                        updateALIList.add(updateALI);
                    }
                    if(!updateALIList.isEmpty()){
                        update updateALIList;
                    }
                    
                }
                
            } catch (exception e){
                System.debug('Exception in Class-APTPS_SNL_Util: Method- handleUnrelatedAgreementFlow() at line' + e.getLineNumber()+' Message->' + e.getMessage());
            }
        
        }
        
        
    
    }
    
}