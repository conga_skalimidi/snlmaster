/************************************************************************************************************************
@Name: APTPS_CPQFields_Share
@Author: Conga PS Dev Team
@CreateDate: 15 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CPQFields_Share{   
    
    public void populateShareFields(Apttus_Proposal__Proposal__c oProposal,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        Double operatingFund, MMF, OHF, Fuel, SupplementalRate1,SupplementalRate2,cardGraduateCredit,incentiveFee,contractDeposit,ferryRate,percentage,remarketingFee,repurchasePrice,adjustedRepurchasePrice,assignmentFee;
        String commitmentPeriod,acName,contractLength;
        boolean hasInterimLeaseOption = false;
        Id parkedAircraft = null;
        commitmentPeriod=acName=null;
        Date reqEndDate,startOfDeferment,endOfDeferment,dateOfNotice = null;
        cardGraduateCredit=incentiveFee=contractDeposit=ferryRate=percentage=remarketingFee=repurchasePrice=adjustedRepurchasePrice=assignmentFee=0;
        boolean hasEnhancementBundle = false;
        for (Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {   
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
               && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   commitmentPeriod = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c;  
                   acName = pli.Apttus_QPConfig__OptionId__r.Name;
                   parkedAircraft = pli.Apttus_QPConfig__OptionId__c;
                   percentage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c;
                   contractLength = String.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c);
                   //Assign fields to proposal in case of Letter Agreement Extension Modification
                   if(APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equalsIgnoreCase(oProposal.APTS_Modification_Type__c)) {
                       reqEndDate = pli.Apttus_QPConfig__AttributeValueId__r.APTS_Requested_End_Date__c;
                   }
                   //This is used for Modification Type = Deferment/Moratorium
                   //Start of GCM-12869
                   if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(oProposal.APTS_Modification_Type__c)) {
                       startOfDeferment = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Start_of_Deferment__c;
                       endOfDeferment = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_End_of_Deferment__c;
                   }
                   //End of GCM-12869
            }
             /**GCM-7276,7277,7329,7330, Assign fields to proposal in case of Repurchase or Reduction Modification START**/
            if((APTS_ConstantUtil.REPURCHASE.equalsIgnoreCase(oProposal.APTS_Modification_Type__c) || APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(oProposal.APTS_Modification_Type__c) 
                || (APTS_ConstantUtil.RENEWAL.equalsIgnoreCase(oProposal.APTS_Modification_Type__c) && APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(oProposal.APTS_Additional_Modification_Type__c))) 
               && APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c)) {
                dateOfNotice = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Date_of_Notice__c;
                if(APTS_ConstantUtil.REPURCHASE_CT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)){
                    repurchasePrice = pli.Apttus_QPConfig__OptionPrice__c;
                    adjustedRepurchasePrice = pli.Apttus_QPConfig__AdjustedPrice__c;
                }
                if(APTS_ConstantUtil.REMARKETING_CT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)){
                    remarketingFee = pli.Apttus_QPConfig__OptionPrice__c;
                }           
                
            }
            /**Assign fields to proposal in case of Repurchase or Reduction Modification END**/
            /**GCM-13045,GCM-13046 Assign fields to proposal in case of Unrelated Assignment  Modification START**/
            if(APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(oProposal.APTS_Additional_Modification_Type__c) && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) && APTS_ConstantUtil.ASSIGNMENT_CT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                assignmentFee = pli.Apttus_QPConfig__NetPrice__c;        
                
            }
            /** GCM-13045,GCM-13046Assign fields to proposal in case of Unrelated Assignment Modification END**/
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.MMF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   MMF = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.OF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   operatingFund = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.OHF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   OHF = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.FUEL_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   Fuel = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.SR1_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   SupplementalRate1 = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.SR2_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   SupplementalRate2 = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.FERRY_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   ferryRate = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.CARD_GRADUATE_CREDIT_NJA.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.CARD_GRADUATE_CREDIT_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           cardGraduateCredit = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_10_Card_Graduate_Credit_Amount__c;
            }
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           incentiveFee = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Incentive_Amount__c;
            }
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.INTERIM_LEASE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.INTERIM_LEASE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           contractDeposit = pli.Apttus_QPConfig__NetPrice__c;
                           hasInterimLeaseOption = true;
            }
            //Check whether configuration has Enhancement Bundle
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && (APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equalsIgnoreCase(pli.Apttus_Proposal__Product__r.ProductCode) 
                   || APTS_ConstantUtil.SNL_NJE_ENHANCEMENT.equalsIgnoreCase(pli.Apttus_Proposal__Product__r.ProductCode))) {
                   hasEnhancementBundle = true;
               }
        }      
        oProposal.Aircraft_Types__c = acName;
        oProposal.APTPS_Contract_aircraft_type__c = acName;
        oProposal.Product_Line__c = APTS_ConstantUtil.SHARE;
        oProposal.APTPS_Minimum_Commitment_Period__c = commitmentPeriod;
        oProposal.APTPS_Contract_Operating_Fund__c = operatingFund;
        oProposal.APTPS_MMF__c = MMF;
        oProposal.APTPS_OHF__c = OHF;
        oProposal.APTPS_Fuel__c = Fuel;
        oProposal.APTPS_Supplemental_Rate1__c = SupplementalRate1;
        oProposal.APTPS_Supplemental_Rate2__c = SupplementalRate2;
        oProposal.APTPS_Purchase_Price_Incentive__c = incentiveFee;
        oProposal.APTPS_10_Card_Graduate_Credit__c = cardGraduateCredit;        
        oProposal.APTPS_Contract_Purchase_Deposit__c = contractDeposit;
        oProposal.APTPS_Ferry_Rate__c = ferryRate;
        oProposal.APTPS_Has_Interim_Lease_Option__c = hasInterimLeaseOption;
        oProposal.APTPS_Share_Percentage__c = percentage;
        oProposal.APTPS_Parked_Aircraft__c= parkedAircraft;
        oProposal.APTS_Requested_End_Date__c= reqEndDate;
        oProposal.APTPS_Is_Share_Lease_Enhancement__c = hasEnhancementBundle;
        //This is used for Modification Type = Deferment/Moratorium
        //Start of GCM-12869
        oProposal.APTPS_Start_of_Deferment__c = startOfDeferment;
        oProposal.APTPS_End_of_Deferment__c = endOfDeferment;
        //End of GCM-12869
        //for Repurchase Modification START
        oProposal.APTPS_Date_of_Notice__c = dateOfNotice;
        oProposal.APTPS_Repurchase_Price__c = repurchasePrice;
        oProposal.APTPS_Adjusted_Repurchase_Price__c = adjustedRepurchasePrice;
        oProposal.APTPS_Remarketing_Fee__c = remarketingFee;
        //for Repurchase Modification END
        oProposal.APTPS_Contract_term_length__c = contractLength;
        //GCM-13045,GCM-13046 for unrelated Assignment Modification
        oProposal.APTPS_Assignment_Fee__c = assignmentFee;
        system.debug('oProposal-->'+oProposal);
    }   
   
    /** 
    @description: Demo NJE implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJE Fields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Share obj = new APTPS_CPQFields_Share();
            obj.populateShareFields(proposalObj,quoteLines);            
            
        }
    }
    
     /** 
    @description: Demo NJA implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJA Fields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Share obj = new APTPS_CPQFields_Share();
            obj.populateShareFields(proposalObj,quoteLines);            
            
        }
    }
}