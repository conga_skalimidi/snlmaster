/*
* Class Name: APTS_Validationcallback
* Description: 
* 1. APTS_Validationcallback is implementation of Standard Apttus Interface Apttus_Config2.CustomClass.IValidationCallback3 
* 2. GCM-6805: In case of Modification scenario, validate that Cart has at least one asset selected.
* Created By: DEV Team, Apttus
* 3. GCM-7018: Add warning message when user is finalizing the cart with two different legal entities.
* 4. In case if Adjustment is applied stamp flag on line item
* 5. GCM-8314: In case of New Card Sale Conversion, Bonus Hours and Business DEV Hours should be stamped on Agreement.
* 6. In case of NCS, DC and EHC - Add a validation that at least one new product should be selected.

* Modification Log
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Developer               Date           US/Defect        Description
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Yash Modi             21/11/19           -                 Added  
* Avinash Bamane        13/12/19         GCM-6805            Added
* Avinash Bamane        04/02/20         GCM-7018            Added
* Avinash Bamane        11/06/20                            Updated
* Ramesh kumar          07/01/20         GCM-7215            Added
* Avinash Bamane        15/07/20         GCM-8314           Updated
* Bijal                 21/07/20         GCM-8289           Updated
* Avinash Bamane        09/03/21                            Updated
* Ramesh kumar          05/04/21         GCM-10335          Updated
* Siva Kumar             18/05/21        -                   Added
* Venu Bairoju          09/03/21         GCM-11577          Updated
*/

global with sharing class APTS_ValidationCallback implements Apttus_Config2.CustomClass.IValidationCallback3 {
    
    String pageAction;
    Apttus_Config2__ProductConfiguration__c oConfiguration;
    Apttus_Config2.CustomClass.ValidationResult result;
    private string newSaleOrModification = '';
    private string modificationType = '';
    private boolean assetFound = false;
    private integer multipleAsset = 0;
    private boolean newProductFound = false;
    private string programType{get;set;}
    private boolean invalidReferringOwnerAcc = false;
    private string progType = '';
    private boolean isCollabCompleted = false;
    private string businessObjType = '';
    private boolean isDuplicateServiceAccout = false;
    
    private decimal configContractLength = 0;
    private boolean isEarlyOutWithoutLease = false;
    private decimal earlyOutTime = 0;
    private boolean isConfigContractInvalid = false;
    
    /**
    * Callback to validate the line items's discount in the cart
    * @param cart - the cart object to validate
    * @return the validation result
    */
    global Apttus_Config2.CustomClass.ValidationResult validateCart (Apttus_Config2.CustomClass.ActionParams params, Apttus_Config2.ProductConfiguration cart) {
        
        Map <String, String> mapRuntimeValidationCallback = Apttus_Config2.RuntimeContext.getParameters ();
        pageAction = mapRuntimeValidationCallback.get ('pageAction');
        system.debug('Check Page Action --> '+pageAction);
        oConfiguration = cart.getConfigSO ();
        result = new Apttus_Config2.CustomClass.ValidationResult(true);
        // Add all validations here.
        // Start - GCM-6805
        // GCM-11577
        if(Test.isRunningTest()) {
            pageAction = 'Finalize';
        } 
        if ('Finalize'.equalsIgnoreCase (pageAction) && oConfiguration != null) {
            newSaleOrModification = oConfiguration.APTS_New_Sale_or_Modification__c != null ? oConfiguration.APTS_New_Sale_or_Modification__c : '';
            modificationType = oConfiguration.APTS_Modification_Type__c;
        }
        
        List<Apttus_Config2.LineItem> allLines = cart.getLineItems();
        List<Apttus_Config2__LineItem__c> allLineItems = getLineItems(allLines);
        programType = allLineItems[0].Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Program__c;
        //SNL - Share validation
        progType = allLineItems[0].Apttus_Config2__ConfigurationId__r.APTPS_Program_Type__c;
        businessObjType = allLineItems[0].Apttus_Config2__ConfigurationId__r.Apttus_Config2__BusinessObjectType__c;
        if(!APTS_ConstantUtil.COLLAB_REQ.equalsIgnoreCase(businessObjType))
        	isCollabCompleted = allLineItems[0].Apttus_Config2__ConfigurationId__r.APTPS_Is_Quote_Collaboration_Completed__c;
        //GCM-8289  only the below one line
        List<Id> lineItemIdList = new List<Id> ();
        
        //START: SNL Split Billing(Share option) validation
        Set<Id> serviceAccIds = new Set<Id>();
        Decimal totalAllocateHours = 0;
        Decimal hours = 0;
        system.debug('progType --> '+progType);
        if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(progType)) {
            for(Apttus_Config2__LineItem__c li : allLineItems) {
                if(APTS_ConstantUtil.SPLIT_BILL_CODE.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode)) {
                    if(!serviceAccIds.contains(li.Apttus_Config2__AttributeValueId__r.APTPS_Service_Account__c))
                        serviceAccIds.add(li.Apttus_Config2__AttributeValueId__r.APTPS_Service_Account__c);
                    else 
                        isDuplicateServiceAccout = true;
                }
                
                //START: SNL Early Out validation
                if(APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(li.Apttus_Config2__LineType__c) && li.Apttus_Config2__IsPrimaryLine__c 
                   && (APTS_ConstantUtil.NJA_SHARE_CODE.equalsIgnoreCase(li.Apttus_Config2__ProductId__r.ProductCode) 
                       || APTS_ConstantUtil.NJE_SHARE_CODE.equalsIgnoreCase(li.Apttus_Config2__ProductId__r.ProductCode)))
                       configContractLength = li.Apttus_Config2__AttributeValueId__r.APTPS_Contract_Length__c;
				
                if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(li.Apttus_Config2__LineType__c) && li.Apttus_Config2__IsPrimaryLine__c 
                   && (APTS_ConstantUtil.NJA_EARLY_OUT.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode) || APTS_ConstantUtil.NJE_EARLY_OUT.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode))){
                       if(li.Apttus_Config2__AttributeValueId__r.APTPS_True_Up__c) {
                           isEarlyOutWithoutLease = isValidEarlyOut(li);
                       }
                       earlyOutTime = Decimal.valueOf(li.Apttus_Config2__AttributeValueId__r.APTPS_Early_Exit_Timeframe__c);
                   }
                //END: SNL Early Out validation 
            }
        }
        
        if(APTS_ConstantUtil.LEASE.equalsIgnoreCase(progType)) {
            for(Apttus_Config2__LineItem__c li : allLineItems) {
                if(APTS_ConstantUtil.SPLIT_BILL_CODE.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode)) {
                    if(!serviceAccIds.contains(li.Apttus_Config2__AttributeValueId__r.APTPS_Service_Account__c))
                        serviceAccIds.add(li.Apttus_Config2__AttributeValueId__r.APTPS_Service_Account__c);
                    else {
                        isDuplicateServiceAccout = true;
                    }
                }
                
                //START: SNL Early Out validation
                if(APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(li.Apttus_Config2__LineType__c) && li.Apttus_Config2__IsPrimaryLine__c 
                   && (APTS_ConstantUtil.NJA_LEASE_CODE.equalsIgnoreCase(li.Apttus_Config2__ProductId__r.ProductCode) 
                       || APTS_ConstantUtil.NJE_LEASE_CODE.equalsIgnoreCase(li.Apttus_Config2__ProductId__r.ProductCode)))
                       configContractLength = Decimal.valueOf(li.Apttus_Config2__AttributeValueId__r.APTPS_Contract_Length_Lease__c);
				
                if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(li.Apttus_Config2__LineType__c) && li.Apttus_Config2__IsPrimaryLine__c 
                   && (APTS_ConstantUtil.NJA_EARLY_OUT.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode) || APTS_ConstantUtil.NJE_EARLY_OUT.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode))){
                       if(li.Apttus_Config2__AttributeValueId__r.APTPS_True_Up__c) {
                           isEarlyOutWithoutLease = isValidEarlyOut(li);
                       }
                       earlyOutTime = Decimal.valueOf(li.Apttus_Config2__AttributeValueId__r.APTPS_Early_Exit_Timeframe__c);
                   }
                //END: SNL Early Out validation 
            }
        }
        
        //START: SNL Enhancement Bundle+Early Out - Validation
        if(APTS_ConstantUtil.SNL_ENHANCEMENT.equalsIgnoreCase(progType)) {
            for(Apttus_Config2__LineItem__c li : allLineItems) {
                if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(li.Apttus_Config2__LineType__c) && li.Apttus_Config2__IsPrimaryLine__c 
                   && (APTS_ConstantUtil.NJA_EARLY_OUT.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode) || APTS_ConstantUtil.NJE_EARLY_OUT.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.ProductCode))){
                       if(li.Apttus_Config2__AttributeValueId__r.APTPS_True_Up__c) 
                           isEarlyOutWithoutLease = isValidEarlyOut(li);
                   }
            }
        }
        //END: SNL Enhancement Bundle+Early Out validation
        
        system.debug('configContractLength --> '+configContractLength);
        system.debug('Early out timeframe --> '+earlyOutTime);
        system.debug('isEarlyOutWithoutLease --> '+isEarlyOutWithoutLease);
        //SNL: Early Out
        isConfigContractInvalid = isValidSNLContractLength(configContractLength, earlyOutTime);
        system.debug('isConfigContractInvalid --> '+isConfigContractInvalid);
        
        if(isEarlyOutWithoutLease && APTS_ConstantUtil.PAGE_ACTION.equalsIgnoreCase(pageAction)) {
            result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTPS_SNL_Early_Out_True_Up_Error_Message));
            result.isSuccess = false;
        }
        
        if(!isConfigContractInvalid && APTS_ConstantUtil.PAGE_ACTION.equalsIgnoreCase(pageAction)) {
            result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTPS_SNL_Early_Out_Contract_Length_Error_Message));
            result.isSuccess = false;
        }
        
        if(isDuplicateServiceAccout) {
            result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTS_ServiceAccountErrorMessage));
            result.isSuccess = false;
        }
        
        /*if(totalAllocateHours != hours) {
            result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTPS_Incorrect_Hour_Allocation_Error_Msg));
            result.isSuccess = false;
        }*/
        
        if(!isCollabCompleted && APTS_ConstantUtil.SHARE.equalsIgnoreCase(progType) 
          && APTS_ConstantUtil.PAGE_ACTION.equalsIgnoreCase(pageAction)) {
            result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTPS_Quote_Collaboration_Error_Message));
            result.isSuccess = false;
        }
        if(!isCollabCompleted && APTS_ConstantUtil.LEASE.equalsIgnoreCase(progType) 
          && APTS_ConstantUtil.PAGE_ACTION.equalsIgnoreCase(pageAction)) {
            result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTPS_Quote_Collaboration_Error_Message));
            result.isSuccess = false;
        }
        //END
        
        // START: Check line item for any adjustment applied.
        //GCM-11577
        if('Finalize'.equalsIgnoreCase (pageAction) && APTS_ConstantUtil.NEW_SALE.equalsIgnoreCase(newSaleOrModification)) {
            boolean isAdjustmentApplied = false;
            List<Apttus_Config2__LineItem__c> liToUpdate = new List<Apttus_Config2__LineItem__c>();
            try {
                for(Apttus_Config2__LineItem__c li : allLineItems) {
                    
                    if(('Product Type'.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.Family) && APTS_PricingUtil.isNewOption(li) && APTS_PricingUtil.isAdjustmentApplied(li)) || 
                       ('Aircraft'.equalsIgnoreCase(li.Apttus_Config2__OptionId__r.Family) && APTS_PricingUtil.isNewOption(li) && 
                       APTS_PricingUtil.isPurchasePriceCT(li) && APTS_PricingUtil.isAdjustmentApplied(li))) {
                           Apttus_Proposal__Proposal__c oProposal = new Apttus_Proposal__Proposal__c (Id = li.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c);
                           oProposal.APTS_Is_Discount_Applied__c = TRUE;
                           update oProposal;
                           break;
                    }   
                }
            } catch (DmlException e) {
                system.debug (e);
            }
            
            
            
        }
        // END: Check line item for any adjustment applied.
        
        //GCM-8289 Start
        //GCM-11577
        if('Finalize'.equalsIgnoreCase (pageAction) && (APTS_ConstantUtil.NEW_SALE.equalsIgnoreCase(newSaleOrModification) || APTS_ConstantUtil.NEW_CARD_SALE_CONVERSION.equalsIgnoreCase(newSaleOrModification) || APTS_ConstantUtil.DIRECT_CONVERSION.equalsIgnoreCase(newSaleOrModification) || modificationType == 'Assignment')){
            try {
                for(Apttus_Config2__LineItem__c li : allLineItems) { 
                    lineItemIdList.add(li.Id);
                }
                 APTS_QuoteProposalUtility.checkUpgradeEligible(lineItemIdList,modificationType);
            } 
            catch (DmlException e) {
                system.debug (e);
            }
        }
        //GCM-8289 End
        
        // Validation: In case of Modification Scenario only.
        //GCM-11577
        if('Finalize'.equalsIgnoreCase (pageAction) && 'Modification'.equalsIgnoreCase(newSaleOrModification)) {
            //List<Apttus_Config2.LineItem> allLines = cart.getLineItems();
            //List<Apttus_Config2__LineItem__c> allLineItems = getLineItems(allLines);
            // Iterate through all Line Items and check if Asset Bundle Line Item exists or not.
            for(Apttus_Config2__LineItem__c li:allLineItems) {
                if(li.Apttus_Config2__LineStatus__c != 'New' && li.Apttus_Config2__IsPrimaryLine__c == true && 
                   li.Apttus_Config2__LineType__c == 'Product/Service') {
                       assetFound = true;
                       break;
                   }
            }
            
            //In case of Share & Lease: Unrelated Assignment, Add Enhancements - Asset is not required. So by-passing this validation
            if((APTS_ConstantUtil.SHARE.equalsIgnoreCase(progType) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(progType) || APTS_ConstantUtil.SNL_ENHANCEMENT.equalsIgnoreCase(progType)) 
               && ((APTS_ConstantUtil.ASSIGNMENT.equalsIgnoreCase(modificationType) && APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(oConfiguration.APTS_Additional_Modification_Type__c)) 
                   || APTS_ConstantUtil.ADD_ENHANCEMENTS.equalsIgnoreCase(modificationType))) {
                       assetFound = true;
                   }
            
            if(!assetFound) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTS_ModificationErrorMessage));
                result.isSuccess = false;
            }
            
            //START: In case of NCS, DC and EHC - Add a validation that at least one new product should be selected.
            //GCM-11577
            if('Finalize'.equalsIgnoreCase (pageAction) && (APTS_Constants.NEW_SALE_CONVERSION.equalsIgnoreCase(modificationType) || 
              APTS_Constants.DIRECT_CONV.equalsIgnoreCase(modificationType) || 
              APTS_Constants.EHC.equalsIgnoreCase(modificationType))) {
                for(Apttus_Config2__LineItem__c li:allLineItems) {
                    if(li.Apttus_Config2__LineStatus__c == 'New' && li.Apttus_Config2__IsPrimaryLine__c == true && 
                       li.Apttus_Config2__LineType__c == 'Product/Service') {
                           newProductFound = true;
                           break;
                       }
                }
                  
                if(!newProductFound) {
                    result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTS_ModNewCardErrorMessage));
                    result.isSuccess = false;
                }
            }
            //END
            
        }
        // End - GCM-6805
        
        // Start - GCM-7018
        //GCM-11577
        if('Finalize'.equalsIgnoreCase (pageAction) && !'Assignment'.equalsIgnoreCase(modificationType)) {
            //List<Apttus_Config2.LineItem> allLines = cart.getLineItems();
            //List<Apttus_Config2__LineItem__c> allLineItems = getLineItems(allLines);
            Set<Id> legalEntityIdSet = new Set<Id>();
            // Iterate through all Line Items and check if Asset Bundle Line Item exists or not.
            for(Apttus_Config2__LineItem__c li:allLineItems) {
                System.debug('Line Item at 123=>'+li.Apttus_Config2__LocationId__c);
                
                if(li.Apttus_Config2__LocationId__c!=null && !legalEntityIdSet.contains(li.Apttus_Config2__LocationId__c)) {
                    legalEntityIdSet.add(li.Apttus_Config2__LocationId__c); 
                }
                
            }
            System.debug('legalEntityIdSet=>'+legalEntityIdSet);
            System.debug('legalEntityIdSet size=>'+legalEntityIdSet.size());
            if(legalEntityIdSet.size() > 1) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTS_DifferentLegalEntityErrorMessage));
                result.isSuccess = false;
                //isDuplicate = true;
            }
        } 
        // End - GCM-7018

        //Cross-Account Assignment Legal Entity validation 
         //GCM-11577
        if ('Finalize'.equalsIgnoreCase (pageAction) && ('Modification'.equalsIgnoreCase (oConfiguration.APTS_New_Sale_or_Modification__c) 
        && APTS_ConstantUtil.CROSS_ACCOUNT_ASSIGNMENT.equalsIgnoreCase (oConfiguration.APTS_Additional_Modification_Type__c))) {
            
            List<Apttus_Config2.LineItem> allLELines = cart.getLineItems();
            List<Apttus_Config2__LineItem__c> allLeLineItems = getLineItems(allLELines);
            Set<Id> LiWithNoLegalEntityIdSet = new Set<Id>();
                    //system.debug('into Legal Entity Validation And EmptylegalEntityIdSet -->'+LiWithNoLegalEntityIdSet.size());
                    for(Apttus_Config2__LineItem__c li:allLeLineItems) {
                        if(li.Apttus_Config2__LocationId__c == null) {
                            //system.debug('IF --> into Legal Entity Validation-->'+ li.Apttus_Config2__LocationId__c);
                            LiWithNoLegalEntityIdSet.add(li.Apttus_Config2__LocationId__c);
                        }

                    }

                    if(LiWithNoLegalEntityIdSet.size() > 0) {
                        //system.debug('into LE empty condition');
                        result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.NJ_CartLegalEntityErrorMessage));
                        result.isSuccess = false;
                    }       
        }


        //START: GCM-10335 -In case of Termination, Assignment and EHC - Add a validation to prevent more than one product.
            system.debug('modificationType-->'+ modificationType);
            //GCM-11577
            if('Finalize'.equalsIgnoreCase (pageAction) && (APTS_Constants.MOD_TERMINATION.equalsIgnoreCase(modificationType) || APTS_Constants.MOD_ADD_ENHANCEMENTS.equalsIgnoreCase(modificationType) || APTS_Constants.MODIFICATION_TYPE_ASSIGNMENT.equalsIgnoreCase(modificationType) ||  APTS_Constants.TERM_EXTENSION.equalsIgnoreCase(modificationType) || APTS_Constants.EHC.equalsIgnoreCase(modificationType))) {
                for(Apttus_Config2__LineItem__c li:allLineItems) {
                    if(li.Apttus_Config2__LineStatus__c == 'Amended' && li.Apttus_Config2__IsPrimaryLine__c == true && li.Apttus_Config2__LineType__c == 'Product/Service' && li.Apttus_Config2__ChargeType__c == 'Purchase Price') {
                            multipleAsset++;
                       }
                }
                  
                if(multipleAsset > 1) {
                    result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTS_ModMultipleAssetErrorMessage));
                    result.isSuccess = false;
                }
            }
            //END GCM-10335

        //Start- GCM-8415
        System.debug('allLineItems==>'+allLineItems);
        Id referringOwnerAccountID;
        for(Apttus_Config2__LineItem__c li:allLineItems){
            if(li.Apttus_Config2__ProductId__r.Name =='Card' && li.Apttus_Config2__OptionId__r.Name =='Enhancements'){
                referringOwnerAccountID = li.Apttus_Config2__AttributeValueId__r.APTS_Referring_Owner_Account__c !=null ? li.Apttus_Config2__AttributeValueId__r.APTS_Referring_Owner_Account__c: null;
            }
        }
        //GCM-11577
        if('Finalize'.equalsIgnoreCase (pageAction) && referringOwnerAccountID!=null){
            Account referingOwnerAcc = [select id,APTS_Has_Active_NJE_Agreement__c,APTS_Has_Active_NJUS_Agreement__c from Account where id= :referringOwnerAccountID ];
            If(programType == APTS_ConstantUtil.NETJETS_USD && referingOwnerAcc.APTS_Has_Active_NJUS_Agreement__c==false){
                invalidReferringOwnerAcc = true; 
            }else if(programType == APTS_ConstantUtil.NETJETS_EUR && referingOwnerAcc.APTS_Has_Active_NJE_Agreement__c==false){
                invalidReferringOwnerAcc = true;
            }
        }
        
        if(invalidReferringOwnerAcc) {
            result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, Label.APTPS_Invalid_Referring_Owner_Account));
            result.isSuccess = false;
        }
        //End- GCM-8415
        
        // Following snippet should always be executed after all the validations are performed.
        // This snipet should be executed if all validations are successful
        if (result.isSuccess) {
            updateEnhancementFieldsOnProposal ();
            updateOverrideHours();
            //This method is called only for Demo Product
            //updateDemoAttributeFieldsOnProposal();
            //This method is called only for Corporate Trail Product
           // updateCTAttributeFieldsOnProposal();
        }
        
        return result;
    }
    
    /**
    * Callback to validate the given list ramp line items
    * @param cart the cart object associated with the ramp line items
    * @param rampLineItems the list of ramp line items
    * @return the validation result
    */
    global Apttus_Config2.CustomClass.ValidationResult validateRampLineItems(Apttus_Config2.CustomClass.ActionParams params,
                                                                             Apttus_Config2.ProductConfiguration cart, List<Apttus_Config2.LineItem> rampLineItems) {
                                                                                 Apttus_Config2.CustomClass.ValidationResult result = new Apttus_Config2.CustomClass.ValidationResult(true);
                                                                                 return result;
                                                                             }
    
    private void updateEnhancementFieldsOnProposal () {
        
        if ('Finalize'.equalsIgnoreCase (pageAction) && oConfiguration != null 
            && ('New Sale'.equalsIgnoreCase (oConfiguration.APTS_New_Sale_or_Modification__c)
                || ('Modification'.equalsIgnoreCase (oConfiguration.APTS_New_Sale_or_Modification__c) 
                    && ('Add Enhancements'.equalsIgnoreCase (oConfiguration.APTS_Modification_Type__c)
                       || 'New Card Sale Conversion'.equalsIgnoreCase (oConfiguration.APTS_Modification_Type__c) || 'Term Extension'.equalsIgnoreCase (oConfiguration.APTS_Modification_Type__c))))) { //GCM-8314: OR condition added
                        APTS_QuoteProposalUtility.updateChangedEnhancementsOnProposal (oConfiguration.Id, oConfiguration.APTS_Modification_Type__c);
                    }
        
    }

    //Description: copy Attribute values to propoposal for Demo Product
    //Created by: Siva Kumar
    private void updateDemoAttributeFieldsOnProposal () {
        
        if ('Finalize'.equalsIgnoreCase (pageAction) && oConfiguration != null ) {
            APTS_QuoteProposalUtility.updateDemoAttributeFieldsOnProposal(oConfiguration.Id);
        }
        
       /* if ('Finalize'.equalsIgnoreCase (pageAction) && oConfiguration != null 
            && ('New Sale'.equalsIgnoreCase (oConfiguration.APTS_New_Sale_or_Modification__c)
                || ('Modification'.equalsIgnoreCase (oConfiguration.APTS_New_Sale_or_Modification__c) 
                    && 'Add Enhancements'.equalsIgnoreCase (oConfiguration.APTS_Modification_Type__c)))) {
                        
                    }*/
        
    }
    
    //Description: copy Attribute values to propoposal for Corporate Trail Product
    //Created by: Siva Kumar
    private void updateCTAttributeFieldsOnProposal () {
        
        if ('Finalize'.equalsIgnoreCase (pageAction) && oConfiguration != null ) {
            APTS_QuoteProposalUtility.updateCTAttributeFieldsOnProposal(oConfiguration.Id);
        }
        
       /* if ('Finalize'.equalsIgnoreCase (pageAction) && oConfiguration != null 
            && ('New Sale'.equalsIgnoreCase (oConfiguration.APTS_New_Sale_or_Modification__c)
                || ('Modification'.equalsIgnoreCase (oConfiguration.APTS_New_Sale_or_Modification__c) 
                    && 'Add Enhancements'.equalsIgnoreCase (oConfiguration.APTS_Modification_Type__c)))) {
                        
                    }*/
        
    }
    
    private void updateOverrideHours () {
        
        if ('Finalize'.equalsIgnoreCase (pageAction) && oConfiguration != null 
            && ('Modification'.equalsIgnoreCase (oConfiguration.APTS_New_Sale_or_Modification__c) 
                    )) { 
                        APTS_QuoteProposalUtility.updateOverrideHours (oConfiguration.Id);
                    }
        
    }
    
    /* Gets the list line items associated
    * @param cart the cart lines
    * @return the list of line item objects
    */
    private static List<Apttus_Config2__LineItem__c> getLineItems(List<Apttus_Config2.LineItem> allLines) {
        list<Apttus_Config2__LineItem__c> lineItems = new list<Apttus_Config2__LineItem__c>();
        // Iterate through all lines
        for (Apttus_Config2.LineItem lineItemMO : allLines) {
            lineItems.add(lineItemMO.getLineItemSO());
        }
        
        return lineItems;
    }
    
    private static Boolean isValidEarlyOut(Apttus_Config2__LineItem__c li) {
        Boolean isWithoutLease = false;
        String earlyOutQuotes = '';
        String earlyOutAg = '';
        List<String> earlyOutQuoteList = new List<String>();
        List<String> earlyOutAgList = new List<String>();
        if(APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) {
            earlyOutQuotes = li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Quote_Ids__c;
            earlyOutQuoteList = earlyOutQuotes.split(',');
        }
        
        if(APTS_ConstantUtil.SNL_ENTITLEMENT_AGR.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c)) {
            earlyOutAg = li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c;
            earlyOutAgList = earlyOutAg.split(',');
        }
        
        if(!earlyOutQuoteList.isEmpty()) {
            List<Apttus_Proposal__Proposal__c> qList = [SELECT Id FROM Apttus_Proposal__Proposal__c WHERE Name IN :earlyOutQuoteList 
                                                        AND APTPS_Program_Type__c =: APTS_ConstantUtil.LEASE];
            if(qList.isEmpty())
                isWithoutLease = true;
        }
        
        if(!earlyOutAgList.isEmpty()) {
            List<Apttus__APTS_Agreement__c> agList = [SELECT Id FROM Apttus__APTS_Agreement__c WHERE Apttus__FF_Agreement_Number__c IN :earlyOutAgList 
                                                      AND APTPS_Program_Type__c =: APTS_ConstantUtil.LEASE];
            if(agList.isEmpty())
                isWithoutLease = true;
        }
        return isWithoutLease;
    }
    
    private static Boolean isValidSNLContractLength(Decimal configContractLength, Decimal earlyOutContractLength) {
        Boolean isValid = true;
        system.debug('configContractLength --> '+configContractLength);
        system.debug('earlyOutContractLength --> '+earlyOutContractLength);
        isValid = configContractLength != null && earlyOutContractLength != null && configContractLength < earlyOutContractLength ? false : true;
        return isValid;
    }
}