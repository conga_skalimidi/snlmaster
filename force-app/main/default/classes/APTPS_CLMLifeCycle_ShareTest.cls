/************************************************************************************************************************
@Name: APTPS_CLMLifeCycle_ShareTest
@Author: Conga PS Dev Team
@CreateDate: 15 July 2021
@Description: Test class for APTPS_CLMLifeCycle_Share implementation.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

@isTest
public class APTPS_CLMLifeCycle_ShareTest {
    @TestSetup
    static void makeData() {
        try{
            Test.startTest();
            //Create SNL Custom settings
            APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
            snlProducts.Products__c = 'Demo,Corporate Trial,Share';
            insert snlProducts;
            Account accToInsert = new Account();
            accToInsert.Name = 'APTS Test Account';
            insert accToInsert;
            
            Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
            insert testOpportunity;
            
            Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
            insert testPriceList;
            
            //START: NJA Share product test setup
            Product2 NJA_SHARE = APTS_CPQTestUtility.createProduct('Share', 'NJUS_SHARE', 'Apttus', 'Bundle', true, true, true, true);
            insert NJA_SHARE;
            
            Apttus_Proposal__Proposal__c shareCLMLifeCycleQuote = APTS_CPQTestUtility.createProposal('Share CLM Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            insert shareCLMLifeCycleQuote;
            
            Apttus__APTS_Agreement__c shareCLMLifeCycleAg = new Apttus__APTS_Agreement__c();
            shareCLMLifeCycleAg.Name = 'CLM LifeCycle Share Agreement';
            shareCLMLifeCycleAg.Agreement_Status__c = 'Draft';
            shareCLMLifeCycleAg.Apttus__Account__c = accToInsert.Id;
            shareCLMLifeCycleAg.Apttus_QPComply__RelatedProposalId__c = shareCLMLifeCycleQuote.Id;
            shareCLMLifeCycleAg.APTPS_Program_Type__c = APTS_ConstantUtil.SHARE;
            shareCLMLifeCycleAg.Apttus__Status__c = 'Request';
            shareCLMLifeCycleAg.Apttus__Status_Category__c = 'Request';
            //shareCLMLifeCycleAg.CurrencyIsoCode = 'USD';
            shareCLMLifeCycleAg.Program__c = APTS_ConstantUtil.NETJETS_USD;
            shareCLMLifeCycleAg.APTPS_Contract_Purchase_Deposit__c = 100000;
            shareCLMLifeCycleAg.Apttus__Total_Contract_Value__c = 200000;
            shareCLMLifeCycleAg.APTPS_Closing_Date__c = Date.today();
            shareCLMLifeCycleAg.First_Flight_Date__c = Date.today();
            insert shareCLMLifeCycleAg;
            
            Apttus__APTS_Agreement__c interimLeaseChild = new Apttus__APTS_Agreement__c();
            interimLeaseChild.Name = 'CLM LifeCycle Share Agreement';
            interimLeaseChild.Agreement_Status__c = APTS_ConstantUtil.AC_POSITIONED;
            interimLeaseChild.Apttus__Account__c = accToInsert.Id;
            interimLeaseChild.Apttus_QPComply__RelatedProposalId__c = shareCLMLifeCycleQuote.Id;
            interimLeaseChild.APTPS_Program_Type__c = APTS_ConstantUtil.SHARE;
            interimLeaseChild.Program__c = APTS_ConstantUtil.NETJETS_USD;
            interimLeaseChild.Apttus__Status__c = 'Request';
            interimLeaseChild.Apttus__Status_Category__c = 'Request';
            interimLeaseChild.APTPS_Closing_Date__c = Date.today();
            interimLeaseChild.First_Flight_Date__c = Date.today();
            interimLeaseChild.Apttus__Parent_Agreement__c = shareCLMLifeCycleAg.Id;
            insert interimLeaseChild;
            
            //Create Agreement Line Item
            Apttus__AgreementLineItem__c li = new Apttus__AgreementLineItem__c();
            li.Apttus__AgreementId__c = shareCLMLifeCycleAg.Id;
            li.Apttus_CMConfig__IsPrimaryLine__c=true;
            li.Apttus_CMConfig__ChargeType__c=APTS_ConstantUtil.PURCHASE_PRICE;
            li.Apttus__NetPrice__c = 1000;
            insert li;
            
            //Create Agreement Product Attribute value
            Apttus_CMConfig__AgreementProductAttributeValue__c agPAV = new Apttus_CMConfig__AgreementProductAttributeValue__c();
            agPAV.APTPS_Minimum_Commitment_Period__c = '36';
            agPAV.APTPS_Delayed_Start_Date__c = Date.today();
            agPAV.APTPS_Delayed_Start_Months__c = '3';
            agPAV.APTPS_Contract_Length__c = 60;
            agPAV.Apttus_CMConfig__LineItemId__c = li.id;
            insert agPAV;
            li.Apttus_CMConfig__AttributeValueId__c = agPAV.id;
            update li;
            
            
            
            
            Test.stopTest();
        } catch(Exception e) {
            System.debug('APTPS_CLMLifeCycle_ShareTest --> Error while preparing test data. Error details --> '+e);
        }
    }
    
    @isTest
    static void testShareCLM() {
        try{
            Apttus__APTS_Agreement__c ag = [SELECT Id, Agreement_Status__c, Chevron_Status__c, APTS_Path_Chevron_Status__c, Funding_Status__c, 
                                            Document_Status__c, Apttus_Approval__Approval_Status__c, APTPS_Owner_Signed__c, APTPS_Request_Aircraft_Positioning__c, 
                                            Accrual_Date__c, APTPS_Aircraft_Positioned_State__c, APTPS_Aircraft_Positioned_Date__c, Apttus__Status__c, Apttus__Status_Category__c 
                                            FROM Apttus__APTS_Agreement__c 
                                            WHERE APTPS_Program_Type__c =: APTS_ConstantUtil.SHARE AND APTPS_Contract_Purchase_Deposit__c != NULL LIMIT 1];
            
            if(ag != null) {
                //Document generated
                ag.Apttus__Status__c = APTS_ConstantUtil.READY_FOR_SIGN;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
                //Send for Review
                ag.Apttus__Status__c = APTS_ConstantUtil.APTTUS_OTHER_PARTY_REVIEW;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Submit For Approvals
                ag.Apttus__Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Submit For Approvals
                ag.Apttus__Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Rejected
                ag.Apttus__Status__c = APTS_ConstantUtil.APPR_REJ;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Reset the status to execute Approved scenario
                ag.Apttus__Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Approved
                ag.Apttus__Status__c = APTS_ConstantUtil.APPR_REQ;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                update ag;
                
                //Send for signatures
                ag.Apttus__Status__c = APTS_ConstantUtil.OTHER_PARTY_SIGN;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
                //Payment entry
                Payment_Entry__c payObj = new Payment_Entry__c();
                payObj.Agreement__c = ag.Id;
                //payObj.CurrencyIsoCode = 'USD';
                payObj.Payment_Amount__c = 1000;
                payObj.Payment_Date__c = Date.today();
                payObj.Payment_Method__c = 'Wire';
                insert payObj;
                
                //Pending Positioning
                ag.APTPS_Owner_Signed__c = true;
                update ag;
                
                //Positioning Requested
                ag.APTPS_Request_Aircraft_Positioning__c = true;
                update ag;
                
                //Aircraft Positioned
                ag.APTPS_Aircraft_Positioned_State__c = 'Alaska';
                ag.APTPS_Aircraft_Positioned_Date__c = Date.today();
                update ag;
                
                //Reset Agreement status
                ag.Apttus__Status__c = APTS_ConstantUtil.OTHER_PARTY_SIGN;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
                //Fully Signed
                ag.Apttus__Status__c = APTS_ConstantUtil.FULLY_SIGNED;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
                //Activation
                ag.Apttus__Status__c = APTS_ConstantUtil.ACTIVATED;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_EFFECT;
                update ag;
                
                //Cancelled
                ag.Apttus__Status__c = APTS_ConstantUtil.CANCEL_REQ;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.CANCEL;
                update ag;
                
                //Test coverage for Interim flow
                delete payObj;
                Payment_Entry__c payDeposit = new Payment_Entry__c();
                payDeposit.Agreement__c = ag.Id;
                payDeposit.Payment_Amount__c = 100000;
                payDeposit.Payment_Date__c = Date.today();
                payDeposit.Payment_Method__c = 'Wire';
                insert payDeposit;
                
                Payment_Entry__c fullPayment = new Payment_Entry__c();
                fullPayment.Agreement__c = ag.Id;
                fullPayment.Payment_Amount__c = 100000;
                fullPayment.Payment_Date__c = Date.today();
                fullPayment.Payment_Method__c = 'Wire';
                insert fullPayment;
                
            }
            
            Apttus__APTS_Agreement__c childAgreement = [SELECT ID, Agreement_Status__c, Apttus__Status__c, Apttus__Status_Category__c 
                                                        FROM Apttus__APTS_Agreement__c 
                                                        WHERE Apttus__Parent_Agreement__c != NULL LIMIT 1];
            if(childAgreement != null) {
                childAgreement.Agreement_Status__c = APTS_ConstantUtil.AC_POSITIONED;
                update childAgreement;
                
                childAgreement.Apttus__Status__c = APTS_ConstantUtil.ACTIVATED;
                childAgreement.Apttus__Status_Category__c = APTS_ConstantUtil.IN_EFFECT;
                update childAgreement;
            }
        } catch(Exception e) {
            System.debug('APTPS_CLMLifeCycle_ShareTest.testShareCLM --> Error while executing test class. Error details --> '+e);
        }
    }
}