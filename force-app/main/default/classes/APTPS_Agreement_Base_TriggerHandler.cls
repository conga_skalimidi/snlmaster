/************************************************************************************************************************
@Name: APTPS_Agreement_Base_TriggerHandler
@Author: Conga PS Dev Team
@CreateDate:
@Description: Agreement Trigger Handler. As per trigger event, execute respective business logic from APTPS_Agreement_Helper
helper class.
************************************************************************************************************************
@ModifiedBy: Conga PS Dev Team
@ModifiedDate: 17/11/2020
@ChangeDescription: Added logic for Shares and Leases.
************************************************************************************************************************/
public class APTPS_Agreement_Base_TriggerHandler extends TriggerHandler{
    public override void afterInsert(){
        List<Apttus__APTS_Agreement__c> allAgList = Trigger.new;
        Map<Id, Apttus__APTS_Agreement__c> newMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.NewMap;
        Map<Id, Id> agQuoteMap = new Map<Id, Id>();
        List<Apttus__APTS_Agreement__c> SNLAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> otherAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ModAgList = new List<Apttus__APTS_Agreement__c>();
        Id onHoldRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.INTERIM_AGREEMENT_HOLD).getRecordTypeId();
        
        for(Apttus__APTS_Agreement__c ag : allAgList) {
            //GCM-7127: TODO, Discuss about it. Adding this condition to avoid recurssion
            if(onHoldRecType != ag.RecordTypeId) {
                if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    SNLAgList.add(ag);
                    if(ag.Apttus_QPComply__RelatedProposalId__c != null)
                        agQuoteMap.put(ag.Id, ag.Apttus_QPComply__RelatedProposalId__c);
                } else {
                    otherAgList.add(ag );
                }
            }
            //START: To set fields of agreement extension for Lease Termination / Deferment / Substitution modifications.
            if((APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c))
              && (APTS_ConstantUtil.LEASE_TERMINATION.equalsIgnoreCase(ag.APTS_Modification_Type__c) || APTS_ConstantUtil.SUBSTITUTION.equalsIgnoreCase(ag.APTS_Modification_Type__c) 
              || APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(ag.APTS_Modification_Type__c)) && APTS_ConstantUtil.AGREEMENT_MOD.equalsIgnoreCase(ag.APTS_New_Sale_or_Modification__c)) {
                ModAgList.add(ag);
                system.debug('ModAgList-->'+ModAgList);
            }
            //END: To set fields of agreement extension for Lease Termination / Deferment / Substitution modifications.
        }
        system.debug('SNLAgList-->'+SNLAgList);
        if(!SNLAgList.isEmpty()) {
            APTPS_Agreement_Helper.createSNLDocusignRecipients(SNLAgList);
            APTPS_Agreement_Helper.setSNLAgreementExtensionFields(SNLAgList); 
        }
        if(!otherAgList.isEmpty()) {
            APTPS_Agreement_Helper.createDocusignRecipients(otherAgList); 
            APTPS_Agreement_Helper.setAgreementExtensionFields(otherAgList);
        }
        //CLM Automation
        APTPS_Agreement_Helper.handleCLMAutomation(Trigger.new);
        
        //START: GCM-7127, Interim Lease agreement
        if(!agQuoteMap.isEmpty())
            APTPS_Agreement_Helper.handleInterimLease(agQuoteMap);
        //END: GCM-7127
        //START: To set fields of agreement extension for Lease Termination / Deferment / Substitution modifications.
        if(!ModAgList.isEmpty()) {
            APTPS_Agreement_Helper.setModAgreementExtensionFields(ModAgList);
        }
        //END: To set fields of agreement extension for Lease Termination / Deferment / Substitution modifications.
    }
    
    public override void beforeInsert(){
        List<Apttus__APTS_Agreement__c> allAgList = Trigger.new;
        APTPS_Agreement_Helper.setAgreementFields(Trigger.new);
        APTPS_Agreement_Helper.createAgreementExtensionRecord(Trigger.new);
        List<Apttus__APTS_Agreement__c> demoAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> SNLAgList = new List<Apttus__APTS_Agreement__c>();
        Map<Id,Apttus__APTS_Agreement__c> substitutionAgMap = new Map<Id,Apttus__APTS_Agreement__c>();
        Map<Id,Apttus__APTS_Agreement__c> subSizeIncAgMap = new Map<Id,Apttus__APTS_Agreement__c>();
        Id onHoldRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.INTERIM_AGREEMENT_HOLD).getRecordTypeId();
        
        for(Apttus__APTS_Agreement__c ag : allAgList) {
            ag.Apttus__VersionAware__c = false;
            if(onHoldRecType != ag.RecordTypeId && (APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c) ||  APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c))) {
                SNLAgList.add(ag);
            }
            //START: GCM-7337, GCM-7338 - Substitution Agreement Flow
            if(APTS_ConstantUtil.IN_AMENDMENT.equalsIgnoreCase(ag.Apttus__Status__c)){
                substitutionAgMap.put(ag.Id,ag);
            }
            //END: GCM-7337, GCM-7338
            //START: GCM-13137 - Size Increase with Substitution
            if(APTS_ConstantUtil.SUBSTITUTION.equalsIgnoreCase(ag.APTS_Additional_Modification_Type__c) && 
               APTS_ConstantUtil.SHARE_SIZE_INCREASE.equalsIgnoreCase(ag.APTS_Modification_Type__c)){
                   system.debug('Size Increase with Substitution-->');
                subSizeIncAgMap.put(ag.Id,ag);
                   system.debug('Size Increase subSizeIncAgMap-->'+subSizeIncAgMap);
            }
            //END: GCM-13137 - Size Increase with Substitution
        }
        system.debug('SNLAgList-->'+SNLAgList+' allAgList-->'+allAgList);
        
        if(!SNLAgList.isEmpty()) {
            system.debug('SNLAgList-->'+SNLAgList);
            APTPS_Agreement_Helper.stampSNLAgreementFields(SNLAgList); 
        }
        //GCM-7337, GCM-7338: Substitution Agreement Flow
        if(!substitutionAgMap.isEmpty())
            APTPS_Agreement_Helper.updateSubstitutionFields(substitutionAgMap);
        //END: GCM-7337, GCM-7338
        //START: GCM-13137 - Size Increase with Substitution
        if(!subSizeIncAgMap.isEmpty()) {
            system.debug('Size Increase with Substitution-->');
            APTPS_Agreement_Helper.updateSubstitutionShareSizeIncreaseSummary(subSizeIncAgMap);
        }
        //END: GCM-13137 - Size Increase with Substitution
    }
    
    public override void afterUpdate(){
        Map<Id, Apttus__APTS_Agreement__c> oldMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.OldMap;
        List<Apttus__APTS_Agreement__c> newList = (List<Apttus__APTS_Agreement__c>)Trigger.new;
        Map<Id,Apttus__APTS_Agreement__c> substitutionAgMap = new Map<Id,Apttus__APTS_Agreement__c>();
        String oldAgStatus, oldAgStatusCat, newAgStatus,oldAgFundStatus,newAgFundStatus, newStatus;
        APTPS_EmailNotification enObj = new APTPS_EmailNotification();
        
        Map<Id,List<Apttus__APTS_Agreement__c>> agrParentMap = new Map<Id,List<Apttus__APTS_Agreement__c>>();
        List<Apttus__APTS_Agreement__c> updateChildAgrList = new List<Apttus__APTS_Agreement__c>();
        set<id> agrIds = new set<id>();
        set<id> activateAgrIds = new set<id>();
        set<id> assignorAgrIds = new set<id>();
        for (Apttus__APTS_Agreement__c agr : newList){ 
            if(agr.Apttus__Parent_Agreement__c == null){
                agrIds.add(agr.id);
            }
        }
        
        for(Apttus__APTS_Agreement__c childAgreementObj : [SELECT Id, Apttus__Parent_Agreement__c,Agreement_Status__c,Funding_Status__c, Chevron_Status__c, APTS_Path_Chevron_Status__c, Document_Status__c, Apttus__Status__c, Apttus__Status_Category__c,APTS_Modification_Type__c,APTS_Additional_Modification_Type__c FROM Apttus__APTS_Agreement__c WHERE Apttus__Parent_Agreement__c IN :agrIds]) {
             if(agrParentMap.containsKey(childAgreementObj.Apttus__Parent_Agreement__c)) {
                List<Apttus__APTS_Agreement__c> listAgr = agrParentMap.get(childAgreementObj.Apttus__Parent_Agreement__c);
                listAgr.add(childAgreementObj);
                agrParentMap.put(childAgreementObj.Apttus__Parent_Agreement__c,listAgr);
            } else {
                agrParentMap.put(childAgreementObj.Apttus__Parent_Agreement__c,new List<Apttus__APTS_Agreement__c>{childAgreementObj});
            }
            
        }
        
        for (Apttus__APTS_Agreement__c ag : newList){ 
            Apttus__APTS_Agreement__c oldAgObj = oldMap.get(ag.Id);
            //START: Compare Agreement Prior details
            oldAgStatus = oldAgObj.Apttus__Status__c;
            oldAgStatusCat = oldAgObj.Apttus__Status_Category__c;
            newAgStatus = ag.Agreement_Status__c;
            oldAgFundStatus = oldAgObj.Funding_Status__c;
            newAgFundStatus = ag.Funding_Status__c;
            newStatus = ag.Apttus__Status__c;
            
            //Agreement Funded Contract 
            if (!APTS_ConstantUtil.FUNDED_CONTRACT.equalsIgnoreCase(oldAgFundStatus) && APTS_ConstantUtil.FUNDED_CONTRACT.equalsIgnoreCase(ag.Funding_Status__c)){
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    try{
                        String Business_Object = APTS_ConstantUtil.AGREEMENT_OBJ;
                        String Product_Code = APTS_ConstantUtil.CT_PRODUCT_CODE;
                        String Status = ag.Funding_Status__c;
                        String objId = ag.id;
                        String contactId = ag.Apttus__Primary_Contact__c;
                        enObj.call(Product_Code, new Map<String, Object>{'Id' => objId,'Business_Object'=>Business_Object,'Product_Code'=>Product_Code,'Status'=>Status,'ContactId'=>contactId});
                        
                    }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                        system.debug('Error while sending Email Notification for Agreement Id --> '+ag.id+' Error details --> '+e.errMsg);
                    }
                }
            }
            //Agreement Activaiton
            if (!APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(oldAgStatus) && !APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(oldAgStatusCat) && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(ag.Apttus__Status__c) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(ag.Apttus__Status_Category__c)){
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    try{
                        String Business_Object = APTS_ConstantUtil.AGREEMENT_OBJ;
                        String Product_Code = '';
                        if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                            Product_Code = APTS_ConstantUtil.CT_PRODUCT_CODE;
                        } else if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                            if(APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(ag.CurrencyIsoCode) ) {
                                Product_Code = APTS_ConstantUtil.NJUS_DEMO;
                            } else if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode) ) {
                                Product_Code = APTS_ConstantUtil.NJE_DEMO;
                            }
                            
                        }               
                        String Status = ag.Apttus__Status__c;
                        String objId = ag.id;
                        String contactId = ag.Apttus__Primary_Contact__c;
                        enObj.call(Product_Code, new Map<String, Object>{'Id' => objId,'Business_Object'=>Business_Object,'Product_Code'=>Product_Code,'Status'=>Status,'ContactId'=>contactId});
                        
                    }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                        system.debug('Error while sending Email Notification for Agreement Id --> '+ag.id+' Error details --> '+e.errMsg);
                    }
                }
                if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    //Terminate Child Interim Lease Agreement
                    if(agrParentMap.containsKey(ag.id)) {
                        for(Apttus__APTS_Agreement__c childAgr : agrParentMap.get(ag.id)){
                            childAgr = APTPS_SNL_Util.setAgreementStatus(childAgr, APTS_ConstantUtil.AGREEMENT_TERMINATED, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_SIGNED);
                            childAgr.Apttus__Status__c = APTS_ConstantUtil.AGREEMENT_TERMINATED;
                            childAgr.Apttus__Status_Category__c = APTS_ConstantUtil.AGREEMENT_TERMINATED;
                            //childAgr.Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_TERMINATED;
                            childAgr.Apttus__Termination_Date__c = ag.Apttus__Activated_Date__c;
                            updateChildAgrList.add(childAgr);
                        }
                        
                    }
                    
                }
                //For Modification unrelated Assignment  
                if(APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(ag.APTS_Additional_Modification_Type__c)) {
                    assignorAgrIds.add(ag.APTPS_Assignor_Agreement__c);
                }
                
                
                
            }
            //For Modifications Letter Agreement Extension and Add Enhancements
            if (APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(oldAgStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldAgStatusCat)&& APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(newStatus) && (APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c))){
                //For Letter Agreement Extension Modification Auto Activation After document fully signed
                if(APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equalsIgnoreCase(ag.APTS_Modification_Type__c)) {
                    activateAgrIds.add(ag.id);
                }                             
                
            }
            
            //For Letter Agreement Modification update Agreement status After document fully signed
            if (APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(oldAgStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldAgStatusCat)&& APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equalsIgnoreCase(ag.APTS_Modification_Type__c)&& (APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c))){
                activateAgrIds.add(ag.id);
            }
            //START: GCM-7337, GCM-7338 - Substitution Agreement Flow
            if(APTS_ConstantUtil.IN_AMENDMENT.equalsIgnoreCase(ag.Apttus__Status__c)){
                substitutionAgMap.put(ag.Id,ag);
            }
            //END: GCM-7337, GCM-7338
        }
        system.debug('activateAgrIds-->'+activateAgrIds);
        system.debug('assignorAgrIds-->'+assignorAgrIds);
        if(!updateChildAgrList.isEmpty()) {
            try{
                update updateChildAgrList;
            }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                system.debug('Error while updating Child Agreements. Error details --> '+e.errMsg);
            }
        }
        
        if(!activateAgrIds.isEmpty()) {
            try{
                APTPS_ActivateAgreementCtrl.autoActivateAgreements(activateAgrIds);
            }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                system.debug('Error while Activating Letter Agreements. Error details --> '+e.errMsg);
            }
        }
        if(!assignorAgrIds.isEmpty()) {
            try{
                APTPS_SNL_Util.handleUnrelatedAgreementFlow(assignorAgrIds);
            }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                system.debug('Error while updating unrelated Agreements. Error details --> '+e.errMsg);
            }
        }
        //GCM-7337, GCM-7338: Substitution Agreement Flow
        //if(!substitutionAgMap.isEmpty())
          //  APTPS_Agreement_Helper.updateSubstitutionDealSummaryAfterUpdate(substitutionAgMap);
        //END: GCM-7337, GCM-7338
    }
    
    public override void beforeUpdate() {
        //START: SNL Agreement lifecycle management
        List<Apttus__APTS_Agreement__c> newAg = Trigger.New;
        List<Apttus__APTS_Agreement__c> demoNewAgList =  new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> demoNJANewAgList =  new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctNewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> shareNJANewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> shareNJENewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> leaseNJANewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> leaseNJENewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> enhancementNJANewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> enhancementNJENewAgList = new List<Apttus__APTS_Agreement__c>();
        Map<Id,Apttus__APTS_Agreement__c> substitutionAgMap = new Map<Id,Apttus__APTS_Agreement__c>();
        Map<Id,Apttus__APTS_Agreement__c> subSizeIncAgMap = new Map<Id,Apttus__APTS_Agreement__c>();
        Map<Id,SObject> oldAgMap = Trigger.OldMap;
        Map<Id,SObject> demoOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> demoNJAOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> ctOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> shareNJAOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> shareNJEOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> leaseNJAOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> leaseNJEOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> enhancementNJAOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> enhancementNJEOldAgMap = new Map<Id,SObject>();
        Id onHoldRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.INTERIM_AGREEMENT_HOLD).getRecordTypeId();
        for(Apttus__APTS_Agreement__c ag : newAg) {
            if(APTPS_SNL_Util.isSNLProgramType(ag.APTPS_Program_Type__c)) {
                //NJE Demo product - Agreement
                if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)){
                    if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                        demoNewAgList.add(ag);
                        demoOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    } else {
                        demoNJANewAgList.add(ag);
                        demoNJAOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    }
                }               
                //Corporate Trial - Agreement
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    ctNewAgList.add(ag);
                    ctOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                }
                //Share Product - Agreement
                if(onHoldRecType != ag.RecordTypeId && APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) && ag.APTS_Modification_Type__c != APTS_ConstantUtil.ADD_ENHANCEMENTS) {
                    if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                        shareNJENewAgList.add(ag);
                        shareNJEOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    } else {
                        shareNJANewAgList.add(ag);
                        shareNJAOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    }
                }
                //Lease Product - Agreement
                if(APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c) && ag.APTS_Modification_Type__c != APTS_ConstantUtil.ADD_ENHANCEMENTS) {
                    if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                        leaseNJENewAgList.add(ag);
                        leaseNJEOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    } else {
                        leaseNJANewAgList.add(ag);
                        leaseNJAOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    }
                }
                
                //Add Enhancement Modification - Agreement
                if(APTS_ConstantUtil.ADD_ENHANCEMENTS.equalsIgnoreCase(ag.APTS_Modification_Type__c)) {
                    if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                        enhancementNJENewAgList.add(ag);
                        enhancementNJEOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    } else {
                        enhancementNJANewAgList.add(ag);
                        enhancementNJAOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    }
                }
            }
            //START: GCM-7337, GCM-7338 - Substitution Agreement Flow
            if(APTS_ConstantUtil.IN_AMENDMENT.equalsIgnoreCase(ag.Apttus__Status__c)){
                substitutionAgMap.put(ag.Id,ag);
            }
            //END: GCM-7337, GCM-7338
            //START: GCM-13137 - Size Increase with Substitution
            if(APTS_ConstantUtil.SUBSTITUTION.equalsIgnoreCase(ag.APTS_Additional_Modification_Type__c) && APTS_ConstantUtil.SHARE_SIZE_INCREASE.equalsIgnoreCase(ag.APTS_Modification_Type__c) &&
              APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) &&
              (APTS_ConstantUtil.NETJETS_USD.equalsIgnoreCase(ag.Program__c) || APTS_ConstantUtil.NETJETS_EUR.equalsIgnoreCase(ag.Program__c))){
                   system.debug('Size Increase with Substitution-->');
                subSizeIncAgMap.put(ag.Id,ag);
                   system.debug('Size Increase subSizeIncAgMap-->'+subSizeIncAgMap);
            }
            //END: GCM-13137 - Size Increase with Substitution
        }
        //NJE Demo product - Agreement Status update
        if(!demoNewAgList.isEmpty()) {
            system.debug('demoNewAgList-->'+demoNewAgList);
            system.debug('demoOldAgMap-->'+demoOldAgMap);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJE_DEMO  ,new Map<String, Object>{'newAgrList'=>demoNewAgList,'oldAgrList'=>demoOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Demo-->'+e.errMsg);
            }
            
        }
        //NJA Demo product - Agreement Status update
        if(!demoNJANewAgList.isEmpty()) {
            system.debug('demoNJANewAgList-->'+demoNJANewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJUS_DEMO   ,new Map<String, Object>{'newAgrList'=>demoNJANewAgList,'oldAgrList'=>demoNJAOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Demo-->'+e.errMsg);
            }
            
        }
        
        //Corporate Trial - Agreement Status update
        if(!ctNewAgList.isEmpty()) {
            system.debug('ctNewAgList-->'+ctNewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.CT_PRODUCT_CODE ,new Map<String, Object>{'newAgrList'=>ctNewAgList,'oldAgrList'=>ctOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Corporate Trail-->'+e.errMsg);
            }
            
        }
        
        //Share NJA - Agreement Status update
        if(!shareNJANewAgList.isEmpty()) {
            system.debug('shareNJANewAgList-->'+shareNJANewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJA_SHARE_CODE ,new Map<String, Object>{'newAgrList'=>shareNJANewAgList,'oldAgrList'=>shareNJAOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Share -->'+e.errMsg);
            } 
        }
        
        //Share NJE - Agreement Status update
        if(!shareNJENewAgList.isEmpty()) {
            system.debug('shareNJENewAgList-->'+shareNJENewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJE_SHARE_CODE ,new Map<String, Object>{'newAgrList'=>shareNJENewAgList,'oldAgrList'=>shareNJEOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Share -->'+e.errMsg);
            } 
        }
        //Lease NJA - Agreement Status update
        if(!leaseNJANewAgList.isEmpty()) {
            system.debug('leaseNJANewAgList-->'+leaseNJANewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJA_LEASE_CODE ,new Map<String, Object>{'newAgrList'=>leaseNJANewAgList,'oldAgrList'=>leaseNJAOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for lease -->'+e.errMsg);
            } 
        }
        
        //Lease NJE - Agreement Status update
        if(!leaseNJENewAgList.isEmpty()) {
            system.debug('leaseNJENewAgList-->'+leaseNJENewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJE_LEASE_CODE ,new Map<String, Object>{'newAgrList'=>leaseNJENewAgList,'oldAgrList'=>leaseNJEOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for lease -->'+e.errMsg);
            } 
        }
        
        //Add Enhancements NJA - Agreement Status update
        if(!enhancementNJANewAgList.isEmpty()) {
            system.debug('enhancementNJANewAgList-->'+enhancementNJANewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.SNL_NJA_ENHANCEMENT,new Map<String, Object>{'newAgrList'=>enhancementNJANewAgList,'oldAgrList'=>enhancementNJAOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Enhancements -->'+e.errMsg);
            } 
        }
        
        //Add Enhancements NJE - Agreement Status update
        if(!enhancementNJENewAgList.isEmpty()) {
            system.debug('enhancementNJENewAgList-->'+enhancementNJENewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.SNL_NJE_ENHANCEMENT,new Map<String, Object>{'newAgrList'=>enhancementNJENewAgList,'oldAgrList'=>enhancementNJEOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Enhancements -->'+e.errMsg);
            } 
        }
        //END: SNL Agreement lifecycle
        //GCM-7337, GCM-7338: Substitution Agreement Flow
        if(!substitutionAgMap.isEmpty())
            APTPS_Agreement_Helper.updateSubstitutionDealSummaryBeforeUpdate(substitutionAgMap);
        //END: GCM-7337, GCM-7338
        //START: GCM-13137 - Size Increase with Substitution
        if(!subSizeIncAgMap.isEmpty()) {
            system.debug('Size Increase with Substitution-->');
            APTPS_Agreement_Helper.updateSubstitutionShareSizeIncreaseSummary(subSizeIncAgMap);
        }
        //END: GCM-13137 - Size Increase with Substitution
    }
}