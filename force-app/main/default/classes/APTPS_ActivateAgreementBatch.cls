/************************************************************************************************************************
@Name: APTPS_ActivateAgreementBatch
@Author: Conga PS Dev Team
@CreateDate: 09 Feb 2021
@Description: Batch Class to Activate Agreements
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_ActivateAgreementBatch implements Database.Batchable<Id>{
    List<Id> activateAgrIds = null;
    public APTPS_ActivateAgreementBatch(List<Id> activateAgrIds) {
        this.activateAgrIds = activateAgrIds;
    }
    
    public List<Id> start(Database.BatchableContext BC) {
        return activateAgrIds;
    }
    
    public void execute(Database.BatchableContext BC, List<Id> activateAgrIds) {
        List<Attachment> files = new List<Attachment>();
        List<String> activateDocIds = new List<String>();
        Map<Id,List<String>> agreementFilesMap = new Map<Id,List<String>>();
        List<Attachment> dummyAttachmentList = new List<Attachment>();
        String[] remDocIds = new String[]{};        
        
        for(Attachment file : [SELECT Id,Name, Body, ContentType,parentId FROM Attachment WHERE parentId IN:activateAgrIds]) {
            agreementFilesMap.put(file.parentId,new List<String>{String.ValueOf(file.id)});
        }
        for(id agId:activateAgrIds){
            if(!agreementFilesMap.containsKey(agId)){
                Attachment dummyAttachment = new Attachment();
                dummyAttachment.Name = 'Test';
                dummyAttachment.Body = Blob.valueOf('Test');
                dummyAttachment.ContentType = 'text/plain';
                dummyAttachment.ParentId = agId;
                dummyAttachmentList.add(dummyAttachment);
            }
        }
        if(!dummyAttachmentList.isEmpty()) {
            insert dummyAttachmentList;
        }
        for(Attachment file : [SELECT Id,Name, Body, ContentType,parentId FROM Attachment WHERE parentId IN:activateAgrIds]) {
            agreementFilesMap.put(file.parentId,new List<String>{String.ValueOf(file.id)});
        }
        for(id recordId:activateAgrIds) {
            Boolean response = Apttus.AgreementWebService.activateAgreement(recordId, agreementFilesMap.get(recordId), remDocIds);
            system.debug('response-->'+response);
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        system.debug('Finish');
        list<Apttus__APTS_Agreement__c> updateAgreementList = new list<Apttus__APTS_Agreement__c>();
        for(id agId:activateAgrIds){ 
            updateAgreementList.add(new Apttus__APTS_Agreement__c(id=agId,APTS_Activate_Click__c=true));
        }
        if(!updateAgreementList.isEmpty()){
            update updateAgreementList;
        }
            
    }
}