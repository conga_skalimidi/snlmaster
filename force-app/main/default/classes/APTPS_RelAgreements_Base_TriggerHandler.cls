/************************************************************************************************************************
@Name: APTPS_RelAgreements_Base_TriggerHandler
@Author: Conga PS Dev Team
@CreateDate:
@Description: Related Agreement Trigger Handler. As per trigger event, execute respective business logic from APTPS_Agreement_Helper
helper class.
************************************************************************************************************************
@ModifiedBy: Conga PS Dev Team
@ModifiedDate: 13/04/2022
@ChangeDescription: Added logic for Shares and Leases.
************************************************************************************************************************/
public class APTPS_RelAgreements_Base_TriggerHandler extends TriggerHandler{
    public override void afterInsert(){
        List<Apttus__APTS_Related_Agreement__c> allAgList = Trigger.new;
        Map<Id, Apttus__APTS_Related_Agreement__c> newMap = (Map<Id, Apttus__APTS_Related_Agreement__c>)Trigger.NewMap;
        Map<Id,Apttus__APTS_Related_Agreement__c> substitutionAgMap = new Map<Id,Apttus__APTS_Related_Agreement__c>();
        
        for(Apttus__APTS_Related_Agreement__c ag : allAgList) {
            //START: GCM-7337, GCM-7338 - Substitution Agreement Flow
            if(ag.Apttus__APTS_Contract_From__c != NULL && ag.Apttus__APTS_Contract_To__c != NULL
               && APTS_ConstantUtil.IS_AMENDMENT_FOR.equalsIgnoreCase(ag.Apttus__Relationship_To_Type__c)){
                   substitutionAgMap.put(ag.Id,ag);
               }
        }
        if(!substitutionAgMap.isEmpty())
            APTPS_RelatedAgreement_Helper.updateSubstitutionDealSummaryBeforeUpdate(substitutionAgMap);
    }
}