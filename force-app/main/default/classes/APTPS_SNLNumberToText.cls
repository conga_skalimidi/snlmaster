public class APTPS_SNLNumberToText {
    public static String populateAgreementExtNumberToTextFields(integer numberValue){
         String spText = null;
        system.debug('numberValue-->'+numberValue);
        /*integer integerPlacesTot = numberValue.indexOf('.');
        String numberVal = numberValue.substring(0, integerPlacesTot);
        system.debug('numberVal-->'+numberVal);*/
       
        /*APTPS_Lease_Number_to_Text__mdt num = APTPS_Lease_Number_to_Text__mdt.getInstance('X'+String.vanumberVal);
        system.debug('num -->'+num );
        if(num != null)
            spText = num.Lease_Number_Text__c; 
        return spText;*/
        
        Map<String,APTPS_Lease_Number_to_Text__mdt> sp = APTPS_Lease_Number_to_Text__mdt.getAll();
        for(String spRec: sp.keySet()){
            if((Integer.ValueOf(sp.get(spRec).MasterLabel))==numberValue){
            system.debug('num -->'+sp.get(spRec).Lease_Number_Text__c );
                spText = sp.get(spRec).Lease_Number_Text__c;  
                break;
            }
        }
        return spText;
    }
}