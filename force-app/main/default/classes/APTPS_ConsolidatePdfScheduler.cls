/************************************************************************************************************************
@Name: APTPS_ConsolidatePdfScheduler
@Author: Conga PS Dev Team
@CreateDate: 29 Dec 2021
@Description: Schedulable Class for creating on PDF's on Proposal Object.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
public class APTPS_ConsolidatePdfScheduler implements Schedulable {
    Id qId = null;
    Integer numberOfAttemptsMade;
    public APTPS_ConsolidatePdfScheduler(Id qId, Integer numberOfAttemptsMade){
        system.debug('qId received --> '+qId);
        this.qId = qId;
        this.numberOfAttemptsMade = numberOfAttemptsMade;
    }
    public void execute(SchedulableContext sc) {
        APTPS_Account_Consolidation_Settings__c fetchAccConsData = APTPS_Account_Consolidation_Settings__c.getOrgDefaults();
        Integer retryCount = Integer.valueOf(fetchAccConsData.Number_Of_Retries__c);
        if(numberOfAttemptsMade < retryCount) {
            List<APTPS_Account_Interest_Information__c> getAIIdetails =[SELECT Id, Name,APTPS_Proposal__c,APTPS_Configuration_Summary__c
                                                                        FROM APTPS_Account_Interest_Information__c
                                                                        WHERE APTPS_Proposal__c =: qId];
            if(!getAIIdetails.isEmpty()) {
                //Generate PDF and attach
                String pdfName = fetchAccConsData.PDF_File_Name__c;
                //APTPS_AccountConsolidationOnQuote.getCustomSettings();
                
                Pagereference ref= Page.APTPS_CreateConsolidationPdfVf;
                ref.getParameters().put('qId',qId);
                List<Attachment> AttachedFile = [SELECT Id, Name, Body, ContentType 
                                                 FROM Attachment 
                                                 WHERE ParentId =: qId and Name LIKE : '%'+pdfName+'%'];
                Apttus_Proposal__Proposal__c quoProp = [SELECT id, APTPS_Is_Share_Lease_Enhancement__c,Document_Status__c
                                                        FROM Apttus_Proposal__Proposal__c
                                                        WHERE id =: qId
                                                        LIMIT 1];
                
                system.debug('AttachedFile-->'+AttachedFile);
                Attachment at=new Attachment();
                blob b = ref.getContentAsPDF();
                at.ParentId=qId;
                system.debug('line 27-->'+ at.ParentId);
                at.name= pdfName+'.pdf';
                at.ContentType = 'application/pdf';
                at.Body=b;
                if(AttachedFile.size() > 0){
                    delete AttachedFile;
                }
                insert at;
                
                if(quoProp.APTPS_Is_Share_Lease_Enhancement__c = true){
                    quoProp.Document_Status__c = 'Document Generated';
                }
                update quoProp;
            } else {
                String nextExecutionTime = Datetime.now().addSeconds(3).format('s m H d M ? yyyy');  
                System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextExecutionTime, new APTPS_ConsolidatePdfScheduler(qId, numberOfAttemptsMade++));
            }
        }
    }
}