public class APTS_AssetLineItemBaseTriggerHandler extends TriggerHandler {  
    
    public override void beforeUpdate() {
        Map<Id,SObject> oldALIMap = Trigger.OldMap;
        List<Apttus_Config2__AssetLineItem__c> newAssetLIs = Trigger.New;
        Map<Id,Apttus_Config2__AssetLineItem__c> assetLineItemsInScope = new Map<Id,Apttus_Config2__AssetLineItem__c>();
        for(Apttus_Config2__AssetLineItem__c ali : newAssetLIs) { 
            Apttus_Config2__AssetLineItem__c oldALI = (Apttus_Config2__AssetLineItem__c)oldALIMap.get(ali.id);
            system.debug('oldALI-->'+oldALI);
            system.debug('ali-->'+ali);
            if(ali.Apttus_CMConfig__AgreementId__c != null && oldALI.Apttus_CMConfig__AgreementId__c !=null && ali.Apttus_CMConfig__AgreementId__c != oldALI.Apttus_CMConfig__AgreementId__c && ali.APTS_Original_Agreement__c == null) {
                ali.APTS_Original_Agreement__c = oldALI.Apttus_CMConfig__AgreementId__c;
            }
            //START: START DATE, END DATE for MODIFICATIONS : Updating start and end date fields
            if(ali.Apttus_CMConfig__AgreementId__c != null && (ali.Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c.equalsIgnoreCase(APTS_ConstantUtil.SHARE) || 
                ali.Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c.equalsIgnoreCase(APTS_ConstantUtil.LEASE)))
                {
                    assetLineItemsInScope.put(ali.Id, ali);
                }
            //END: START DATE, END DATE for MODIFICATIONS : Updating start and end date fields
        }
        if(!assetLineItemsInScope.isEmpty()) {
            APTS_AssetLineItemBaseTriggerHelper.updateStartEndDateFields(assetLineItemsInScope);
        }
         
    }
    
    public override void afterUpdate() {
        Map<Id,SObject> oldALIMap = Trigger.OldMap;
        List<Apttus_Config2__AssetLineItem__c> newAssetLIs = Trigger.New;
        set<id> assetIdList = new set<id>();
        
         system.debug('>>>>>>>>>>afterUpdate - newAssetLIs>>>>>> ' + newAssetLIs);
         APTS_AssetLineItemBaseTriggerHelper.updateEHCOnParentAssetPAV(newAssetLIs);
        for(Apttus_Config2__AssetLineItem__c ali : newAssetLIs) {
            Apttus_Config2__AssetLineItem__c oldALI = (Apttus_Config2__AssetLineItem__c)oldALIMap.get(ali.id);
            if(ali.Apttus_CMConfig__AgreementId__c != null && oldALI.Apttus_CMConfig__AgreementId__c !=null && ali.Apttus_CMConfig__AgreementId__c != oldALI.Apttus_CMConfig__AgreementId__c && ali.APTS_Original_Agreement__c != null) {
                assetIdList.add(ali.id);
            }
        }
        if(!assetIdList.isEmpty()) {
            System.enqueueJob(new APTPS_UpdateAssetAgrQueueble(assetIdList));
        }
       
    }   
}