/************************************************************************************************************************
@Name: APTPS_UnrelatedAssignmentController
@Author: Conga PS Dev Team
@CreateDate: 21 FEB 2022
@Description: APTPS_GetUnrelatedAssignmentDetails controller. It is used for Share & Lease Unrelated Assignment.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
public class APTPS_UnrelatedAssignmentController {
    @AuraEnabled
    public static List<Apttus__APTS_Agreement__c> getRelatedAgreements(String accId, String currencyCode) {
        return [SELECT Id, Name, Apttus__FF_Agreement_Number__c FROM Apttus__APTS_Agreement__c 
                WHERE Apttus__Account__c =: accId 
                AND (APTPS_Program_Type__c =: APTS_ConstantUtil.SHARE OR APTPS_Program_Type__c =: APTS_ConstantUtil.LEASE) 
                AND Agreement_Status__c =: APTS_ConstantUtil.STATUS_ACTIVE 
                AND CurrencyIsoCode =: currencyCode];
    }
    
    @AuraEnabled
    public static List<Apttus_Config2__AccountLocation__c> getLegalEntities(String accId) {
        return [SELECT Id, Name FROM Apttus_Config2__AccountLocation__c 
                WHERE Apttus_Config2__AccountId__c =: accId];
    }
    
    @AuraEnabled
    public static boolean generateConfig(String agId, String currentAccId, String legalEntityId, String newQuoteId) {
        Boolean result = false;
        system.debug('APTPS_UnrelatedAssignmentController.generateConfig: Input parameters, agId --> '+agId+' currentAccId --> '+currentAccId+' legalEntityId --> '+legalEntityId+' newQuoteId --> '+newQuoteId);
        if(agId == null || currentAccId == null || legalEntityId == null || newQuoteId == null) 
            return result;
        
        try{
            //Get Quote/Proposal Id
            Apttus__APTS_Agreement__c agObj = [SELECT Apttus_QPComply__RelatedProposalId__c FROM Apttus__APTS_Agreement__c WHERE Id =: agId LIMIT 1];
            Id assetQuoteId = agObj != null ? agObj.Apttus_QPComply__RelatedProposalId__c : null;
            
            //Copy configuration from Asset Proposal to New Proposal
            Apttus_Config2__ProductConfiguration__c  config = new Apttus_Config2__ProductConfiguration__c();
            Boolean apiResult = Apttus_QPConfig.QPConfigWebService.copyProductConfiguration(assetQuoteId, newQuoteId);
            if(apiResult){
                config = [SELECT Id, Apttus_Config2__Status__c, Apttus_Config2__AccountId__c  
                          FROM Apttus_Config2__ProductConfiguration__c 
                          WHERE Apttus_QPConfig__Proposald__c = :newQuoteId];
                config.Apttus_Config2__Status__c = APTS_ConstantUtil.FINALIZED;
                //config.Apttus_Config2__Status__c = 'Saved';
                config.Apttus_Config2__AccountId__c = currentAccId;
                update config;
            }
            
            //Update Line Items
            Id configID = config.id;
            //Set<Id> attrIdSet = new Set<Id>();
            List<Apttus_Config2__LineItem__c> lineList = [SELECT Id, Apttus_Config2__LocationId__c, Apttus_Config2__AttributeValueId__c, 
                                                          Apttus_Config2__BasePriceOverride__c, Apttus_Config2__NetUnitPrice__c, 
                                                          APTS_Modification_Type__c, APTS_Additional_Modification_Type__c, 
                                                          Apttus_Config2__LineType__c, Apttus_Config2__OptionId__r.Family, 
                                                          Apttus_Config2__ChargeType__c 
                                                          FROM Apttus_Config2__LineItem__c 
                                                          WHERE Apttus_Config2__ConfigurationId__c = :configID];
            for(Apttus_Config2__LineItem__c li : lineList){
                li.Apttus_Config2__LocationId__c = legalEntityId;
                li.Apttus_Config2__BasePriceOverride__c = li.Apttus_Config2__NetUnitPrice__c;
            }
            update lineList;
           
            //Delete Proposal Line Items
            List<Apttus_Proposal__Proposal_Line_Item__c> plis = [SELECT Id FROM Apttus_Proposal__Proposal_Line_Item__c WHERE Apttus_Proposal__Proposal__c =: newQuoteId];
            system.debug('Delete PLIs, count --> '+plis.size());
            if(!plis.isEmpty())
                delete plis;
            
            //Stamp Assignor Agreement Id on Current/New Quote/Proposal
            Apttus_Proposal__Proposal__c qObj = new Apttus_Proposal__Proposal__c(Id = newQuoteId);
            qObj.APTPS_Assignor_Agreement__c = agId;
            update qObj;
            
            result = true;
        } catch(Exception e) {
            system.debug('Error while generating unrelated assignment configuration. Error details --> '+e);
            result = false;
        }
        return result;
    }
}