/************************************************************************************************************************
@Name: APTPS_DealSummary_Demo
@Author: Conga PS Dev Team
@CreateDate: 17 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_DealSummary_Demo {
    /** 
    @description: Read PAVs and prepare deal summary
    @param: Quote Id, Currency Sign, List of Proposal Lines
    @return: Final value of Deal Summary
    */
    private static String getSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        //Get required Proposal Line Item
        Apttus_Proposal__Proposal_Line_Item__c demoPLI = null;
        if(quoteLines != null) {
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c)) {
                    demoPLI = pli;
                    break;
                }
            }
        }
        
        String dsText = '';
        if(demoPLI != null) {
            dsText += '<b>Transaction Type:</b> Demo Flight<br/><br/>';
            dsText += '<b>Type of Demo:</b> '+demoPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Type_of_Demo__c+'<br/><br/>';
            if(currencySign=='$'){
                dsText += '<b>Aircraft Type:</b> '+demoPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Demo_Aircraft__c+'<br/><br/>';
                dsText += '<b>Rate Type:</b> '+demoPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Rate_Type__c+'<br/><br/>';
            } else if(currencySign=='€') {
                dsText += '<b>Aircraft Type:</b> '+demoPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Demo_Aircraft__c+'<br/><br/>';
            	dsText += '<b>Rate Type:</b> '+demoPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Rate_Type__c+'<br/><br/>';
            }
            dsText += '<b>Rate:</b> '+currencySign+demoPLI.Apttus_QPConfig__NetPrice__c;
        }
        return dsText;
    }
    
    /** 
    @description: NJUS Demo implementation
    @param:
    @return: 
    */
    public class NJUS_Summary implements APTPS_DealSummaryInterface {
        public Apttus_Proposal__Proposal__c updateSummary(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating DEMO NJUS deal summary');
            Id qId = (Id)args.get('qId');
            String dsText = APTPS_DealSummary_Demo.getSummary(qId, '$', quoteLines);
            Apttus_Proposal__Proposal__c quoteToUpdate = new Apttus_Proposal__Proposal__c(Id = qId);
            quoteToUpdate.Deal_Description__c = dsText;
            return quoteToUpdate;
        }
    }
    
    /** 
    @description: NJE Demo implementation
    @param:
    @return: 
    */
    public class NJE_Summary implements APTPS_DealSummaryInterface {
        public Apttus_Proposal__Proposal__c updateSummary(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating DEMO NJE deal summary');
            Id qId = (Id)args.get('qId');
            String dsText = APTPS_DealSummary_Demo.getSummary(qId, '€', quoteLines);
            Apttus_Proposal__Proposal__c quoteToUpdate = new Apttus_Proposal__Proposal__c(Id = qId);
            quoteToUpdate.Deal_Description__c = dsText;
            return quoteToUpdate;
        }
    }
}