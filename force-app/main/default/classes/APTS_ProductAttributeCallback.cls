/*******************************************************************************************************************************************
@Name: APTS_ProductAttributeCallback
@Author: Avinash Bamane
@CreateDate: 15/10/2019
@Description: In case of ABO, there is need to copy attribute values from asset line item to newly added option.
Business Logic - In case of ABO, consider funding date for all assets which are selected in cart.
Pick Term and Enhancement options with most recent funding date and copy attributes 
from these options to newly added Term and Enhancement options.
********************************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 20/11/2019
@ChangeDescription: For 'Expired Hours Card', need to get 'Active Until Date' or 'Grace Period Date' from Asset Line Item
Use these dates to populate 'Requested End Date'

@ModifiedBy: Avinash Bamane
@ModifiedDate: 10/12/2019
@ChangeDescription: For 'Direct Conversion', need to get 'Death or Disability Clause' and 'Death or Disability Recipient 2' from Asset Line Item
Stamp these values on Term option for newly added card.

@ModifiedBy: Anjali Kumari
@ModifiedDate: 25/09/2020
@ChangeDescription: GCM-9207 : For 100-Hour card, Auto check split payment on Enhancement option
********************************************************************************************************************************************/
global with sharing class APTS_ProductAttributeCallback implements Apttus_Config2.CustomClass.IProductAttributeCallback {
    private Apttus_Config2.ProductConfiguration cart = null;
    private Apttus_Config2__ProductConfiguration__c configSO;
    private Apttus_Config2__AssetAttributeValue__c assetTermPAV = null;
    private String modificationType = '';
    private String additionalModificationType = '';
    private Date activeUntilDate;
    private Date gracePeriodEndDate;
    private Date requestedEndDate;
    private List<Apttus_Config2__AssetAttributeValue__c> assetEnhancementPAVList = new List<Apttus_Config2__AssetAttributeValue__c>();
     
    global void start(Apttus_Config2.ProductConfiguration config) {
        system.debug('***ProdAttrCallBack > start() > ' + config);
        Map<String, Id> optionAssetLineItemMap = new Map<String, Id>();
        this.cart = config;
        this.configSO = cart.getConfigSO();
        //String proposalModificationType = configSO.Apttus_QPConfig__Proposald__r.APTS_Modification_Type__c;
        // Get Modification Type from the proposal.
        // TODO: Get modification type from Line Item custom settings and remove this SOQL.
        Apttus_Config2__ProductConfiguration__c relatedProposalList = [SELECT Apttus_QPConfig__Proposald__r.APTS_Modification_Type__c, Apttus_QPConfig__Proposald__r.APTS_Additional_Modification_Type__c
                                                                       FROM Apttus_Config2__ProductConfiguration__c
                                                                       WHERE Id =: configSO.Id];
        if (relatedProposalList != null) {
            modificationType = relatedProposalList.Apttus_QPConfig__Proposald__r.APTS_Modification_Type__c != null ? relatedProposalList.Apttus_QPConfig__Proposald__r.APTS_Modification_Type__c : '';
            additionalModificationType = relatedProposalList.Apttus_QPConfig__Proposald__r.APTS_Additional_Modification_Type__c !=null ? relatedProposalList.Apttus_QPConfig__Proposald__r.APTS_Additional_Modification_Type__c : '';
        }
        // Check if Modification type is 'Direct Conversion' OR 'Direct Conversion + Term Extension. If yes, then look for any assets and get required details.
        if(modificationType == APTS_ConstantUtil.DIRECT_CONVERSION) {
            // TODO: Get Term and Enhancement results in one SOQL and check feasibility.
            List<Apttus_Config2__LineItem__c> assetLiAttrTermList = [SELECT Id, Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__c 
                                                                     FROM Apttus_Config2__LineItem__c 
                                                                     WHERE Apttus_Config2__ConfigurationId__c =: configSO.Id 
                                                                     AND Apttus_Config2__OptionId__r.ProductCode in ('NJUS_TERM','NJE_TERM') 
                                                                     AND Apttus_Config2__LineStatus__c != 'New' order by Apttus_Config2__AssetLineItemId__r.Funding_Date__c desc];
            if(!assetLiAttrTermList.isEmpty()) {
                Apttus_Config2__LineItem__c assetTerm = assetLiAttrTermList[0];
                assetTermPAV = [SELECT Delayed_Start_Amount_Months__c, Initial_Term_Amount_Months__c, Grace_Period_Amount_Months__c, Override_Term__c, Overridden_Delayed_Start_Months__c, Overridden_Grace_Period_Months__c, Overridden_Initial_Term_Months__c
                                FROM Apttus_Config2__AssetAttributeValue__c 
                                WHERE Id =: assetTerm.Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__c];
            }
            // Logic to iterate through each Enhancements and pick values accordingly.
            List<Apttus_Config2__LineItem__c> assetLiAttrEnhancementList = [SELECT Id, Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__c 
                                                                            FROM Apttus_Config2__LineItem__c 
                                                                            WHERE Apttus_Config2__ConfigurationId__c =: configSO.Id 
                                                                            AND Apttus_Config2__OptionId__r.ProductCode in ('Enhancements','Enhancements_NJE') 
                                                                            AND Apttus_Config2__LineStatus__c != 'New' order by Apttus_Config2__AssetLineItemId__r.Funding_Date__c desc];
            List<Id> assetPavId = new List<Id>();
            for(Apttus_Config2__LineItem__c li : assetLiAttrEnhancementList) {
                assetPavId.add(li.Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__c);
            }
            // Pick latest asset attribute values with latest funding date
            if(!assetPavId.isEmpty()) {
                assetEnhancementPAVList = [SELECT Non_Standard__c, Death_or_Disability_Recipient__c, Death_or_Disability_Clause__c, APTS_Death_or_Disability_Recipient_2__c, APTS_Death_or_Disability_Recipient__c, APTS_Death_or_Disability_Recipient_txt_2__c
                                           FROM Apttus_Config2__AssetAttributeValue__c 
                                           WHERE Id IN :assetPavId
                                           ORDER by Apttus_Config2__AssetLineItemId__r.Funding_Date__c desc];
            }
        }
        // In case of EXPIRED HOURS CARD, need to populate Requested End Date
        if(modificationType == APTS_ConstantUtil.EXPIRED_HOURS_CARD) {
            // Get Active Until Date and Grace Period End Date from Term Asset Line Item.
            List<Apttus_Config2__LineItem__c> assetLiAttrTermList = [SELECT Id, Apttus_Config2__AssetLineItemId__r.Active_Until_Date__c, Apttus_Config2__AssetLineItemId__r.Grace_Period_End_Date__c
                                                                     FROM Apttus_Config2__LineItem__c 
                                                                     WHERE Apttus_Config2__ConfigurationId__c =: configSO.Id 
                                                                     AND Apttus_Config2__OptionId__r.ProductCode in ('NJUS_TERM','NJE_TERM') 
                                                                     AND Apttus_Config2__LineStatus__c != 'New' order by Apttus_Config2__AssetLineItemId__r.Funding_Date__c desc];
            if(!assetLiAttrTermList.isEmpty()) {
                Apttus_Config2__LineItem__c assetTerm = assetLiAttrTermList[0];
                activeUntilDate = assetTerm.Apttus_Config2__AssetLineItemId__r.Active_Until_Date__c;
                gracePeriodEndDate = assetTerm.Apttus_Config2__AssetLineItemId__r.Grace_Period_End_Date__c;
                if(activeUntilDate != null) {
                    requestedEndDate = activeUntilDate.addMonths(6);
                } else if(gracePeriodEndDate != null) {
                	requestedEndDate = gracePeriodEndDate.addMonths(6);
                }
            }
        }
        
        //SNL debug POC
        /*system.debug('Getting detais for SNL');
        List<Apttus_Config2__LineItem__c> snlLis = [SELECT Id, Apttus_Config2__OptionId__r.Name, Apttus_Config2__OptionId__r.Cabin_Class_Rank__c 
                                                    FROM Apttus_Config2__LineItem__c 
                                                    WHERE Apttus_Config2__ConfigurationId__c =: configSO.Id 
                                                    AND Apttus_Config2__OptionGroupLabel__c = 'Aircraft Type' 
                                                    AND Apttus_Config2__LineType__c = 'Option' 
                                                    AND Apttus_Config2__IsPrimaryLine__c = true];
        if(!snlLis.isEmpty())
        	system.debug('SNL LIs --> '+'Name --> '+snlLis[0].Apttus_Config2__OptionId__r.Name+' Rank -->'+snlLis[0].Apttus_Config2__OptionId__r.Cabin_Class_Rank__c);*/
        //END
    }
    
    global void setDefaults(Apttus_Config2.LineItem lineItem, Apttus_Config2__ProductAttributeValue__c prodAttr) {
        system.debug('Debug PAV, inside defaults');
        Apttus_Config2__LineItem__c item = lineItem.getLineItemSO();
        //START: SNL POC
        /*system.debug('Line item details in PAV --> '+item);
        //system.debug('Family --> '+item.Apttus_Config2__OptionId__r.Family+' Rank --> '+item.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c);
        system.debug('Family --> '+item.Apttus_Config2__OptionId__r.Family);
        if('Option'.equalsIgnoreCase(item.Apttus_Config2__LineType__c) && item.Apttus_Config2__IsPrimaryLine__c 
           && item.Apttus_Config2__OptionId__c != null && 'Aircraft'.equalsIgnoreCase(item.Apttus_Config2__OptionId__r.Family))
            system.debug('AC LINE');*/
        //END: SNL POC
        List<String> productNameList = new List<String>{'Enhancements'};
        // TODO: Read Product Code and use it instead of description.
        if(assetTermPAV != null && item.Apttus_Config2__Description__c == 'Term') {
            /*
			Commented logic to replace Delayed Start Amount, Initial Term Amount and Grace Period Amount
			prodAttr.Delayed_Start_Amount_Months__c = assetTermPAV.Delayed_Start_Amount_Months__c;
			prodAttr.Initial_Term_Amount_Months__c = assetTermPAV.Initial_Term_Amount_Months__c;
			prodAttr.Grace_Period_Amount_Months__c = assetTermPAV.Grace_Period_Amount_Months__c;
			*/
            // Added new logic to stamp Asset Term Values on backup fields. Use these fields in field expressions.
            prodAttr.APTS_Asset_Delayed_Start_Amount__c = assetTermPAV.Delayed_Start_Amount_Months__c;
            prodAttr.APTS_Asset_Initial_Term_Amount__c = assetTermPAV.Initial_Term_Amount_Months__c;
            prodAttr.APTS_Asset_Grace_Period_Amount__c = assetTermPAV.Grace_Period_Amount_Months__c;
            
            prodAttr.Override_Term__c = assetTermPAV.Override_Term__c;
            prodAttr.Overridden_Delayed_Start_Months__c	= assetTermPAV.Overridden_Delayed_Start_Months__c;
            prodAttr.Overridden_Grace_Period_Months__c = assetTermPAV.Overridden_Grace_Period_Months__c;
            prodAttr.Overridden_Initial_Term_Months__c = assetTermPAV.Overridden_Initial_Term_Months__c;
        }
        // Enhancement logic - Iterate through all PAV's
        if(!assetEnhancementPAVList.isEmpty() && item.Apttus_Config2__Description__c == 'Enhancements') {
            // Iterate and fetch latest 'Non Standard'
            for(Apttus_Config2__AssetAttributeValue__c pav : assetEnhancementPAVList) {
                if(pav.Non_Standard__c != null && pav.Non_Standard__c != '') {
                    prodAttr.Non_Standard__c = pav.Non_Standard__c;
                    break;
                }
            }
            // Iterate and fetch latest 'Death or Disability Recipient 1'
            for(Apttus_Config2__AssetAttributeValue__c pav : assetEnhancementPAVList) {
                if(pav.Death_or_Disability_Recipient__c != null) {
                    prodAttr.Death_or_Disability_Recipient__c = pav.Death_or_Disability_Recipient__c;
                    break;
                }
            }
            
            //Iterate and fetch latest 'Death or Disability Clause'
            
            for(Apttus_Config2__AssetAttributeValue__c pav : assetEnhancementPAVList) {
                if(pav.Death_or_Disability_Clause__c != null) {
                    prodAttr.Death_or_Disability_Clause__c = pav.Death_or_Disability_Clause__c;
                    break;
                }
            }
            // Iterate and fetch latest 'Death or Disability Recipient 2'
            for(Apttus_Config2__AssetAttributeValue__c pav : assetEnhancementPAVList) {
                if(pav.APTS_Death_or_Disability_Recipient_2__c != null) {
                    prodAttr.APTS_Death_or_Disability_Recipient_2__c = pav.APTS_Death_or_Disability_Recipient_2__c;
                    break;
                }
            }
            /* Check if we need to copy it or not. It's getting populated by Field Expressions
            // Iterate and fetch latest 'Death or Disability Recipient 2'
            for(Apttus_Config2__AssetAttributeValue__c pav : assetEnhancementPAVList) {
                if(pav.APTS_Death_or_Disability_Recipient__c != null) {
                    prodAttr.APTS_Death_or_Disability_Recipient__c = pav.APTS_Death_or_Disability_Recipient__c;
                    break;
                }
            }*/
            /* Check if we need to copy it or not. It's getting populated by Field Expressions
            // Iterate and fetch latest 'Death or Disability Recipient 2'
            for(Apttus_Config2__AssetAttributeValue__c pav : assetEnhancementPAVList) {
                if(pav.APTS_Death_or_Disability_Recipient_txt_2__c != null) {
                    prodAttr.APTS_Death_or_Disability_Recipient_txt_2__c = pav.APTS_Death_or_Disability_Recipient_txt_2__c;
                    break;
                }
            }*/
        }
        // In case of 'Direct Conversion' and 'Expired Hours Card', set Aircraft Hours to 0
        if(modificationType == APTS_ConstantUtil.DIRECT_CONVERSION || modificationType == APTS_ConstantUtil.EXPIRED_HOURS_CARD) {
            prodAttr.Aircraft_Hours__c = 0;
        }
        
        if(modificationType == APTS_ConstantUtil.EXPIRED_HOURS_CARD && requestedEndDate != null) {
            if (requestedEndDate > Date.today()) {
                prodAttr.APTS_Requested_End_Date__c = requestedEndDate;
            }
        }
       //Start: GCM-9207
        if( productNameList.contains(item.Apttus_Config2__Description__c) && item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c=='100-Hour' ){
               prodAttr.APTS_Split_Payment__c = TRUE;
        }
       //End: GCM-9207        
    }
    
    global void finish() {
        system.debug('***ProdAttrCallBack > finish()');
    }
    
}