/************************************************************************************************************************
@Name: APTPS_sendEmailNotification
@Author: Conga PS Dev Team
@CreateDate: 21 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_sendEmailNotification implements APTPS_ENInterface  {
    public void sendNotification(Map<String, Object> args){
        String recipients = (String)args.get('recipients');
        String additionalEmails = (String)args.get('additionalEmails');
        String fromEmail = (String)args.get('fromEmail');
        List<String> toAddress=new List<String>();
        Set<Id> recipientIds= new Set<Id>();
        List<String> recipientsList = new List<String>();
        List<String> additionalEmailsList = new List<String>();
        List<Attachment> files = new List<Attachment>();
            
        String businessObject = (String)args.get('businessObject');
        String status = (String)args.get('Status');
        String productCode = (String)args.get('productCode');
        Id objId = (Id)args.get('Id');
        if(businessObject != null && recipients !=null) {
            
            recipientsList = recipients.split(',');
            List<Sobject> objList = Database.query('Select Id,' + recipients + ' FROM ' +businessObject+ ' WHERE Id = \''+objId+'\'');
            for(String r : recipientsList) {
                Id resId = (Id)(objList[0].get(r));
                if(resId != null){
                    recipientIds.add(resId);
                }
                    
            }
            
        }
        system.debug('businessObject-->'+businessObject+' status-->'+status+' productCode-->'+productCode);
        if(APTS_ConstantUtil.AGREEMENT_OBJ.equalsIgnoreCase(businessObject) && APTS_ConstantUtil.ACTIVATED .equalsIgnoreCase(status) &&
           (APTS_ConstantUtil.NJUS_DEMO.equalsIgnoreCase(productCode) || APTS_ConstantUtil.NJE_DEMO.equalsIgnoreCase(productCode) ||
           APTS_ConstantUtil.NJA_SHARE_CODE.equalsIgnoreCase(productCode) || APTS_ConstantUtil.NJE_SHARE_CODE.equalsIgnoreCase(productCode)
           || APTS_ConstantUtil.NJA_LEASE_CODE.equalsIgnoreCase(productCode) || APTS_ConstantUtil.NJE_LEASE_CODE.equalsIgnoreCase(productCode))) {
        system.debug('files');
            files = [SELECT Name, Body, ContentType FROM Attachment WHERE parentId = :objId LIMIT 1];                     
            
        }
        system.debug('recipientIds-->'+recipientIds);
        if(!recipientIds.isEmpty()){
            for(User u :[SELECT Email FROM User WHERE Id IN : recipientIds]) {
                toAddress.add(u.Email);
            }
        }
        if(additionalEmails != null && additionalEmails != '') {
            additionalEmailsList = additionalEmails.split(',');
            if(!additionalEmailsList.isEmpty()) {
                toAddress.addAll(additionalEmailsList);
            }           
            
        }
        
        system.debug('businessObject-->'+businessObject+' recipientsList-->'+recipientsList+' recipients-->'+recipients+' toAddress-->'+toAddress+' additionalEmailsList-->'+additionalEmailsList);
        String emailTemplateName  = (String)args.get('emailTemplate');
        //Id objId = (Id)(args.get('Id'));
        Id contactId = (Id)(args.get('ContactId'));
        EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE DeveloperName =:emailTemplateName];
        Contact tempContact;
        List<contact> conList = [SELECT Id,Name,email FROM contact WHERE Id=:contactId AND Email!=null];
        if(!conList.isEmpty()) {
            tempContact = conList[0];
        } else {
            List<Apttus__APTS_Admin__c> adminList = [SELECT Id,Name,Apttus__Value__c FROM Apttus__APTS_Admin__c WHERE Name ='APTS_DefaultEmailContactName'];
            if(!adminList .isEmpty()) {
                String contactName = adminList[0].Apttus__Value__c;
                List<contact> adminConList = [SELECT Id,Name,email FROM contact WHERE Name =:contactName];
                if(!adminConList.isEmpty()) {
                    tempContact = adminConList[0];
                }
            }
            
        }
        if(tempContact != null) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(et.Id);
            mail.setToAddresses(toAddress);
            mail.setTargetObjectId(tempContact.id);
            mail.setWhatId(objId);
            mail.setTreatTargetObjectAsRecipient(false);
            mail.setSaveAsActivity(false);
            mail.setUseSignature(false); 
            if(!files.isEmpty()){
            system.debug('files1-->'+files);
                List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(files[0].Name);
                efa.setBody(files[0].Body);
                efa.setContentType(files[0].ContentType);
                attachments.add(efa);
                mail.setFileAttachments(attachments);
            }         
    
            try {
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                
            } catch (Exception e) {
                System.debug('Exception while sending Email-->'+e.getMessage());
            }
        } else {
             system.debug('Error while Sending Email--> No Contact Object found');
        } 
        
        
        
    }
    
}