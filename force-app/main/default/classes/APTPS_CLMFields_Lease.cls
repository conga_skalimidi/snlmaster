/************************************************************************************************************************
@Name: APTPS_CLMFields_Lease
@Author: Conga PS Dev Team
@CreateDate: 19 Oct 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMFields_Lease {
    /** 
    @description: ppoulate Agreeement Fields
    @param: AgreementList and Currency Code
    @return: 
    */
    private static void populateLeaseFields(List<Apttus__APTS_Agreement__c> agrList, String currencyCode) {
        system.debug('Populate Agreement Fileds');
        set<Id> proposalIds = new set<Id>();
        set<Id> agrIds = new set<Id>();
        Map<Id,Apttus_Proposal__Proposal__c> proposalMap = new Map<Id,Apttus_Proposal__Proposal__c>();
        Map<String, String> NJEAircraftMap = new Map<String, String>();
        if(currencyCode == APTS_ConstantUtil.CUR_EUR ) {
            for(APTPS_NJE_Aircraft_Mapping__mdt mapping : [SELECT Agreement_Type__c, Associated_Aircraft__c FROM APTPS_NJE_Aircraft_Mapping__mdt]) {
                NJEAircraftMap.put(mapping.Agreement_Type__c, mapping.Associated_Aircraft__c);
            }
        }
        system.debug('NJEAircraftMap-->'+NJEAircraftMap);
        
       
        for(Apttus__APTS_Agreement__c agr : agrList) {
            proposalIds.add(agr.Apttus_QPComply__RelatedProposalId__c);
            agrIds.add(agr.Id);
        }
        for(Apttus_Proposal__Proposal__c pObj : [SELECT id, Apttus_Proposal__Amount__c,Name,Aircraft_Types__c,Approval_Primary_AE_User__c, Approval_Primary_SE_User__c, Approval_Sales_Director_User__c, Approval_RVP_User__c, APTPS_Approval_Secondary_SE_User__c,Legal_Entity_Name__c,Apttus_QPConfig__LocationId__c,Product_Line__c,APTPS_Start_of_Deferment__c,APTPS_End_of_Deferment__c FROM Apttus_Proposal__Proposal__c WHERE Id IN :proposalIds]) {
            proposalMap.put(pObj.id,pObj);
        }
        //Id leaseRecTypeId;
        /*if(currencyCode == APTS_ConstantUtil.CUR_EUR ) {
            leaseRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.LEASE_NJE_RECORD_TYPE ).getRecordTypeId();
        } else if(currencyCode == APTS_ConstantUtil.CUR_USD ) {
            leaseRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.LEASE_NJA_RECORD_TYPE).getRecordTypeId();
        }*/
        for(Apttus__APTS_Agreement__c agObj : agrList) {
               
                agObj.Funding_Status__c = APTS_ConstantUtil.FUND_PENDING ;
                agObj.Document_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                agObj.Agreement_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                agObj.Chevron_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                agObj.APTS_Path_Chevron_Status__c = APTS_ConstantUtil.STATUS_DRAFT ;
                agObj.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.APPROVAL_REQUIRED; 
                //agObj.RecordTypeId = leaseRecTypeId;
                agObj.Apttus_CMConfig__AutoActivateOrder__c = true;
                agObj.Generate_Card_Number__c = true;
                if(!proposalMap.isEmpty()){
                    agObj.Account_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Primary_AE_User__c;
                    agObj.Primary_Sales_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Primary_SE_User__c;
                    agObj.Sales_Consultant__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_Sales_Director_User__c;
                    agObj.RVP__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Approval_RVP_User__c;
                    agObj.Secondary_Sales_Executive__c = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).APTPS_Approval_Secondary_SE_User__c;
                     String agName = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Legal_Entity_Name__c+', '+proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Product_Line__c+' - '+proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Aircraft_Types__c;
                    agObj.Name = agName;
                    agObj.Document_Name__c = agName;
                    agObj.Apttus_CMConfig__LocationId__c=  proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_QPConfig__LocationId__c;
                    agObj.APTS_Total_Purchase_Price__c=  proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Apttus_Proposal__Amount__c;
                    //Start of GCM-12869
                    if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(agObj.APTS_Modification_Type__c)){
                        APTPS_SNL_Util.defermentModificationOnAgreement(agObj,proposalMap);
                    }
                    //End of GCM-12869
                }
                if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(currencyCode) && !NJEAircraftMap.isEmpty()) {
                     String aircraftTypes = proposalMap.get(agObj.Apttus_QPComply__RelatedProposalId__c).Aircraft_Types__c;
                     system.debug('aircraftTypes-->'+aircraftTypes);
                    for(String agreementTypeKey : NJEAircraftMap.keyset()) {
                        if(NJEAircraftMap.get(agreementTypeKey).contains(aircraftTypes)) {
                            agObj.APTPS_NJE_Aircraft_Type__c = agreementTypeKey;
                            break;
                        }
                    }
                }
        }
        system.debug('agrList-->'+agrList);
    }
    
    /** 
    @description: NJE Lease implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
            APTPS_CLMFields_Lease.populateLeaseFields(agrList,APTS_ConstantUtil.CUR_EUR);
        }
    }
    
    /** 
    @description: NJA Lease implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
            APTPS_CLMFields_Lease.populateLeaseFields(agrList,APTS_ConstantUtil.CUR_USD);
        }
    }
}