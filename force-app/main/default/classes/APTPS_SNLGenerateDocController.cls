/************************************************************************************************************************
@Name: APTPS_SNLGenerateDocController
@Author: Conga PS Dev Team
@CreateDate: 11 Nov 2021
@Description: 
************************************************************************************************************************
@ModifiedBy: 
@ModifiedDate: 03 December 2021
@ChangeDescription: Added code to update Agreement Extension Object OHF Text fields
************************************************************************************************************************/

public class APTPS_SNLGenerateDocController {
    public Id agId {get;set;}
    public String userAction {get;set;}
    public Map<decimal, String> rankACMap {get;set;} 
    public APTPS_SNLGenerateDocController() {
        this.agId = System.currentPageReference().getParameters().get('agId');
        this.userAction = System.currentPageReference().getParameters().get('action');
        this.rankACMap = new Map<decimal, String>();
        system.debug('Agreement Id --> '+agId+' userAction --> '+userAction);
    }
    
    public PageReference init()
    {
        List<Apttus__AgreementLineItem__c> aliToUpdates = new List<Apttus__AgreementLineItem__c>();
        Map<String, Decimal> appliedOHFMap = new Map<String, Decimal>();
        Map<String, Decimal> allOHFLinesMap = new Map<String, Decimal>();
        Map<String, Id> ohfIdMap = new Map<String, Id>();
        Map<Integer, String> ohfPricingConfigMap = new Map<Integer, String>();
        List<String> applicableOHFList = new List<String>();
        Set<Id> ohfIdSet = new Set<Id>();
        Date warrantyDate = null;
        Integer numberOfMonths = 0;
        Decimal ohfAdjustmentAmount = 0;
        PageReference pg = null;
        String OHF_Increase_1 = '';
        String OHF_Increase_2 = '';
        String OHF_Increase_3 = '';
        String OHF_Increase_4 = '';
        String agreementExtensionId = null;
        Decimal updatedAmount = 0;
        boolean recomputeOperatingFund = false;
        boolean updateAgreement = false;
        Apttus__AgreementLineItem__c operatingFundALI = null;
        if(agId != null){
            /*GCM-12642 Set Peak Aircraft START*/
            Id accId = null;
            //Get corresponding Account ID
            Apttus__APTS_Agreement__c ag = [SELECT Id, Apttus__Account__c, Agreement_Status__c,APTPS_Peak_Aircraft_Type__c,APTPS_Assignor_Agreement__r.Apttus_CMConfig__LocationId__c,APTS_Additional_Modification_Type__c FROM Apttus__APTS_Agreement__c WHERE Id =:agId LIMIT 1];
            accId = (ag != null && ag.Apttus__Account__c != null) ? ag.Apttus__Account__c : null;
            //Read current Agreement ALIs           
            for(Apttus__AgreementLineItem__c ali : [SELECT Id, 
                                                    Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c, 
                                                    Apttus_CMConfig__OptionId__r.Cabin_Class__c, 
                                                    Apttus_CMConfig__OptionId__r.Name 
                                                    FROM Apttus__AgreementLineItem__c 
                                                    WHERE Apttus__AgreementId__c =: ag.Id 
                                                    AND Apttus_CMConfig__IsPrimaryLine__c = true 
                                                    AND Apttus_CMConfig__OptionId__r.Family =: APTS_ConstantUtil.AIRCRAFT]){
                                                        if(!rankACMap.containsKey(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c))
                                                            rankACMap.put(ali.Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c, ali.Apttus_CMConfig__OptionId__r.Name);
                                                    }
            if(accId != null) {
                for(Apttus_Config2__AssetLineItem__c assetLi : [SELECT Apttus_CMConfig__AgreementId__c, 
                                                                Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, 
                                                                Apttus_Config2__OptionId__r.Cabin_Class__c, 
                                                                Apttus_Config2__OptionId__r.Name  
                                                                FROM Apttus_Config2__AssetLineItem__c 
                                                                WHERE Apttus_CMConfig__AgreementId__r.Agreement_Status__c =: APTS_ConstantUtil.AGREEMENT_ACTIVE 
                                                                AND (Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c =: APTS_ConstantUtil.SHARE OR Apttus_CMConfig__AgreementId__r.APTPS_Program_Type__c =: APTS_ConstantUtil.LEASE) 
                                                                AND Apttus_Config2__AccountId__c =: accId 
                                                                AND Apttus_Config2__IsPrimaryLine__c = true 
                                                                AND Apttus_Config2__OptionId__r.Family =: APTS_ConstantUtil.AIRCRAFT]) {
                                                                    if(!rankACMap.containsKey(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c))
                                                                        rankACMap.put(assetli.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c, assetli.Apttus_Config2__OptionId__r.Name);
                                                                    
                                                                }
            } else
                system.debug('Account Id is NULL.');
            if(!rankACMap.isEmpty()) {
                List<decimal> crList = new List<decimal>();
                crList.addAll(rankACMap.keySet());
                decimal maxRank = APTPS_AccountConsolidationOnQuote.getMaxRank(crList);
                String peakAircraft = rankACMap.get(maxRank);
                system.debug('peakAircraft-->'+peakAircraft);
                ag.APTPS_Peak_Aircraft_Type__c = peakAircraft;
                updateAgreement = true;
            }
            if(APTS_ConstantUtil.UNRELATED_FULL.equalsIgnoreCase(ag.APTS_Additional_Modification_Type__c) && ag.APTPS_Assignor_Agreement__r.Apttus_CMConfig__LocationId__c !=null) {
                ag.APTPS_Assignor_Account_Location__c = ag.APTPS_Assignor_Agreement__r.Apttus_CMConfig__LocationId__c;
                updateAgreement = true;
            }
            if(updateAgreement) {
                try{
                    update ag;
                }catch (exception e){
                    System.debug('Exception in Class-APTPS_SNLGenerateDocController: Method- init() at line' + e.getLineNumber()+' Message->' + e.getMessage());
                }
            }
            
            
            
            
        
            /*GCM-12642 Set Peak Aircraft START*/           
            for(Apttus__AgreementLineItem__c ali : [SELECT Id, Apttus_CMConfig__ChargeType__c, Apttus__NetPrice__c, Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c, 
                                                    APTPS_Is_Included_In_OHF__c, APTPS_Is_OHF_Warranty_Date_Considered__c, Apttus_CMConfig__BasePriceOverride__c,
                                                    APTPS_Is_NA__c,
                                                    APTPS_Is_TBD__c,
                                                    Apttus__ListPrice__c, 
                                                    Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c 
                                                    FROM Apttus__AgreementLineItem__c 
                                                    WHERE Apttus__AgreementId__c = :agId 
                                                    AND (Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE_1 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE_2 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE_3 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE_4 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.LONG_RANGE_OHF_CHARGE_TYPE 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OF_CHARGE_TYPE)]) {
                                                             agreementExtensionId = ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__c != null ? ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__c : null;
                                                             if(APTS_ConstantUtil.OHF_CHARGE_TYPE.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c) || APTS_ConstantUtil.LONG_RANGE_OHF_CHARGE_TYPE.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c)) 
                                                                 warrantyDate = ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__c != null ? ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c : null;
                                                             if(ali.APTPS_Is_Included_In_OHF__c)
                                                                 appliedOHFMap.put(ali.Apttus_CMConfig__ChargeType__c, ali.Apttus__NetPrice__c);
                                                             
                                                             //Get Operating Fund ALI
                                                             if(APTS_ConstantUtil.OF_CHARGE_TYPE.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c))
                                                                 operatingFundALI = ali;
                                                             else {
                                                                 allOHFLinesMap.put(ali.Apttus_CMConfig__ChargeType__c, ali.Apttus__NetPrice__c);
                                                                 ohfIdMap.put(ali.Apttus_CMConfig__ChargeType__c, ali.Id);
                                                             }
                                                             
                                                            if(APTS_ConstantUtil.OHF_CHARGE_TYPE_1.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c)) {
                                                                OHF_Increase_1 = getOHFText(ali.APTPS_Is_NA__c, ali.APTPS_Is_TBD__c, ali.Apttus__ListPrice__c,ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c);
                                                            } else if(APTS_ConstantUtil.OHF_CHARGE_TYPE_2.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c)) {
                                                                OHF_Increase_2 = getOHFText(ali.APTPS_Is_NA__c, ali.APTPS_Is_TBD__c, ali.Apttus__ListPrice__c,ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c);
                                                            } else if(APTS_ConstantUtil.OHF_CHARGE_TYPE_3.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c)) {
                                                                OHF_Increase_3 = getOHFText(ali.APTPS_Is_NA__c, ali.APTPS_Is_TBD__c, ali.Apttus__ListPrice__c,ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c);
                                                            } else if(APTS_ConstantUtil.OHF_CHARGE_TYPE_4.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c)) {
                                                                OHF_Increase_4 = getOHFText(ali.APTPS_Is_NA__c, ali.APTPS_Is_TBD__c, ali.Apttus__ListPrice__c,ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c);
                                                            }  
                                                             
                                                         }
            system.debug('agreementExtensionId --> '+agreementExtensionId+' OHF_Increase_1 --> '+OHF_Increase_1+' OHF_Increase_2 --> '+OHF_Increase_2+' OHF_Increase_3 --> '+OHF_Increase_3+' OHF_Increase_4 --> '+OHF_Increase_4);
            system.debug('warrantyDate --> '+warrantyDate+' appliedOHFMap --> '+appliedOHFMap);
            if(agreementExtensionId != null) {
                APTPS_Agreement_Extension__c agrExtObj = new APTPS_Agreement_Extension__c(id=agreementExtensionId,APTPS_OHF_Increase_1__c=OHF_Increase_1,APTPS_OHF_Increase_2__c=OHF_Increase_2,APTPS_OHF_Increase_3__c=OHF_Increase_3,APTPS_OHF_Increase_4__c=OHF_Increase_4);
                update agrExtObj;
            }
            if(warrantyDate != null) {
                ohfPricingConfigMap = APTPS_SNL_Util.getOHFPricingConfig();
                numberOfMonths = APTPS_SNL_Util.calculateMonths(warrantyDate, Date.today());
                system.debug('Difference in terms of months --> '+numberOfMonths);
                //Iterate OHF Pricing Config
                for(Integer key : ohfPricingConfigMap.keySet()) {
                    if(key <= numberOfMonths && !applicableOHFList.contains(ohfPricingConfigMap.get(key))) {
                        applicableOHFList.add(ohfPricingConfigMap.get(key));
                    }
                }
                system.debug('applicableOHFList --> '+applicableOHFList);
                
                //Iterate applied OHF charge types/ALIs
                Integer i=0;
                if(!appliedOHFMap.isEmpty()) {
                    for(String pli : appliedOHFMap.keySet()) {
                        system.debug('Checking for --> '+pli);
                        if(!applicableOHFList.isEmpty() && applicableOHFList.contains(pli))
                            applicableOHFList.remove(applicableOHFList.indexOf(pli));
                        else {
                            ohfAdjustmentAmount += appliedOHFMap.get(pli)*-1;
                            Id aliId = ohfIdMap.get(pli);
                            if(aliId!=null){
                                Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Id=ohfIdMap.get(pli));
                                ali.APTPS_Is_Included_In_OHF__c = false;
                                ali.APTPS_Is_OHF_Warranty_Date_Considered__c = true;
                                ohfIdSet.add(ohfIdMap.get(pli));
                                aliToUpdates.add(ali);
                            }
                        }
                        i+=1;
                    }
                }
                
                //Check if there is need to consider new OHF
                for(String pli : applicableOHFList) {
                    Decimal ohfAmoutToAdd = !allOHFLinesMap.isEmpty() && allOHFLinesMap.containsKey(pli) ? allOHFLinesMap.get(pli) : 0;
                    ohfAdjustmentAmount += ohfAmoutToAdd;
                    Id aliId = ohfIdMap.get(pli);
                    if(aliId!=null){
                        Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Id=ohfIdMap.get(pli));
                        ali.APTPS_Is_Included_In_OHF__c = true;
                        ali.APTPS_Is_OHF_Warranty_Date_Considered__c = true;
                        ohfIdSet.add(ohfIdMap.get(pli));
                        aliToUpdates.add(ali);
                    }
                }
                
                system.debug('ohfAdjustmentAmount --> '+ohfAdjustmentAmount);
                if(ohfAdjustmentAmount != 0) {
                    if(ohfIdMap != null && ohfIdMap.containsKey(APTS_ConstantUtil.OHF_CHARGE_TYPE)) {
                        Apttus__AgreementLineItem__c ali = getOhfLineWithAdjustment(ohfIdMap.get(APTS_ConstantUtil.OHF_CHARGE_TYPE), APTS_ConstantUtil.OHF_CHARGE_TYPE, allOHFLinesMap, ohfAdjustmentAmount);
                        ohfIdSet.add(ohfIdMap.get(APTS_ConstantUtil.OHF_CHARGE_TYPE));
                        aliToUpdates.add(ali);
                        updatedAmount = ali.Apttus_CMConfig__BasePriceOverride__c;
                        recomputeOperatingFund = true;
                    }
                    
                    if(ohfIdMap != null && ohfIdMap.containsKey(APTS_ConstantUtil.LONG_RANGE_OHF_CHARGE_TYPE)) {
                        Apttus__AgreementLineItem__c ali = getOhfLineWithAdjustment(ohfIdMap.get(APTS_ConstantUtil.LONG_RANGE_OHF_CHARGE_TYPE), APTS_ConstantUtil.LONG_RANGE_OHF_CHARGE_TYPE, allOHFLinesMap, ohfAdjustmentAmount);
                        ohfIdSet.add(ohfIdMap.get(APTS_ConstantUtil.LONG_RANGE_OHF_CHARGE_TYPE));
                        aliToUpdates.add(ali);
                    }
                }

                
                //Iterate ALIs and stamp Is Warranty Date available?
                for(Id recId : ohfIdMap.values()){
                    if(ohfIdSet != null && !ohfIdSet.contains(recId)) {
                        Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Id=recId);
                        ali.APTPS_Is_OHF_Warranty_Date_Considered__c = true;
                        aliToUpdates.add(ali);
                    }
                }
                
                //START:Update Operating Fund based on new OHF
                if(recomputeOperatingFund && updatedAmount != 0) {
                    Integer acHours = operatingFundALI !=null && operatingFundALI.Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c != null ? Integer.valueOf(operatingFundALI.Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c) : 0;
                    APTPS_Pricing_Callback__c pricingCustSettings = APTPS_Pricing_Callback__c.getOrgDefaults();
                    Decimal snlOperatingFundCustSettings = pricingCustSettings.APTPS_SNL_Operating_Fund_Factor__c != null ? pricingCustSettings.APTPS_SNL_Operating_Fund_Factor__c : 0;
                    system.debug('Operating Fund calculation denominator value from Custom Settings --> '+snlOperatingFundCustSettings);
                    Decimal operatingFundFactor = snlOperatingFundCustSettings != 0 ? (acHours/snlOperatingFundCustSettings) : 0;
                    
                    system.debug('Aircraft Hours --> '+acHours);
                    system.debug('operatingFundFactor --> '+operatingFundFactor);
                    system.debug('updatedAmount --> '+updatedAmount);
                    if(operatingFundFactor != 0) {
                        operatingFundALI.Apttus__NetPrice__c = updatedAmount*operatingFundFactor;
                        operatingFundALI.Apttus_CMConfig__BasePriceOverride__c = updatedAmount*operatingFundFactor;
                        aliToUpdates.add(operatingFundALI);
                    }
                }
                //END
            } else
                system.debug('Either OHF is not applied or Warranty Date is NULL');
            //End
        }
        system.debug('aliToUpdates --> '+aliToUpdates);
        if(!aliToUpdates.isEmpty())
            update aliToUpdates;
        
        //Redirect
        String url = '';
        url = getRedirectURL(userAction);
        pg = new PageReference(url);
        pg.setRedirect(true);
        return pg;
    }
    
    private String getRedirectURL(String action) {
        String url = '';
        String userTheme = UserInfo.getUiThemeDisplayed();
        if(userTheme.equalsIgnoreCase('Theme4d') || userTheme.equalsIgnoreCase('Theme4t') || userTheme.equalsIgnoreCase('Theme4u'))
            url = '/apex/Apttus__SelectTemplate?id='+agId+'&action='+action+'&templateType=Agreement';
        else 
            url = '/apex/Apttus__LightningSelectTemplate?id='+agId+'&action='+action+'&templateType=Agreement';
        return url;
    }
    
    private Apttus__AgreementLineItem__c getOhfLineWithAdjustment(Id aliId, String chargeType, Map<String, Decimal> allOHFLinesMap, Decimal ohfAdjustmentAmount) {
        Decimal updatedAmount = allOHFLinesMap.get(chargeType) + ohfAdjustmentAmount;
        system.debug('Updated amount for charge type '+chargeType+' is : '+updatedAmount);
        Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Id=aliId);
        ali.Apttus__NetPrice__c = updatedAmount;
        ali.Apttus_CMConfig__BasePriceOverride__c = updatedAmount;
        ali.APTPS_Is_OHF_Warranty_Date_Considered__c = true;
        return ali;
    }
    
    private String getOHFText(boolean IS_NA, boolean IS_TBD, Decimal listPrice, date warrantyDate) {
        String OHFText = '';
        date currentDate = date.today();
        if(IS_NA) {
            OHFText = APTS_ConstantUtil.OHF_NA;
        } else if(IS_TBD) {
            OHFText =  APTS_ConstantUtil.OHF_TBD;
        } else if(!IS_NA && !IS_TBD && listPrice>0 && warrantyDate == null) {
            OHFText = '$'+listPrice+' '+APTS_ConstantUtil.OHF_WB_NODATE;
            
        } else if(!IS_NA && !IS_TBD && listPrice>0 && warrantyDate != null && warrantyDate > currentDate) {
            OHFText = '$'+listPrice+' '+APTS_ConstantUtil.OHF_WB_WITH_FUTURE_DATE;
            
        } else if(!IS_NA && !IS_TBD && listPrice>0 && warrantyDate != null && warrantyDate <= currentDate) {
            OHFText = '$'+listPrice+' '+APTS_ConstantUtil.OHF_EFFECT_ON+' '+warrantyDate+' '+APTS_ConstantUtil.OHF_WB_WITH_PAST_DATE;
            
        }
        return OHFText;
    }
    
}