/************************************************************************************************************************
@Name: APTPS_CollaborationTriggerHandler
@Author: Conga PS Dev Team
@CreateDate: 20 July 2021
@Description: Collaboration Request trigger handler
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CollaborationTriggerHandler extends TriggerHandler {
    
    public override void beforeInsert() {
        List<Apttus_Config2__CollaborationRequest__c> newList = Trigger.new;
        for(Apttus_Config2__CollaborationRequest__c cr : newList) {
            if(cr.Apttus_Config2__ParentBusinessObjectId__c != null) {
                //Stamp related Quote/Proposal on Collaboration Request record.
                cr.APTPS_Related_Quote_Proposal__c = cr.Apttus_Config2__ParentBusinessObjectId__c;
            }
        }
    }
    
    public override void afterUpdate() {
        List<Apttus_Config2__CollaborationRequest__c> newList = Trigger.new;
        Map<Id, Apttus_Config2__CollaborationRequest__c> oldCollabMap = (Map<Id, Apttus_Config2__CollaborationRequest__c>)Trigger.OldMap;
        List<Apttus_Config2__CollaborationRequest__c> collabList = new List<Apttus_Config2__CollaborationRequest__c>();
        Set<Id> configIdSet = new Set<Id>();
        
        for(Apttus_Config2__CollaborationRequest__c cr : newList) {
            Apttus_Config2__CollaborationRequest__c oldCollabObj = oldCollabMap.get(cr.Id);
            String oldStatus = oldCollabObj.Apttus_Config2__Status__c;
            Id oldOwner = oldCollabObj.OwnerId;
            system.debug('Previous Collaboration status --> '+oldStatus+' and Previous Collaboration Owner --> '+oldOwner);
            
            //New Collaboration request assignment
            if(APTS_ConstantUtil.STATUS_SUBMITTED.equalsIgnoreCase(cr.Apttus_Config2__Status__c) 
               && oldStatus.equalsIgnoreCase(APTS_ConstantUtil.STATUS_NEW)) {
                   collabList.add(cr);
               }
            
            //Collaboration Owner is updated
            if(APTS_ConstantUtil.STATUS_SUBMITTED.equalsIgnoreCase(cr.Apttus_Config2__Status__c) 
               && oldStatus.equalsIgnoreCase(cr.Apttus_Config2__Status__c) && oldOwner != cr.OwnerId) {
                   collabList.add(cr);
               }
            
            //Collaboration is Completed
            if(APTS_ConstantUtil.STATUS_COMPLETED.equalsIgnoreCase(cr.Apttus_Config2__Status__c) 
               && !oldStatus.equalsIgnoreCase(APTS_ConstantUtil.STATUS_COMPLETED)) {
                   collabList.add(cr);
                   configIdSet.add(cr.Apttus_Config2__ParentConfigurationId__c);
               }
            
            //Collaboration is Accepted
            if(APTS_ConstantUtil.STATUS_ACCEPTED.equalsIgnoreCase(cr.Apttus_Config2__Status__c) 
               && !oldStatus.equalsIgnoreCase(APTS_ConstantUtil.STATUS_ACCEPTED)) {
                   collabList.add(cr);
               }
        }
        
        if(!collabList.isEmpty()) {
            system.debug('Send collaboration created/assigned email notification. List --> '+collabList);
            APTPS_CollaborationTriggerHelper.sendCollabEmailNotification(collabList, false);
        }

        if(!configIdSet.isEmpty())
            APTPS_CollaborationTriggerHelper.updateConfig(configIdSet, true);
    }

    public override void beforeDelete() {
        List<Apttus_Config2__CollaborationRequest__c> collabDelList = new List<Apttus_Config2__CollaborationRequest__c>();
        Set<Id> configIdSet = new Set<Id>();
        Map<Id, Apttus_Config2__CollaborationRequest__c> oldCollabMap = (Map<Id, Apttus_Config2__CollaborationRequest__c>)Trigger.OldMap;
        for(Id key : oldCollabMap.keySet()) {
            collabDelList.add(oldCollabMap.get(key));
            configIdSet.add(oldCollabMap.get(key).Apttus_Config2__ParentConfigurationId__c);
            system.debug('Checking isDeleted flag --> '+oldCollabMap.get(key).IsDeleted);
        }

        system.debug('List to delete --> '+collabDelList);
        if(!collabDelList.isEmpty())
            APTPS_CollaborationTriggerHelper.sendCollabEmailNotification(collabDelList, true);

        system.debug('Config set to update --> '+configIdSet);
        if(!configIdSet.isEmpty())
            APTPS_CollaborationTriggerHelper.updateConfig(configIdSet, false);
        
    }
}