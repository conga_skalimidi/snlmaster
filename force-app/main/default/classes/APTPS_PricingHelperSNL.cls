/************************************************************************************************************************
@Name: APTPS_PricingHelperSNL
@Author: Conga PS Dev Team
@CreateDate: 09 Nov 2021
@Description: Helper class for SNL Pricing
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_PricingHelperSNL {
    /**
     @description: Stamp OHF flags on Line Items in case of SNL Pricing
     @param: item - Line Item
     @param: ohfListPrice - List Price
	 @param: applicableOHFList - List of applicable OHF charge types
     @return: Computed MMF price
     */
    public static boolean stampOHFDetails(Apttus_Config2__LineItem__c item, Decimal ohfListPrice, List<String> applicableOHFList, Decimal ohfLimits) {
        boolean isValidOHFFound = false;
        system.debug('ohfLimits --> '+ohfLimits);
        Decimal ohfNA = ohfLimits*-1;
        system.debug('ohfNA --> '+ohfNA);
        if(ohfListPrice != null && ohfListPrice != ohfNA && ohfListPrice != ohfLimits) {   
            item.APTPS_Is_TBD__c = false;
            item.APTPS_Is_NA__c = false;
            isValidOHFFound = true;
        }
        
        if(ohfListPrice != null && ohfListPrice == ohfNA) {
            item.APTPS_Is_TBD__c = false;
            item.APTPS_Is_NA__c = true;
        }
        
        if(ohfListPrice != null && ohfListPrice == ohfLimits) {
            item.APTPS_Is_TBD__c = true;
            item.APTPS_Is_NA__c = false;
        }
        
        item.APTPS_Is_Included_In_OHF__c = (applicableOHFList != null && applicableOHFList.contains(item.Apttus_Config2__ChargeType__c)) ? true : false;
        system.debug('stampOHFDetails --> isValidOHFFound, '+isValidOHFFound);
        if(!isValidOHFFound)
            item.Apttus_Config2__BasePrice__c = 0;
        return isValidOHFFound;
    }
    
    /**
     @description: Compute the MMF price for Share
     @param: item - Line Item
     @param: listPrice - List Price
     @return: Computed MMF price
     */
    public static Decimal computeMMFPrice(Apttus_Config2__LineItem__c item, Decimal listPrice, Decimal definedPriceFactor) {
        Decimal computedMMFPrice = 0;
        try{
            Decimal shareHours = item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c != null ? Decimal.valueOf(item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c) : 0;
            Decimal mmfPriceFactor = shareHours/definedPriceFactor; //We are not adding validation to check for zero, instead execution should fail.
            Decimal mmfListPrice = listPrice != null ? listPrice : 0;
            system.debug('definedPriceFactor --> '+definedPriceFactor+'shareHours --> '+shareHours+' mmfPriceFactor --> '+mmfPriceFactor+' mmfListPrice --> '+mmfListPrice);
            computedMMFPrice = mmfPriceFactor*mmfListPrice;
        } catch(Exception e) {
            system.debug('Error while computing MMF Price. Error details --> ' + e + ' Line Number-->' + e.getLineNumber());
        }

        system.debug('computeMMFPrice, MMF computed Price is --> '+computedMMFPrice);
        return computedMMFPrice;
    }

    /**
     @description: Compute OHF
     @param: applicableOHFList - List of applicable OHF charge types
     @param: ohfEligibleMap - Map of eligible OHF Charge Types
     @return: Cumulative OHF
     */
    public static Decimal getBasePriceWithOHF(List<String> applicableOHFList, Map<String, Decimal> ohfEligibleMap, Decimal ohfBasePrice) {
        Decimal cumulativeOHF = 0;
        Decimal finalOHF = 0;
        for(String ohfChargeType : applicableOHFList) {
            if(ohfEligibleMap.containsKey(ohfChargeType)) {
                cumulativeOHF += ohfEligibleMap.get(ohfChargeType);
            }
        }
        system.debug('Computed OHF price --> '+cumulativeOHF);
        finalOHF = ohfBasePrice+cumulativeOHF;
        system.debug('Final computed OHF --> '+finalOHF);
        return finalOHF;
    }
    
    public static boolean isRelatedPartialAssignment(String progType, String modType, String additionalModType) {
        boolean result = false;
        //Related Partial Assignment is applicable to Share only.
        if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(progType) && APTS_ConstantUtil.ASSIGNMENT.equalsIgnoreCase(modType) 
          && APTS_ConstantUtil.RELATED_PARTIAL.equalsIgnoreCase(additionalModType)) {
            result = true;
        }
        return result;
    }
    
    public static boolean isSNLNewSaleOptionLine(String progType, Apttus_Config2__LineItem__c item) {
        boolean result = false;
        if((APTS_ConstantUtil.SHARE.equalsIgnoreCase(progType) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(progType)) 
           && APTS_PricingUtil.isNewOption(item) && !APTS_PricingUtil.isPurchasePriceCT(item)) 
               result = true;
        return result;
    }
    
    public static boolean isSNLRelatedFullAssignmentOptionLine(String progType, Apttus_Config2__LineItem__c item, String modType, String additionalModType) {
        boolean result = false;
        if((APTS_ConstantUtil.SHARE.equalsIgnoreCase(progType) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(progType)) 
           && APTS_ConstantUtil.ASSIGNMENT.equalsIgnoreCase(modType) 
           && APTS_ConstantUtil.RELATED_FULL.equalsIgnoreCase(additionalModType) 
           && APTS_PricingUtil.isOption(item) && !APTS_PricingUtil.isPurchasePriceCT(item) 
           && item.Apttus_Config2__LineStatus__c != null && item.Apttus_Config2__LineStatus__c != 'New') 
            result = true;
        return result;
    }

    public static Decimal computeOF(Apttus_Config2__LineItem__c item, Decimal ohfListPrice, Decimal definedOperatingFundFactor) {
        Decimal computedOF = 0;
        Decimal shareHours = item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c != null ? Decimal.valueOf(item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c) : 0;
        system.debug('APTPS_PricingHelperSNL:computeOF, ohfListPrice --> '+ohfListPrice+' definedOperatingFundFactor --> '+definedOperatingFundFactor+' shareHours --> '+shareHours);
        computedOF = (shareHours/definedOperatingFundFactor)*ohfListPrice;
        system.debug('APTPS_PricingHelperSNL:computeOF, computedOF --> '+computedOF);
        return computedOF;
    }
    
    public static Decimal computeSizeIncreasePurchasePrice(Apttus_Config2__LineItem__c item, Decimal snlAcHoursPriceFactor) {
        Decimal purchasePrice = 0;
        Decimal acHours = item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c != null ? Decimal.valueOf(item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c) : 0;
        Decimal snlOriginalAcHours = item.Apttus_Config2__AttributeValueId__r.APTPS_SNL_Original_Hours__c != null ? item.Apttus_Config2__AttributeValueId__r.APTPS_SNL_Original_Hours__c : 0;
        Decimal sizeIncreaseHr = acHours-snlOriginalAcHours;
        system.debug('SNL Size Increase, acHours --> '+acHours+' snlOriginalAcHours --> '+snlOriginalAcHours+ ' snlAcHoursPriceFactor --> '+snlAcHoursPriceFactor);
        purchasePrice = item.Apttus_Config2__ListPrice__c*(sizeIncreaseHr/snlAcHoursPriceFactor);
        return purchasePrice;
    }
}