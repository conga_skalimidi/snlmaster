/************************************************************************************************************************
@Name: APTS_QuoteProposalBaseTriggerHandler
@Author: Conga PS Dev Team
@CreateDate: 
@Description: Quote/Proposal trigger to handle Approval Business logic/functionality
************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 18/02/2021
@ChangeDescription: GCM-10120 In case of ABO, when Quote/Proposal is deleted, reset Parent Agreement Status.
*************************************************************************************************************************
@ModifiedBy: Conga PS Dev Team
@ModifiedDate: 17/11/2020
@ChangeDescription: Added logic for Shares and Leases.
************************************************************************************************************************
@ModifiedBy: Conga PS Dev Team
@ModifiedDate: 18/05/2021
@ChangeDescription: GCM-10351, GCM-10352, GCM-10360 - Deal Summary implementation for S&L
************************************************************************************************************************/

public class APTS_QuoteProposalBaseTriggerHandler extends TriggerHandler {
    //START: GCM-10120 Define static Agreement List so that it can be referred in afterDelete
    public static List<Apttus__APTS_Agreement__c> agList = new List<Apttus__APTS_Agreement__c>();
    public override void beforeInsert() {
        List<Apttus_Proposal__Proposal__c> newQuotes = Trigger.New;
        Map<String,String> rtMap = new Map<String,String>();
        rtMap = APTPS_SNL_Util.getProductMap(APTS_ConstantUtil.QUOTE_OBJ,APTS_ConstantUtil.RECORDTYPE);
        system.debug('rtMap-->'+rtMap);
        for(Apttus_Proposal__Proposal__c qObj : newQuotes) {
            if(APTPS_SNL_Util.isSNLProgramType(qObj.APTPS_Program_Type__c)) {
                String rtKey = APTPS_SNL_Util.getRTKey(qObj.APTPS_Program_Type__c, qObj.CurrencyIsoCode, qObj.APTS_New_Sale_or_Modification__c, qObj.APTS_Modification_Type__c, qObj.APTS_Additional_Modification_Type__c);
                if(rtMap.containsKey(rtKey)) {
                    String recordType = rtMap.get(rtKey);
                    system.debug('recordType-->'+recordType);
                    qObj.RecordTypeId  = Schema.SObjectType.Apttus_Proposal__Proposal__c.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId(); 
                }                
                qObj.Proposal_Chevron_Status__c = APTS_ConstantUtil.PROPOSAL_SOLUTION;
                qObj.Apttus_QPApprov__Approval_Status__c = APTS_ConstantUtil.APPROVAL_REQUIRED;
                qObj.Apttus_Proposal__Approval_Stage__c = APTS_ConstantUtil.APPROVAL_REQUIRED;
                qObj.Apttus_QPConfig__Intent__c = APTS_ConstantUtil.CONTRACT;
            }
            system.debug('qObj-->'+qObj);
        }
        
    }
    
    public override void beforeUpdate() {
        List<Apttus_Proposal__Proposal__c> newProposals = Trigger.New;
        Map<Id,SObject> oldProposalMap = Trigger.OldMap;
        //For renewal modification
        Map<Id,Apttus_Proposal__Proposal__c> renewalProposalsInScope = new Map<Id,Apttus_Proposal__Proposal__c>();
        //For renewal modification
        List<Apttus_Proposal__Proposal__c> proposalsInScope = new List<Apttus_Proposal__Proposal__c>();
        List<Apttus_Proposal__Proposal__c> proposalsAutoApprove = new List<Apttus_Proposal__Proposal__c>();
        string netjetsCompany = '';
         Map<Id, Apttus_Proposal__Proposal__c> approvalsInScope = new Map<Id, Apttus_Proposal__Proposal__c>();
        //List<Apttus_Proposal__Proposal__c> snlQuotes = new List<Apttus_Proposal__Proposal__c>();
        system.debug('>>>>>>>>>>newProposals>>>>>> ' + newProposals);
        system.debug('>>>>>>>>>>oldProposalMap>>>>>> ' + oldProposalMap);
        for (Apttus_Proposal__Proposal__c proposalObj : newProposals){
            Apttus_Proposal__Proposal__c oldProposalObj = (Apttus_Proposal__Proposal__c) oldProposalMap.get(proposalObj.Id);
            if (oldProposalObj != null){
                system.debug('>>>>>>>>>>oldProposalObj Found>>>>>> ' + oldProposalObj);
                if (oldProposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c == null && proposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c != null) {
                    proposalObj.Requires_Re_Approval__c = true;
                }
                if (proposalObj.Requires_Re_Approval__c && proposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c!=null && proposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c != oldProposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c) {
                    if(APTPS_SNL_Util.isSNLProgramType(proposalObj.APTPS_Program_Type__c)) {
                        approvalsInScope.put(proposalObj.Id, proposalObj);
                    } else {
                        proposalsInScope.add(proposalObj);
                    }
                    
                }
                else if (!proposalObj.Requires_Re_Approval__c && proposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c!=null && proposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c != oldProposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c){
                    proposalsAutoApprove.add(proposalObj);
                }
                
                //START: SNL Quote/Proposal Chevron management
                if(APTPS_SNL_Util.isSNLProgramType(proposalObj.APTPS_Program_Type__c)) {
                    String oldApprovalStage = oldProposalObj.Apttus_Proposal__Approval_Stage__c;
                    String oldApprovalStatus = oldProposalObj.Apttus_QPApprov__Approval_Status__c;
                    String newApprovalStage = proposalObj.Apttus_Proposal__Approval_Stage__c;
                    String newApprovalStatus = proposalObj.Apttus_QPApprov__Approval_Status__c;
                    //Document Generated
                    if(APTS_ConstantUtil.APPROVAL_REQUIRED.equalsIgnoreCase(oldApprovalStage) && APTS_ConstantUtil.APPROVAL_REQUIRED.equalsIgnoreCase(oldApprovalStatus) 
                       && APTS_ConstantUtil.GENERATED.equalsIgnoreCase(newApprovalStage) && APTS_ConstantUtil.APPROVAL_REQUIRED.equalsIgnoreCase(newApprovalStatus)){
                           proposalObj.Proposal_Chevron_Status__c = APTS_ConstantUtil.PROPOSAL_SOLUTION;
                           proposalObj.Document_Status__c = APTS_ConstantUtil.DOC_GENERATED;
                       }
                    
                    //Submitted for Approval
                    if((APTS_ConstantUtil.GENERATED.equalsIgnoreCase(oldApprovalStage) && APTS_ConstantUtil.APPROVAL_REQUIRED.equalsIgnoreCase(oldApprovalStatus)) 
                       || (APTS_ConstantUtil.APPROVAL_REQUIRED.equalsIgnoreCase(oldApprovalStage) && APTS_ConstantUtil.APPROVAL_REQUIRED.equalsIgnoreCase(oldApprovalStatus)) 
                       && APTS_ConstantUtil.IN_REVIEW.equalsIgnoreCase(newApprovalStage) && APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(newApprovalStatus)){
                           proposalObj.Proposal_Chevron_Status__c = APTS_ConstantUtil.IN_APPROVAL;
                       }
                    
                    //Approved
                    if(APTS_ConstantUtil.IN_REVIEW.equalsIgnoreCase(oldApprovalStage) && APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(oldApprovalStatus) 
                       && APTS_ConstantUtil.APPROVED.equalsIgnoreCase(newApprovalStage) && APTS_ConstantUtil.APPROVED.equalsIgnoreCase(newApprovalStatus)){
                           proposalObj.Proposal_Chevron_Status__c = APTS_ConstantUtil.APPROVED;
                       }

                    //Rejected
                    if(APTS_ConstantUtil.IN_REVIEW.equalsIgnoreCase(oldApprovalStage) && APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(oldApprovalStatus) 
                       && APTS_ConstantUtil.QUOTE_DRAFT.equalsIgnoreCase(newApprovalStage) && APTS_ConstantUtil.REJECTED.equalsIgnoreCase(newApprovalStatus)){
                           proposalObj.Proposal_Chevron_Status__c = APTS_ConstantUtil.PROPOSAL_SOLUTION;
                       }
                }
                //END: SNL Quote/Proposal Chevron management
                
                //START: RENEWAL MODIFICATION : Updating fields and record type for renewal modification
                if(APTS_ConstantUtil.RENEWAL.equalsIgnoreCase(proposalObj.Apttus_QPConfig__ABOType__c) 
                   && (string.isBlank(proposalObj.Program__c) || !string.isBlank(proposalObj.Program__c))
                   && (string.isBlank(proposalObj.APTPS_Program_Type__c) || !string.isBlank(proposalObj.APTPS_Program_Type__c))
                   && (string.isBlank(proposalObj.APTS_New_Sale_or_Modification__c) || !string.isBlank(proposalObj.APTS_New_Sale_or_Modification__c))
                   && (string.isBlank(proposalObj.APTS_Modification_Type__c)|| !string.isBlank(proposalObj.APTS_Modification_Type__c)))
                  {
                       renewalProposalsInScope.put(proposalObj.Id, proposalObj);
                       APTS_QuoteProposalBaseTriggerHelper.updateRenewalFields(renewalProposalsInScope);
                   }
                //END: RENEWAL MODIFICATION
            }
        }
        system.debug('>>>>>>>>>>proposalsInScope>>>>>> ' + proposalsInScope);
        
        if (proposalsInScope != null && proposalsInScope.size() > 0) {
            if(proposalsInScope[0].CurrencyIsoCode == 'USD')
                netjetsCompany = 'NJA';
            else if(proposalsInScope[0].CurrencyIsoCode == 'EUR')
                netjetsCompany = 'NJE';

            APTS_QuoteProposalBaseTriggerHelper.clearApprovalFlags(proposalsInScope);
            APTS_QuoteProposalBaseTriggerHelper.updateApproversOnProposal(proposalsInScope, netjetsCompany);
            APTS_QuoteProposalBaseTriggerHelper.checkApprovalRequiredFlags(proposalsInScope);
            APTS_QuoteProposalBaseTriggerHelper.setProposalApprovedStatus(proposalsInScope);
        }
        
        if (proposalsAutoApprove != null && proposalsAutoApprove.size() > 0) {
            APTS_QuoteProposalBaseTriggerHelper.clearApprovalFlags(proposalsAutoApprove);
            APTS_QuoteProposalBaseTriggerHelper.setProposalApprovedStatus(proposalsAutoApprove);
        }
        
        if(!approvalsInScope.isEmpty()) {
            APTS_QuoteProposalBaseTriggerHelper.updateFieldsAndApprovals(approvalsInScope);
            
            
        }
            
    }
    
    public override void afterUpdate() {
        List<Apttus_Proposal__Proposal__c> newProposals = Trigger.New;
        Map<Id,SObject> oldProposalMap = Trigger.OldMap;
        Map<Id, Apttus_Proposal__Proposal__c> dsInScope = new Map<Id, Apttus_Proposal__Proposal__c>();
        Map<Id, Apttus_Proposal__Proposal__c> demoQuotesInScope = new Map<Id, Apttus_Proposal__Proposal__c>();
        Map<Id, Apttus_Proposal__Proposal__c> enQuotesInScope = new Map<Id, Apttus_Proposal__Proposal__c>();
        system.debug('>>>>>>>>>>newProposals>>>>>> ' + newProposals);
        system.debug('>>>>>>>>>>oldProposalMap>>>>>> ' + oldProposalMap);
        for (Apttus_Proposal__Proposal__c proposalObj : newProposals){
            Apttus_Proposal__Proposal__c oldProposalObj = (Apttus_Proposal__Proposal__c) oldProposalMap.get(proposalObj.Id);
            //START:Deal Summary implementation
            if(proposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c != oldProposalObj.Apttus_QPConfig__ConfigurationFinalizedDate__c)
                dsInScope.put(proposalObj.Id, proposalObj);
            //END
            
            //START:CLM Agreement Automation
            if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(proposalObj.APTPS_Program_Type__c) && APTS_ConstantUtil.APPROVED.equalsIgnoreCase(proposalObj.Apttus_QPApprov__Approval_Status__c) 
              && !APTS_ConstantUtil.APPROVED.equalsIgnoreCase(oldProposalObj.Apttus_QPApprov__Approval_Status__c)){
                demoQuotesInScope.put(proposalObj.Id, proposalObj);
                enQuotesInScope.put(proposalObj.Id, proposalObj);
            }
            //END:CLM Agreement Automation
            //START:Email Notification 
            if((APTS_ConstantUtil.APPROVED.equalsIgnoreCase(proposalObj.Apttus_QPApprov__Approval_Status__c)
              && !APTS_ConstantUtil.APPROVED.equalsIgnoreCase(oldProposalObj.Apttus_QPApprov__Approval_Status__c)) || (APTS_ConstantUtil.REJECTED.equalsIgnoreCase(proposalObj.Apttus_QPApprov__Approval_Status__c)
              && !APTS_ConstantUtil.REJECTED.equalsIgnoreCase(oldProposalObj.Apttus_QPApprov__Approval_Status__c))){
                if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(proposalObj.APTPS_Program_Type__c) || APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(proposalObj.APTPS_Program_Type__c)) {
                    enQuotesInScope.put(proposalObj.Id, proposalObj);
                }
                
            }
            //END:Email Notification 
        }
        if(!dsInScope.isEmpty())
            APTS_QuoteProposalBaseTriggerHelper.setDealSummary(dsInScope);
        
        if(!demoQuotesInScope.isEmpty())
            APTS_QuoteProposalBaseTriggerHelper.processDemoQuotes(demoQuotesInScope);
        if(!enQuotesInScope.isEmpty())
            APTS_QuoteProposalBaseTriggerHelper.processEmailNotification(enQuotesInScope);
    }
    
    //START: GCM-10120
    public override void beforeDelete() {
        Map<Id,Apttus_Proposal__Proposal__c> oldProposalMap = (Map<Id, Apttus_Proposal__Proposal__c>)Trigger.OldMap;
        system.debug('>>>>>>>>>>Before Delete --> oldProposalMap>>>>>> ' + oldProposalMap);
        Set<Id> ehcProp = new Set<Id>();
        Set<Id> otherModProp = new Set<Id>();
        for(Apttus_Proposal__Proposal__c oldProp : oldProposalMap.values()) {
            if(oldProp.APTS_Modification_Type__c != null && APTS_Constants.EHC.equalsIgnoreCase(oldProp.APTS_Modification_Type__c))
                ehcProp.add(oldProp.Id);
            else if(oldProp.APTS_New_Sale_or_Modification__c != null && APTS_Constants.MODIFICATION.equalsIgnoreCase(oldProp.APTS_New_Sale_or_Modification__c) 
                   && !String.isEmpty(oldProp.APTS_Modification_Type__c))
                otherModProp.add(oldProp.Id);
        }

        if(!oldProposalMap.isEmpty())
            agList = APTS_QuoteProposalBaseTriggerHelper.resetParentAgreementStatus(oldProposalMap.keySet(), ehcProp, otherModProp);
    }
    
    public override void afterDelete() {
        system.debug('>>>>>>>>>>After Delete>>>>>>');
        if(!agList.isEmpty()) {
            update agList;
            system.debug('Agreement Records are successfully updated! Record Count = '+agList.size());
            agList = new List<Apttus__APTS_Agreement__c>();
        }
    }
    //END: GCM-10120
}