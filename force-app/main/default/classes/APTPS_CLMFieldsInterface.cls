/************************************************************************************************************************
@Name: APTPS_CLMFieldsInterface
@Author: Conga PS Dev Team
@CreateDate: 28 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CLMFieldsInterface {
    void populateCLMFields(Object args);
}