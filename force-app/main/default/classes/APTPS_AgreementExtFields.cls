/************************************************************************************************************************
@Name: APTPS_AgreementExtFields
@Author: Conga PS Dev Team
@CreateDate: 16 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public with sharing class APTPS_AgreementExtFields {
    public static boolean createRelatedAgreement = true;
    /** 
    @description:  populate AgreementExtension Fileds
    @param: Agreement List
    @return: 
    */
    public static void populateAgreementExtFields(List<Apttus__APTS_Agreement__c> agreementList) {
        List<APTPS_Agreement_Extension__c> agrExtListResult = new List<APTPS_Agreement_Extension__c>();
        Map<id, Id> agrToExtMapping = new Map<id, id>();
        Map<id, Id> agrProposalMapping = new Map<id, id>();
        Map<id, List<Apttus_Proposal__Proposal_Line_Item__c>> quoteToPropLinesMapping = new Map<id, List<Apttus_Proposal__Proposal_Line_Item__c>>();
        List<Apttus__APTS_Related_Agreement__c> relAgreementList = new List<Apttus__APTS_Related_Agreement__c>();
        for (Apttus__APTS_Agreement__c agr : agreementList){
            agrToExtMapping.put(agr.Apttus_QPComply__RelatedProposalId__c, agr.APTPS_Agreement_Extension__c);
            //To Populate relate to related from on the Related Agreement Obj
            if(APTS_ConstantUtil.AGREEMENT_MOD.equalsIgnoreCase(agr.APTS_New_Sale_or_Modification__c)) {
                agrProposalMapping.put(agr.Apttus_QPComply__RelatedProposalId__c, agr.Id);
            }
            system.debug('agrProposalMapping-->'+agrProposalMapping);
            
        }
        
        Map<Id, APTPS_Agreement_Extension__c> agrExtensionsMap = new Map<Id, APTPS_Agreement_Extension__c>([SELECT Id,APTPS_NetJets_Sales_Entity__c, APTPS_NetJets_Manager_Entity__c, APTPS_Contract_Borrowing_Hours__c, APTPS_Contract_Advance_Notice__c, APTPS_Contract_Compliance__c,APTPS_Contract_compliance_in_Words__c, APTPS_Contract_Insurance_Limit__c,APTPS_Contract_insurance_limit_in_words__c, APTPS_Contract_War_Risk_Limit__c,APTPS_Contract_war_risk_limit_in_words__c, APTPS_Contract_Purchase_Deposit__c,
        APTPS_X1_Lease__c,APTPS_Contract_Interim_Lease__c,APTPS_Prepayment_of_Fees__c,APTPS_X5_MMF_Incentive__c,APTPS_Borrowing__c,APTPS_Delayed_Start_Date__c,APTPS_Delayed_Start_Months__c, APTPS_Tail_Number__c, APTPS_Legal_Name__c,
        APTPS_Manufacturer__c, APTPS_Serial_Number__c, APTPS_Aircraft_Model__c,
        APTPS_Number_of_Engines__c, APTPS_Warranty_Date__c, APTPS_Cabin_Class_Size__c,
        APTPS_Actual_Delivery_Date__c       
        FROM APTPS_Agreement_Extension__c WHERE Id IN :agrToExtMapping.values()]);
        
        for (Apttus_Proposal__Proposal_Line_Item__c pli : [SELECT Id, Apttus_QPConfig__OptionId__r.Name, Apttus_QPConfig__OptionId__r.ProductCode, Apttus_QPConfig__LineType__c, Apttus_QPConfig__NetPrice__c, Product_Family__c, Apttus_QPConfig__IsPrimaryLine__c, Apttus_QPConfig__ChargeType__c, Apttus_Proposal__Proposal__c,Apttus_QPConfig__AttributeValueId__r.APTPS_Borrowing__c ,Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c, Apttus_QPConfig__OptionId__r.Cabin_Class__c, Apttus_QPConfig__LineStatus__c,Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__c,Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c,  
                                                        Apttus_QPConfig__AttributeValueId__r.APTPS_X1_Lease__c,Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c,Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Tail_Number__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Number_of_Engines__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Legal_Name__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Manufacturer__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Cabin_Class_Size__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Serial_Number__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Actual_Delivery_Date__c,
                                                        Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Aircraft_Model__c,
                                                        Apttus_Proposal__Proposal__r.APTS_Modification_Type__c   
                                                        FROM Apttus_Proposal__Proposal_Line_Item__c
                                                        WHERE Apttus_Proposal__Proposal__c IN :agrToExtMapping.keySet()]){
            if (quoteToPropLinesMapping.containsKey(pli.Apttus_Proposal__Proposal__c)){
                List<Apttus_Proposal__Proposal_Line_Item__c> existingLines = quoteToPropLinesMapping.get(pli.Apttus_Proposal__Proposal__c);
                existingLines.add(pli);
                quoteToPropLinesMapping.put(pli.Apttus_Proposal__Proposal__c, existingLines);
            } else{
                quoteToPropLinesMapping.put(pli.Apttus_Proposal__Proposal__c, new List<Apttus_Proposal__Proposal_Line_Item__c>{pli});
            }
        
        }
        System.debug('quoteToPropLinesMapping=>' + quoteToPropLinesMapping);
        try{

            for (id quoteId : quoteToPropLinesMapping.keySet()){
                APTPS_Agreement_Extension__c extension = agrExtensionsMap.get(agrToExtMapping.get(quoteId));
                Apttus__APTS_Related_Agreement__c relAgreement;
                System.debug('extension==>' + extension);
                System.debug('Line Items==>' + quoteToPropLinesMapping.get(quoteId));
                extension.APTPS_Contract_Borrowing_Hours__c = 0;
                extension.APTPS_NetJets_Sales_Entity__c = APTS_ConstantUtil.NJ_SALES_ENTITY ;
                extension.APTPS_NetJets_Manager_Entity__c = APTS_ConstantUtil.NJ_MANAGER_ENTITY;
                for (Apttus_Proposal__Proposal_Line_Item__c pli : quoteToPropLinesMapping.get(quoteId)){
                    if(APTS_ConstantUtil.INTERIM_LEASE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Name)){
                       extension.APTPS_Contract_Interim_Lease__c  = true; 
                    }      
                    if(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_X1_Lease__c){
                       extension.APTPS_X1_Lease__c  = true; 
                    }
                    if(APTS_ConstantUtil.PREPAYMENT_OF_FEES.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Name)){
                        extension.APTPS_Prepayment_of_Fees__c   = true; 
                    }
                    if(APTS_ConstantUtil.MMF_INCENTIVE_5X.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Name)){
                        extension.APTPS_X5_MMF_Incentive__c   = true; 
                    }
                    if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                    && (APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)
                      || APTS_ConstantUtil.LEASE_DEPOSIT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) ) {
                        
                        extension.APTPS_Borrowing__c = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Borrowing__c;
                        extension.APTPS_Delayed_Start_Date__c   = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c;
                        extension.APTPS_Delayed_Start_Months__c = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c;
                        if(APTS_ConstantUtil.G450_AIRCRAFT.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Name)) {
                            extension.APTPS_NetJets_Sales_Entity__c = APTS_ConstantUtil.NJI_SALES_ENTITY ;
                            extension.APTPS_NetJets_Manager_Entity__c = APTS_ConstantUtil.NJI_MANAGER_ENTITY;
                        } 
                        if(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Borrowing__c) {
                            integer hours = Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                            extension.APTPS_Contract_Borrowing_Hours__c = (hours)*(0.25);
                        }
                        if(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c != NULL) {
                            extension.APTPS_Share_Percentage__c = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c;
                            //extension.APTPS_Share_Percentage_Text__c = APTPS_SNLSharePercentageText.populateAgreementExtFields(decimal.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c));
                        }
                            
                        if(APTS_ConstantUtil.LIGHT_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c)) {
                            APTPS_SNLAgreementExtFieldStaticVal__mdt agrExt = APTPS_SNLAgreementExtFieldStaticVal__mdt.getInstance('LIGHT_CABIN');
                            extension.APTPS_Contract_Compliance__c = agrExt.APTPS_Contract_Compliance__c;
                            extension.APTPS_Contract_compliance_in_Words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_Compliance__c));
                            extension.APTPS_Contract_War_Risk_Limit__c = agrExt.APTPS_Contract_War_Risk_Limit__c;
                            extension.APTPS_Contract_war_risk_limit_in_words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_War_Risk_Limit__c));
                            extension.APTPS_Contract_Insurance_Limit__c = agrExt.APTPS_Contract_Insurance_Limit__c;
                            extension.APTPS_Contract_insurance_limit_in_words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_Insurance_Limit__c));
                        } 
                        else if(APTS_ConstantUtil.MID_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c)) {
                           APTPS_SNLAgreementExtFieldStaticVal__mdt agrExt = APTPS_SNLAgreementExtFieldStaticVal__mdt.getInstance('MID_CABIN');
                            extension.APTPS_Contract_Compliance__c = agrExt.APTPS_Contract_Compliance__c;
                            extension.APTPS_Contract_compliance_in_Words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_Compliance__c));
                            extension.APTPS_Contract_War_Risk_Limit__c = agrExt.APTPS_Contract_War_Risk_Limit__c;
                            extension.APTPS_Contract_war_risk_limit_in_words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_War_Risk_Limit__c));
                            extension.APTPS_Contract_Insurance_Limit__c = agrExt.APTPS_Contract_Insurance_Limit__c;
                            extension.APTPS_Contract_insurance_limit_in_words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_Insurance_Limit__c));
                           
                            //extension.APTPS_Contract_Purchase_Deposit__c = 150000/50;
                        }  
                        else if(APTS_ConstantUtil.SUPER_MID_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c)) {
                            APTPS_SNLAgreementExtFieldStaticVal__mdt agrExt = APTPS_SNLAgreementExtFieldStaticVal__mdt.getInstance('SUPER_MID_CABIN');
                            extension.APTPS_Contract_War_Risk_Limit__c = agrExt.APTPS_Contract_War_Risk_Limit__c;
                            extension.APTPS_Contract_war_risk_limit_in_words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_War_Risk_Limit__c));
                            extension.APTPS_Contract_Insurance_Limit__c = agrExt.APTPS_Contract_Insurance_Limit__c;
                            extension.APTPS_Contract_insurance_limit_in_words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_Insurance_Limit__c));
                            //extension.APTPS_Contract_Purchase_Deposit__c = 150000/50;
                        } 
                        else if(APTS_ConstantUtil.LARGE_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c)) {
                            APTPS_SNLAgreementExtFieldStaticVal__mdt agrExt = APTPS_SNLAgreementExtFieldStaticVal__mdt.getInstance('LARGE_CABIN');
                            extension.APTPS_Contract_Compliance__c = agrExt.APTPS_Contract_Compliance__c;
                            extension.APTPS_Contract_compliance_in_Words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_Compliance__c));
                            extension.APTPS_Contract_War_Risk_Limit__c = agrExt.APTPS_Contract_War_Risk_Limit__c;
                            extension.APTPS_Contract_war_risk_limit_in_words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_War_Risk_Limit__c));
                            extension.APTPS_Contract_Insurance_Limit__c = agrExt.APTPS_Contract_Insurance_Limit__c;
                            extension.APTPS_Contract_insurance_limit_in_words__c = APTPS_SNLNumberToText.populateAgreementExtNumberToTextFields(integer.valueOf(extension.APTPS_Contract_Insurance_Limit__c));
                            //extension.APTPS_Contract_Purchase_Deposit__c = 200000/50;
                        }
                        if(APTS_ConstantUtil.GLOBAL7500_AIRCRAFT.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Name)){
                            //extension.APTPS_Contract_Purchase_Deposit__c = 500000/50;
                        }
                        if(APTS_ConstantUtil.LIGHT_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c) && Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) < 200) {
                            extension.APTPS_Contract_Advance_Notice__c = 8;
                        } else if(APTS_ConstantUtil.LIGHT_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c) && Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 200) {
                            extension.APTPS_Contract_Advance_Notice__c = 4;
                        } else if(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c != APTS_ConstantUtil.LIGHT_CABIN && Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) < 200) {
                            extension.APTPS_Contract_Advance_Notice__c = 10;
                        } else if(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c != APTS_ConstantUtil.LIGHT_CABIN && Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 200) {
                            extension.APTPS_Contract_Advance_Notice__c = 6;
                        }
                        
                    }
                    //To Populate relate to related from on the Related Agreement Obj
                    if(APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) && !APTS_ConstantUtil.STATUS_NEW.equalsIgnoreCase(pli.Apttus_QPConfig__LineStatus__c)  && pli.Apttus_QPConfig__IsPrimaryLine__c && agrProposalMapping.containsKey(quoteId) && pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__c != null) {                      
                        Id relatedToId = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__c;
                        Id relatedFromId = agrProposalMapping.get(quoteId);
                        relAgreement = new Apttus__APTS_Related_Agreement__c(Apttus__APTS_Contract_To__c=relatedToId,Apttus__APTS_Contract_From__c=relatedFromId);
                    }
                    //GCM-7329,GCM-7330 for SizeIncrease Flow / Lease Termination / Deferment / Substitution modifications
                    if(APTS_ConstantUtil.SHARE_SIZE_INCREASE.equalsIgnoreCase(pli.Apttus_Proposal__Proposal__r.APTS_Modification_Type__c) ||
                       APTS_ConstantUtil.LEASE_TERMINATION.equalsIgnoreCase(pli.Apttus_Proposal__Proposal__r.APTS_Modification_Type__c) || 
                       APTS_ConstantUtil.SUBSTITUTION.equalsIgnoreCase(pli.Apttus_Proposal__Proposal__r.APTS_Modification_Type__c) || 
                       APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(pli.Apttus_Proposal__Proposal__r.APTS_Modification_Type__c)) {
                        extension.APTPS_Tail_Number__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Tail_Number__c; 
                        extension.APTPS_Number_of_Engines__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Number_of_Engines__c; 
                        extension.APTPS_Legal_Name__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Legal_Name__c; 
                        extension.APTPS_Warranty_Date__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c; 
                        extension.APTPS_Manufacturer__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Manufacturer__c;
                        extension.APTPS_Cabin_Class_Size__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Cabin_Class_Size__c;
                        extension.APTPS_Serial_Number__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Serial_Number__c;
                        extension.APTPS_Actual_Delivery_Date__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Actual_Delivery_Date__c; 
                        extension.APTPS_Aircraft_Model__c = pli.Apttus_QPConfig__AssetLineItemId__r.Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Aircraft_Model__c; 
                    }
                }
                system.debug('relAgreement-->'+relAgreement);
                if(relAgreement != null) {
                    relAgreementList.add(relAgreement);
                }
                agrExtListResult.add(extension);
            }
            if(!agrExtListResult.isEmpty()) {
                update agrExtListResult;
            }
            //Insert Related Agreement Obj
            system.debug('relAgreementList-->'+relAgreementList);
            if(!relAgreementList.isEmpty() && APTPS_AgreementExtFields.createRelatedAgreement) {
                APTPS_AgreementExtFields.createRelatedAgreement = false;
                Insert relAgreementList;
            }
            
        } catch (Exception e){
            System.debug('Exception in Class-APTPS_AgreementExtFields: Method- populateAgreementExtFields() at line' + e.getLineNumber()+' Message->' + e.getMessage());
        }
    }
}