public with sharing class APTS_ReconfigureCartPageController {
    
    private final Apttus_Proposal__Proposal__c proposalSO;
    private final String proposalId;

    public APTS_ReconfigureCartPageController(ApexPages.StandardController stdController) {
        this.proposalId = ApexPages.CurrentPage().getparameters().get('id');
        this.proposalSO = [SELECT Id,Name,APTS_Modification_Type__c,Apttus_Proposal__Approval_Stage__c, Contract_Request__c,APTPS_Program_Type__c,CurrencyIsoCode FROM Apttus_Proposal__Proposal__c WHERE Id = :proposalId];
        //this.proposalSO = (Apttus_Proposal__Proposal__c)stdController.getRecord();
    }
    
    public PageReference urlRedirection() {

        boolean updateProposal = false;
        if (proposalSO != null && proposalSO.Apttus_Proposal__Approval_Stage__c == 'Approval Required') {
            proposalSO.Apttus_Proposal__Approval_Stage__c = 'Draft';
            updateProposal = true;            
        }
        /*For GCM-9934 to reset Contract_Request__c field when we reconfigure the proposal*/
        if(proposalSO.Contract_Request__c) {
            proposalSO.Contract_Request__c= false;
            updateProposal = true;
            
        }
        if(updateProposal) {
            update proposalSO;
        }
        
        string url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=NewCardFlow';
        if (proposalSO.APTPS_Program_Type__c == 'Card') {
            if ('Expired Hours Card'.equals(proposalSO.APTS_Modification_Type__c)) {
                url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=ModExpired&launchstate=assetsgrid';
            }
            else if ('Term Extension'.equals(proposalSO.APTS_Modification_Type__c) || 'Add Enhancements'.equals(proposalSO.APTS_Modification_Type__c) || 'Termination'.equals(proposalSO.APTS_Modification_Type__c) || 'Assignment'.equals(proposalSO.APTS_Modification_Type__c)) {
                url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=ModificationFlow&launchstate=assetsgrid';
            }
            else if (proposalSO.APTS_Modification_Type__c != null && proposalSO.APTS_Modification_Type__c.length() > 0) {
                url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=NGDefault&launchstate=assetsgrid';
            }
        } else  if (APTS_ConstantUtil.SHARE.equalsIgnorecase(proposalSO.APTPS_Program_Type__c) || APTS_ConstantUtil.LEASE.equalsIgnorecase(proposalSO.APTPS_Program_Type__c)) {
            if(proposalSO.APTS_Modification_Type__c != null && (APTS_ConstantUtil.Repurchase.equalsIgnoreCase(proposalSO.APTS_Modification_Type__c) 
                                                                || APTS_ConstantUtil.SHARE_SIZE_INCREASE.equalsIgnoreCase(proposalSO.APTS_Modification_Type__c) 
                                                                || APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(proposalSO.APTS_Modification_Type__c)
                                                                || APTS_ConstantUtil.AMENDMENT.equalsIgnoreCase(proposalSO.APTS_Modification_Type__c))) {
                                                                    url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=SNL_Repurchase&launchstate=assetsgrid';
                                                                } else if(proposalSO.APTS_Modification_Type__c != null && APTS_ConstantUtil.ASSIGNMENT.equalsIgnoreCase(proposalSO.APTS_Modification_Type__c)) {
                                                                    url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=SNL_RelatedAssignment&launchstate=assetsgrid';
                                                                } else if (proposalSO.APTS_Modification_Type__c != null && proposalSO.APTS_Modification_Type__c.length() > 0) {
                                                                    url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=SLModificationFlow&launchstate=assetsgrid';
                                                                } else {
                                                                    if(proposalSO.CurrencyIsoCode=='EUR')
                                                                        //Commented it out. We may implement new approach to resolve the issue.
                                                                        //url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=ShareFlow_NJE&isCartTotalingDisabled=true';
                                                                        url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=ShareFlow_NJE';
                                                                    else
                                                                        url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=ShareFlow';
                                                                }
        } else if(APTS_ConstantUtil.SNL_ENHANCEMENT.equalsIgnorecase(proposalSO.APTPS_Program_Type__c)) {
            if(proposalSO.APTS_Modification_Type__c != null && APTS_ConstantUtil.ADD_ENHANCEMENTS.equalsIgnoreCase(proposalSO.APTS_Modification_Type__c)) 
                url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=SNL_AddEnhancements&launchstate=assetsgrid';
            else 
                url = '/apex/Apttus_QPConfig__ProposalConfiguration?id=' + proposalSO.Id + '&flow=SNL_Enhancements';
        }
        
            

        PageReference page = new PageReference(url);
        page.setRedirect(true);
        return page;
  }
}