/************************************************************************************************************************
@Name: APTPS_Approvals_Lease
@Author: Conga PS Dev Team
@CreateDate: 29 October 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
public class APTPS_Approvals_Lease implements APTPS_ApprovalUsersInterface_V2{
   //set EDC Legal flag for letter agreement modification approval      
   public void checkLegalApprovedRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){ }
    
   //set HCS flag for letter agreement modification approval        
   public void checkHeadofCommercialSalesApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
   //set flag for Letter Agreement Extension 
   public void checkCAManagerApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
   public void checkDDCApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
       for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {
           //Commented for Hours 03.11
            /*if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) && Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) == 25) {                        
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //proposalObj.APTPS_Hours__c = Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                
                break;                                        
            }*/
            if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null && (Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 6)) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;
            }
            if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && (proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c != '60')) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //APTPS_Contract_Length__c
                //proposalObj.APTPS_Contract_Length__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c);
                break;
            }
           //GCM-7288: Lease Termination Approvals
            if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalObj.APTS_Modification_Type__c.equalsIgnoreCase(APTS_ConstantUtil.LEASE_TERMINATION)
                && proposalLineItem.Apttus_QPConfig__LineType__c.equalsIgnoreCase(APTS_ConstantUtil.OPTION) && !APTS_ConstantUtil.NJA_EARLY_OUT.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode)) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                break;
            }
        }
        if(proposalObj.APTPS_Requires_DDC_Approval__c){
               proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = false;
               proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
           }
   }
    
    public void checkSalesOpsApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Lease NJA START*/
            if (APTS_Constants.NJUS_LEASE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null &&(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 3) && (Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <=6) ) {                        
                proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c  = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;                                        
            }
            /*For Lease NJA END*/
           if(proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c ){
               proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
           }

        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkSalesDirectorApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Share NJA START*/
            if (APTS_Constants.NJUS_LEASE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null &&(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <= 3) ) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;                                        
            }
            system.debug('DELAYED START END DATE ------'+proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_End_Date__c);
            if (APTS_Constants.NJUS_LEASE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_End_Date__c != null ) {                        
                system.debug('inside DELAYED START END DATE');
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Date__c = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c;
                break;                                        
            }
            if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) && Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 25) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Hours__c = Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                break;                                        
            }
            /*For Share NJA END*/
           

        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkDemoTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    public void checkDirectorARApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        system.debug('inside checkDirectorARApprovalRequired');
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJUS_LEASE .equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_Constants.DISCOUNT_AMOUNT.equals(proposalLineItem.Apttus_QPConfig__AdjustmentType__c)&& proposalLineItem.Apttus_QPConfig__AdjustmentAmount__c > 0) {                        
                proposalObj.APTPS_Requires_Director_AR_Approval__c = true;
                //proposalObj.APTPS_Operating_Fund_Discount_Applied__c = true;
                break;                                        
            }
        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    //set RVP flag for letter agreement modification approval
    public void checkRVPApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    public void checkSalesOpsTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJE_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode)) {                        
                proposalObj.APTPS_Requires_Sales_Ops_Team_Approval__c = true;
                break;                                        
            }
        }
         system.debug('proposalObj-->'+proposalObj);        
    }
    
    public void checkAssigmentApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        if(APTS_ConstantUtil.ASSIGNMENT.equals(proposalObj.APTS_Modification_Type__c)){
            APTPS_SNL_Util.updateAssignmentApprovals(proposalObj,quoteLines);           
        }
    }
    
    public void checkExtensionApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        if(APTS_ConstantUtil.LETTER_AGREEMENT_EXTENSION.equals(proposalObj.APTS_Modification_Type__c)){
            APTPS_SNL_Util.updateExtensionApprovals(proposalObj,quoteLines);            
        }
    }
    public void checkRepurchaseApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
    public void checkDefermentApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        system.debug('checkDefermentApprovalRequired method==>'+proposalObj.APTS_Modification_Type__c);
        if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equals(proposalObj.APTS_Modification_Type__c)){
            APTPS_SNL_Util.updateDefermentApprovals(proposalObj,quoteLines);            
        }
    }
    //GCM-7328 Size Increase Approvals
    public void checkSizeIncreaseApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    //GCM-7320 Reduction Approvals
    public void checkReductionApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
    
    //GCM-12253 Share NJE Approvals
    public void checkNJEApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
       
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {
            
            /*Lease Deposit exceptions Exceptions  Approvals START*/
            if(APTS_ConstantUtil.LINE_TYPE.equals(proposalLineItem.Apttus_QPConfig__LineType__c) && APTS_ConstantUtil.LEASE_DEPOSIT.equals(proposalLineItem.Apttus_QPConfig__ChargeType__c)) {
                if(proposalLineItem.Apttus_QPConfig__AdjustedPrice__c < proposalLineItem.Apttus_QPConfig__OptionPrice__c) {
                    proposalObj.APTPS_Requires_BillingReceivablesManager__c = true;
                    proposalObj.APTPS_Requires_Dir_Of_Finance_Approval__c = true;
                    proposalObj.APTPS_Requires_CFO_Approval__c = true;
                    break;
                 
                }
            }           
            /*Lease Deposit exceptions Exceptions  Approvals END*/
            
            /*Delayed Start Amount Period  Approvals START*/
            if (APTS_Constants.NJE_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c != null ) {    
                if(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <= 3) { 
                    proposalObj.APTS_Require_AE_SalesVP_Approval__c = true;
                    break;
                } else if(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 3 && Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <= 6) { 
                    proposalObj.APTS_Requires_RVP_Approval__c = true;
                    break;
                } else if(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 6) { 
                    proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;
                    break;
                }
            }
            if (APTS_Constants.NJE_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c != null ) {                        
                proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                 break;                                        
            }
            /*Delayed Start Amount Period  Approvals END*/
            /*Non-Standard Term Length  Approvals START*/
            if (APTS_Constants.NJE_SHARE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c != null ) { 
                if(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c) < 60) {
                    proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                    break;
                }
                                                        
            }
            /*Non-Standard Term Length  Approvals End*/
            /* 36 month Minimum Commitment Date  Approvals START*/
            if(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c != '25' && Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c) < 36 && !APTS_ConstantUtil.NEW_SALE.equalsIgnoreCase(proposalObj.APTS_New_Sale_or_Modification__c)){
                proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                break;
            }
            /* 36 month Minimum Commitment Date  Approvals END*/
            
        }
    }

    
    /** 
    @description: Lease NJE implementation
    @param:
    @return: 
    */
    public class NJE_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Lease NJE Approvals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Lease obj = new APTPS_Approvals_Lease();
            /*obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            obj.checkRVPApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesOpsTeamApprovalRequired(proposalObj,quoteLines);
            obj.checkHeadofCommercialSalesApprovalRequired(proposalObj,quoteLines);
            obj.checkLegalApprovedRequired(proposalObj,quoteLines);
            obj.checkExtensionApprovalRequired(proposalObj,quoteLines);
            obj.checkDefermentApprovalRequired(proposalObj,quoteLines);*/
            obj.checkNJEApprovalRequired(proposalObj,quoteLines);
            obj.checkAssigmentApprovalRequired(proposalObj,quoteLines);
            obj.checkExtensionApprovalRequired(proposalObj,quoteLines);
            obj.checkDefermentApprovalRequired(proposalObj,quoteLines);
            
        }
    }
    
    /** 
    @description: Lease NJA implementation
    @param:
    @return: 
    */
    public class NJA_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Lease NJA Approvals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Lease obj = new APTPS_Approvals_Lease();
            obj.checkDirectorARApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesOpsApprovalRequired(proposalObj,quoteLines);
            obj.checkDDCApprovalRequired(proposalObj,quoteLines);
            obj.checkCAManagerApprovalRequired(proposalObj,quoteLines);
            obj.checkExtensionApprovalRequired(proposalObj,quoteLines);
            obj.checkAssigmentApprovalRequired(proposalObj,quoteLines);
            obj.checkDefermentApprovalRequired(proposalObj,quoteLines);
        }
    }
}