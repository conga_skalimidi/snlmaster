public with sharing class APTPS_TailNumberController {

    @AuraEnabled
    public static string getTails(Id agreementid,string aircraftType,string RecordType_Name){
        try {
            String tails = '';
            tails = getTailList(agreementid,aircraftType,RecordType_Name);
            return tails;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public static String formatDate(Date d) {
        return d.year() + '-' + d.month() + '-' + d.day();
    }
    
    @AuraEnabled
    public static APTPS_Agreement_Extension__c populateTailInformation(String tailNumber,Id agreementid,String agreementExtId){
        try {
            if(agreementid != NULL){
                string aircraftTailjson='';
                if(tailNumber!=null && !String.isBlank(tailNumber)){
                   // aircraftTailjson = ACI_AircraftAPIInfo.getAircraftTailInformation(tailNumber);
                    system.debug('aircraftTailjson=> '+aircraftTailjson);  
                }
                
                if(String.isBlank(aircraftTailjson)){
                    AuraHandledException e = new AuraHandledException('Error occured while receiving JSON');
                    e.setMessage('Error occured while receiving JSON');
                    throw e;      
                    
                }                              
                if(agreementExtId != null && !String.isBlank(agreementExtId) && !String.isBlank(aircraftTailjson)){
                    populateAgreementExtension(aircraftTailjson, agreementExtId);                    
                }
            }
             
            system.debug('getAgreementExtension:- '+ getAgreementExtension(agreementExtId)); 
            
            return getAgreementExtension(agreementExtId);
        } catch (Exception e) {
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    
    @AuraEnabled
    public static APTPS_Agreement_Extension__c populateInterimTailInformation(String tailNumber,Id agreementid,String agreementExtId){
        try {
            if(agreementid != NULL){
                string aircraftTailjson='';
                if(tailNumber!=null && !String.isBlank(tailNumber)){
                   // aircraftTailjson = ACI_AircraftAPIInfo.getAircraftTailInformation(tailNumber);
                    system.debug('aircraftTailjson=> '+aircraftTailjson);  
                }
                
                if(String.isBlank(aircraftTailjson)){
                    AuraHandledException e = new AuraHandledException('Error occured while receiving JSON');
                    e.setMessage('Error occured while receiving JSON');
                    throw e;      
                    
                }                              
                if(agreementExtId != null && !String.isBlank(agreementExtId) && !String.isBlank(aircraftTailjson)){
                    populateAgreementExtension(aircraftTailjson, agreementExtId);                    
                }
            }
            
            APTPS_Agreement_Extension__c  extension =  getAgreementExtension(agreementExtId);
            
            //fetching parent agreement id.
            Apttus__APTS_Agreement__c parentAgreementId = [SELECT id ,Apttus__Parent_Agreement__c FROM Apttus__APTS_Agreement__c
                                WHERE Id =: agreementid ];
            
            system.debug('parentAgreementId:- '+ parentAgreementId.Id);
            
            List<Apttus__APTS_Agreement__c> parentAgreement = [SELECT APTPS_Interim_Tail_Number__c, APTPS_Interim_Manufacturer__c, APTPS_Interim_Aircraft_Model__c   FROM Apttus__APTS_Agreement__c
                                                         WHERE Id =: parentAgreementId.Id];
            
            if(parentAgreement.size() > 0){
                parentAgreement[0].APTPS_Interim_Tail_Number__c = extension.APTPS_Tail_Number__c;
                parentAgreement[0].APTPS_Interim_Manufacturer__c = extension.APTPS_Manufacturer__c;
                parentAgreement[0].APTPS_Interim_Aircraft_Model__c = extension.APTPS_Aircraft_Model__c;
                
                update parentAgreement;
            }
            
            String [] cabinSize;
            cabinSize = extension.APTPS_Cabin_Class_Size__c.split(' ');
            system.debug('cabinSize:- '+ cabinSize[0]);
            String newSearchText = '%'+cabinSize[0]+'%';
            
           List<APTPS_Deposit_Rule__c> depositeRule = null;
            if(cabinSize!=null){
                depositeRule = [SELECT APTPS_Interim_Lease_Deposit__c  FROM APTPS_Deposit_Rule__c
                                WHERE APTPS_Cabin_Size__c  like: newSearchText ];
                
            }
            
            if(depositeRule.size() > 0){
                Apttus__APTS_Agreement__c agreement = [SELECT APTPS_Interim_Lease_Deposit__c   FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid];
                agreement.APTPS_Interim_Lease_Deposit__c = depositeRule[0].APTPS_Interim_Lease_Deposit__c;
                update agreement; 
                system.debug('Test APTPS_Interim_Lease_Deposit__c:-  '+ agreement.APTPS_Interim_Lease_Deposit__c);
            }
            
            system.debug('getAgreementExtension:- '+ getAgreementExtension(agreementExtId)); 
            
            return getAgreementExtension(agreementExtId);
        } catch (Exception e) {
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Decimal getInterimLeaseDeposit(Id agreementid, Id agreementExtId){
        
        system.debug(' getInterimLeaseDeposit function called in controller !!!!!! :- ');
            
        APTPS_Agreement_Extension__c extrec = [SELECT APTPS_Cabin_Class_Size__c FROM APTPS_Agreement_Extension__c WHERE Id =:agreementExtId];
        
        String [] cabinSize;
        //cabinSize = extension.APTPS_Cabin_Class_Size__c.split(' ');
        cabinSize = extrec.APTPS_Cabin_Class_Size__c.split(' ');
        system.debug('cabinSize in getInterimLeaseDeposit:- '+ cabinSize[0]);
        String newSearchText = '%'+cabinSize[0]+'%';
        
       List<APTPS_Deposit_Rule__c> depositeRule = null;
        if(cabinSize!=null){
            depositeRule = [SELECT APTPS_Interim_Lease_Deposit__c  FROM APTPS_Deposit_Rule__c
                            WHERE APTPS_Cabin_Size__c  like: newSearchText ];
            
        }
        
        Apttus__APTS_Agreement__c agreement = null;
        if(depositeRule.size() > 0){
            agreement = [SELECT id,APTPS_Interim_Lease_Deposit__c   FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid];
            agreement.APTPS_Interim_Lease_Deposit__c = depositeRule[0].APTPS_Interim_Lease_Deposit__c;
            update agreement;
            system.debug('Test APTPS_Interim_Lease_Deposit__c in getInterimLeaseDeposit:-  '+ agreement.APTPS_Interim_Lease_Deposit__c);
        }
        
       return agreement.APTPS_Interim_Lease_Deposit__c;
      
    }
    
    //ACI_RestAPIWrapper
    //ACI_AircraftAPIInfo
    
    public static String getTailList(Id agreementId,string aircraftType, string RecordType_Name){
        String returnTailList = '',vintage='';
        if(agreementId!=null){        
            Apttus__APTS_Agreement__c agreement = [SELECT id,Aircraft_Types__c,APTPS_Parked_Aircraft__c,APTPS_Parked_Aircraft__r.Name,APTPS_Agreement_Extension__c,APTPS_Agreement_Extension__r.Name,RecordType.Name FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid];
            aircraftType =  agreement.APTPS_Parked_Aircraft__r.Name;
            Apttus__AgreementLineItem__c agrLI = [select id,Apttus_CMConfig__AttributeValueId__r.APTPS_Vintage__c from Apttus__AgreementLineItem__c where Apttus__AgreementId__c=:agreementId AND Apttus_CMConfig__OptionId__c!=null AND Apttus_CMConfig__IsPrimaryLine__c=true
                                                  AND Apttus_CMConfig__OptionId__r.Family='Aircraft' limit 1];
            
            vintage = agrLI.Apttus_CMConfig__AttributeValueId__r.APTPS_Vintage__c;
            System.debug('vintage:-  '+vintage);
            if(aircraftType!=null && String.isNotBlank(aircraftType)){
                system.debug('aircraftType '+aircraftType);
                if(RecordType_Name==APTS_ConstantUtil.AGREEMENT_NJA_LEASE_PURCHASE || RecordType_Name==APTS_ConstantUtil.AGREEMENT_NJE_LEASE_PURCHASE){
                  //  returnTailList = ACI_AircraftAPIInfo.getAircraftTailList(aircraftType,'');
                }else{
                    //returnTailList = ACI_AircraftAPIInfo.getAircraftTailList(aircraftType,vintage);
                }
            }            
            System.debug('returnTailList '+returnTailList);
        }
        
        return returnTailList;
    }
    
    @AuraEnabled
    public static String getIntrimTailList(String agreementId,string aircraftTypeInterim, string RecordType_Name){
        system.debug('Test agreementId '+agreementId);
        system.debug('Test aircraftTypeInterim '+aircraftTypeInterim);
        system.debug('Test RecordType_Name '+RecordType_Name);
        String returnIntrimTailList = '',vintage='';
        if(agreementId!=null){        
            Apttus__APTS_Agreement__c agreement = [SELECT id,Aircraft_Types__c,APTPS_Parked_Aircraft__c,APTPS_Aircraft_type_interim__c,APTPS_Parked_Aircraft__r.Name,APTPS_Agreement_Extension__c,APTPS_Agreement_Extension__r.Name,RecordType.Name FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid];
            aircraftTypeInterim =  agreement.APTPS_Aircraft_type_interim__c;
            
            if(aircraftTypeInterim!=null && String.isNotBlank(aircraftTypeInterim)){
                system.debug('aircraftTypeInterim :- inside getIntrimTailList '+aircraftTypeInterim);
                //    returnIntrimTailList = ACI_AircraftAPIInfo.getAircraftTailList(aircraftTypeInterim,'');
            }            
            System.debug('returnIntrimTailList '+returnIntrimTailList);
        }
        
        return returnIntrimTailList;
    }
    
    
    @AuraEnabled
    public static APTPS_Agreement_Extension__c getAgreementExtension(Id agreementExtId){
        APTPS_Agreement_Extension__c extrec = new APTPS_Agreement_Extension__c();
        if(agreementExtId != NULL && !String.isBlank(agreementExtId)){
            extrec = [SELECT Id, APTPS_Actual_Delivery_Date__c,                              
                      APTPS_Serial_Number__c, APTPS_Tail_Number__c, 
                      APTPS_Vintage__c, APTPS_Warranty_Date__c,APTPS_Legal_Name__c,
                      APTPS_Manufacturer__c,APTPS_Number_of_Engines__c,APTPS_Cabin_Class_Size__c,APTPS_Aircraft_Model__c
                      FROM APTPS_Agreement_Extension__c WHERE Id =:agreementExtId];
            system.debug('extrec==>'+extrec);
            return extrec;
        }
        
        return extrec;
    }
    
    
    public static void populateAgreementExtension(String aircraftTailjson, Id agreementExtId ){
        APTPS_Agreement_Extension__c extrec = new APTPS_Agreement_Extension__c(id=agreementExtId);
        /*APTPS_TailUpdateWrapper tailw = new APTPS_TailUpdateWrapper(aircraftTailjson);
        
        extrec.APTPS_Actual_Delivery_Date__c = tailw.actualDeliveryDate;
        extrec.APTPS_Warranty_Date__c = tailw.warrantyDate;
        extrec.APTPS_Serial_Number__c = tailw.serialNumber; 
        extrec.APTPS_Tail_Number__c = tailw.tailNumber; 
        extrec.APTPS_Legal_Name__c = tailw.aircraftTypeName;
        extrec.APTPS_Manufacturer__c = tailw.manufacturer; 
        extrec.APTPS_Number_of_Engines__c = tailw.noOfEngines;
        extrec.APTPS_Cabin_Class_Size__c =  tailw.cabinClassSize;
        extrec.APTPS_Aircraft_Model__c = tailw.aircraftModel;
        */
        update extrec;     
    }
    
    @AuraEnabled
    public static Map<String,String> getData(Id agreementid)
    {
        try{
            
            Map<String,String> recordTypeMap = new Map<String,String>();
            Map<String,String> agrLeaseMap = new Map<String,String>();
            
            List<Apttus__APTS_Agreement__c> agrList = new List<Apttus__APTS_Agreement__c>();
            
            Apttus__APTS_Agreement__c agr =[select id,APTPS_Parked_Aircraft__c,APTPS_Parked_Aircraft__r.Name,APTPS_Agreement_Extension__c,APTS_Additional_Modification_Type__c,APTS_Modification_Type__c,
                                            APTPS_Agreement_Extension__r.Name,RecordType.Name,APTPS_Share_Percentage__c,APTPS_Business_Number__c,
                                            APTPS_Aircraft_Allotment_Id__c from Apttus__APTS_Agreement__c where id=:agreementid limit 1];
            if(agr.APTPS_Parked_Aircraft__c!=null){
                agrLeaseMap.put('parkedAircraft',agr.APTPS_Parked_Aircraft__c);
                agrLeaseMap.put('aircraftType',agr.APTPS_Parked_Aircraft__r.Name);
            }
            if(agr.APTPS_Agreement_Extension__c!=null){
                agrLeaseMap.put('agreementExtId',agr.APTPS_Agreement_Extension__c);
            }
            if(agr.APTPS_Share_Percentage__c!=null){
                agrLeaseMap.put('interestSizePercent',string.valueOf(agr.APTPS_Share_Percentage__c));
            }
            if(APTS_ConstantUtil.SHARE_SIZE_INCREASE.equalsIgnoreCase(agr.APTS_Modification_Type__c) && APTS_ConstantUtil.NONE.equalsIgnoreCase(agr.APTS_Additional_Modification_Type__c)) {
                
                agrLeaseMap.put('sizeIncreaseModification','true');
            }
            agrLeaseMap.put('BusinessNumber',agr.APTPS_Business_Number__c);
            agrLeaseMap.put('aircraftAllotmentId',agr.APTPS_Aircraft_Allotment_Id__c);
            agrLeaseMap.put('RecordType_Name',agr.RecordType.Name);
            agrList = [select id,Apttus__Parent_Agreement__c,recordtype.name,Apttus_QPComply__RelatedProposalId__r.APTPS_Program_Type__c,APTPS_Agreement_Extension__c from Apttus__APTS_Agreement__c where Apttus__Parent_Agreement__c=:agreementid limit 1];
            if(agrList.size()>0){
                if(agrList[0].Apttus__Parent_Agreement__c!=null &&
                   (agrList[0].recordtype.name==APTS_ConstantUtil.AGREEMENT_NJA_INTERIM_LEASE || 
                    agrList[0].recordtype.name==APTS_ConstantUtil.AGREEMENT_ON_HOLD_INTERIM_LEASE || 
                    agrList[0].recordtype.name==APTS_ConstantUtil.AGREEMENT_NJE_INTERIM_LEASE)){
                        agrLeaseMap.put('childAgreement',agrList[0].id);
                        agrLeaseMap.put('interimAgreementExtId',agrList[0].APTPS_Agreement_Extension__c);
                    }
            }
            
            
            return agrLeaseMap;
        }
        catch (Exception e) {
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }  
    
    @AuraEnabled
    public static String callReserveInventory(string agreementId,string tailNumber, string BusinessNumber, string interestSizePercent, string aircraftAllotmentId){
        try{
        decimal interestSize = Decimal.valueOf(interestSizePercent);
        String newAircraftAllotmentId = '';
            //calling reserveInventory API
           // newAircraftAllotmentId = ACI_AircraftAPIInfo.reserveInventory(tailNumber,interestSize,aircraftAllotmentId);
            if(!string.isBlank(newAircraftAllotmentId)){
                Apttus__APTS_Agreement__c agr = new Apttus__APTS_Agreement__c(id= agreementId,APTPS_Aircraft_Allotment_Id__c = newAircraftAllotmentId);
                update agr;
                return newAircraftAllotmentId;
            }else{  return newAircraftAllotmentId;}      
        
        }
        catch (Exception e) {
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static String updateParkedAircraftCheck(Id agreementid,Id parkedAircraftId,Id agreementExtId){
        try{
            Apttus__APTS_Agreement__c agr = new Apttus__APTS_Agreement__c(id=agreementid,APTPS_Parked_Aircraft__c=parkedAircraftId);
            update agr;
            APTPS_Agreement_Extension__c extrec = new APTPS_Agreement_Extension__c(id=agreementExtId,APTPS_Actual_Delivery_Date__c=null,APTPS_Warranty_Date__c=null,
                                                                                   APTPS_Serial_Number__c='',APTPS_Tail_Number__c='',
                                                                                  APTPS_Legal_Name__c='',APTPS_Manufacturer__c='',
                                                                                  APTPS_Number_of_Engines__c=null,APTPS_Cabin_Class_Size__c='',APTPS_Aircraft_Model__c='');
            update extrec;
            return [Select APTPS_Parked_Aircraft__r.Name from Apttus__APTS_Agreement__c where id=:agreementid].APTPS_Parked_Aircraft__r.Name;
        }
        catch (Exception e) {
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    
    @AuraEnabled
    public static String updateAircraftTypeInterimCheck(Id agreementid,String aircraftTypeInterim,Id agreementExtId){
        try{
            
            String [] output = new List<String>();
            system.debug('aircraftTypeInterimId:- '+ aircraftTypeInterim);
            system.debug('agreementid:- '+ agreementid);
            system.debug('agreementExtId:- '+ agreementExtId);
            Apttus__APTS_Agreement__c agr = new Apttus__APTS_Agreement__c(id=agreementid,APTPS_Aircraft_type_interim__c=aircraftTypeInterim);

            update agr;
           /* APTPS_Agreement_Extension__c extrecintrim = new APTPS_Agreement_Extension__c(id=agreementExtId,APTPS_Actual_Delivery_Date__c=null,APTPS_Warranty_Date__c=null,
                                                                                   APTPS_Serial_Number__c='',APTPS_Tail_Number__c='',
                                                                                  APTPS_Legal_Name__c='',APTPS_Manufacturer__c='',
                                                                                  APTPS_Number_of_Engines__c=null,APTPS_Cabin_Class_Size__c='',APTPS_Aircraft_Model__c='');
            update extrecintrim; */
            
            List<Apttus_Config2__PriceMatrixEntry__c> AdjustmentAmount = [select Apttus_Config2__AdjustmentAmount__c from Apttus_Config2__PriceMatrixEntry__c where Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__ChargeType__c =: APTS_ConstantUtil.MONTHLY_LEASE_PAYMENT 
                                                                    AND Apttus_Config2__PriceMatrixId__r.Apttus_Config2__Dimension1Id__r.Name =: APTS_ConstantUtil.CONTRACT_LENGTH
                                                                    AND Apttus_Config2__Dimension1Value__c =: APTS_ConstantUtil.DIMENTION_VALUE AND Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__ProductId__r.Name =:aircraftTypeInterim ];
            
            Apttus__APTS_Agreement__c agreement = [SELECT APTPS_Interim_Monthly_Lease_Fee__c FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid];
             if(!AdjustmentAmount.isEmpty()){
                 system.debug('AdjustmentAmount:- '+ AdjustmentAmount[0]);
                 agreement.APTPS_Interim_Monthly_Lease_Fee__c = AdjustmentAmount[0].Apttus_Config2__AdjustmentAmount__c;
                 update agreement;
            }
            
            return [Select APTPS_Aircraft_type_interim__c from Apttus__APTS_Agreement__c where id=:agreementid].APTPS_Aircraft_type_interim__c;
        }
        catch (Exception e) {
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Decimal calculateInterimMonthlyLeaseFee(String aircraftTypeInterim, Id agreementid){
        try{
            
             List<Apttus_Config2__PriceMatrixEntry__c> AdjustmentAmount = [select Apttus_Config2__AdjustmentAmount__c from Apttus_Config2__PriceMatrixEntry__c where Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__ChargeType__c =: APTS_ConstantUtil.MONTHLY_LEASE_PAYMENT 
                                                                    AND Apttus_Config2__PriceMatrixId__r.Apttus_Config2__Dimension1Id__r.Name =: APTS_ConstantUtil.CONTRACT_LENGTH
                                                                    AND Apttus_Config2__Dimension1Value__c =: APTS_ConstantUtil.DIMENTION_VALUE AND Apttus_Config2__PriceMatrixId__r.Apttus_Config2__PriceListItemId__r.Apttus_Config2__ProductId__r.Name =:aircraftTypeInterim ];
            
            Apttus__APTS_Agreement__c agreement = [SELECT APTPS_Interim_Monthly_Lease_Fee__c FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid];
            
            system.debug('AdjustmentAmount:- '+ AdjustmentAmount[0]);
            agreement.APTPS_Interim_Monthly_Lease_Fee__c = AdjustmentAmount[0].Apttus_Config2__AdjustmentAmount__c;
            update agreement;
            
            system.debug('Test AdjustmentAmount:- '+ agreement.APTPS_Interim_Monthly_Lease_Fee__c);
            
            return agreement.APTPS_Interim_Monthly_Lease_Fee__c;
        }catch(Exception e){
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    
    @AuraEnabled
    public static Apttus__APTS_Agreement__c getInterimAgreementInformation(String agreementExtId){
        try{
            Apttus__APTS_Agreement__c agreement = [SELECT APTPS_Interim_Lease_Deposit__c, APTPS_Interim_Monthly_Lease_Fee__c, APTPS_Aircraft_type_interim__c FROM Apttus__APTS_Agreement__c WHERE Id =:agreementExtId];

            system.debug('Test return getInterimAgreementInformation:- ' + agreement);
            return agreement;      
        }catch(Exception e){
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }

    
}