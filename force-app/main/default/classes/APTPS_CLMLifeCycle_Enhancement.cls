/************************************************************************************************************************
@Name: APTPS_CLMLifeCycle_Enhancement
@Author: Conga PS Dev Team
@CreateDate: 13 January 2022
@Description: Implement CLM Lifecycle for Enhancement(NJA and NJE)
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/


public class APTPS_CLMLifeCycle_Enhancement {
    
    /**
    @description: Update Status 
    @param: args 
    @return: void
    */
    
    public static void updateCLMStatus(Map<String, Object> args,String currencyCode) {
        List<Apttus__APTS_Agreement__c> newAg = (List<Apttus__APTS_Agreement__c>)args.get('newAgrList');
        Map<Id,SObject> oldAgMap = (Map<Id,SObject>)args.get('oldAgrList');        
       
        for(Apttus__APTS_Agreement__c ag : newAg) {
            Apttus__APTS_Agreement__c oldAgObj = (Apttus__APTS_Agreement__c) oldAgMap.get(ag.Id);
            if (oldAgObj != null) {
                String oldStatus = oldAgObj.Apttus__Status__c;
                String oldStatusCat = oldAgObj.Apttus__Status_Category__c;
                String newStatus = ag.Apttus__Status__c;
                String newStatusCat = ag.Apttus__Status_Category__c;
                String oldAgStatus = oldAgObj.Agreement_Status__c;
                system.debug('SNL Agreement Status Lifecycle. oldStatus --> '+oldStatus+' oldStatusCat --> '+oldStatusCat+
                             ' newStatus --> '+newStatus+' newStatusCat --> '+newStatusCat);
                
                //Generate the document
                if(APTS_ConstantUtil.REQUEST.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.REQUEST.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_GENERATED, ag.Funding_Status__c, APTS_ConstantUtil.DOC_GENERATED);
                        //Stamp contract generated date
                        ag.APTPS_Contract_Generated_Date__c = Date.today(); 
                   }
                
                //Send for Review
                if((!oldAgObj.APTPS_Sent_For_Review_Outside_Conga__c && ag.APTPS_Sent_For_Review_Outside_Conga__c) || 
                   (APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.APTTUS_OTHER_PARTY_REVIEW.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(newStatusCat))) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.IN_REVIEW, ag.Funding_Status__c, APTS_ConstantUtil.DOC_GENERATED);
                       //Check with the team
                       ag.Apttus__Status__c = APTS_ConstantUtil.APTTUS_OTHER_PARTY_REVIEW;
                       ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                   }
                
                //Submit For Approvals
                if(((APTS_ConstantUtil.APTTUS_OTHER_PARTY_REVIEW.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat)) 
                    || (APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat)) 
                    || (APTS_ConstantUtil.APPR_REJ.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat))) 
                    && APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(newStatusCat)) {
                    ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
                    APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_IN_APPROVALS, ag.Funding_Status__c, APTS_ConstantUtil.DOC_GENERATED);
                }
                
                //Approved
                if(APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.APPR_REQ.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(newStatusCat)) {
                       ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.APPROVED;
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_APPROVED, ag.Funding_Status__c, APTS_ConstantUtil.DOC_GENERATED);
                   }
                
                //Rejected
                if(APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.APPR_REJ.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(newStatusCat)) {
                       ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.REJECTED;
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_IN_APPROVALS, ag.Funding_Status__c, APTS_ConstantUtil.DOC_GENERATED);
                   }
                
                //Send for signatures
                if(APTS_ConstantUtil.APPR_REQ.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat)
                   && APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                       ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.APPROVED;
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_PENDING, ag.Funding_Status__c, APTS_ConstantUtil.DOC_PENDING);
                       //Stamp sent for signatures date
                        ag.APTPS_Document_Sent_Date__c = Date.today();
                }
                
                //Fully Signed
                if(APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_PENDING_ACTIVATION, ag.Funding_Status__c, APTS_ConstantUtil.DOC_SIGNED);
                       
                }
                
                //Activation
                if(APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(newStatusCat)) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_ACTIVE, ag.Funding_Status__c, APTS_ConstantUtil.DOC_SIGNED);
                       
                        
                }  
                   
                //Cancelled
                if(!APTS_ConstantUtil.CANCEL_REQ.equalsIgnoreCase(oldStatus) && !APTS_ConstantUtil.CANCEL.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.CANCEL_REQ.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.CANCEL.equalsIgnoreCase(newStatusCat)) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.CANCEL, ag.Funding_Status__c, ag.Document_Status__c);
                   }
            } else 
                system.debug('Old agreement object is null');
            
        }       
        
    }
    
    /** 
    @description: NJE Enhancement implementation
    @param:
    @return: 
    */
    public class NJE_CLMFlow implements APTPS_CLMLifeCycleInterface {
        public void callCLMAction(Map<String, Object> args) {
            APTPS_CLMLifeCycle_Share.updateCLMStatus(args,APTS_ConstantUtil.CUR_EUR);         
        }
    }
    
    /** 
    @description: NJA Enhancement implementation
    @param:
    @return: 
    */
    public class NJA_CLMFlow implements APTPS_CLMLifeCycleInterface {
        public void callCLMAction(Map<String, Object> args) {
            APTPS_CLMLifeCycle_Share.updateCLMStatus(args,APTS_ConstantUtil.CUR_USD);
        }
    }
    
    
}