public with sharing class APTPS_ConsolidateAgreementController {       
    			private static Integer countExtraHours = 0;
    			private static boolean checkHEPricing = false;
                @AuraEnabled
                public static void createLAConsolidateRecords(String agreementId) {      
                    system.debug('agreementId >> ' + agreementId);
                    
                    Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(
                        Id = agreementId,
                        Chevron_Status__c = APTS_ConstantUtil.LACONSOLIDATIONINPROGRESS
                    );
                    update agreement;
                    
                    processLACRecords(agreementId);
                }
    //method to create consolidate records from  Share/Lease assetLI/AgrLI
    public static APTPS_Consolidate_Letter_Agreement__c generateConsolidateLA(Apttus_Config2__AssetLineItem__c assetLI,Apttus__AgreementLineItem__c agrLI,Id agrId,Id accId, Integer count,string commAgreement){
        APTPS_Consolidate_Letter_Agreement__c conLA = new APTPS_Consolidate_Letter_Agreement__c();
        //Consolidated Agreement Id from relevant asset LI and agreement LI
        conLA.APTPS_zzz_Agreement_Id__c = agrId;
        conLA.APTPS_AccountId__c = accId;
        String countAgr = APTPS_NumberToOrdinalFormat.toOrdinalFormat(count);
        countAgr += ' Program Agreement';
        conLA.APTPS_Agreement__c = countAgr;
        conLA.APTPS_Agreement_Number__c = commAgreement;
        if(assetLI!=null){
            conLA.APTPS_Asset_Attribute_Values__c = assetLI.Apttus_Config2__AttributeValueId__c;
            //conLA.APTPS_Hours__c = Decimal.valueOf(assetLI.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c);
            conLA.Product__c = assetLI.Apttus_Config2__ProductId__c;
            conLA.APTPS_Product_Name__c = assetLI.Apttus_Config2__ProductId__r.Name;
            conLA.APTPS_Aircraft_Type__c = assetLI.Apttus_CMConfig__AgreementId__r.APTPS_Contract_aircraft_type__c;
            conLA.APTPS_Agreement_Start_Date__c = assetLI.Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c;
        }else if(agrLI!=null){
            conLA.APTPS_Agreement_Attribute_Values__c = agrLI.Apttus_CMConfig__AttributeValueId__c;
            //conLA.APTPS_Hours__c = Decimal.valueOf(agrLI.Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c);
            conLA.Product__c = agrLI.Apttus__ProductId__c;
            conLA.APTPS_Product_Name__c = agrLI.Apttus__ProductId__r.Name;
            conLA.APTPS_Aircraft_Type__c = agrLI.Apttus__AgreementId__r.APTPS_Contract_aircraft_type__c;
        }               
        return conLA;
    }
    
    //@future
    public static void processLACRecords(String agreementId) {  
        Integer count = 0;  
        Integer total_hours=0;
        Id accId = null;
        Set<Id> agIds = new Set<Id>();
        Map<Id, Id> agConfigMap = new Map<Id, Id>();
        List<Apttus_Config2__AssetLineItem__c> assetList = new List<Apttus_Config2__AssetLineItem__c>();
        List<String> enhCodes = new List<String>();
        List<APTPS_Consolidate_Letter_Agreement__c> conLAList = new List<APTPS_Consolidate_Letter_Agreement__c>();
        Map<Id,String> agrIdProgramAgrMap = new Map<Id,String>();
        
        List<Apttus__APTS_Agreement__c> agreements = [
            SELECT Id,
            Apttus__Account__c
            FROM Apttus__APTS_Agreement__c WHERE Id=:agreementId
        ];
        
        if(agreements.isEmpty()) 
            return ;
        
        accId = agreements[0].Apttus__Account__c;
        system.debug('Account id '+accId);
        
        DELETE [
            SELECT Id,APTPS_Agreement_Number__c 
            FROM APTPS_Consolidate_Letter_Agreement__c 
            WHERE APTPS_Agreement_Number__c =:agreementId
        ];
        
        Map<Id,APTPS_Consolidate_Letter_Agreement__c> InterimMap = new Map<Id,APTPS_Consolidate_Letter_Agreement__c>();
        Map<Apttus_Config2__AssetLineItem__c,string> assetLICountMap = new Map<Apttus_Config2__AssetLineItem__c,string>(); 
        
        //Query AssetLI for the AccountId and status='Activated'
        for(Apttus_Config2__AssetLineItem__c assetLineItem : [
            SELECT Id, Name, Apttus_CMConfig__AgreementId__c, Apttus_Config2__ChargeType__c,
            Apttus_CMConfig__AgreementLineItemId__c, Asset_Agreement__c, 
            APTS_Original_Agreement__c, Apttus_Config2__BillToAccountId__c, 
            Apttus_Config2__AccountId__c, Apttus_Config2__ShipToAccountId__c,
            Apttus_Config2__ProductId__c, Apttus_Config2__OptionId__c,
            Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c,
            Apttus_CMConfig__AgreementId__r.APTPS_Share_Percentage__c,
            Apttus_CMConfig__AgreementId__r.Apttus__Total_Contract_Value__c,
            Apttus_CMConfig__AgreementId__r.Apttus__Contract_End_Date__c,
            Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c,Apttus_Config2__AttributeValueId__r.APTPS_Hours__c,
            Apttus_Config2__AttributeValueId__c, Apttus_Config2__ListPrice__c,
            Apttus_Config2__Quantity__c, Apttus_Config2__ProductId__r.ProductCode,Apttus_Config2__ProductId__r.Name,
            Apttus_Config2__OptionId__r.Name,
            Apttus_Config2__OptionId__r.ProductCode,Apttus_CMConfig__AgreementId__r.Aircraft_Types__c,
            Apttus_Config2__ProductId__r.Family, Apttus_Config2__OptionId__r.Family,Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__c,
            Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Manufacturer__c,
            Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Tail_Number__c,
            Apttus_CMConfig__AgreementId__r.APTPS_Has_Interim_Lease_Option__c,
            Apttus_Config2__AttributeValueId__r.Account__c,Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c,Apttus_Config2__AttributeValueId__r.APTPS_X1_Lease__c,
            Apttus_CMConfig__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Serial_Number__c,Apttus_Config2__LineType__c,Apttus_CMConfig__AgreementId__r.APTPS_Contract_aircraft_type__c 
            FROM Apttus_Config2__AssetLineItem__c 
            WHERE Apttus_CMConfig__AgreementId__c != null 
            AND Apttus_Config2__AccountId__c =:accId AND Apttus_Config2__AssetStatus__c=:APTS_ConstantUtil.ACTIVATED
            Order By Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c desc]){
                system.debug('Inside SOQL For loop for assets');
                if(!agIds.contains(assetLineItem.Apttus_CMConfig__AgreementId__c) 
                   && APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(assetLineItem.Apttus_Config2__LineType__c)
                  && !APTS_ConstantUtil.SNL_ENHANCEMENT.equalsIgnoreCase(assetLineItem.Apttus_Config2__ProductId__r.Name)) {
                       agIds.add(assetLineItem.Apttus_CMConfig__AgreementId__c);
                       assetList.add(assetLineItem);
                       count++;
                       system.debug('count-->'+count);
                       APTPS_Consolidate_Letter_Agreement__c conLA =  generateConsolidateLA(assetLineItem,null,assetLineItem.Apttus_CMConfig__AgreementId__c,accId,count,agreementId);
                       if(conLA.APTPS_Hours__c!=null){
                       	   total_hours = total_hours+ Integer.valueOf(conLA.APTPS_Hours__c);
                       } 
                      if(!agrIdProgramAgrMap.containsKey(assetLineItem.Apttus_CMConfig__AgreementId__c)){
                           agrIdProgramAgrMap.put(assetLineItem.Apttus_CMConfig__AgreementId__c,conLA.APTPS_Agreement__c);
                       }
                       if(assetLineItem.Apttus_CMConfig__AgreementId__c!=null && (assetLineItem.Apttus_CMConfig__AgreementId__r.APTPS_Has_Interim_Lease_Option__c==true)){
                        conLA.APTPS_Has_Interim_Lease_Option__c = true;
                        InterimMap.put(assetLineItem.Apttus_CMConfig__AgreementId__c,conLA);
                    	}
                       system.debug('conLA-->'+conLA);
                       conLAList.add(conLA);
                   }
               
                if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(assetLineItem.Apttus_Config2__LineType__c) 
                   && (APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equalsIgnoreCase(assetLineItem.Apttus_Config2__ProductId__r.ProductCode) 
                       || APTS_ConstantUtil.SNL_NJE_ENHANCEMENT.equalsIgnoreCase(assetLineItem.Apttus_Config2__ProductId__r.ProductCode)) 
                   && !enhCodes.contains(assetLineItem.Apttus_Config2__OptionId__r.ProductCode)){
                       system.debug('Inside Asset Enhancements!!');
                       enhCodes.add(assetLineItem.Apttus_Config2__OptionId__r.ProductCode);
                   }
                
            }//end assLI loop
        
        for(Apttus__AgreementLineItem__c agreementLI : [
            SELECT Id, Name, Apttus_CMConfig__OptionId__c, Apttus__ProductId__c, Apttus__ProductId__r.Name,
            Apttus__AgreementId__c, Apttus__AgreementId__r.Apttus__Account__c, 
            Apttus_CMConfig__ChargeType__c, Apttus__Quantity__c,
            Apttus__ProductId__r.ProductCode, Apttus_CMConfig__OptionId__r.ProductCode,
            Apttus__ProductId__r.Family, Apttus_CMConfig__OptionId__r.Family,
            Apttus_CMConfig__AttributeValueId__r.APTPS_Hours__c,
            Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c,Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length__c,
            Apttus__AgreementId__r.APTPS_Share_Percentage__c,
            Apttus__AgreementId__r.Total_Tax_Amount__c,                    
            Apttus_CMConfig__AttributeValueId__c, Apttus__ListPrice__c,Apttus_CMConfig__AttributeValueId__r.APTPS_Entitlement_Level__c,
            Apttus_CMConfig__AssetLineItemId__c,Apttus_CMConfig__LineType__c,
            Apttus_CMConfig__OptionId__r.Name, Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Manufacturer__c,
            Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Tail_Number__c, 
            Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Serial_Number__c,
            Apttus_CMConfig__AttributeValueId__r.Account__c,Apttus_CMConfig__AttributeValueId__r.APTPS_Agreement__c,Apttus__AgreementId__r.APTPS_Contract_aircraft_type__c,
            Apttus__AgreementId__r.APTPS_Has_Interim_Lease_Option__c
            FROM Apttus__AgreementLineItem__c
            WHERE Apttus__AgreementId__r.Apttus__Account__c =: accId 
            AND Apttus__AgreementId__r.Agreement_Status__c!=:APTS_ConstantUtil.AGREEMENT_CANCELLED
            AND id NOT IN (select Apttus_CMConfig__AgreementLineItemId__c from Apttus_Config2__AssetLineItem__c where Apttus_Config2__AccountId__c =:accId) ]){
                system.debug('Inside SOQL For loop for agreement line items');
                
                if(!agIds.contains(agreementLI.Apttus__AgreementId__c) 
                   && APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(agreementLI.Apttus_CMConfig__LineType__c)
                  &&  !APTS_ConstantUtil.SNL_ENHANCEMENT.equalsIgnoreCase(agreementLI.Apttus__ProductId__r.Name) ) {
                       agIds.add(agreementLI.Apttus__AgreementId__c);
                       count++;
                       APTPS_Consolidate_Letter_Agreement__c conLA =  generateConsolidateLA(null,agreementLI,agreementLI.Apttus__AgreementId__c,accId,count,agreementId);
                       if(conLA.APTPS_Hours__c!=null){
                       	   total_hours = total_hours+ Integer.valueOf(conLA.APTPS_Hours__c);
                       }
                      	if(!agrIdProgramAgrMap.containsKey(agreementLI.Apttus__AgreementId__c)){
                           agrIdProgramAgrMap.put(agreementLI.Apttus__AgreementId__c,conLA.APTPS_Agreement__c);
                       	}
                       if(agreementLI.Apttus__AgreementId__r!=null && (agreementLI.Apttus__AgreementId__r.APTPS_Has_Interim_Lease_Option__c==true)){
                        conLA.APTPS_Has_Interim_Lease_Option__c = true;
                        InterimMap.put(agreementLI.Apttus__AgreementId__c,conLA);
                    	}
                       conLAList.add(conLA);
                   }
                if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(agreementLI.Apttus_CMConfig__LineType__c) 
                   && (APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equalsIgnoreCase(agreementLI.Apttus__ProductId__r.ProductCode) 
                       || APTS_ConstantUtil.SNL_NJE_ENHANCEMENT.equalsIgnoreCase(agreementLI.Apttus__ProductId__r.ProductCode)) 
                   && !enhCodes.contains(agreementLI.Apttus_CMConfig__OptionId__r.ProductCode)){
                       system.debug('Inside Agreement Line Item Enhancements!!');
                       enhCodes.add(agreementLI.Apttus_CMConfig__OptionId__r.ProductCode);
                   }
                
            }//end for loop for agreementLI
        
        system.debug('total_hours ==>'+total_hours);
        
        for(Apttus_Config2__ProductConfiguration__c config: [SELECT Id, Apttus_CMConfig__AgreementId__c 
                                                             FROM Apttus_Config2__ProductConfiguration__c 
                                                             WHERE Apttus_CMConfig__AgreementId__c IN :agIds]) {
                                                                 agConfigMap.put(config.Apttus_CMConfig__AgreementId__c, config.Id);
                                                             }
        
        system.debug('Enhancement Codes --> '+enhCodes);
        //START: Custom Metadata Logic. Note - If required, replace it with Cache Logic in future.
        //Read Custom Metadata and form Product Specific Maps
        Map<String, String> prodFldSetMap = new Map<String, String>();
        Map<String, List<String>> fieldMap = new Map<String, List<String>>();
        Map<String, Map<String, String>> prodApiMAP = new Map<String, Map<String, String>>();
        for(APTPS_Account_Consolidation_Field_Map__mdt fldMap : [SELECT Id, Product_Code__c, Field_Set__c 
                                                                 FROM APTPS_Account_Consolidation_Field_Map__mdt 
                                                                 WHERE Product_Code__c IN :enhCodes]) {
                                                                     prodFldSetMap.put(fldMap.Product_Code__c, fldMap.Field_Set__c);
                                                                 }
        system.debug('prodFldSetMap --> '+prodFldSetMap);
        for(String key : prodFldSetMap.keySet()) {
            List<String> dsFields = new List<String>();
            Map<String, String> apiMap = new Map<String, String>();
            for(Schema.FieldSetMember fld : Schema.SObjectType.Apttus_Config2__ProductAttributeValue__c.fieldSets.getMap().get(prodFldSetMap.get(key)).getFields()) {
                String fldName = 'Apttus_Config2__AttributeValueId__r.'+fld.getFieldPath();
                apiMap.put(fld.getFieldPath(), fld.getLabel());
                if(!dsFields.contains(fldName))
                    dsFields.add(fldName);
            }
            fieldMap.put(key, dsFields);
            prodApiMAP.put(key, apiMap);
        }
        system.debug('fieldMap --> '+fieldMap);
        system.debug('prodApiMAP --> '+prodApiMAP);
        //END: Custom Metadata logic
        
        //Read extra parameters from Custom Settings
        String extraParam = '';
        APTPS_Account_Consolidation_Settings__c consolidationParam = APTPS_Account_Consolidation_Settings__c.getOrgDefaults();
        extraParam = consolidationParam.Additional_Parameters__c != null ? consolidationParam.Additional_Parameters__c : extraParam;
        if(consolidationParam.Additional_Parameters_1__c != null) {
            if(!String.isEmpty(extraParam)) 
                extraParam = extraParam+','+consolidationParam.Additional_Parameters_1__c;
            else 
                extraParam = consolidationParam.Additional_Parameters_1__c;
        }   
        if(consolidationParam.Additional_Parameters_2__c != null) {
            if(!String.isEmpty(extraParam)) 
                extraParam = extraParam+','+consolidationParam.Additional_Parameters_2__c;
            else 
                extraParam = consolidationParam.Additional_Parameters_2__c;
        }
        if(consolidationParam.Additional_Parameters_3__c != null) {
            if(!String.isEmpty(extraParam)) 
                extraParam = extraParam+','+consolidationParam.Additional_Parameters_3__c;
            else 
                extraParam = consolidationParam.Additional_Parameters_3__c;
        }
        if(consolidationParam.Additional_Parameters_4__c != null) {
            if(!String.isEmpty(extraParam)) 
                extraParam = extraParam+','+consolidationParam.Additional_Parameters_4__c;
            else 
                extraParam = consolidationParam.Additional_Parameters_4__c;
        }
        //intInfoList = getConfigDetails(agConfigMap, fieldMap, prodApiMAP, extraParam, assetList, qId, intInfoList);
        List<APTPS_Consolidate_Letter_Agreement__c> conLAEnhcmentList = getEnhancementDetails(agConfigMap, fieldMap, prodApiMAP, extraParam, agrIdProgramAgrMap,accId,agreementId);
        conLAList.addAll(conLAEnhcmentList);
        if(!conLAList.isEmpty()) {
            INSERT conLAList;
        }
        
       
        
        //Update Interim lease records on ConLA
        set<Id> AgreementIds = new set<Id>();
        AgreementIds = InterimMap.keySet();
        system.debug('AgreementIds '+AgreementIds);
        List<APTPS_Consolidate_Letter_Agreement__c> consolidateList = [Select Id,APTPS_zzz_Agreement_Id__c from APTPS_Consolidate_Letter_Agreement__c where APTPS_zzz_Agreement_Id__c IN : InterimMap.keySet()];
        Map<Id,APTPS_Consolidate_Letter_Agreement__c> agrMap = new Map<Id,APTPS_Consolidate_Letter_Agreement__c>(); 
        for(APTPS_Consolidate_Letter_Agreement__c c: consolidateList){
            if(!agrMap.containsKey(c.APTPS_zzz_Agreement_Id__c)){ 
                agrMap.put(c.APTPS_zzz_Agreement_Id__c,c);
            }
        }
        system.debug('agrMap '+agrMap);
        List<APTPS_Consolidate_Letter_Agreement__c> conLAInterimList = new List<APTPS_Consolidate_Letter_Agreement__c>();
        for(Apttus__APTS_Agreement__c agr : [select id,Apttus__Parent_Agreement__c,APTPS_Agreement_Extension__r.APTPS_Tail_Number__c,APTPS_Agreement_Extension__r.APTPS_Manufacturer__c,APTPS_Agreement_Extension__r.APTPS_Serial_Number__c,APTPS_Agreement_Extension__r.APTPS_Cabin_Class_Size__c,
                                             APTPS_Agreement_Extension__r.APTPS_Legal_Name__c,APTPS_Closing_Date__c,APTPS_Agreement_Extension__r.APTPS_Aircraft_Model__c,Current_Date_Doc_Generation__c from Apttus__APTS_Agreement__c where Apttus__Parent_Agreement__c IN : AgreementIds ]){
            
            
           
         APTPS_Consolidate_Letter_Agreement__c conLAInterim = new APTPS_Consolidate_Letter_Agreement__c(Id = agrMap.get(agr.Apttus__Parent_Agreement__c).id,
                                                                                                           APTPS_Tail_Number_Interim__c = agr.APTPS_Agreement_Extension__r.APTPS_Tail_Number__c,
                                                                                                           APTPS_Manufacturer_Interim__c= agr.APTPS_Agreement_Extension__r.APTPS_Manufacturer__c,
                                                                                                           APTPS_Serial_Number_Interim__c=agr.APTPS_Agreement_Extension__r.APTPS_Serial_Number__c,
                                                                                                           APTPS_Cabin_Class_Size_Interim__c=agr.APTPS_Agreement_Extension__r.APTPS_Cabin_Class_Size__c,
                                                                                                          APTPS_Legal_Name_Interim__c=agr.APTPS_Agreement_Extension__r.APTPS_Legal_Name__c,
                                                                                                          APTPS_Closing_Date_Interim__c= agr.APTPS_Closing_Date__c,
                                                                                                        APTPS_CurrentDate_Doc_Generation_Interim__c = agr.Current_Date_Doc_Generation__c,
                                                                                                        APTPS_Aircraft_Model_Interim__c= agr.APTPS_Agreement_Extension__r.APTPS_Aircraft_Model__c);
            conLAInterimList.add(conLAInterim);
        }
        
        
        
        
        
        if(!conLAInterimList.isEmpty()) {
            UPDATE conLAInterimList;
        }
        
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(
            Id = agreementId, APTPS_Total_Hours__c = total_hours,APTPS_Count_Program_Agreement__c=count,APTPS_Count_Extra_Hours__c=countExtraHours,
            APTPS_Check_HEPricing__c = checkHEPricing,Chevron_Status__c = APTS_ConstantUtil.LACONSOLIDATIONCOMPLETED
        );
        update agreement;
		
		
        
        
    }
    
    public static List<APTPS_Consolidate_Letter_Agreement__c> getEnhancementDetails(Map<Id, Id> configMap, Map<String, List<String>> fieldMap, Map<String, Map<String, String>> prodApiMAP, String extraParam,Map<Id,String> agrIdProgramAgrMap,Id accId,string commAgreement){
        System.debug('configMap-->'+configMap);
        system.debug('fieldMap-->'+fieldMap);
        System.debug('prodApiMAP-->'+prodApiMAP);
        System.debug('extraParam-->'+extraParam);
        String query = 'SELECT Id';
        //Prepare Attribute List
        List<String> dsFields = new List<String>();
        List<Id> configIds = configMap != null ? configMap.values() : null; 
        List<Apttus_Config2__LineItem__c> lineItems = new List<Apttus_Config2__LineItem__c>();
        String fieldSet = '';
        Map<String, List<Apttus_Config2__LineItem__c>> enhancemetLIMap = new Map<String, List<Apttus_Config2__LineItem__c>>();
        Map<String, String> apiMap = new Map<String, String>();
        String family = APTS_ConstantUtil.OPTION;
        List<String> enhCodes = new List<String>{APTS_ConstantUtil.SNL_NJA_ENHANCEMENT, APTS_ConstantUtil.SNL_NJE_ENHANCEMENT};
            //Form List of all Unique attributes
            for(String key : fieldMap.keySet()) {
                List<String> fields = fieldMap.get(key);
                for(String fld : fields) {
                    if(!dsFields.contains(fld))
                        dsFields.add(fld);
                }
            }
        system.debug('dsFields --> '+dsFields);
        String enhFields = dsFields != null ? String.join(dsFields,',') : null;
        query = !String.isEmpty(extraParam) ? query+','+extraParam : query;
        query = enhFields != null ? query+','+enhFields : query;
        //TODO: Temp fix, find better way to remove extra ',' if there is any
        query = query.removeEnd(',');
        query = query+' FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c IN :configIds AND Apttus_Config2__IsPrimaryLine__c = true AND Apttus_Config2__LineType__c =: family AND Apttus_Config2__ProductId__r.ProductCode IN :enhCodes';
        system.debug('Dynamic SOQL Query --> '+query);
        lineItems = Database.query(query);
        system.debug('Line Items result, list size --> '+lineItems.size()+' Actual records --> '+lineItems);
        
        //Iterate through resultset
        for(Apttus_Config2__LineItem__c li : lineItems) {
            //Check for Enhancement and prepare map of Enhancements & Respective attribute Id/record
            if((APTS_ConstantUtil.SNL_NJA_ENHANCEMENT.equalsIgnoreCase(li.Apttus_Config2__ProductId__r.ProductCode) || APTS_ConstantUtil.SNL_NJE_ENHANCEMENT.equalsIgnoreCase(li.Apttus_Config2__ProductId__r.ProductCode)) 
               && APTS_ConstantUtil.OPTION.equalsIgnoreCase(li.Apttus_Config2__LineType__c)) {
                   //Build Map of Enhancement and Line Items. Later use it to compare attributes
                   String prodCode = li.Apttus_Config2__OptionId__r.ProductCode;
                   if(enhancemetLIMap.containsKey(prodCode)) {
                       List<Apttus_Config2__LineItem__c> liList = enhancemetLIMap.get(prodCode);
                       liList.add(li);
                       system.debug('Updated List for Product --> '+prodCode+' is --> '+liList);
                       enhancemetLIMap.put(prodCode, liList);
                   } else {
                       enhancemetLIMap.put(prodCode, new List<Apttus_Config2__LineItem__c>{li});
                   }
               }
        }
        
        system.debug('Enhancements and respective line item map --> '+enhancemetLIMap);
        //used for attribute comparison
        List<APTPS_Consolidate_Letter_Agreement__c> tempConLA = new List<APTPS_Consolidate_Letter_Agreement__c>(); 
        Integer numberCount;
        for(String prodCode : enhancemetLIMap.keySet()) {
            system.debug('Comparing attributes for Product --> '+prodCode);
            boolean equal = true;
            integer counter=0;
            List<Apttus_Config2__LineItem__c> relatedLineItems = enhancemetLIMap.get(prodCode);
            if(relatedLineItems.size() > 1)
            {
                //compare
                Map<string,Apttus_Config2__LineItem__c> keyAttributesMap =  new Map<string,Apttus_Config2__LineItem__c>();
                for(Apttus_Config2__LineItem__c li : enhancemetLIMap.get(prodCode)) {
                    system.debug('=========START==========');
                    //Read all other attributes : Dynamic Way
                    apiMap = (prodApiMAP != null && prodApiMAP.containsKey(prodCode)) ? prodApiMAP.get(prodCode) : null;                   
                    apiMap =  addEntitlementLevel(apiMap);
                    system.debug('apiMap==>'+apiMap);
                    system.debug('agreement '+li.Apttus_Config2__ConfigurationId__r.Apttus_CMConfig__AgreementId__c);
                    system.debug('display APTPS_Selected_Agreements__c==>'+li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreements__c);
                    if(apiMap != null) {
                        List<string> values = new List<string>();
                        for(String api : apiMap.keySet()) {                            
                            Object obj = li.getSObject('Apttus_Config2__AttributeValueId__r').get(api);
                            values.add(String.valueOf(obj));
                            system.debug(apiMap.get(api)+' : '+obj);
                         
                        }//end loop each api for attributes of enhancement type
                        String key = String.join(values,';');
                        system.debug('key==>'+key);
                        Id agrId = li.Apttus_Config2__ConfigurationId__r.Apttus_CMConfig__AgreementId__c;
                        if(!keyAttributesMap.containsKey(key)){
                            li.APTPS_Program_Agreement__c = agrIdProgramAgrMap.get(agrId);
                            keyAttributesMap.put(key,li);
                        }
                        else{
                            system.debug('liExisting Program Agr==>'+keyAttributesMap.get(key).APTPS_Program_Agreement__c);
                            keyAttributesMap.get(key).APTPS_Program_Agreement__c = keyAttributesMap.get(key).APTPS_Program_Agreement__c+','+agrIdProgramAgrMap.get(agrId);
                            system.debug('liadded pr agr ==>'+keyAttributesMap.get(key).APTPS_Program_Agreement__c);
                        }
                       				
                    }
                    
                    system.debug('=========END==========');
                }// end loop each li for enhancement type
                numberCount = 0;
                for(Apttus_Config2__LineItem__c lItem: keyAttributesMap.values()){
                    APTPS_Consolidate_Letter_Agreement__c conLAComp = new APTPS_Consolidate_Letter_Agreement__c();
                    conLAComp.APTPS_Product_Attribute_Value__c = lItem.Apttus_Config2__AttributeValueId__c;
                    conLAComp.APTPS_Enhancement_Upgrade_Type__c = lItem.Apttus_Config2__Description__c;
                    if(APTS_ConstantUtil.HE_PRICING.equalsIgnoreCase(lItem.Apttus_Config2__Description__c)){ 
                    checkHEPricing = true;
                    }
                    numberCount++;
                    countExtraHours++;
                    conLAComp.APTPS_EnhancementTypeCount__c =numberCount;
                    conLAComp.APTPS_Agreement__c = lItem.APTPS_Program_Agreement__c;
                    conLAComp.APTPS_AccountId__c=  accId;
                    conLAComp.APTPS_Agreement_Number__c=  commAgreement;
                    conLAComp.APTPS_Product_Name__c = APTS_ConstantUtil.SNL_ENHANCEMENT;
                    tempConLA.add(conLAComp);
                }
                
            }
            else
            {//If not multple occurences of enhancement type
                APTPS_Consolidate_Letter_Agreement__c conLA = new APTPS_Consolidate_Letter_Agreement__c();
                conLA.APTPS_Product_Attribute_Value__c = relatedLineItems[0].Apttus_Config2__AttributeValueId__c;
                conLA.APTPS_Enhancement_Upgrade_Type__c = relatedLineItems[0].Apttus_Config2__Description__c;
                if(APTS_ConstantUtil.HE_PRICING.equalsIgnoreCase(relatedLineItems[0].Apttus_Config2__Description__c)){ 
                    checkHEPricing = true;
                }
                conLA.APTPS_EnhancementTypeCount__c =1;
                countExtraHours = 1;
                Id agrId = relatedLineItems[0].Apttus_Config2__ConfigurationId__r.Apttus_CMConfig__AgreementId__c;
                conLA.APTPS_Agreement__c = agrIdProgramAgrMap.get(agrId);
                conLA.APTPS_AccountId__c =  accId;
                conLA.APTPS_Agreement_Number__c = commAgreement;
                conLA.APTPS_Product_Name__c = APTS_ConstantUtil.SNL_ENHANCEMENT;
                system.debug('conLA Enhancement single-->'+conLA);
                tempConLA.add(conLA);
            }
            
        }//end each prodCode
        return tempConLA;
    }
	
    public static Map<String,String> addEntitlementLevel(Map<String,String> apiMap){
        apiMap.put('APTPS_Entitlement_Level__c','Entitlement Level');
        apiMap.put('Account__c','Account Name');
        apiMap.put('APTPS_Agreement__c','Share or Lease agreement');
        apiMap.put('APTPS_Selected_Agreements__c','Selected Agreements');
        apiMap.put('APTPS_Override_Agreement_Selection__c','Override Agreement Selection');
        apiMap.put('APTPS_Quote_Proposal__c','Quote/Proposal');
        apiMap.put('APTPS_Selected_Quotes__c','Selected Quotes');
        apiMap.put('APTPS_Override_Quote_Proposal_Selection__c','Override Quote Selection');
        return apiMap;
    }   
    
    
}