public class APTPS_Agreement_TriggerHandler_NV extends TriggerHandler{
    
    public override void afterInsert(){
        List<Apttus__APTS_Agreement__c> allAgList = Trigger.new;
    Map<Id, Apttus__APTS_Agreement__c> newMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.NewMap;
        Map<Id, Apttus__APTS_Agreement__c> proposalIdAndAgreementMap = 
            new Map<Id, Apttus__APTS_Agreement__c>();

        for (Apttus__APTS_Agreement__c ag : allAgList) {
            if (ag.Apttus_QPComply__RelatedProposalId__c != null) {
                proposalIdAndAgreementMap.put(ag.Apttus_QPComply__RelatedProposalId__c, ag);
            }           
        }

        if (!proposalIdAndAgreementMap.isEmpty()) {
            populateAgreementOnLAConsolidateRecords(proposalIdAndAgreementMap);
        }
    }

    public static void populateAgreementOnLAConsolidateRecords(
        Map<Id, Apttus__APTS_Agreement__c> proposalIdAndAgreementMap
    ) {
        List<APTPS_Consolidate_Letter_Agreement__c> updateLAConsolidateRecords = 
            new List<APTPS_Consolidate_Letter_Agreement__c>();
        
        for (APTPS_Consolidate_Letter_Agreement__c lac : [
            SELECT Id,
                APTPS_Proposal__c
            FROM APTPS_Consolidate_Letter_Agreement__c
            WHERE APTPS_Proposal__c IN :proposalIdAndAgreementMap.keySet()            
        ]) {
            lac.APTPS_Agreement_Number__c = proposalIdAndAgreementMap.get(lac.APTPS_Proposal__c).Id;
            updateLAConsolidateRecords.add(lac);
        }

        if (!updateLAConsolidateRecords.isEmpty()) {
            update updateLAConsolidateRecords;
        }
    }
    
    public override void beforeDelete(){
        List<Apttus__APTS_Agreement__c> newList = Trigger.new;
        Map<Id, Apttus__APTS_Agreement__c> oldMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.oldMap;
        Set<Id> deletedAgmts = new Set<Id>();
        
        for (Apttus__APTS_Agreement__c objAgreement : newList) {
            if (objAgreement.Apttus_QPComply__RelatedProposalId__c != null) {
                deletedAgmts.add(objAgreement.Id);
            }           
        }

        if (!deletedAgmts.isEmpty()) {
            deleteLAConsolidateRecords(deletedAgmts);
        }
    }
    public override void beforeUpdate(){
        List<Apttus__APTS_Agreement__c> newList = Trigger.new;
        Map<Id, Apttus__APTS_Agreement__c> oldMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.oldMap;
        Set<Id> deletedAgmts = new Set<Id>();
        
        for (Apttus__APTS_Agreement__c objAgreement : newList) {
            if(objAgreement.Apttus__Status_Category__c != oldMap.get(objAgreement.Id).Apttus__Status_Category__c && 
               objAgreement.Apttus__Status__c != oldMap.get(objAgreement.Id).Apttus__Status__c &&
               ( (objAgreement.Apttus__Status_Category__c.equalsIgnoreCase('Cancelled') && objAgreement.Apttus__Status__c.equalsIgnoreCase('Cancelled Request'))
               || objAgreement.Apttus__Status_Category__c.equalsIgnoreCase('Terminated') && objAgreement.Apttus__Status__c.equalsIgnoreCase('Terminated'))) {
                deletedAgmts.add(objAgreement.Id);
            }           
        }

        if (!deletedAgmts.isEmpty()) {
            deleteLAConsolidateRecords(deletedAgmts);
        }
    }
    public static void deleteLAConsolidateRecords(Set<Id> deletedAgmts) {
        List<APTPS_Consolidate_Letter_Agreement__c> todeleteLAConsolidateRecords = new List<APTPS_Consolidate_Letter_Agreement__c>();
        
        for (APTPS_Consolidate_Letter_Agreement__c lac : [
            SELECT Id,
                APTPS_Proposal__c
            FROM APTPS_Consolidate_Letter_Agreement__c
            WHERE APTPS_Agreement_Number__c IN :deletedAgmts
        ]) {
            todeleteLAConsolidateRecords.add(lac);
        }

        if (!todeleteLAConsolidateRecords.isEmpty()) {
            delete todeleteLAConsolidateRecords;
        }
    }
}