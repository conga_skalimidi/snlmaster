/************************************************************************************************************************
@Name: APTPS_DealSummary
@Author: Conga PS Dev Team
@CreateDate: 17 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_DealSummary implements APTPS_DealSummaryCallable {
    private static String dsMetadataName = 'APTPS_DS_Registry__mdt';
    public static Map<String, String> prodCodeClassNameMap = new Map<String, String>();
    public static Map<String, String> prodCodeFieldsMap = new Map<String, String>();
    //TODO: For now, as there is only one field-use a constant. If in future there are more such fields then use List.
    private static String serviceAcName = 'Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__r.Name';
    
    public APTPS_DealSummary() {
        setProdCodeClassNameMap();
        setDealSummaryFieldMap();
    }
    
    /** 
    @description: Find out the APEX class implementation for the given productCode
    @param: Product Code
    @return: APEX Class name
    */
    private String findDS_Implementation(String productCode) {
        String className = null;
        if(productCode != null && !prodCodeClassNameMap.isEmpty() 
           && prodCodeClassNameMap.containsKey(productCode)) {
               className = prodCodeClassNameMap.get(productCode);
           }
        system.debug('APEX Class Details --> '+className);
        return className;
    }
    
    /** 
    @description: Execute product specific Deal Summary logic.
    @param: Product code and arguments
    @return: 
    */
    public Object call(Map<Id, String> quoteProductMap, Map<Id, Map<String, Object>> quoteArgMap) {
        String retMsg = 'SUCCESS';
        Map<Id, String> quoteClassImplMap = new Map<Id, String>();
        List<String> dsFields = new List<String>();
        Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> quotePLIMap = new Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>>();
        system.debug('quoteProductMap --> '+quoteProductMap);
        system.debug('quoteArgMap --> '+quoteArgMap);
        
        for(Id qId : quoteProductMap.keySet()){
            String productCode = quoteProductMap.get(qId);
            if(productCode != null) {
                String className = findDS_Implementation(productCode);
                quoteClassImplMap.put(qId, className);
            }
            
            //START: Dynamic SOQL for All SNL products
            if(!prodCodeFieldsMap.isEmpty() && prodCodeFieldsMap.containsKey(productCode) && prodCodeFieldsMap.get(productCode) != null) {
                String fieldSetName = prodCodeFieldsMap.get(productCode);
                for(Schema.FieldSetMember fld : Schema.SObjectType.Apttus_QPConfig__ProposalProductAttributeValue__c.fieldSets.getMap().get(fieldSetName).getFields()) {
                    String fldName = 'Apttus_QPConfig__AttributeValueId__r.'+fld.getFieldPath();
                    if(!dsFields.contains(fldName))
                    	dsFields.add(fldName);
                }
                
                //In case of Share product, add Service Account Name field explicitly. It is not possible to get it through field set. It's Salesforce limitation.
                if(productCode.equalsIgnoreCase(APTS_ConstantUtil.NJA_SHARE_CODE) || 
                productCode.equalsIgnoreCase(APTS_ConstantUtil.NJE_SHARE_CODE)) {
                    dsFields.add(serviceAcName);
                }
            }
            //END
        }
        system.debug('List of fields to retrieve from Proposal Product Attribute Value --> '+dsFields);
        system.debug('quoteClassImplMap --> '+quoteClassImplMap);
        
        //START: Get Proposal Line Item details
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItems = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Set<Id> quoteIds = quoteProductMap.keySet();
        if(!quoteIds.isEmpty()) {
            String query = 'SELECT Id, Apttus_QPConfig__LineStatus__c, Apttus_QPConfig__OptionId__r.Name, Apttus_QPConfig__OptionId__r.ProductCode, Apttus_QPConfig__LineType__c, Apttus_QPConfig__NetPrice__c, Product_Family__c, Apttus_QPConfig__IsPrimaryLine__c, Apttus_QPConfig__ChargeType__c, Apttus_Proposal__Proposal__c, Apttus_Proposal__Proposal__r.Apttus_Proposal__Account__r.Name, Apttus_Proposal__Proposal__r.APTPS_Assignor_Agreement__c, Apttus_QPConfig__LocationId__r.Name,'+String.join(dsFields,',')+' FROM Apttus_Proposal__Proposal_Line_Item__c WHERE Apttus_Proposal__Proposal__c IN :quoteIds';
            system.debug('SNL deal summary attributes query --> '+query);
            proposalLineItems = Database.query(query);
        }
        
        //Generate a Map of Quote ID and related Proposal Line Items
        for(Apttus_Proposal__Proposal_Line_Item__c pli : proposalLineItems) {
            if(!quotePLIMap.isEmpty() && quotePLIMap.containsKey(pli.Apttus_Proposal__Proposal__c))
                quotePLIMap.get(pli.Apttus_Proposal__Proposal__c).add(pli);
            else
                quotePLIMap.put(pli.Apttus_Proposal__Proposal__c, new List<Apttus_Proposal__Proposal_Line_Item__c>{pli});
        }
        system.debug('Quote-Proposal Line Item Map --> '+quotePLIMap);
        //END
        
        List<Apttus_Proposal__Proposal__c> quoteList = new List<Apttus_Proposal__Proposal__c>();
        for(Id qId : quoteClassImplMap.keySet()) {
            String className = quoteClassImplMap.get(qId);
            List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines = !quotePLIMap.isEmpty() && quotePLIMap.containsKey(qId) ? quotePLIMap.get(qId) : null;
            if(className != null) {
                if(quoteArgMap == null || quoteArgMap.isEmpty()) 
                    throw new ExtensionMalformedCallException('Quote/Proposal argument map is empty');
                
                Map<String, Object> args = quoteArgMap.get(qId);
                if(args == null || args.get('qId') == null) 
                    throw new ExtensionMalformedCallException('Quote/Proposal Id is missing.');
                APTPS_DealSummaryInterface dsObj = (APTPS_DealSummaryInterface)Type.forName(className).newInstance();
                Apttus_Proposal__Proposal__c qObj = dsObj.updateSummary(args, quoteLines);
                if(qObj != null)
                    quoteList.add(qObj);
            }       
        }
        
        system.debug('Proposal list size --> '+quoteList.size());
        try {
            if(!quoteList.isEmpty())
                update quoteList; 
        }catch(Exception e) {
            retMsg = 'ERROR';
            system.debug('Error while updating deal summary --> '+e.getMessage());
        }
        
        return retMsg;
    }
    
    /** 
    @description: Custom Exception implementation
    @param:
    @return: 
    */
    public class ExtensionMalformedCallException extends Exception {
        public String errMsg = '';
        public void ExtensionMalformedCallException(String errMsg) {
            this.errMsg = errMsg;
        }
    }
    
    /** 
    @description: Read Deal Summary Registry metadata and prepare the Map
    @param:
    @return: 
    */
    private void setProdCodeClassNameMap() {
        List<APTPS_DS_Registry__mdt> dealSummaryHandlers = [SELECT Product_Code__c, Class_Name__c 
                                                            FROM APTPS_DS_Registry__mdt];
        for(APTPS_DS_Registry__mdt handler : dealSummaryHandlers) {
            if(handler.Product_Code__c != null && handler.Class_Name__c != null) 
                prodCodeClassNameMap.put(handler.Product_Code__c, handler.Class_Name__c);
        }
        system.debug('prodCodeClassNameMap --> '+prodCodeClassNameMap);
    }
    
    /** 
    @description: Read Deal Summary Field metadata and prepare the Map
    @param:
    @return: 
    */
    private void setDealSummaryFieldMap() {
        List<APTPS_Deal_Summary_Field_Registry__mdt> dealSummaryFields = [SELECT Product_Code__c, Field_Set__c 
                                                                          FROM APTPS_Deal_Summary_Field_Registry__mdt];
        for(APTPS_Deal_Summary_Field_Registry__mdt dsField : dealSummaryFields) {
            if(dsField.Product_Code__c != null && dsField.Field_Set__c != null) 
                prodCodeFieldsMap.put(dsField.Product_Code__c, dsField.Field_Set__c);
        }
        system.debug('prodCodeFieldsMap --> '+prodCodeFieldsMap);
    }
    
}