public with sharing class APTS_AssetLineItemBaseTriggerHelper {    
    
    public static void updateEHCOnParentAssetPAV(List<Apttus_Config2__AssetLineItem__c> assetLIInScope)
    {
        system.debug('>>>>>>>>>>updateEHCOnParentAsset>>>>>> ');
        List<Apttus_Config2__AssetAttributeValue__c> UpdateAssetPAVList = new List<Apttus_Config2__AssetAttributeValue__c>();
        String currentAgreementId = '';
        Decimal dExpiredHrs = 0.0;


        
        if(currentAgreementId =='') {
            currentAgreementId = assetLIInScope[0].Apttus_CMConfig__AgreementId__c;
        }
         
        List<Id> assetALIIds = new List<Id>();
        for (Apttus_Config2__AssetLineItem__c assetALIObj : assetLIInScope) {
            assetALIIds.add(assetALIObj.Id);
        }

        system.debug('>>>>>>>>>>currentAgreementId>>>>>> '+ currentAgreementId);

        List<Apttus_Config2__AssetLineItem__c> aliEHCList = [ Select Id,  
                Apttus_Config2__AttributeValueId__c,
                Apttus_Config2__ChargeType__c,
                Apttus_CMConfig__AgreementId__c
                From Apttus_Config2__AssetLineItem__c
                // Where Id IN :assetALIIds AND  Apttus_Config2__ChargeType__c = 'Expired Hours Value' AND Apttus_Config2__AttributeValueId__r.APTPS_Is_Expired_Hours_Reduced__c =:false];
                Where Apttus_CMConfig__AgreementId__c =:currentAgreementId AND Apttus_Config2__ChargeType__c = 'Expired Hours Value' AND Apttus_Config2__LineType__c = 'Option'];

        system.debug('>>>>>>>>>>aliEHCList>>>>>> '+ aliEHCList);

        List<Id> assetPavds = new List<Id>();
        for (Apttus_Config2__AssetLineItem__c aliEHC : aliEHCList) {
            assetPavds.add(aliEHC.Apttus_Config2__AttributeValueId__c);
        }
        system.debug('>>>>>>>>>>assetPavds>>>>>> '+ assetPavds);

        List<Apttus_Config2__AssetAttributeValue__c> ehcAPAVList = [ Select Id,  
        APTPS_Is_Expired_Hours_Reduced__c,
        APTS_Aircraft_Expired_Hours__c ,
        APTS_Overridden_Hours_To_Unexpire__c,
        APTPS_Stop_Integration_Updates__c,
        APTS_Hours_To_Unexpire__c ,
        APTS_Override_Hours_To_Unexpire__c 
        From Apttus_Config2__AssetAttributeValue__c
        Where Id IN :assetPavds AND APTPS_Is_Expired_Hours_Reduced__c =: false];


        system.debug('>>>>>>>>>>ehcAPAVList>>>>>> ' + ehcAPAVList);

        for (Apttus_Config2__AssetAttributeValue__c aPAV : ehcAPAVList){

            system.debug('>>>>>>>>>>aPAV - into for loop >>>>>> ' + aPAV);
            system.debug('>>>>>>>>>>aPAV.APTPS_Is_Expired_Hours_Reduced__c>>>>>> ' + aPAV.APTPS_Is_Expired_Hours_Reduced__c);
            system.debug('>>>>>>>>>>aPAV.APTS_Aircraft_Expired_Hours__c>>>>>> ' + aPAV.APTS_Aircraft_Expired_Hours__c);
            system.debug('>>>>>>>>>>aPAV.APTS_Overridden_Hours_To_Unexpire__c>>>>>> ' + aPAV.APTS_Overridden_Hours_To_Unexpire__c);
            system.debug('>>>>>>>>>>aPAV.APTPS_Stop_Integration_Updates__c>>>>>> ' + aPAV.APTPS_Stop_Integration_Updates__c);
            system.debug('>>>>>>>>>> - Into IF cond - APTPS_Stop_Integration_Updates__c >>>>>> ');
                Apttus_Config2__AssetAttributeValue__c assetPAV = new Apttus_Config2__AssetAttributeValue__c(id = aPAV.Id);
                
                //Calculate UnExpired Hours
                double unExpiredHours = aPAV.APTS_Hours_To_Unexpire__c;
                if(aPAV.APTS_Override_Hours_To_Unexpire__c && aPAV.APTS_Overridden_Hours_To_Unexpire__c != null && aPAV.APTS_Overridden_Hours_To_Unexpire__c != 0) {
                    unExpiredHours = aPAV.APTS_Overridden_Hours_To_Unexpire__c;
                }
                
                dExpiredHrs = aPAV.APTS_Aircraft_Expired_Hours__c - unExpiredHours;
                
                
                system.debug('>>>>>>>>>> - dExpiredHrs >>>>>> '+dExpiredHrs);
                assetPAV.APTS_Aircraft_Expired_Hours__c = dExpiredHrs;
                assetPAV.APTPS_Is_Expired_Hours_Reduced__c = true;
                assetPAV.APTPS_Stop_Integration_Updates__c = true;
                UpdateAssetPAVList.add(assetPAV);     
                
                system.debug('>>>>>>>>>> - After for loop >>>>>> ' + UpdateAssetPAVList);

                try{

                    if(UpdateAssetPAVList.size()>0)
                    {
                        update UpdateAssetPAVList;
                    }
                } catch(Exception e){
                            System.debug('[APTS_AssetLineItemBaseTriggerHelper].updateEHCOnParentAssetPAV Message: ' + e.getMessage());
                            System.debug('[APTS_AssetLineItemBaseTriggerHelper].updateEHCOnParentAssetPAV StackTrace: ' + e.getStackTraceString());
                            //throw e; 
                }

            }

    }
	//START: START DATE, END DATE for MODIFICATIONS : Updating start and end date fields
     public static void updateStartEndDateFields(Map<Id,Apttus_Config2__AssetLineItem__c> assetLineItems) 
    {
        system.debug('assetLineItems.values()-->'+assetLineItems.values());
        for(Apttus_Config2__AssetLineItem__c ali : [SELECT Id,Apttus_Config2__EndDate__c,Apttus_Config2__StartDate__c,
                                                    Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c,
                                                    Apttus_CMConfig__AgreementId__r.Apttus__Contract_End_Date__c
                                                    FROM Apttus_Config2__AssetLineItem__c
                                                    WHERE Apttus_Config2__AssetLineItem__c.id IN :assetLineItems.values()])
        {
            Apttus_Config2__AssetLineItem__c updateAliObj = assetLineItems.get(ali.Id);
            if(ali.Apttus_CMConfig__AgreementId__c != NULL){
                updateAliObj.Apttus_Config2__StartDate__c = ali.Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c;
                updateAliObj.Apttus_Config2__EndDate__c = ali.Apttus_CMConfig__AgreementId__r.Apttus__Contract_End_Date__c;
            }
        }
 
    }
    //END: START DATE, END DATE for MODIFICATIONS : Updating start and end date fields
}