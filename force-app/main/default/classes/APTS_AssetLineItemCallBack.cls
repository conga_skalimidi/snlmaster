/*
 *************************************************************************************
 Class Name     :  APTS_AssetLineItemCallBack

 **************************************************************************************
 */
global with sharing class APTS_AssetLineItemCallBack implements Apttus_Config2.CustomClass.IAssetLineItemCallback3{
    private String assetSearchFilter = null;
    private List<String> assetSearchScope = null;
    private Apttus_Config2__ProductConfiguration__c productConfiguration;
    private string ModificationType = '';
    private string AdditionalModificationType = '';
    private string currencyType = '';
    private string programType = '';
    /**
     * Callback at the beginning of the asset selection call.
     * Use the start method to initialize state
     * @param cart the cart object or null if there is no cart context
     * @param assetSearchFilter the preset static filter used in the asset search or null if there is no preset filter
     * @param assetSearchScope the list of asset fields to match the search text or empty to use the default list of fields
     */
    public void start(Apttus_Config2.ProductConfiguration cart, String assetSearchFilter, List<String> assetSearchScope){

        System.debug('***AssetLineItemCB: start()-1: ' + assetSearchScope);
        System.debug('***AssetLineItemCB: start()-2: ' + cart);

        Set<ID> assetIDs = new Set<ID>();

        this.assetSearchFilter = assetSearchFilter;
        this.assetSearchScope = assetSearchScope;

        this.productConfiguration = cart.getConfigSO();
    }

    /******************************************************************************
     * Gets the asset search scope
     * @return the asset search scope or null to use the default asset search scope
     ******************************************************************************* */
    public List<String> getAssetSearchScope(){
        System.debug('***AssetLineItemCB: getAssetSearchScope()-returning: ' + this.assetSearchScope);
        return this.assetSearchScope;
    }

    /*******************************************************************************
     * Callback to return the filter expression for the asset query where clause
     * This filter is used in listing installed products
     * @param params the parameters for the method
     * @return the filter expression or null to use the default filter.
     * e.g. Name LIKE 'A%' AND Quantity__c > 100
     *         Id IN ('000123', '000124')
     ***************************************************************************** */
    public String getFilterExpr(Apttus_Config2.CustomClass.ActionParams params){

        System.debug('***AssetLineItemCB: getFilterExpr()-params: ' + params);
        System.debug('***AssetLineItemCB: productConfiguration: ' + this.productConfiguration);
        Id AccountId = params.AccountId;

        String whereClause = 'Apttus_Config2__AccountId__c = \'' + AccountId + '\'';
        whereClause = whereClause + ' AND ';

        whereClause = whereClause + '(Apttus_Config2__BundleAssetId__r.Apttus_Config2__AssetStatus__c!= \'Cancelled\')';
        if (productConfiguration != null){
            ModificationType = productConfiguration.APTS_Modification_Type__c != null ? productConfiguration.APTS_Modification_Type__c : '';
            programType = productConfiguration.APTPS_Program_Type__c != null ? productConfiguration.APTPS_Program_Type__c : '';
            AdditionalModificationType = productConfiguration.APTS_Additional_Modification_Type__c != null ? productConfiguration.APTS_Additional_Modification_Type__c : '';
            currencyType = productConfiguration.CurrencyIsoCode;
            System.debug('***productConfiguration: ' + productConfiguration);

            //currencyType = productConfiguration.Apttus_QPConfig__Proposald__r.CurrencyIsoCode;
            //System.debug('***productConfiguration.Apttus_QPConfig__Proposald__r.CurrencyIsoCode-->' + productConfiguration.Apttus_QPConfig__Proposald__r.CurrencyIsoCode);

            System.debug('***AssetLineItemCB: ModificationType' + ModificationType);
            //System.debug('***AssetLineItemCB: AdditionalModificationType' + AdditionalModificationType);
            whereClause = whereClause + ' AND CurrencyIsoCode = \'' + currencyType + '\' AND ';
            //whereClause = whereClause + ' AND ';
            if (ModificationType != null){
                if(APTPS_SNL_Util.isSNLProgramType(programType)) {
                    whereClause = whereClause + APTPS_SNL_Util.getAssetsWhereClause(programType, ModificationType, currencyType);
                } else {
                        if (ModificationType == APTS_ConstantUtil.EXPIRED_HOURS_CARD){
                        whereClause = whereClause + '(Apttus_Config2__BundleAssetId__r.APTS_Is_Expired__c = true AND    (Apttus_Config2__BundleAssetId__r.Agreement_Status__c =\'Inactive\' OR Apttus_Config2__BundleAssetId__r.APTS_Original_Agreement__r.Agreement_Status__c =\'Inactive\'))';

                    } else if (ModificationType == APTS_ConstantUtil.ASSIGNMENT && AdditionalModificationType != null && AdditionalModificationType == APTS_ConstantUtil.CROSS_ACCOUNT_ASSIGNMENT){
                        whereClause = whereClause + '(Apttus_Config2__BundleAssetId__r.Agreement_Status__c = \'Active\' AND Apttus_Config2__BundleAssetId__r.APTS_Is_Expired__c = false AND NJ_Is_CAAssignment_Pending__c = true)';

                    } else{
                        whereClause = whereClause + '(Apttus_Config2__BundleAssetId__r.Agreement_Status__c = \'Active\' AND Apttus_Config2__BundleAssetId__r.APTS_Is_Expired__c = false) AND Apttus_Config2__BundleAssetId__r.Apttus_Config2__ProductId__r.ProductCode IN (\'NJUS_CARD\',\'NJE_CARD\')';

                    }
                }
                

            }

        }
        System.debug('***AssetLineItemCB: getFilterExpr()-whereClause' + whereClause);

        return whereClause;
    }

    /*******************************************************************************
     * Callback to return part of SOQL filter clause
     * This filter is used in listing installed products
     * @param accountId is the context account id
     * @return The query filter is like the following.
     *         Name LIKE 'A%' AND Quantity__c > 100
     *         Id IN ('000123', '000124')
     ***************************************************************************** */
    public String getQueryFilter(ID accountId){
        System.debug('***AssetLineItemCB: getQueryFilter()-accountId: ' + accountId);
        return '';
    }

    global Boolean validateAssetTermination(Set<ID> assetIds, Set<ID> accountIds, Date eDate){
        return true;
    }

    /*******************************************************************************
     * Callback after the filter is used
     * Use the finish method to release state
     ***************************************************************************** */
    public void finish(){
        System.debug('***AssetLineItemCB: finish()');
    }
}