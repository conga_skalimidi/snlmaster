/************************************************************************************************************************
@Name: APTPS_Enhancement_Approvals
@Author: Conga PS Dev Team
@CreateDate: 20 January 2022
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_Enhancement_Approvals {    
     // START: Read Enhancement Approvals details from Custom settings 
    public static APTPS_Enhancement_Approvals__c approvalLimits = APTPS_Enhancement_Approvals__c.getOrgDefaults();
    public static Decimal DDC_WIF_Limit = approvalLimits.APTPS_DDC_WIF_Limit__c != null ? approvalLimits.APTPS_DDC_WIF_Limit__c : 0.0;
    public static Decimal Sales_Director_WIF_Limit = approvalLimits.APTPS_Sales_Director_WIF_Limit__c   != null ? approvalLimits.APTPS_Sales_Director_WIF_Limit__c  : 0.0;
    public static Decimal SalesOps_WIF_Limit = approvalLimits.APTPS_SalesOps_WIF_Limit__c != null ? approvalLimits.APTPS_SalesOps_WIF_Limit__c : 0.0;
    public static Decimal Hours_Factor = approvalLimits.APTPS_Hours_Factor__c != null ? approvalLimits.APTPS_Hours_Factor__c : 0.0;   
    public static Map<String,APTPS_Additional_Hours_Text__mdt> sp = new Map<String,APTPS_Additional_Hours_Text__mdt>();
    // END: Read Enhancement Approvals details from Custom settings 
    
    // START: Approval Valriables assignement
    public static id quoteConfigId;
    public static Map<String, List<String>> fieldMap = new Map<String, List<String>>();
    public static String extraParam;
    public static Map<String, Integer> enhAggHrsMap = new Map<String, Integer>();
    public static Map<String,decimal> enhCRMap = new Map<String,decimal>();
    public static Apttus_Proposal__Proposal__c proposalObj; 
    public static List<Apttus_Config2__LineItem__c> lineItems = new List<Apttus_Config2__LineItem__c>();
    // END: Approval Valriables assignement 
    
   /** 
    @description: updateEnhancementApprovals - update Proposal Approval flags
    @param: quoteConfigId - Quote Proposal Config Id 
    @param: fieldMap - Product Code and related Attribute API Map
    @param: extraParam - Extra Parameters to be queried
    @param: enhAggHrsMap - enhancement Aggregate hours Map
    @param: proposalObj - Proposal data
    @return: Proposal Object
    */
    public static Apttus_Proposal__Proposal__c updateEnhancementApprovals(id param_quoteConfigId, Map<String, List<String>> param_fieldMap, String param_extraParam, Map<String, Integer> param_enhAggHrsMap, Map<String,decimal> param_enhCRMap, Apttus_Proposal__Proposal__c param_proposalObj) {
        quoteConfigId = param_quoteConfigId;
        fieldMap = param_fieldMap;
        extraParam = param_extraParam;
        enhAggHrsMap = param_enhAggHrsMap;
        enhCRMap = param_enhCRMap;
        proposalObj = param_proposalObj;
        List<Id> configIds = new List<Id>{quoteConfigId};
        lineItems = APTPS_AccountConsolidationOnQuote.getConfigLI(configIds,fieldMap,extraParam);
        Boolean proposalApprovedStatus = true;
        Set<id> attrIdSet = new set<id>();
        system.debug('Line Items result, list size --> '+lineItems.size()+' Actual records --> '+lineItems);
        system.debug('enhAggHrsMap-->'+enhAggHrsMap);
        system.debug('enhCRMap-->'+enhCRMap );
        if(!lineItems.isEmpty()) {
            try{
                for (Apttus_Config2__LineItem__c lineItem : lineItems) { 
                    system.debug('Product Code-->'+lineItem.Apttus_Config2__OptionId__r.ProductCode);
                    //For Waiver of International Fees Product
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_WAIVE_INTERNATIONAL_FEES.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_Waive_International_Fees_Approvals(lineItem);
                    }
                    
                    //For  Simultaneous Usage Product
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_SIMULTANEOUS_USAGE.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_SimultaneousUsage_Approvals(lineItem);
                    }               
                                
                    //For  Upgrades Product 
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_UPGRADES.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_Upgrades_Approvals(lineItem);    
                    }                           
                    //For Lock Current Interchange Fees Product
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_LOCK_CURRENT_INTERCHANGE.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_Lock_Interchange_Fees_Approvals(lineItem);
                    }
                    
                    //For Guaranteed Renewal Option Product
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_GUARANTEED_RENEWAL_OPTION.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_Guaranteed_Renewal_Option_Approvals(lineItem);
                    }
                    
                    //For Non Standard Product
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_NON_STANDARD.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_Non_Standard_Approvals(lineItem);
                    }
                        
                    //For Interchange Rate Deviation Product
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_INTERCHANGE_RATE_DEVIATION.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_Interchange_Rate_Approvals(lineItem);
                    }
                    
                    //For Additional Hours Product
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_ADDITIONAL_HOURS.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        attrIdSet.add(lineItem.id);
                        update_Additional_Hours_Approvals(lineItem);
                    }
                    
                    //For Waive Ferry Charges Product
                    if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJUS_WAIVE_FERRY_CHARGES.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_Waive_Ferry_Charges_Approvals(lineItem);
                    }
                    //For Early Out Product
                    /*if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
                    APTS_ConstantUtil.NJA_EARLY_OUT.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
                        update_Early_Out_Approvals(lineItem);
                    }*/
                    
                }
            } catch (exception e){
                System.debug('Exception in Class-APTPS_Enhancement_Approvals: Method- updateEnhancementApprovals() at line' + e.getLineNumber()+' Message->' + e.getMessage());
            }
               
            if (proposalObj.APTS_Require_AE_SalesVP_Approval__c || proposalObj.APTS_Requires_Sales_Director_Approval__c || proposalObj.APTPS_Requires_Director_AR_Approval__c || proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c || proposalObj.APTPS_Requires_DDC_Approval__c ) {
                proposalApprovedStatus = false;
                proposalObj.Apttus_Proposal__Approval_Stage__c = APTS_Constants.STATUS_APPROVAL_REQUIRED ;
                proposalObj.Apttus_QPApprov__Approval_Status__c = APTS_Constants.STATUS_APPROVAL_REQUIRED ;
                proposalObj.Proposal_Chevron_Status__c = APTS_ConstantUtil.PROPOSAL_SOLUTION;
            }           
            if(proposalObj.APTS_Requires_Sales_Director_Approval__c) {
                proposalObj.APTS_Require_AE_SalesVP_Approval__c = false;
            }
            if(proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c){
                proposalObj.APTS_Require_AE_SalesVP_Approval__c = false;
                proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
            }   
            if(proposalObj.APTPS_Requires_DDC_Approval__c){
               proposalObj.APTS_Require_AE_SalesVP_Approval__c = false;
               proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
               proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = false;
               
            }
            if(proposalApprovedStatus) {
                proposalObj.Apttus_Proposal__Approval_Stage__c = APTS_Constants.STATUS_APPROVED ;
                proposalObj.Apttus_QPApprov__Approval_Status__c = APTS_Constants.STATUS_APPROVED;
            }
        }
        //GCM-13173 Extra hours number to word 
        if(!attrIdSet.isEmpty()) {  
            sp = APTPS_Additional_Hours_Text__mdt.getAll();
            List<Apttus_QPConfig__ProposalProductAttributeValue__c> updateAttrList = new List<Apttus_QPConfig__ProposalProductAttributeValue__c>();
            for(Apttus_QPConfig__ProposalProductAttributeValue__c pav : [SELECT id,APTPS_Additional_Hours_Text__c,APTPS_Number_of_Additional_Hours__c FROM Apttus_QPConfig__ProposalProductAttributeValue__c WHERE Apttus_QPConfig__LineItemId__r.Apttus_QPConfig__DerivedFromId__c IN:attrIdSet] ) {
                pav.APTPS_Additional_Hours_Text__c = convertNumberToWord(pav.APTPS_Number_of_Additional_Hours__c);
                updateAttrList.add(pav);
            }
            if(!updateAttrList.isEmpty()) {
                try{
                    update updateAttrList;
                }catch (exception e){
                    System.debug('Exception in Class-APTPS_Enhancement_Approvals: Method- updateEnhancementApprovals() at line' + e.getLineNumber()+' Message->' + e.getMessage());
                }
            }
        }
        
        system.debug('in Enhancement Approvals proposalObj-->'+proposalObj);
        return proposalObj;
        
    }
    
    public static void update_Waive_International_Fees_Approvals(Apttus_Config2__LineItem__c lineItem) {
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
        APTS_ConstantUtil.NJUS_WAIVE_INTERNATIONAL_FEES.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)
        && enhAggHrsMap.containsKey(lineItem.Apttus_Config2__OptionId__r.ProductCode) && lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Waive_International_Amount__c != null && 
        Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode)) > 0 && 
        lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Waive_International_Amount__c > 0) {
            Decimal calculatedHour = Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode));
            Decimal internationalFees = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Waive_International_Amount__c;
            Decimal caluculateFees = APTPS_SNL_Util.calculateWIF(calculatedHour,Hours_Factor,internationalFees);
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                if (caluculateFees <= Sales_Director_WIF_Limit){
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;                
                } else if (caluculateFees > Sales_Director_WIF_Limit && caluculateFees <= SalesOps_WIF_Limit){
                    proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = true;              
                } else if (caluculateFees > DDC_WIF_Limit){
                    proposalObj.APTPS_Requires_DDC_Approval__c = true;              
                }   
            } else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                if (caluculateFees <= 10000){
                proposalObj.APTS_Requires_RVP_Approval__c = true;                
                } else if (caluculateFees > 10000 && caluculateFees <= 20000){
                    proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;              
                } else {
                    proposalObj.APTPS_Requires_EDC_Approvals__c = true;              
                } 
            }
                    
        }
        
    }   
   
    public static void update_SimultaneousUsage_Approvals(Apttus_Config2__LineItem__c lineItem) {
       if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
        APTS_ConstantUtil.NJUS_SIMULTANEOUS_USAGE.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)
        && enhAggHrsMap.containsKey(lineItem.Apttus_Config2__OptionId__r.ProductCode) && lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Aircraft_per_day__c > 0 && 
        Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode)) > 0 && 
        lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_days__c > 0) {
            Decimal calculatedHour = Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode));
            Decimal aircrafts = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Aircraft_per_day__c;
            Decimal number_of_days = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_days__c;
            Decimal caluculatedDays = APTPS_SNL_Util.calculatePer16(calculatedHour,Hours_Factor,number_of_days);
            Decimal aircraft_per_day = APTPS_SNL_Util.calculatePer16(calculatedHour,Hours_Factor,aircrafts);
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                if((aircraft_per_day >0 &&  aircraft_per_day <= 2) && (caluculatedDays >0 && caluculatedDays<=4 && lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_days__c  <= 12) && APTS_ConstantUtil.BLACKOUT_DAYS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJA_SNL_Restricted_Calendar__c) ) {
                    proposalObj.APTS_Require_AE_SalesVP_Approval__c = true;                 
                } else if((aircraft_per_day >0 &&  aircraft_per_day <= 2 && caluculatedDays <=12) && (aircraft_per_day <= 4 && lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_days__c <= 24) && APTS_ConstantUtil.CARD_PEAK_PERIOD_DAYS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJA_SNL_Restricted_Calendar__c) ) {
                    proposalObj.APTS_Requires_Sales_Director_Approval__c = true;                    
                } else if((aircraft_per_day >0 &&  aircraft_per_day <= 2 && caluculatedDays <= 12) || (aircraft_per_day <= 4 && caluculatedDays <= 6)) {
                        proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = true;                  
                } else {
                    proposalObj.APTPS_Requires_DDC_Approval__c = true;
                }
            } else if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                if((aircraft_per_day >0 &&  aircraft_per_day <= 2) && (caluculatedDays >0 && caluculatedDays<=4 && lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_days__c  <= 12) && APTS_ConstantUtil.NO_RESTRICTIONS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJE_SNL_Restricted_Calendar__c) ) {
                    proposalObj.APTS_Require_AE_SalesVP_Approval__c = true;                 
                } else if((aircraft_per_day >0 &&  aircraft_per_day <= 2 && caluculatedDays <=12) && (aircraft_per_day <= 4 && lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_days__c <= 24) && APTS_ConstantUtil.NO_RESTRICTIONS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJE_SNL_Restricted_Calendar__c) ) {
                    proposalObj.APTS_Requires_RVP_Approval__c = true;                    
                } else if((aircraft_per_day >0 &&  aircraft_per_day <= 2 && caluculatedDays <= 12) || (aircraft_per_day <= 4 && caluculatedDays <= 6)) {
                    proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;                  
                } else {
                    proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                }
            }
            
                
        }
    }
    
    public static void update_Upgrades_Approvals(Apttus_Config2__LineItem__c lineItem) {
        system.debug('In Upgrades Product Code-->'+lineItem.Apttus_Config2__OptionId__r.ProductCode);
        system.debug('In Upgrades enhAggHrsMap-->'+enhAggHrsMap);
        system.debug('In Upgrades enhCRMap-->'+enhCRMap );
        system.debug('In Upgrades Usage_Type-->'+lineItem.Apttus_Config2__AttributeValueId__r.Usage_Type__c );
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
        APTS_ConstantUtil.NJUS_UPGRADES.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)
        && enhAggHrsMap.containsKey(lineItem.Apttus_Config2__OptionId__r.ProductCode) && APTS_ConstantUtil.USAGE_TYPE_SEGMENTS.equals(lineItem.Apttus_Config2__AttributeValueId__r.Usage_Type__c) && 
        Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode)) > 0 && 
        lineItem.Apttus_Config2__AttributeValueId__r.APTS_Number_of_Upgrades__c > 0) {
            Decimal calculatedHour = Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode));
            Decimal number_of_upgrades = lineItem.Apttus_Config2__AttributeValueId__r.APTS_Number_of_Upgrades__c;
            Decimal cabinRankDiff = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_To_Aircraft_Type_SNL__r.Cabin_Class_Rank__c - enhCRMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode);
            Decimal caluculatedSegments = APTPS_SNL_Util.calculatePer16(calculatedHour,Hours_Factor,number_of_upgrades);
            Decimal serviceAreaZoneRank =  getServiceAreaZoneRank(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJUS_Service_Area_SNL__c);
            String serviceArea =  lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJUS_Service_Area_SNL__c;
            Decimal upgradeAirCraftZoneRank = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_To_Aircraft_Type_SNL__r.Ferry_Waiver_Zone_Rank__c;
            String upgradeAircraft = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_To_Aircraft_Type_SNL__r.ProductCode;
            system.debug('##Upgrades#'+'cabinRankDiff-->'+cabinRankDiff+' caluculatedSegments-->'+caluculatedSegments+' serviceArea-->'+serviceArea+ ' upgradeAirCraftZoneRank-->'+upgradeAirCraftZoneRank+' serviceAreaZoneRank-->'+' Restricted Calendar-->'+lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJA_SNL_Restricted_Calendar__c+' upgradeAircraft-->'+upgradeAircraft);
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                if(cabinRankDiff == 1 && caluculatedSegments < 3 &&  (serviceArea.contains(APTS_ConstantUtil.COLLECTIVE_SERVICE_AREA) || upgradeAirCraftZoneRank == serviceAreaZoneRank) && APTS_ConstantUtil.BLACKOUT_DAYS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJA_SNL_Restricted_Calendar__c)) {
                    proposalObj.APTS_Require_AE_SalesVP_Approval__c = true;             
                } else if((cabinRankDiff == 2 ||(cabinRankDiff == 1 && (upgradeAircraft.contains(APTS_ConstantUtil.UPGRADE_GULFSTREAM) ||upgradeAircraft.contains(APTS_ConstantUtil.UPGRADE_GLOBAL) ))) && ((caluculatedSegments > 2 && caluculatedSegments < 5) || APTS_ConstantUtil.CARD_PEAK_PERIOD_DAYS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJA_SNL_Restricted_Calendar__c)) &&  (serviceArea.contains(APTS_ConstantUtil.COLLECTIVE_SERVICE_AREA) || upgradeAirCraftZoneRank == serviceAreaZoneRank)) {
                    proposalObj.APTS_Requires_Sales_Director_Approval__c = true;                
                } else if(cabinRankDiff == 2 && ((caluculatedSegments > 4 && caluculatedSegments < 11) || serviceAreaZoneRank > upgradeAirCraftZoneRank || APTS_ConstantUtil.CARD_PEAK_PERIOD_DAYS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJA_SNL_Restricted_Calendar__c) || APTS_ConstantUtil.NO_RESTRICTIONS .equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJA_SNL_Restricted_Calendar__c))) {
                    proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = true;                  
                } else if(cabinRankDiff > 2 || caluculatedSegments > 10) {
                    proposalObj.APTPS_Requires_DDC_Approval__c = true;
                }
            } else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)){
                if(APTS_ConstantUtil.UPGRADE_ONE_INTERCHANGE.equals(lineItem.Apttus_Config2__AttributeValueId__r.Interchange_Type__c)) {
                    if(cabinRankDiff == 1 && caluculatedSegments <= 2 &&  (serviceArea.contains(APTS_ConstantUtil.COLLECTIVE_SERVICE_AREA) || upgradeAirCraftZoneRank == serviceAreaZoneRank) && APTS_ConstantUtil.NO_RESTRICTIONS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJE_SNL_PPD_Restricted__c)) {
                        proposalObj.APTS_Require_AE_SalesVP_Approval__c = true;             
                    } else if(cabinRankDiff == 1 && caluculatedSegments <= 4 &&  (serviceArea.contains(APTS_ConstantUtil.COLLECTIVE_SERVICE_AREA) || upgradeAirCraftZoneRank == serviceAreaZoneRank) && APTS_ConstantUtil.NO_RESTRICTIONS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJE_SNL_PPD_Restricted__c)) {
                        proposalObj.APTS_Requires_RVP_Approval__c = true;             
                    } else if(cabinRankDiff == 1 && caluculatedSegments <= 4 &&  (serviceArea.contains(APTS_ConstantUtil.COLLECTIVE_SERVICE_AREA) || upgradeAirCraftZoneRank == serviceAreaZoneRank) && APTS_ConstantUtil.NO_RESTRICTIONS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJE_SNL_PPD_Restricted__c)) {
                        proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;             
                    } else {
                        proposalObj.APTPS_Requires_EDC_Approvals__c = true; 
                    }
                }else if(APTS_ConstantUtil.UPGRADE_APPLICABLE_INTERCHANGE.equals(lineItem.Apttus_Config2__AttributeValueId__r.Interchange_Type__c)) {
                    if(cabinRankDiff == 1 && caluculatedSegments <= 2 &&  (serviceArea.contains(APTS_ConstantUtil.COLLECTIVE_SERVICE_AREA) || upgradeAirCraftZoneRank == serviceAreaZoneRank) && APTS_ConstantUtil.NO_RESTRICTIONS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJE_SNL_PPD_Restricted__c)) {
                        proposalObj.APTS_Require_AE_SalesVP_Approval__c = true;             
                    } else if(cabinRankDiff == 1 && caluculatedSegments <= 4 &&  (serviceArea.contains(APTS_ConstantUtil.COLLECTIVE_SERVICE_AREA) || upgradeAirCraftZoneRank == serviceAreaZoneRank) && APTS_ConstantUtil.NO_RESTRICTIONS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJE_SNL_PPD_Restricted__c)) {
                        proposalObj.APTS_Requires_RVP_Approval__c = true;             
                    } else if(cabinRankDiff == 1 && caluculatedSegments <= 4 &&  (serviceArea.contains(APTS_ConstantUtil.COLLECTIVE_SERVICE_AREA) || upgradeAirCraftZoneRank == serviceAreaZoneRank) && APTS_ConstantUtil.NO_RESTRICTIONS.equals(lineItem.Apttus_Config2__AttributeValueId__r.APTPS_NJE_SNL_PPD_Restricted__c)) {
                        proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;             
                    } else {
                        proposalObj.APTPS_Requires_EDC_Approvals__c = true; 
                    }
                }
                
            }
            
                
        }
    }
    
    public static void update_Additional_Hours_Approvals(Apttus_Config2__LineItem__c lineItem) {
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
        APTS_ConstantUtil.NJUS_ADDITIONAL_HOURS.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode) && enhAggHrsMap.containsKey(lineItem.Apttus_Config2__OptionId__r.ProductCode) &&
        Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode)) > 0) {
            Decimal calculatedHours = (Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode))*10)/100;
            Decimal extraHours = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Additional_Hours__c;
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                if(extraHours <= calculatedHours && extraHours <= 20 ) {
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                } else if(extraHours <= calculatedHours && extraHours <= 40 ) {
                    proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = true;
                } else {
                    proposalObj.APTPS_Requires_DDC_Approval__c = true;
                }
            } else if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                Decimal calculatedHour = Decimal.valueOf(enhAggHrsMap.get(lineItem.Apttus_Config2__OptionId__r.ProductCode));
                Decimal caluculatedExtraHours = APTPS_SNL_Util.calculatePer16(calculatedHour,Hours_Factor,extraHours);
                if(caluculatedExtraHours <= 4 && extraHours < 20) {
                    proposalObj.APTS_Requires_RVP_Approval__c = true;
                } else if(caluculatedExtraHours > 4 && extraHours < 20) {
                    proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;
                } else {
                    proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                }               
            }
            
                                    
        }
    }
    
    public static void update_Waive_Ferry_Charges_Approvals(Apttus_Config2__LineItem__c lineItem) {
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
            APTS_ConstantUtil.NJUS_WAIVE_FERRY_CHARGES.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
            Decimal segments = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Segments__c;
            Decimal amount = lineItem.Apttus_Config2__AttributeValueId__r.APTPS_Waive_International_Amount__c;
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                if(segments <= 2 && amount <= 40000) {
                    proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                } else if(segments > 2 && segments <= 4 && amount <= 80000) {
                    proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = true;
                } else {
                    proposalObj.APTPS_Requires_DDC_Approval__c = true;
                } 
            } else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                if(segments <= 2) {
                    proposalObj.APTS_Requires_RVP_Approval__c = true;
                } else if(segments > 2 && segments <= 4) {
                    proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true;
                } else {
                    proposalObj.APTPS_Requires_EDC_Approvals__c = true;
                } 
            }
                                
        }
    }
        
    
    public static void update_Lock_Interchange_Fees_Approvals(Apttus_Config2__LineItem__c lineItem) {
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
            APTS_ConstantUtil.NJUS_LOCK_CURRENT_INTERCHANGE.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true; 
            } else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                proposalObj.APTPS_Requires_Dir_of_Sales_Ops_Approval__c = true; 
                proposalObj.APTPS_Requires_EDC_Approvals__c = true; 
            }
                                   
        }
    }
    
    public static void update_Guaranteed_Renewal_Option_Approvals(Apttus_Config2__LineItem__c lineItem) {
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
            APTS_ConstantUtil.NJUS_GUARANTEED_RENEWAL_OPTION.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
            } /*else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                
            }*/
                                    
        }
    } 
    
    public static void update_Non_Standard_Approvals(Apttus_Config2__LineItem__c lineItem) {
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
            APTS_ConstantUtil.NJUS_NON_STANDARD.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
            } else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                proposalObj.APTPS_Requires_EDC_Approvals__c = true;
            }
                                  
        }
    }
    
    public static void update_Interchange_Rate_Approvals(Apttus_Config2__LineItem__c lineItem) {
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
            APTS_ConstantUtil.NJUS_INTERCHANGE_RATE_DEVIATION.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
            if(APTS_ConstantUtil.CUR_USD.equals(proposalObj.CurrencyIsoCode)) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true; 
            }else if(APTS_ConstantUtil.CUR_EUR.equals(proposalObj.CurrencyIsoCode)) {
                proposalObj.APTPS_Requires_EDC_Approvals__c = true; 
            }
                                 
        }
    }
    
    public static Decimal getServiceAreaZoneRank(String serviceArea) {
        String[] serviceAreaVals = serviceArea.split(';');
        Decimal maxWaiverZone = 0;
        if (serviceAreaVals != null && serviceAreaVals.size() > 0) {
            for (string serviceAreaval : serviceAreaVals) {
                system.debug('Service Area Value: '+ serviceAreaval);
                if (APTS_Constants.FERRY_WAIVER_ZONE_1.equals(serviceAreaval)) {
                    if (maxWaiverZone < 1)
                        maxWaiverZone = 1;
                }
                if (APTS_Constants.FERRY_WAIVER_ZONE_2.equals(serviceAreaval)) {
                    if (maxWaiverZone < 2)
                        maxWaiverZone = 2;
                }
                if (APTS_Constants.FERRY_WAIVER_ZONE_3.equals(serviceAreaval)) {
                    if (maxWaiverZone < 3)
                        maxWaiverZone = 3;
                }
                if (APTS_Constants.FERRY_WAIVER_ZONE_4.equals(serviceAreaval)) {
                    if (maxWaiverZone < 4)
                        maxWaiverZone = 4;
                }
                if (APTS_Constants.FERRY_WAIVER_ZONE_5.equals(serviceAreaval)) {
                    if (maxWaiverZone < 5)
                        maxWaiverZone = 5;
                }
                if (APTS_Constants.NO_RESTRICTION.equals(serviceAreaval)) {
                    if (maxWaiverZone < 6)
                        maxWaiverZone = 6;
                }
            }
        }
        return maxWaiverZone;
    }
    //Function to convert hours in text
    public static String convertNumberToWord(Decimal hours){
        String result = '';
        String extraHours = String.valueOf(hours.setScale(2));
        if (hours != null){
            integer integerPlacesTot = extraHours.indexOf('.');
            if (Double.valueOf(extraHours) > 0 && extraHours.contains('.')){
                String extraHoursFull = extraHours.substring(0, integerPlacesTot);
                String extraHoursDecimal = extraHours.substring(integerPlacesTot + 1, integerPlacesTot + 2);        

                if (CS_NetJets_Translation__c.getValues(extraHoursFull) != null){
                    result = (CS_NetJets_Translation__c.getValues(extraHoursFull)).Value__c;
                    System.debug('(CS_NetJets_Translation__c.getValues(extraHoursFull)).Value__c: ' + (CS_NetJets_Translation__c.getValues(extraHoursFull)).Value__c);
                }

                if (String.isNotBlank(extraHoursDecimal) && extraHoursDecimal != '0' ){
                    
                    String spText = '';
                    for(String spRec: sp.keySet()){
                        if(sp.get(spRec).MasterLabel==extraHoursDecimal){
                        system.debug('num -->'+sp.get(spRec).Number_to_Text__c );
                            spText = sp.get(spRec).Number_to_Text__c;  
                            break;
                        }
                    }
                    if(spText!=''){
                        result += ' and '+spText;
                    }
                    
                }
            }else if (CS_NetJets_Translation__c.getValues(String.valueOf(hours)) != null){
                result = (CS_NetJets_Translation__c.getValues(String.valueOf(hours))).Value__c;
            }
        }
        return result;
    }
    //Used for Lease termination
    /*public static void update_Early_Out_Approvals(Apttus_Config2__LineItem__c lineItem) {
        if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
            !APTS_ConstantUtil.NJA_EARLY_OUT.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
            proposalObj.APTPS_Requires_DDC_Approval__c = true;                      
        } else if (lineItem.Apttus_Config2__OptionId__r.ProductCode != null && 
            !APTS_ConstantUtil.NJE_EARLY_OUT.equals(lineItem.Apttus_Config2__OptionId__r.ProductCode)) {
            //proposalObj.APTPS_Requires_DDC_Approval__c = true;                      
        }
    }*/
}