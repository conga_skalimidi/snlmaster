/************************************************************************************************************************
@Name: APTPS_CLMLifeCycle_Share
@Author: Conga PS Dev Team
@CreateDate: 8 July 2021
@Description: Implement CLM Lifecycle for Share(NJA and NJE)
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/


public class APTPS_CLMLifeCycle_Share {
    
    /**
    @description: Update Status 
    @param: args 
    @return: void
    */
    
    public static void updateCLMStatus(Map<String, Object> args,String currencyCode) {
        List<Apttus__APTS_Agreement__c> newAg = (List<Apttus__APTS_Agreement__c>)args.get('newAgrList');
        Map<Id,SObject> oldAgMap = (Map<Id,SObject>)args.get('oldAgrList');
        /*For Agreement Dates START*/
        set<id> agrIds = new set<id>();
        Map<Id,List<Apttus__AgreementLineItem__c>> agrLIMap = new Map<Id,List<Apttus__AgreementLineItem__c>>();
        for(Apttus__APTS_Agreement__c agr : newAg) {
            agrIds.add(agr.id);
        }
        String PURCHASE_PRICE  = APTS_ConstantUtil.PURCHASE_PRICE;
        for(Apttus__AgreementLineItem__c agLI : [SELECT Id,Apttus__AgreementId__c,Apttus_CMConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c,Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c,Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c,Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length__c 
        FROM Apttus__AgreementLineItem__c 
        WHERE Apttus_CMConfig__ChargeType__c =:PURCHASE_PRICE AND Apttus_CMConfig__IsPrimaryLine__c=true AND Apttus_CMConfig__OptionId__c=null AND  Apttus__AgreementId__c IN :agrIds]) {
            if(agrLIMap.containsKey(agLI.Apttus__AgreementId__c)) {
                List<Apttus__AgreementLineItem__c> listAgli = agrLIMap.get(agLI.Apttus__AgreementId__c);
                listAgli.add(agLI);
                agrLIMap.put(agLI.Apttus__AgreementId__c,listAgli);
            } else {
                agrLIMap.put(agLI.Apttus__AgreementId__c,new List<Apttus__AgreementLineItem__c>{agLI});
            }
        }
        /*For Agreement Dates End*/
        Set<Id> interimPositionAgSet = new Set<Id>();
        Set<Id> pendingPositionAgSet = new Set<Id>();
        for(Apttus__APTS_Agreement__c ag : newAg) {
            Apttus__APTS_Agreement__c oldAgObj = (Apttus__APTS_Agreement__c) oldAgMap.get(ag.Id);
            if (oldAgObj != null) {
                String oldStatus = oldAgObj.Apttus__Status__c;
                String oldStatusCat = oldAgObj.Apttus__Status_Category__c;
                String newStatus = ag.Apttus__Status__c;
                String newStatusCat = ag.Apttus__Status_Category__c;
                String oldAgStatus = oldAgObj.Agreement_Status__c;
                system.debug('SNL Agreement Status Lifecycle. oldStatus --> '+oldStatus+' oldStatusCat --> '+oldStatusCat+
                             ' newStatus --> '+newStatus+' newStatusCat --> '+newStatusCat);
                
                //Generate the document
                if(APTS_ConstantUtil.REQUEST.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.REQUEST.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_GENERATED, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_GENERATED);
                        //Stamp contract generated date
                        ag.APTPS_Contract_Generated_Date__c = Date.today(); 
                   }
                
                //Send for Review
                if((!oldAgObj.APTPS_Sent_For_Review_Outside_Conga__c && ag.APTPS_Sent_For_Review_Outside_Conga__c) || 
                   (APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.APTTUS_OTHER_PARTY_REVIEW.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(newStatusCat))) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.IN_REVIEW, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_GENERATED);
                       //Check with the team
                       ag.Apttus__Status__c = APTS_ConstantUtil.APTTUS_OTHER_PARTY_REVIEW;
                       ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_AUTH;
                   }
                
                //Submit For Approvals
                if(((APTS_ConstantUtil.APTTUS_OTHER_PARTY_REVIEW.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat)) 
                    || (APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat)) 
                    || (APTS_ConstantUtil.APPR_REJ.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat))) 
                    && APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(newStatusCat)) {
                    ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
                    APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_IN_APPROVALS, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_GENERATED);
                }
                
                //Approved
                if(APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.APPR_REQ.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(newStatusCat)) {
                       ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.APPROVED;
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_APPROVED, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_GENERATED);
                   }
                
                //Rejected
                if(APTS_ConstantUtil.PENDING_APPROVAL.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.APPR_REJ.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(newStatusCat)) {
                       ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.REJECTED;
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_IN_APPROVALS, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_GENERATED);
                   }
                
                //Send for signatures
                if((APTS_ConstantUtil.APPR_REQ.equalsIgnoreCase(oldStatus) || APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(oldStatus)) && (APTS_ConstantUtil.IN_AUTH.equalsIgnoreCase(oldStatusCat) || APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat))
                   && APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                       ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.APPROVED;
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_PENDING, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_PENDING);
                       //Stamp sent for signatures date
                        ag.APTPS_Document_Sent_Date__c = Date.today();
                   }

                //Agreement Status should be set to "Pending Funding" when Owner Signed to True and Funding Status is "Pending Funding"
                if(!oldAgObj.APTPS_Owner_Signed__c && ag.APTPS_Owner_Signed__c && APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING.equalsIgnoreCase(ag.Funding_Status__c)) {
                    APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_RECEIVED);
                }
                
                //Till this point Interim Lease parent agreement flow is same as other Share agreement.
                //START: Interim Lease Parent Agreement Payment logic
                //Partial payment and full payment
                if(ag.APTPS_Contract_Purchase_Deposit__c != null && oldAgObj.Number_of_Payments_Made__c != ag.Number_of_Payments_Made__c){
                    if(ag.Total_Payment__c >= ag.APTPS_Contract_Purchase_Deposit__c && ag.Total_Balance_Due__c != 0 && !APTS_ConstantUtil.REPURCHASE.equalsIgnoreCase(ag.APTS_Modification_Type__c) && !APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(ag.APTS_Modification_Type__c)) {
                        APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.INTERIM_POSITIONING, APTS_ConstantUtil.FUNDED_DEPOSIT, APTS_ConstantUtil.DOC_PENDING);
                        if(APTS_ConstantUtil.NETJETS_USD.equalsIgnoreCase(ag.Program__c))
                            interimPositionAgSet.add(ag.Id);
                    } else if(ag.Total_Payment__c > ag.APTPS_Contract_Purchase_Deposit__c 
                              && ag.Total_Balance_Due__c == 0) { //Either entire payment at once or split payment
                                  if(ag.APTPS_Owner_Signed__c)
                                      APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.PENDING_POSITIONING, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED);
                                  else 
                                      APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.PENDING_POSITIONING, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_PENDING);
                                  if(APTS_ConstantUtil.NETJETS_USD.equalsIgnoreCase(ag.Program__c))
                                      pendingPositionAgSet.add(ag.Id);
                              }
                }
                //END: Interim Lease Parent Agreement Payment logic
                
                //Pending Positioning
                //Agreement Status should be set to "Pending Positioning" only when Owner Signed is True and Funding Status is "Funded Contract".
                if(!oldAgObj.APTPS_Owner_Signed__c && ag.APTPS_Owner_Signed__c && APTS_ConstantUtil.FUNDED_CONTRACT.equalsIgnoreCase(ag.Funding_Status__c)) {
                    APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.PENDING_POSITIONING, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED);
                }
                
                //Payment entry
                //Agreement Status should remain as "Contract Pending" when Owner Signed is False and Funding Status is "Funded Contract".
                if((ag.APTPS_Contract_Purchase_Deposit__c == null || ag.APTPS_Contract_Purchase_Deposit__c == 0) 
                   && oldAgObj.Number_of_Payments_Made__c == 0 && ag.Number_of_Payments_Made__c > 0 && !APTS_ConstantUtil.REPURCHASE.equalsIgnoreCase(ag.APTS_Modification_Type__c) && !APTS_ConstantUtil.REDUCTION.equalsIgnoreCase(ag.APTS_Modification_Type__c)) {
                    if(!ag.APTPS_Owner_Signed__c) {
                        //APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_PENDING, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED);
                        APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_PENDING, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED);
                        ag.Chevron_Status__c = APTS_ConstantUtil.FUNDED_SIGN_PENDING;
                    }
                    else
                        APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.PENDING_POSITIONING, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED);
                }
                
                //Positioning Requested
                if(!oldAgObj.APTPS_Request_Aircraft_Positioning__c && ag.APTPS_Request_Aircraft_Positioning__c) {
                    APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_POSITIONING_REQUESTED, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_PENDING);
                    /*Send Email Notification START*/
                     APTPS_EmailNotification enObj = new APTPS_EmailNotification();
                      try{
                            String Business_Object = APTS_ConstantUtil.AGREEMENT_OBJ;
                            String Product_Code = '';
                            if(ag.Apttus__Parent_Agreement__c !=null) {
                                Product_Code = APTS_ConstantUtil.INTERIM_LEASE_CODE;
                            } else {
                                if(APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(currencyCode)) {
                                    Product_Code = APTS_ConstantUtil.NJA_SHARE_CODE;
                                } else if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(currencyCode)) {
                                    Product_Code = APTS_ConstantUtil.NJE_SHARE_CODE;
                                }
                            }
                            
                            String Status = ag.Agreement_Status__c;
                            String objId = ag.id;
                            enObj.call(Product_Code, new Map<String, Object>{'Id' => objId,'Business_Object'=>Business_Object,'Product_Code'=>Product_Code,'Status'=>Status});
                           
                        }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                            system.debug('Error while sending Email Notification for Agreement Id --> '+ag.id+' Error details --> '+e.errMsg);
                        }
                        /*Send Email Notification END*/
                }
                
                //Accrual : For Interim Lease Share agreement, Accrual scenarios is not applicable
                if(oldAgObj.Accrual_Date__c != ag.Accrual_Date__c) {
                    ag.Apttus_Approval__Approval_Status__c = APTS_ConstantUtil.APPROVED;
                    APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.ACCRUAL, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED);
                }
                
                //Aircraft Positioned
                if(oldAgObj.APTPS_Aircraft_Positioned_State__c != ag.APTPS_Aircraft_Positioned_State__c 
                   && oldAgObj.APTPS_Aircraft_Positioned_Date__c != ag.APTPS_Aircraft_Positioned_Date__c 
                   && (APTS_ConstantUtil.AGREEMENT_POSITIONING_REQUESTED.equalsIgnoreCase(oldAgStatus) || APTS_ConstantUtil.ACCRUAL.equalsIgnoreCase(oldAgStatus))) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AC_POSITIONED, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED);
                       //Accrual+AC Positioned
                       if(ag.Accrual_Date__c != null)
                           ag.Chevron_Status__c = APTS_ConstantUtil.ACCRUAL_AC_POSITIONED;
                   }
                
                //Fully Signed
                if(APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_PENDING_ACTIVATION, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_SIGNED);
                       //Accrual+AC Positioned+Fully Signed
                       if(ag.Accrual_Date__c != null && ag.APTPS_Aircraft_Positioned_State__c != null && ag.APTPS_Aircraft_Positioned_Date__c != null)
                           ag.Chevron_Status__c = APTS_ConstantUtil.ACCRUAL_PENDING_ACTIVATION;
                   }
                
                //Activation
                if(APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(newStatusCat)) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_ACTIVE, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_SIGNED);
                       //Accrual+Active
                       if(ag.Accrual_Date__c != null)
                           ag.Chevron_Status__c = APTS_ConstantUtil.ACCRUAL_ACTIVE;
                        if(agrLIMap.containsKey(ag.id)) {
                            APTPS_SNL_Util.populateDates(ag,agrLIMap.get(ag.id));
                        }
                        
                   }
                
                //START: Interim Lease child agreement activation
                if(ag.Apttus__Parent_Agreement__c != null && APTS_ConstantUtil.AC_POSITIONED.equalsIgnoreCase(oldAgStatus) 
                   && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(newStatusCat)) {
                    APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_ACTIVE, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED);
                }
                //END: Interim Lease child agreement activation
                
                //When First Flight Date is Changed after Activation
                if(ag.First_Flight_Date__c != null && oldAgObj.First_Flight_Date__c != ag.First_Flight_Date__c
                   && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(oldStatusCat)) {
                       system.debug('agrLIMap-->'+agrLIMap);
                       if(agrLIMap.containsKey(ag.id)) {
                            APTPS_SNL_Util.populateDates(ag,agrLIMap.get(ag.id));
                            system.debug('agrLIMap.get(ag.id)-->'+agrLIMap.get(ag.id));
                        }
                }
                //Cancelled
                if(!APTS_ConstantUtil.CANCEL_REQ.equalsIgnoreCase(oldStatus) && !APTS_ConstantUtil.CANCEL.equalsIgnoreCase(oldStatusCat) 
                   && APTS_ConstantUtil.CANCEL_REQ.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.CANCEL.equalsIgnoreCase(newStatusCat)) {
                       APTPS_SNL_Util.setAgreementStatus(ag, APTS_ConstantUtil.CANCEL, ag.Funding_Status__c, ag.Document_Status__c);
                   }
                //Start of GCM-12869
                if(APTS_ConstantUtil.DEFERMENT_MORATORIUM.equalsIgnoreCase(ag.APTS_Modification_Type__c) && ag.Apttus__Contract_End_Date__c != null 
                   && ag.APTPS_Minimum_Commitment_Date__c != null && ag.APTPS_End_of_Deferment__c != null){
                       Integer earlyTermination = oldAgObj.APTPS_End_of_Deferment__c.daysBetween(ag.APTPS_End_of_Deferment__c);
                       ag.Apttus__Contract_End_Date__c = ag.Apttus__Contract_End_Date__c.addDays(earlyTermination);
                       ag.APTPS_Minimum_Commitment_Date__c = ag.APTPS_Minimum_Commitment_Date__c.addDays(earlyTermination);
                   }
                //End of GCM-12869
            } else 
                system.debug('Old agreement object is null');
            
        }
        
        //START: In case of Interim Lease, update Agreement Status & Funding Status on child agreement as well
        system.debug('interimPositionAgSet --> '+interimPositionAgSet);
        system.debug('pendingPositionAgSet --> '+pendingPositionAgSet);
        List<Apttus__APTS_Agreement__c> agListToUpdate = new List<Apttus__APTS_Agreement__c>();
        if(!interimPositionAgSet.isEmpty()) 
            agListToUpdate = updateChildAgreementStatus(interimPositionAgSet, APTS_ConstantUtil.INTERIM_POSITIONING, APTS_ConstantUtil.FUNDED_DEPOSIT, APTS_ConstantUtil.DOC_RECEIVED, currencyCode);
        
        if(!pendingPositionAgSet.isEmpty())
            agListToUpdate = updateChildAgreementStatus(pendingPositionAgSet, APTS_ConstantUtil.PENDING_POSITIONING, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_RECEIVED, currencyCode);
        
        system.debug('agListToUpdate --> '+agListToUpdate);
        if(!agListToUpdate.isEmpty())
            update agListToUpdate;
        //END
    }
    
    /** 
    @description: NJE Share implementation
    @param:
    @return: 
    */
    public class NJE_CLMFlow implements APTPS_CLMLifeCycleInterface {
        public void callCLMAction(Map<String, Object> args) {
            APTPS_CLMLifeCycle_Share.updateCLMStatus(args,APTS_ConstantUtil.CUR_EUR);         
        }
    }
    
    /** 
    @description: NJA Share implementation
    @param:
    @return: 
    */
    public class NJA_CLMFlow implements APTPS_CLMLifeCycleInterface {
        public void callCLMAction(Map<String, Object> args) {
            APTPS_CLMLifeCycle_Share.updateCLMStatus(args,APTS_ConstantUtil.CUR_USD);
        }
    }
    
    @TestVisible
    private static List<Apttus__APTS_Agreement__c> updateChildAgreementStatus(Set<Id> agSet, String agStatus, String fundStatus, String docStatus, String currencyCode) {
        List<Apttus__APTS_Agreement__c> agListToUpdate = new List<Apttus__APTS_Agreement__c>();
        Id childNJA = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.NJA_INTERIM_CHILD).getRecordTypeId();
        Id childNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.NJE_INTERIM_CHILD).getRecordTypeId();
        for(Apttus__APTS_Agreement__c ag : [SELECT Id, APTS_Modification_Type__c ,APTS_Additional_Modification_Type__c, Agreement_Status__c, 
                                            Chevron_Status__c, APTS_Path_Chevron_Status__c, Funding_Status__c, 
                                            Document_Status__c, RecordTypeId, Apttus__Status_Category__c, Apttus__Status__c, 
                                            APTPS_Owner_Signed__c   
                                            FROM Apttus__APTS_Agreement__c 
                                            WHERE Apttus__Parent_Agreement__c IN :agSet]) {
                                                Id onHoldRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.INTERIM_AGREEMENT_HOLD).getRecordTypeId();
                                                system.debug('onHoldRecType --> '+onHoldRecType+' Agreement Rec Type --> '+ag.RecordTypeId);
                                                //Update Child agreement record type and fields only once i.e. if it's On Hold Agreement
                                                if(ag.RecordTypeId == onHoldRecType) {
                                                    ag = APTPS_SNL_Util.setAgreementStatus(ag, agStatus, fundStatus, docStatus);
                                                    //START: Change Record type and set other fields on Child Agreements
                                                    ag.RecordTypeId = APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(currencyCode) ? childNJA : childNJE;
                                                    ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                                                    ag.Apttus__Status__c = APTS_ConstantUtil.FULLY_SIGNED;
                                                    ag.APTPS_Owner_Signed__c = true;
                                                }
                                                //END: Change Record type and set other fields on Child Agreements
                                                agListToUpdate.add(ag);
                                            }
        return agListToUpdate;
    }
}