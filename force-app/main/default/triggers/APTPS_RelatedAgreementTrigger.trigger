trigger APTPS_RelatedAgreementTrigger on Apttus__APTS_Related_Agreement__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerHandlerDispatcher.execute(Apttus__APTS_Related_Agreement__c.getSObjectType());
}