/************************************************************************************************************************
@Name: APTPS_CollaborationRequestTrigger
@Author: Conga PS Dev Team
@CreateDate: 20 July 2021
@Description: Collaboration Request trigger implementation.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
trigger APTPS_CollaborationRequestTrigger on Apttus_Config2__CollaborationRequest__c (before insert, after update, before delete) {
	system.debug('>>>>>>>>>>Before dispatching to Collaboration Request trigger handler ');
	TriggerHandlerDispatcher.execute(Apttus_Config2__CollaborationRequest__c.getSObjectType());
}